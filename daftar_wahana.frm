VERSION 5.00
Begin VB.Form daftar_wahana 
   Caption         =   "Form1"
   ClientHeight    =   7395
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9150
   LinkTopic       =   "Form1"
   ScaleHeight     =   7395
   ScaleWidth      =   9150
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BackColor       =   &H8000000A&
      Height          =   7215
      Left            =   120
      ScaleHeight     =   7155
      ScaleWidth      =   8835
      TabIndex        =   0
      Top             =   120
      Width           =   8895
      Begin VB.VScrollBar VScroll1 
         Height          =   7150
         LargeChange     =   10
         Left            =   8480
         Max             =   0
         TabIndex        =   1
         Top             =   0
         Width           =   375
      End
      Begin VB.PictureBox picScroll 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   7215
         Left            =   0
         ScaleHeight     =   7215
         ScaleWidth      =   8895
         TabIndex        =   2
         Top             =   0
         Width           =   8895
         Begin MySIS.ItemBox ItemBox1 
            Height          =   4095
            Index           =   0
            Left            =   120
            TabIndex        =   3
            Top             =   120
            Width           =   3230
            _ExtentX        =   5689
            _ExtentY        =   7223
            Image           =   "daftar_wahana.frx":0000
         End
      End
   End
End
Attribute VB_Name = "daftar_wahana"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim atas As Double
Dim kiri As Double
Dim margin As Double

Private Sub Form_Load()
    koneksi
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    Dim row As Integer

    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select *, picture from mstock m left join zfoto_barang f on m.bara=f.bara where picture is not null order by nama asc limit 100", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            ItemBox1(i).Visible = True
            ItemBox1(i).Keterangan = rs1.Fields!nama
            ItemBox1(i).Harga = "Rp." & Format(i, "#,#0.00")
            Call FillPhoto(rs1, "picture", ItemBox1(i).Image)
            
            i = i + 1
            If i > 0 Then
                Load ItemBox1(i)
    
                atas = ItemBox1(i - 1).Top + ItemBox1(0).Height + 120 + 15
                kiri = ItemBox1(i - 1).Left + ItemBox1(0).Width + 120 + 15
                margin = picScroll.Width - ItemBox1(0).Width
                row = row + IIf(kiri >= margin, 1, 0)
    
                ItemBox1(i).Top = IIf(kiri >= margin, atas, ItemBox1(i - 1).Top)
                ItemBox1(i).Left = IIf(kiri >= margin, ItemBox1(0).Left, kiri)
            End If
            rs1.MoveNext
        Loop
    End If
    
    'adjust the height of the scroll picture box
    picScroll.Height = (ItemBox1.UBound * ItemBox1(0).Height) + (ItemBox1.UBound * 120 + 15)
    
    'maximum scroll value
    VScroll1.Max = row
End Sub

Private Sub Form_Resize()
    Picture1.Width = Me.Width - 450
    Picture1.Height = Me.Height - 800
    VScroll1.Left = Picture1.Width - VScroll1.Width - 50
    VScroll1.Height = Picture1.Height - 50
    picScroll.Width = VScroll1.Left
    
    Dim i As Integer
    Dim row As Integer
    For i = 1 To ItemBox1.UBound
        atas = ItemBox1(i - 1).Top + ItemBox1(0).Height + 120 + 15
        kiri = ItemBox1(i - 1).Left + ItemBox1(0).Width + 120 + 15
        margin = picScroll.Width - ItemBox1(0).Width
        row = row + IIf(kiri >= margin, 1, 0)
        
        ItemBox1(i).Top = IIf(kiri >= margin, atas, ItemBox1(i - 1).Top)
        ItemBox1(i).Left = IIf(kiri >= margin, ItemBox1(0).Left, kiri)
        
        ItemBox1(i).Visible = True
        ItemBox1(i).Harga = "Rp." & Format(i, "#,#0.00")
    Next
    VScroll1.Max = row
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub ItemBox1_DetailClick(Index As Integer)
    Debug.Print Index, " Detail"
End Sub

Private Sub ItemBox1_TambahClick(Index As Integer)
    Debug.Print Index, " Tambah"
End Sub

Private Sub VScroll1_Change()
    picScroll.Top = -(ItemBox1(0).Height * VScroll1.Value)
End Sub

Private Sub VScroll1_Scroll()
    Call VScroll1_Change
End Sub
