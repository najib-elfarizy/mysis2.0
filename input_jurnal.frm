VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form input_jurnal 
   Caption         =   "Input Jurnal"
   ClientHeight    =   6495
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11955
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6495
   ScaleWidth      =   11955
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   660
      Left            =   50
      TabIndex        =   10
      Top             =   960
      Width           =   9720
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   7750
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   330
         Width           =   1950
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   2100
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   330
         Width           =   3705
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   550
         MaxLength       =   20
         TabIndex        =   3
         Top             =   330
         Width           =   1550
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   5800
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   330
         Width           =   1950
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   7750
         TabIndex        =   25
         Top             =   50
         Width           =   1950
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Kode Akun"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   555
         TabIndex        =   14
         Top             =   45
         Width           =   1550
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Debet"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   5805
         TabIndex        =   13
         Top             =   45
         Width           =   1950
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2100
         TabIndex        =   12
         Top             =   45
         Width           =   3705
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   550
         Index           =   0
         Left            =   50
         TabIndex        =   11
         Top             =   45
         Width           =   500
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3525
      Left            =   60
      TabIndex        =   15
      Top             =   1560
      Width           =   9730
      _ExtentX        =   17171
      _ExtentY        =   6218
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   1
         Text            =   "No"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Kode"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Nama"
         Object.Width           =   6527
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "Debte"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Kredit"
         Object.Width           =   3440
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   5175
      Left            =   0
      ScaleHeight     =   5145
      ScaleWidth      =   9795
      TabIndex        =   16
      Top             =   0
      Width           =   9820
      Begin VB.TextBox Text1 
         Height          =   675
         Index           =   1
         Left            =   4680
         MaxLength       =   15
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   120
         Width           =   3135
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   1440
         TabIndex        =   17
         Top             =   480
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   105185283
         CurrentDate     =   43159
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Keterangan :"
         Height          =   255
         Index           =   5
         Left            =   3240
         TabIndex        =   22
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   135
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal :"
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   18
         Top             =   495
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   945
         Left            =   0
         TabIndex        =   20
         Top             =   0
         Width           =   9825
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1305
      ScaleWidth      =   9795
      TabIndex        =   1
      Top             =   5160
      Width           =   9820
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   3
         Left            =   7800
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   26
         Top             =   120
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   2
         Left            =   5880
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   23
         Top             =   120
         Width           =   1935
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_jurnal.frx":0000
         PICN            =   "input_jurnal.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_jurnal.frx":0176
         PICN            =   "input_jurnal.frx":0192
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button3 
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   120
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Hapus Item"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_jurnal.frx":05C8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Batalkan"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_jurnal.frx":05E4
         PICN            =   "input_jurnal.frx":0600
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Total :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   4440
         TabIndex        =   24
         Top             =   200
         Width           =   1335
      End
   End
End
Attribute VB_Name = "input_jurnal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public edit As Boolean

Private Sub cmd_batal_Click()
    On Error GoTo err
    pesan = MsgBox("Hapus jurnal jurnal keluar " & Text1(0).Text & "?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from jour_head where vc='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
                
        kosong
        otomatis
        
        If daftar_jurnal.Visible = True Then
            daftar_jurnal.tampil_jurnal
        End If
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo err
    If KeyCode = vbKeyDelete Then
        KeyAscii = 0
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
        End If
        Text3(0).SetFocus
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub Image2_Click()
    frame_jurnal.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
'        SendKeys "{tab}"
    End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
    If Index = 1 Then
        Text1(1).Text = Format(Text1(1).Text, "#,##0")
    End If
End Sub

Private Sub Text3_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 3
                Text3(z).Text = ""
                Text3(z).Locked = True
            Next z
    End Select
End Sub

Private Sub Text3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
        cari_akun.Left = 700
        cari_akun.Top = 4600
        cari_akun.Text1.Text = Text1(1).Text
        Set cari_akun.FormPemanggil = Me
        cari_akun.Show 1
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text3(0).Text <> "" Then
                    If get_akun(Text3(0).Text) = False Then
                        cari_akun.Left = 700
                        cari_akun.Top = 4600
                        cari_akun.Text1.Text = Text1(1).Text
                        Set cari_akun.FormPemanggil = Me
                        cari_akun.Show 1
                    End If
                End If
            End If
        Case 3
            If KeyAscii = 13 Then
                If Text3(0).Text <> "" And (Val(Format(Text3(2).Text, "##0.00")) > 0 Or Val(Format(Text3(3).Text, "##0.00")) > 0) Then
                    tambah_item
                End If
            End If
        Case Else
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
            Else
                If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
                KeyAscii = 0
                End If
            End If
    End Select
End Sub

Private Sub Text3_LostFocus(Index As Integer)
    If Index = 2 Then
        Text3(2).Text = Format(Text3(2).Text, "#,##0")
    End If
End Sub

Private Sub Text3_GotFocus(Index As Integer)
    Select Case Index
        Case 1 To 3
            With Text3(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Button3_Click(Index As Integer)
    On Error GoTo err
    If Index = 0 Then
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
            Text3(0).SetFocus
        End If
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    If Text1(0).Text = "" Or Text1(1).Text = "" Then
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
        Exit Sub
    End If
    
    If ListView1.ListItems.Count = 0 Then
        MsgBox "Detail jurnal masih kosong", vbOKOnly
        Text3(0).SetFocus
        Exit Sub
    End If
    
    If Val(Format(Text1(2).Text, "#,##0.00")) <> Val(Format(Text1(3).Text, "#,##0.00")) Then
        MsgBox ("Tidak bisa menyimpan, jurnal belum balance!"), vbInformation, "Peringatan"
        Exit Sub
    End If
    
    If edit Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from jour_head where vc='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    Else
        otomatis
    End If
    
'    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "insert into jour_head (TGL,VC,JENIS,KET,NILAI,BO,USR) values ('" & Format(DTPicker1.Value, "yyyy-MM-dd") & "','" & Text1(0).Text & "','JURNAL HARIAN','" & Text1(1).Text & "','" & Format(Text1(3).Text, "##0.00") & "','" & xx & "','" & xy & "')", con, adOpenKeyset, adLockOptimistic
    
    For z = 1 To ListView1.ListItems.Count
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "insert into jour_det (TGL,VC,KODE,NAMA,KET,DEBET,KREDIT,BO,USR,URUT) values ('" & Format(DTPicker1.Value, "yyyy-MM-dd") & "','" & Text1(0).Text & "','" & ListView1.ListItems(z).SubItems(2) & "','" & ListView1.ListItems(z).SubItems(3) & "','" & Text1(1).Text & "','" & Format(ListView1.ListItems(z).SubItems(4), "##0.00") & "','" & Format(ListView1.ListItems(z).SubItems(5), "##0.00") & "','" & xx & "','" & xy & "','" & z & "')", con, adOpenKeyset, adLockOptimistic
    Next z
        
    MsgBox ("Input jurnal sudah disimpan")
    
    kosong
    otomatis
    
    If daftar_jurnal.Visible Then
        daftar_jurnal.tampil_jurnal
    End If
    Exit Sub
exc:
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "delete from jour_head where vc='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    koneksi
    kosong
'    otomatis
End Sub

Private Sub Form_Activate()
    Text1(1).SetFocus
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture2.Top = Me.Height - 2000
'    Picture2.Width = Me.Width - 255
    Picture1.Height = Me.Height - 2000
'    Picture1.Width = Me.Width - 255
    ListView1.Height = Picture1.Height - 1600
'    ListView1.Width = Me.Width - 495
'    Label1.Width = Me.Width - 50
End Sub

Private Sub tambah_item()
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(2) = Text3(0).Text
    item.SubItems(3) = Text3(1).Text
    item.SubItems(4) = Format(Text3(2).Text, "#,##0")
    item.SubItems(5) = Format(Text3(3).Text, "#,##0")
    Text3(0).Text = ""
    Text3(0).SetFocus
    hitung_total
End Sub

Sub hitung_total()
    Dim debet, kredit As Double
    For j = 1 To ListView1.ListItems.Count
        debet = debet + Val(Format(ListView1.ListItems(j).SubItems(4), "##0.00"))
        kredit = kredit + Val(Format(ListView1.ListItems(j).SubItems(5), "##0.00"))
    Next j
    Text1(2).Text = Format(Val(debet), "#,##0")
    Text1(3).Text = Format(Val(kredit), "#,##0")
End Sub

Sub tampil_jurnal(vc As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
        
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from jour_head where jenis='JURNAL HARIAN' and vc='" & vc & "'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Text1(0).Text = "" & rs1.Fields!vc
        Text1(1).Text = "" & rs1.Fields!ket
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from jour_det where vc='" & vc & "' ", con, adOpenKeyset, adLockOptimistic
        
        j = 0
        If Not rs2.EOF Then
            Do While Not rs2.EOF
                j = j + 1
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!kode
                item.SubItems(3) = "" & rs2.Fields!nama
                item.SubItems(4) = "" & Format(rs2.Fields!debet, "#,##0")
                item.SubItems(5) = "" & Format(rs2.Fields!kredit, "#,##0")
                rs2.MoveNext
            Loop
        End If
        edit = True
        cmd_batal.Enabled = True
        hitung_total
    End If
    
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Function get_akun(kode As String) As Boolean
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where kode='" & kode & "'", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text3(0).Text = "" & rs1.Fields!kode
        Text3(1).Text = "" & rs1.Fields!nama
        Text3(2).Text = "0.00"
        Text3(2).Locked = False
        Text3(3).Text = "0.00"
        Text3(3).Locked = False
        get_akun = True
    Else
        MsgBox ("Kode akun tidak ada")
        get_akun = False
    End If
End Function

Sub kosong()
    Text1(1).Text = ""
    Text1(2).Text = "0.00"
    Text1(3).Text = "0.00"
    
    Text3(0).Text = ""
        
    DTPicker1.Value = Date
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    cmd_batal.Enabled = False
End Sub

Public Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol1, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Jurnal'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = xx
        
        kol3 = rs1.Fields!kol3
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = xx
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = xx
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = xx
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(vc) as nota from jour_head where length(vc)=" & j + z & " and right(vc," & z & ")='" & nomor_nota & "' and left(vc," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(vc) as nota from po_head where length(vc)=" & j + z & " and left(vc," & z & ")='" & nomor_nota & "' and right(vc," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
    End If
    Exit Sub
exc:
    MsgBox "Error : " & err.Description
End Sub
