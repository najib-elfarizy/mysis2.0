VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form master_agen 
   Caption         =   "Data Agency"
   ClientHeight    =   7695
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   10830
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7695
   ScaleWidth      =   10830
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   10785
      TabIndex        =   0
      Top             =   6960
      Width           =   10815
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_agen.frx":0000
         PICN            =   "master_agen.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   1440
         TabIndex        =   2
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_agen.frx":0176
         PICN            =   "master_agen.frx":0192
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7005
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   12356
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Data Agen"
      TabPicture(0)   =   "master_agen.frx":05C8
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Picture2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Paket Harga"
      TabPicture(1)   =   "master_agen.frx":05E4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Picture4"
      Tab(1).Control(1)=   "ListView1"
      Tab(1).Control(2)=   "cmd_hapus"
      Tab(1).ControlCount=   3
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   6420
         Left            =   120
         ScaleHeight     =   6390
         ScaleWidth      =   10545
         TabIndex        =   11
         Top             =   400
         Width           =   10575
         Begin VB.TextBox Text3 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   4560
            Locked          =   -1  'True
            TabIndex        =   50
            Text            =   "0.00"
            Top             =   4440
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   0
            Left            =   1080
            MaxLength       =   15
            TabIndex        =   26
            Text            =   "Text1"
            Top             =   240
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   1
            Left            =   1080
            MaxLength       =   50
            TabIndex        =   25
            Text            =   "Text1"
            Top             =   600
            Width           =   6135
         End
         Begin VB.TextBox Text1 
            Height          =   675
            Index           =   2
            Left            =   1080
            MaxLength       =   100
            MultiLine       =   -1  'True
            TabIndex        =   24
            Text            =   "master_agen.frx":0600
            Top             =   960
            Width           =   6135
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   3
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   23
            Text            =   "Text1"
            Top             =   1680
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   4
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   22
            Text            =   "Text1"
            Top             =   1680
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   5
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   21
            Text            =   "Text1"
            Top             =   2040
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   6
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   20
            Text            =   "Text1"
            Top             =   2040
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   7
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   19
            Text            =   "Text1"
            Top             =   2520
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   8
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   18
            Text            =   "Text1"
            Top             =   2520
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   9
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   17
            Text            =   "Text1"
            Top             =   2880
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   10
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   16
            Text            =   "Text1"
            Top             =   2880
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   11
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   15
            Text            =   "Text1"
            Top             =   3360
            Width           =   2535
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   12
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   14
            Text            =   "Text1"
            Top             =   3360
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   13
            Left            =   4560
            MaxLength       =   30
            TabIndex        =   13
            Text            =   "Text1"
            Top             =   3720
            Width           =   2655
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   14
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   12
            Text            =   "Text1"
            Top             =   3720
            Width           =   2535
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Caption         =   "Saldo DP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3480
            TabIndex        =   51
            Top             =   4560
            Width           =   975
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Kode : "
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   41
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Nama : "
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   40
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Kota : "
            Height          =   255
            Index           =   2
            Left            =   0
            TabIndex        =   39
            Top             =   1680
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Alamat : "
            Height          =   255
            Index           =   3
            Left            =   0
            TabIndex        =   38
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Telepon : "
            Height          =   255
            Index           =   4
            Left            =   0
            TabIndex        =   37
            Top             =   2520
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Negara : "
            Height          =   255
            Index           =   5
            Left            =   0
            TabIndex        =   36
            Top             =   2040
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Kontak : "
            Height          =   255
            Index           =   6
            Left            =   3480
            TabIndex        =   35
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Email : "
            Height          =   255
            Index           =   7
            Left            =   0
            TabIndex        =   34
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Kode POS : "
            Height          =   255
            Index           =   8
            Left            =   3480
            TabIndex        =   33
            Top             =   2040
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Propinsi : "
            Height          =   255
            Index           =   9
            Left            =   3480
            TabIndex        =   32
            Top             =   1680
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Fax : "
            Height          =   255
            Index           =   10
            Left            =   3480
            TabIndex        =   31
            Top             =   2520
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Rek a/n : "
            Height          =   255
            Index           =   11
            Left            =   3480
            TabIndex        =   30
            Top             =   3720
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "No Rek : "
            Height          =   255
            Index           =   12
            Left            =   3480
            TabIndex        =   29
            Top             =   3360
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Bank : "
            Height          =   255
            Index           =   14
            Left            =   0
            TabIndex        =   28
            Top             =   3720
            Width           =   1095
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "NPWP : "
            Height          =   255
            Index           =   20
            Left            =   0
            TabIndex        =   27
            Top             =   3360
            Width           =   1095
         End
      End
      Begin VB.PictureBox Picture4 
         Height          =   630
         Left            =   -74880
         ScaleHeight     =   570
         ScaleWidth      =   10515
         TabIndex        =   4
         Top             =   480
         Width           =   10575
         Begin VB.TextBox Text2 
            Enabled         =   0   'False
            Height          =   315
            Index           =   2
            Left            =   2160
            Locked          =   -1  'True
            TabIndex        =   45
            Top             =   240
            Width           =   2530
         End
         Begin VB.TextBox Text2 
            Height          =   315
            Index           =   1
            Left            =   480
            TabIndex        =   44
            Top             =   240
            Width           =   1680
         End
         Begin VB.TextBox Text2 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   315
            Index           =   3
            Left            =   8640
            Locked          =   -1  'True
            TabIndex        =   48
            Top             =   240
            Width           =   1875
         End
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            Height          =   315
            Index           =   0
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   240
            Width           =   495
         End
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   330
            Index           =   0
            Left            =   4695
            TabIndex        =   46
            Top             =   240
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   127795201
            CurrentDate     =   43231
         End
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   330
            Index           =   1
            Left            =   6675
            TabIndex        =   47
            Top             =   240
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   127795201
            CurrentDate     =   43231
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Nama Paket"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   2
            Left            =   2160
            TabIndex        =   43
            Top             =   0
            Width           =   2535
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Kode"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   42
            Top             =   0
            Width           =   1695
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Harga "
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   5
            Left            =   8640
            TabIndex        =   9
            Top             =   0
            Width           =   1860
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   8
            Top             =   0
            Width           =   500
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Periode Awal"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   3
            Left            =   4695
            TabIndex        =   7
            Top             =   0
            Width           =   1950
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Periode Akhir"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   4
            Left            =   6660
            TabIndex        =   6
            Top             =   0
            Width           =   1965
         End
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   5325
         Left            =   -74880
         TabIndex        =   10
         Top             =   1080
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   9393
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Kode Paket"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Kode"
            Object.Width           =   2963
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Nama"
            Object.Width           =   4463
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   5
            Text            =   "Awal"
            Object.Width           =   3440
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   6
            Text            =   "Akhir"
            Object.Width           =   3440
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   7
            Text            =   "Harga"
            Object.Width           =   3263
         EndProperty
      End
      Begin MySIS.Button cmd_hapus 
         Height          =   345
         Left            =   -74880
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   6480
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Hapus Paket"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_agen.frx":0606
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "master_agen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd_hapus_Click()
    If ListView1.ListItems.Count > 0 Then
        pesan = MsgBox("Hapus Paket " & ListView1.SelectedItem.SubItems(4) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "delete from paket_agen where kdpkt='" & ListView1.SelectedItem.SubItems(2) & "'", con, adOpenKeyset, adLockOptimistic
            tampil_paket
        End If
    End If
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    'On Error GoTo exc
    If Text1(0).Text <> "" And Text1(1).Text <> "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from magen where kdag='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            If Text1(0).Locked = False Then
                MsgBox ("Kode Sales sudah ada, Silahkan pakai kode lain!"), vbInformation, "Info"
                Exit Sub
            Else
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "update magen set namaag='" & Text1(1).Text & "',alamat='" & Text1(2).Text & "',kota='" & Text1(3).Text & "',propinsi='" & Text1(4).Text & "',negara='" & Text1(5).Text & "',kodepos='" & Text1(6).Text & "'," & _
                         "telp='" & Text1(7).Text & "',fax='" & Text1(8).Text & "',email='" & Text1(9).Text & "',kontak='" & Text1(10).Text & "',npwp='" & Text1(11).Text & "',rek='" & Text1(12).Text & "',namarek='" & Text1(13).Text & "'," & _
                         "bank='" & Text1(14).Text & "' where kdag='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            End If
        Else
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "insert into magen (kdag,namaag,alamat,kota,propinsi,negara,kodepos,telp,fax,email,kontak,npwp,rek,namarek,bank,tglreg,usr) values ('" & Text1(0).Text & "','" & Text1(1).Text & "','" & Text1(2).Text & "','" & Text1(3).Text & "'," & _
                     "'" & Text1(4).Text & "','" & Text1(5).Text & "','" & Text1(6).Text & "','" & Text1(7).Text & "','" & Text1(8).Text & "','" & Text1(9).Text & "','" & Text1(10).Text & "','" & Text1(11).Text & "','" & Text1(12).Text & "','" & Text1(13).Text & "'," & _
                     "'" & Text1(14).Text & "','" & Format(Date, "yyyy-MM-dd") & "','" & xy & "')", con, adOpenKeyset, adLockOptimistic
        
        End If
        
        Dim item As ListItem
        For Each item In ListView1.ListItems
            If item.SubItems(2) = "" Then
                csql = "INSERT INTO paket_agen(KDPKT,BARA,NAMA,KDAG,TGL1,TGL2,HARGA,USR) " & _
                        "VALUES('" & Format(Date, "yymmdd") & Format(Time, "HHmmss") & "','" & item.SubItems(3) & "','" & item.SubItems(4) & "','" & Text1(0).Text & "','" & _
                        item.SubItems(5) & "','" & item.SubItems(6) & "'," & Format(item.SubItems(7), "##0.00") & ",'" & xy & "')"
            Else
                csql = "UPDATE paket_agen SET BARA='" & item.SubItems(3) & "',NAMA='" & item.SubItems(4) & "',TGL1='" & item.SubItems(5) & _
                        "',TGL2='" & item.SubItems(6) & "',HARGA=" & Format(item.SubItems(7), "##0.00") & " WHERE KDPKT='" & item.SubItems(2) & "'"
            End If
            
'            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next item
        
        tampil_paket
        MsgBox ("simpan sukses!"), vbInformation, "Info"
        
        If daftar_agen.Visible = True Then
            daftar_agen.tampil_agen
        End If
    Else
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
    End If
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub Form_Load()
    Text1(0).Text = ""
    On Error GoTo exc
    Me.KeyPreview = True
    
    DTPicker1(0).Value = DateSerial(Year(Date), Month(Date), 1)
    DTPicker1(1).Value = DateSerial(Year(Date), Month(Date), Day(DateSerial(Year(Date), Month(Date) + 1, 1 - 1)))
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture5.Top = Me.Height - 1305
    SSTab1.Height = Me.Height - 1290
    Picture2.Height = SSTab1.Height - 500
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 1 Then
        Text2(1).SetFocus
    End If
End Sub

Private Sub Text1_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 14
                Text1(z).Text = ""
            Next z
    End Select
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text1(0).Text <> "" Then
                    getagen
                End If
                SendKeys "{tab}"
            Else
                If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
            End If
       
        Case 1 To 14
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
                        
            Else
                If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
            End If
    End Select
End Sub

Private Sub Text2_Change(Index As Integer)
    If Index = 1 Then
        Text2(2).Text = ""
        Text2(3).Text = ""
        Text2(2).Locked = True
        Text2(2).Enabled = False
        Text2(3).Locked = True
        Text2(3).Enabled = False
        DTPicker1(0).Enabled = False
        DTPicker1(1).Enabled = False
    End If
End Sub

Private Sub Text2_GotFocus(Index As Integer)
    Select Case Index
        Case 2 To 3
            With Text2(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text2_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 1 Then
        cari_stok.Left = 750
        cari_stok.Top = 3600
        cari_stok.bara = Text2(1).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
End Sub

Private Sub Text2_KeyPress(Index As Integer, KeyAscii As Integer)
    If Text2(Index).Text = "" Then Exit Sub
    
    If KeyAscii = 13 Then
        If Index = 1 Then
            KeyAscii = 0
            get_item Text2(1).Text
        ElseIf Index = 3 Then
            KeyAscii = 0
            tambah_item
        Else
            KeyAscii = 0
            SendKeys "{tab}"
        End If
    End If
End Sub

Private Sub DTPicker1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    End If
End Sub

Sub getagen()
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from magen where kdag='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        On Error Resume Next
        Text1(1).Text = "" & rs1.Fields!namasl
        Text1(2).Text = "" & rs1.Fields!alamat
        Text1(3).Text = "" & rs1.Fields!kota
        Text1(4).Text = "" & rs1.Fields!propinsi
        Text1(5).Text = "" & rs1.Fields!negara
        Text1(6).Text = "" & rs1.Fields!kodepos
        Text1(7).Text = "" & rs1.Fields!telp
        Text1(8).Text = "" & rs1.Fields!fax
        Text1(9).Text = "" & rs1.Fields!email
        Text1(10).Text = "" & rs1.Fields!kontak
        Text1(11).Text = "" & rs1.Fields!npwp
        Text1(12).Text = "" & rs1.Fields!rek
        Text1(13).Text = "" & rs1.Fields!namarek
        Text1(14).Text = "" & rs1.Fields!bank
        
        Text3.Text = Format(rs1.Fields!masuk - rs1.Fields!keluar, "#,##0.00")
    End If
    
    tampil_paket
    Exit Sub
exc:

End Sub

Sub tampil_paket()
    On Error GoTo exc
    
    ListView1.ListItems.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from paket_agen where kdag='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            On Error Resume Next
            j = ListView1.ListItems.Count + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = rs1.Fields!kdpkt
            item.SubItems(3) = rs1.Fields!bara
            item.SubItems(4) = rs1.Fields!nama
            item.SubItems(5) = Format(rs1.Fields!TGL1, "yyyy-mm-dd")
            item.SubItems(6) = Format(rs1.Fields!tgl2, "yyyy-mm-dd")
            item.SubItems(7) = Format(rs1.Fields!harga, "#,#0.00")
            rs1.MoveNext
        Loop
    End If
Exit Sub
exc:

End Sub

Sub get_item(bara As String)
    If rs1.State <> 0 Then rs1.Close
    csql = "select m.bara, nama, hjual from mstock m join mstock_bo b on m.bara=b.bara where m.bara='" & bara & "' limit 1"
        
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Text2(1).Text = "" & rs1.Fields!bara
        Text2(2).Text = "" & rs1.Fields!nama
        Text2(3).Text = "" & Format(rs1.Fields!hjual, "#,##0.00")
        Text2(2).Locked = False
        Text2(2).Enabled = True
        Text2(3).Locked = False
        Text2(3).Enabled = True
        DTPicker1(0).Enabled = True
        DTPicker1(1).Enabled = True
    Else
        MsgBox ("Kode barang tidak ada")
        cari_stok.Left = 750
        cari_stok.Top = 3600
        cari_stok.bara = Text2(1).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
    SendKeys "{tab}"
End Sub

Private Sub tambah_item()
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(3) = Text2(1).Text
    item.SubItems(4) = Text2(2).Text
    item.SubItems(5) = Format(DTPicker1(0).Value, "yyyy-mm-dd")
    item.SubItems(6) = Format(DTPicker1(1).Value, "yyyy-mm-dd")
    item.SubItems(7) = Format(Text2(3).Text, "#,#0.00")
    
    Text2(1).Text = ""
    Text2(1).SetFocus
End Sub

Sub otomasi()
    On Error GoTo exc
    Dim nomor_agen As String
    Dim NO
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Agen'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        If rs1.Fields!kol1 = "[CNT]" Then
            nomor_agen = "" & rs1.Fields!kol2 & Left(rs1.Fields!kol3, 4)
            z = Len(nomor_agen)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(kdag) as kdag from magen where length(kdag)=" & j + z & " and right(kdag," & z & ")='" & nomor_agen & "' and left(KDSL," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                Text1(0).Text = Left("0000000000", j - 1) & "1" & nomor_agen
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    Text1(0).Text = NO & nomor_agen
                Else
                    Text1(0).Text = Left("0000000000", j - 1) & "1" & nomor_agen
                End If
            End If
        
        Else
            nomor_agen = "" & Left(rs1.Fields!kol1, 4) & rs1.Fields!kol2
            z = Len(nomor_agen)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(kdag) as kdag from magen where length(kdag)=" & j + z & " and left(kdag," & z & ")='" & nomor_agen & "' and RIGHT(KDAG," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                Text1(0).Text = nomor_agen & Left("0000000000", j - 1) & "1"
            Else
                If Right(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Right(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    Text1(0).Text = nomor_agen + NO
                Else
                    Text1(0).Text = nomor_agen & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
    End If
Exit Sub
exc:

End Sub
