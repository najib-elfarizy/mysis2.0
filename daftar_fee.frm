VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form daftar_fee 
   Caption         =   "Daftar Fee"
   ClientHeight    =   6870
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9855
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6870
   ScaleWidth      =   9855
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   9825
      TabIndex        =   11
      Top             =   6120
      Width           =   9855
      Begin MySIS.Button cmd_cetak 
         Height          =   495
         Left            =   2760
         TabIndex        =   19
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Cetak"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":0000
         PICN            =   "daftar_fee.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4080
         TabIndex        =   12
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":03B6
         PICN            =   "daftar_fee.frx":03D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   5400
         TabIndex        =   20
         Top             =   120
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":0808
         PICN            =   "daftar_fee.frx":0824
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   21
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":097E
         PICN            =   "daftar_fee.frx":099A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Batalkan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":0D34
         PICN            =   "daftar_fee.frx":0D50
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   6255
      Left            =   0
      ScaleHeight     =   6225
      ScaleWidth      =   9825
      TabIndex        =   0
      Top             =   0
      Width           =   9855
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   840
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   300
         Left            =   1680
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   130
         Width           =   4575
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "daftar_fee.frx":0EAA
         Left            =   1680
         List            =   "daftar_fee.frx":0EB4
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1200
         Width           =   2055
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Asc"
         Height          =   195
         Left            =   3840
         TabIndex        =   2
         Top             =   1280
         Value           =   -1  'True
         Width           =   615
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Desc"
         Height          =   195
         Left            =   4560
         TabIndex        =   1
         Top             =   1280
         Width           =   855
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   4335
         Left            =   120
         TabIndex        =   5
         Top             =   1680
         Width           =   9615
         _ExtentX        =   16960
         _ExtentY        =   7646
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tanggal"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "No. Kunjungan"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Total"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   5
            Text            =   "Fee"
            Object.Width           =   3528
         EndProperty
      End
      Begin MySIS.Button cmd_cari 
         Height          =   375
         Left            =   6360
         TabIndex        =   6
         Top             =   120
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tampil"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_fee.frx":0ED0
         PICN            =   "daftar_fee.frx":0EEC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Index           =   0
         Left            =   1680
         TabIndex        =   16
         Top             =   480
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   582
         _Version        =   393216
         Format          =   105250817
         CurrentDate     =   43218
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Index           =   1
         Left            =   4200
         TabIndex        =   17
         Top             =   480
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   582
         _Version        =   393216
         Format          =   105250817
         CurrentDate     =   43218
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "s/d"
         Height          =   255
         Index           =   4
         Left            =   3795
         TabIndex        =   18
         Top             =   525
         Width           =   255
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tampil Data : "
         Height          =   255
         Index           =   3
         Left            =   600
         TabIndex        =   15
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept/Gudang : "
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   14
         Top             =   855
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   5280
         Width           =   6135
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Urut Berdasar : "
         Height          =   255
         Index           =   2
         Left            =   285
         TabIndex        =   8
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kata Kunci : "
         Height          =   255
         Index           =   0
         Left            =   170
         TabIndex        =   7
         Top             =   180
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1605
         Index           =   0
         Left            =   -240
         TabIndex        =   9
         Top             =   0
         Width           =   10095
      End
   End
End
Attribute VB_Name = "daftar_fee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim cryApp As CRAXDRT.Application
Dim rptApp As CRAXDRT.Report

Private Sub Button5_Click()
    Frame1.Visible = False
End Sub

Private Sub cmd_batal_Click()
    On Error GoTo err
    If ListView1.ListItems.Count > 0 Then
    pesan = MsgBox("Hapus penarikan fee " & ListView1.SelectedItem.SubItems(1) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "delete from fee_head where kdtrk='" & ListView1.SelectedItem.SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
                    
            daftar_fee.tampil_fee
        End If
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub cmd_cetak_Click()
    If ListView1.ListItems.Count > 0 Then
        Dim fso As FileSystemObject
        Set fso = New FileSystemObject
        
        If fso.FileExists(App.Path & "\Report\pembelian.rpt") Then
            Set cryApp = New CRAXDRT.Application
            Set rptApp = cryApp.OpenReport(App.Path & "\Report\pembelian.rpt")
            rptApp.EnableParameterPrompting = False
        
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select * from mperusahaan limit 1", con, adOpenKeyset, adLockOptimistic
            If Not rs1.EOF Then
                On Error Resume Next
                rptApp.ParameterFields.GetItemByName("NAMA_PERUSAHAAN").AddCurrentValue "" & rs1.Fields!NAMA_PERUSAHAAN
                rptApp.ParameterFields.GetItemByName("ALAMAT1").AddCurrentValue "" & rs1.Fields!alamat1
                rptApp.ParameterFields.GetItemByName("ALAMAT2").AddCurrentValue "" & rs1.Fields!alamat2
                rptApp.ParameterFields.GetItemByName("TELP").AddCurrentValue "" & rs1.Fields!telp
                rptApp.ParameterFields.GetItemByName("FAX").AddCurrentValue "" & rs1.Fields!fax
                rptApp.ParameterFields.GetItemByName("NPWP").AddCurrentValue "" & rs1.Fields!npwp
            End If
            
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "SELECT * FROM fee_head b JOIN beli_det d ON b.no_ej=d.kdtrk WHERE b.no_ej='" & ListView1.SelectedItem.SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
            rptApp.Database.SetDataSource rs1
            
            form_cetak.Visible = True
            form_cetak.ShowReport rptApp
        Else
            MsgBox "Report File Doesn't Exist!"
        End If
    End If
End Sub

Private Sub cmd_edit_Click()
    If ListView1.ListItems.Count > 0 Then
        input_fee.Show
        input_fee.WindowState = 2
        input_fee.SetFocus
        MDIForm1.tabref
        input_fee.tampil_fee ListView1.SelectedItem.SubItems(1)
    End If
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_cari_Click()
    tampil_fee
End Sub

Sub tampil_fee()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    If cmb_cabang.ListIndex = 0 Then
        csql = "select * from fee_head where 1=1 "
    Else
        csql = "select * from fee_head where bo='" & cmb_cabang.Text & "' "
    End If
    
    If Text1.Text <> "" Then
        csql = csql & "and (kdtrk like '%" & Text1.Text & "%' or kdag like '%" & Text1.Text & "%' or namasl like '%" & Text1.Text & "%') "
    End If
    
    csql = csql & "and tgl between '" & Format(DTPicker1(0).Value, "yyyy-MM-dd") & "' and '" & Format(DTPicker1(1).Value, "yyyy-MM-dd") & "'"
    
    If Combo1.ListIndex = 3 Then
        csql = csql & " order by namasl "
    ElseIf Combo1.ListIndex = 2 Then
        csql = csql & " order by kdag "
    ElseIf Combo1.ListIndex = 1 Then
        csql = csql & " order by tgl "
    Else
        csql = csql & " order by no_kunj "
    End If
        
    If Option1.Value = True Then
        csql = csql & "asc"
    Else
        csql = csql & "desc"
    End If
'    Debug.Print csql
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    j = 0
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!kdtrk
            item.SubItems(2) = "" & Format(rs1.Fields!tgl, "dd-mm-yyyy")
            item.SubItems(3) = "" & rs1.Fields!no_kunj
            item.SubItems(4) = "" & Format(rs1.Fields!netto, "#,##00")
            item.SubItems(5) = "" & Format(rs1.Fields!tarik, "#,##00")
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_tambah_Click()
    input_fee.Show
    input_fee.WindowState = 2
    input_fee.SetFocus
    input_fee.otomatis
    MDIForm1.tabref
End Sub

Private Sub Form_Load()
    Text1.Text = ""
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    Combo1.Text = Combo1.List(0)
    DTPicker1(0).Value = DateSerial(Year(Date), Month(Date), 1)
    DTPicker1(1).Value = Date
    
    koneksi
    
    On Error Resume Next
    cmb_cabang.Clear
    cmb_cabang.AddItem ("Semua Dept.")
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
        rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
    
    tampil_fee
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1290
    ListView1.Height = Picture2.Height - 1800
    Picture2.Width = Me.Width - 255
    Picture5.Width = Me.Width - 255
    ListView1.Width = Me.Width - 495
    Label1(0).Width = Me.Width - 50
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        input_fee.Show
        input_fee.WindowState = 2
        input_fee.SetFocus
        input_fee.tampil_fee ListView1.SelectedItem.SubItems(1)
    End If
End Sub
