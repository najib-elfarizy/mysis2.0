VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form cari_stok 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Cari Item"
   ClientHeight    =   4140
   ClientLeft      =   2760
   ClientTop       =   3690
   ClientWidth     =   10500
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   10500
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmd_nav 
      Caption         =   ">|"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   10200
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   0
      Width           =   300
   End
   Begin VB.CommandButton cmd_nav 
      Caption         =   ">"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   9960
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   0
      Width           =   255
   End
   Begin VB.CommandButton cmd_nav 
      Caption         =   "<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   9120
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   0
      Width           =   255
   End
   Begin VB.CommandButton cmd_nav 
      Caption         =   "|<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   8835
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   300
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   50
      Width           =   5415
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3735
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   10515
      _ExtentX        =   18547
      _ExtentY        =   6588
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Kode"
         Object.Width           =   2822
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Nama"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Stok"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Satuan"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Harga Pokok"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Harga Jual"
         Object.Width           =   2646
      EndProperty
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   210
      Left            =   7320
      TabIndex        =   7
      Top             =   70
      Width           =   1335
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9360
      TabIndex        =   6
      Top             =   15
      Width           =   615
   End
End
Attribute VB_Name = "cari_stok"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim pageIndex As Integer
Dim pageSize As Integer
Dim pageCount As Integer
Dim recordCount As Integer

Public FormPemanggil As Form
Public bara, sql As String

Private Sub cmd_nav_Click(Index As Integer)
    Select Case Index
        Case 0
            If pageIndex > 1 Then
                pageIndex = 1
                tampil_item
            End If
        Case 1
            If pageIndex > 1 Then
                pageIndex = pageIndex - 1
                tampil_item
            End If
        Case 2
            If pageIndex < pageCount Then
                pageIndex = pageIndex + 1
                tampil_item
            End If
        Case 3
            If pageIndex < pageCount Then
                pageIndex = pageCount
                tampil_item
            End If
    End Select
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then Unload Me
End Sub

Private Sub Form_Load()
    pageIndex = 1
    pageSize = 100
End Sub

Private Sub Form_Activate()
    Text1.Text = bara
    tampil_item
End Sub

Sub tampil_item()
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    j = 0
    If rs1.State <> 0 Then rs1.Close
    
    sql = "select count(m.bara) from mstock m join mstock_bo b on m.bara=b.bara "
    csql = "select m.bara, m.nama, m.satuan, b.awal, b.masuk, b.keluar, b.aver hbeli, b.hjual from mstock m join mstock_bo b on m.bara=b.bara "
     
    If Text1.Text <> "" Then
        sql = sql & "where m.bara = '" & Text1.Text & "' or nama like '%" & Text1.Text & "%' "
        csql = csql & "where m.bara = '" & Text1.Text & "' or nama like '%" & Text1.Text & "%' "
    End If
    
    rs1.Open sql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            recordCount = Val(rs1.Fields(0))
            If recordCount > 0 Then
                pageCount = Int(recordCount / pageSize) + 1
                pageCount = IIf(pageCount = 0, 1, pageCount)
                pageIndex = IIf(pageIndex > pageCount, 1, pageIndex)
            End If
            rs1.MoveNext
        Loop
        Label1.Caption = "" & pageIndex & "/" & pageCount
        Label2.Caption = recordCount & " item"
    End If
    
    If rs1.State <> 0 Then rs1.Close
    
    csql = csql & "limit " & Val(pageSize * (pageIndex - 1)) & "," & pageSize
    
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!bara
            item.SubItems(2) = "" & rs1.Fields!nama
            item.SubItems(3) = "" & Format(rs1.Fields!AWAL + rs1.Fields!masuk - rs1.Fields!keluar, "#,##0.00")
            item.SubItems(4) = "" & rs1.Fields!satuan
            item.SubItems(5) = "" & Format(rs1.Fields!hbeli, "#,##0.00")
            item.SubItems(6) = "" & Format(rs1.Fields!hjual, "#,##0.00")
            rs1.MoveNext
        Loop
    End If
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        If FormPemanggil Is input_pembelian Then
            input_pembelian.get_item ListView1.SelectedItem.SubItems(1)
'           input_pembelian.Text3(1).Text = ListView1.SelectedItem.SubItems(2)
'           SendKeys "{tab}"
        ElseIf FormPemanggil Is input_po Then
            input_po.get_item ListView1.SelectedItem.SubItems(1)
        ElseIf FormPemanggil Is input_returbeli Then
            input_returbeli.get_item ListView1.SelectedItem.SubItems(1)
        ElseIf FormPemanggil Is master_agen Then
            master_agen.get_item ListView1.SelectedItem.SubItems(1)
        End If
    End If
    Unload Me
End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If ListView1.ListItems.Count > 0 Then
            If FormPemanggil Is input_pembelian Then
                input_pembelian.get_item ListView1.SelectedItem.SubItems(1)
'            input_pembelian.Text3(1).Text = ListView1.SelectedItem.SubItems(2)
'            SendKeys "{tab}"
            ElseIf FormPemanggil Is input_po Then
                input_po.get_item ListView1.SelectedItem.SubItems(1)
            ElseIf FormPemanggil Is input_returbeli Then
                input_returbeli.get_item ListView1.SelectedItem.SubItems(1)
            ElseIf FormPemanggil Is master_agen Then
                master_agen.get_item ListView1.SelectedItem.SubItems(1)
            End If
        End If
        Unload Me
    End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        tampil_item
    End If
End Sub
