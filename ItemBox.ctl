VERSION 5.00
Begin VB.UserControl ItemBox 
   ClientHeight    =   3015
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2505
   MousePointer    =   99  'Custom
   ScaleHeight     =   3015
   ScaleWidth      =   2505
   Begin MySIS.Button cmd_detail 
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2595
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "ItemBox.ctx":0000
      PICN            =   "ItemBox.ctx":001C
      UMCOL           =   0   'False
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MySIS.Button cmd_pilih 
      Height          =   375
      Left            =   2040
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2595
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "ItemBox.ctx":03B6
      PICN            =   "ItemBox.ctx":03D2
      UMCOL           =   0   'False
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label label_curr 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Caption         =   "IDR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   50
      TabIndex        =   4
      Top             =   2700
      Width           =   345
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000A&
      X1              =   50
      X2              =   2450
      Y1              =   2520
      Y2              =   2520
   End
   Begin VB.Label label_nama 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   90
      TabIndex        =   2
      Top             =   1920
      Width           =   2335
      WordWrap        =   -1  'True
   End
   Begin VB.Label label_harga 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   480
      TabIndex        =   3
      Top             =   2700
      Width           =   390
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   90
      TabIndex        =   5
      Top             =   1920
      Width           =   2335
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   2520
      Left            =   45
      Picture         =   "ItemBox.ctx":0789
      Stretch         =   -1  'True
      Top             =   45
      Width           =   2400
   End
End
Attribute VB_Name = "ItemBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Public Event TambahClick()
Public Event DetailClick()
Private mKode As String
Private mNama As String
Private mKeterangan As String
Private mCurr As String
Private mHarga As Double
Private mJumlah As Integer
Private mFeePersen As Double
Private mFeeNominal As Double

Public Property Get Image() As Image
    Set Image = Image1
End Property

Public Property Let Image(ByVal Image As Image)
    Set Image1 = Image
    PropertyChanged "Image"
End Property

Public Property Get picture() As IPictureDisp
    Set picture = Image1.picture
End Property

Public Property Set picture(ByVal p As IPictureDisp)
    Set Image1.picture = p
    PropertyChanged "Picture"
End Property

Public Property Get kode() As String
    kode = mKode
End Property

Public Property Let kode(ByVal kode As String)
    mKode = kode
    PropertyChanged "Kode"
End Property

Public Property Get Harga() As Double
    Harga = mHarga
End Property

Public Property Let Harga(ByVal Harga As Double)
    label_harga.Caption = Format(Harga, "#,##0.00")
    mHarga = Harga
    PropertyChanged "Harga"
End Property
    
Public Property Get matauang() As String
    matauang = mCurr
End Property

Public Property Let matauang(ByVal curr As String)
    mCurr = curr
    label_curr.Caption = curr
    PropertyChanged "MataUang"
End Property

Public Property Get Jumlah() As Integer
    Jumlah = mJumlah
End Property

Public Property Let Jumlah(ByVal Jumlah As Integer)
    mJumlah = Jumlah
    PropertyChanged "Jumlah"
End Property

Public Property Get FeePersen() As Double
    FeePersen = mFeePersen
End Property

Public Property Let FeePersen(ByVal fee As Double)
    mFeePersen = fee
End Property

Public Property Get FeeNominal() As Double
    FeeNominal = mFeeNominal
End Property

Public Property Let FeeNominal(ByVal fee As Double)
    mFeeNominal = fee
End Property

Public Property Get nama() As String
    nama = mNama
End Property

Public Property Let nama(ByVal nama As String)
    mNama = nama
    label_nama.Caption = nama
    PropertyChanged "Nama"
End Property

Public Property Get keterangan() As String
    keterangan = mKeterangan
End Property

Public Property Let keterangan(ByVal keterangan As String)
    mKeterangan = keterangan
    PropertyChanged "Keterangan"
End Property

Public Property Get FontHarga() As StdFont
    Set FontHarga = label_harga.Font
End Property

Public Property Set FontHarga(ByVal mfont As StdFont)
    Set label_harga.Font = mfont
    Refresh
    PropertyChanged "FontHarga"
End Property

Public Property Get FontKeterangan() As StdFont
    Set FontKeterangan = label_nama.Font
End Property

Public Property Set FontKeterangan(ByVal Nfont As StdFont)
    Set label_nama.Font = Nfont
    Refresh
    PropertyChanged "FontKeterangan"
End Property

Private Sub cmd_detail_Click()
    RaiseEvent DetailClick
End Sub

Private Sub cmd_pilih_Click()
    RaiseEvent TambahClick
End Sub

Private Sub Image1_Click()
    RaiseEvent TambahClick
End Sub

Private Sub label_nama_Click()
    RaiseEvent TambahClick
End Sub

Private Sub UserControl_Initialize()
    MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
  With PropBag
    Set Me.picture = .ReadProperty("Picture", Nothing)
    Set Me.Image = .ReadProperty("Image", Nothing)
  End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
  With PropBag
    .WriteProperty "Picture", Me.picture, Nothing
    .WriteProperty "Image", Me.Image, Nothing
  End With
End Sub
