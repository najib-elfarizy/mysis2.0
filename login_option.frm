VERSION 5.00
Begin VB.Form login_option 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pilihan Login"
   ClientHeight    =   3360
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      ScaleHeight     =   585
      ScaleWidth      =   5145
      TabIndex        =   0
      Top             =   2640
      Width           =   5175
      Begin VB.CommandButton CancelButton 
         Caption         =   "&Keluar"
         Height          =   375
         Left            =   3840
         Picture         =   "login_option.frx":0000
         TabIndex        =   1
         Top             =   120
         Width           =   1215
      End
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Back Office"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Index           =   1
      Left            =   3360
      MousePointer    =   99  'Custom
      TabIndex        =   3
      Top             =   1995
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kasir/Lobi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Index           =   0
      Left            =   720
      MousePointer    =   99  'Custom
      TabIndex        =   2
      Top             =   2000
      Width           =   1335
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   2400
      Index           =   1
      Left            =   2880
      MousePointer    =   99  'Custom
      Picture         =   "login_option.frx":519F
      Stretch         =   -1  'True
      Top             =   120
      Width           =   2400
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   2400
      Index           =   0
      Left            =   120
      MousePointer    =   99  'Custom
      Picture         =   "login_option.frx":E978
      Stretch         =   -1  'True
      Top             =   120
      Width           =   2400
   End
End
Attribute VB_Name = "login_option"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
    End
End Sub

Private Sub Form_Load()
    Set Image1(0).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
    Set Image1(1).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
    Set Label1(0).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
    Set Label1(1).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Image1_Click(Index As Integer)
    Hide
    If Index = 0 Then
        penjualan_karcis.Show 1
'        penjualan_karcis.otomatis
    ElseIf Index = 1 Then
        MDIForm1.Show
    End If
End Sub

Private Sub Label1_Click(Index As Integer)
    Hide
    If Index = 0 Then
        penjualan_karcis.Show 1
    ElseIf Index = 1 Then
        MDIForm1.Show
    End If
End Sub
