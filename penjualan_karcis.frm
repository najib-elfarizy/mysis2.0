VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form penjualan_karcis 
   BackColor       =   &H00FFFFC0&
   BorderStyle     =   0  'None
   Caption         =   "Penjualan"
   ClientHeight    =   9345
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   20490
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   20490
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox PictureStruk 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H80000008&
      Height          =   5700
      Left            =   3840
      ScaleHeight     =   5670
      ScaleWidth      =   5025
      TabIndex        =   89
      Top             =   2350
      Visible         =   0   'False
      Width           =   5055
      Begin MSComctlLib.ListView ListViewStruk 
         Height          =   4695
         Left            =   120
         TabIndex        =   93
         Top             =   360
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   8281
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Width           =   8026
         EndProperty
      End
      Begin MySIS.Button ButtonCloseStruk 
         Height          =   435
         Left            =   120
         TabIndex        =   91
         Top             =   5160
         Width           =   1215
         _extentx        =   2143
         _extenty        =   767
         btype           =   3
         tx              =   "&Tutup"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":0000
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   15790320
         bcolo           =   15790320
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":002C
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_cetak 
         Height          =   435
         Left            =   3720
         TabIndex        =   92
         Top             =   5160
         Width           =   1215
         _extentx        =   2143
         _extenty        =   767
         btype           =   3
         tx              =   "Cetak"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":004A
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":0076
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   300
         Left            =   4450
         Picture         =   "penjualan_karcis.frx":0094
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Print Struk"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   90
         Top             =   0
         Width           =   5055
      End
   End
   Begin VB.PictureBox Picture7 
      Height          =   360
      Left            =   9240
      ScaleHeight     =   300
      ScaleWidth      =   6075
      TabIndex        =   75
      Top             =   2280
      Visible         =   0   'False
      Width           =   6135
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   315
         Index           =   5
         Left            =   8100
         Locked          =   -1  'True
         TabIndex        =   81
         TabStop         =   0   'False
         Top             =   0
         Width           =   1300
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         Left            =   6800
         Locked          =   -1  'True
         TabIndex        =   80
         TabStop         =   0   'False
         Top             =   0
         Width           =   1300
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         Height          =   315
         Index           =   3
         Left            =   5760
         TabIndex        =   79
         Top             =   0
         Width           =   1035
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   78
         Top             =   0
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   1500
         Locked          =   -1  'True
         TabIndex        =   77
         Top             =   0
         Width           =   3500
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   315
         Index           =   2
         Left            =   5000
         Locked          =   -1  'True
         TabIndex        =   76
         TabStop         =   0   'False
         Top             =   0
         Width           =   800
      End
   End
   Begin VB.PictureBox PictureTotal2 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   9240
      ScaleHeight     =   705
      ScaleWidth      =   6105
      TabIndex        =   44
      Top             =   960
      Width           =   6135
      Begin VB.Label total_idr 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H80000012&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   29.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   720
         Left            =   4920
         TabIndex        =   46
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "IDR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   60
         TabIndex        =   45
         Top             =   25
         Width           =   975
      End
   End
   Begin VB.PictureBox PictureTotal1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   9240
      ScaleHeight     =   825
      ScaleWidth      =   6105
      TabIndex        =   19
      Top             =   50
      Width           =   6135
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "USD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   60
         TabIndex        =   43
         Top             =   25
         Width           =   975
      End
      Begin VB.Label total_usd 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H80000012&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   29.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   720
         Left            =   4920
         TabIndex        =   42
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFC0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   9240
      ScaleHeight     =   450
      ScaleWidth      =   6135
      TabIndex        =   39
      Top             =   1800
      Width           =   6135
      Begin MySIS.Button cmd_batal 
         Height          =   435
         Left            =   3240
         TabIndex        =   40
         Top             =   0
         Width           =   1335
         _extentx        =   2355
         _extenty        =   767
         btype           =   3
         tx              =   "&Batal"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":049F
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   15790320
         bcolo           =   15790320
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":04CB
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_pending 
         Height          =   435
         Left            =   1680
         TabIndex        =   41
         Top             =   0
         Width           =   1335
         _extentx        =   2355
         _extenty        =   767
         btype           =   3
         tx              =   "&Pending"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":04E9
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   14737632
         bcolo           =   14737632
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":0515
         picn            =   "penjualan_karcis.frx":0533
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_showbayar 
         Height          =   435
         Left            =   4680
         TabIndex        =   47
         Top             =   0
         Width           =   1455
         _extentx        =   2566
         _extenty        =   767
         btype           =   3
         tx              =   "&Bayar [END]"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":06E7
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   14737632
         bcolo           =   14737632
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":0713
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_dftpending 
         Height          =   435
         Left            =   0
         TabIndex        =   82
         Top             =   0
         Width           =   1575
         _extentx        =   2778
         _extenty        =   767
         btype           =   3
         tx              =   "&Dft. Pending"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":0731
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   14737632
         bcolo           =   14737632
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":075D
         picn            =   "penjualan_karcis.frx":077B
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
   End
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   400
      Left            =   3240
      ScaleHeight     =   375
      ScaleWidth      =   3345
      TabIndex        =   32
      Top             =   1800
      Width           =   3375
      Begin VB.OptionButton OptionTipe 
         Caption         =   "Barang"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   920
         TabIndex        =   35
         Top             =   80
         Width           =   975
      End
      Begin VB.OptionButton OptionTipe 
         Caption         =   "Konsinyasi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1920
         TabIndex        =   34
         Top             =   80
         Width           =   1335
      End
      Begin VB.OptionButton OptionTipe 
         Caption         =   "Jasa"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   33
         Top             =   80
         Value           =   -1  'True
         Width           =   735
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3405
      Left            =   9240
      TabIndex        =   22
      Top             =   5895
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   6006
      _Version        =   393216
      Tabs            =   1
      TabHeight       =   520
      BackColor       =   16777152
      TabCaption(0)   =   "Pengunjung"
      TabPicture(0)   =   "penjualan_karcis.frx":090B
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "cmd_customer"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ListView2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Picture4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      Begin VB.PictureBox Picture4 
         Height          =   660
         Left            =   50
         ScaleHeight     =   600
         ScaleWidth      =   5955
         TabIndex        =   24
         Top             =   350
         Width           =   6015
         Begin VB.TextBox Text2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   4170
            TabIndex        =   30
            Top             =   240
            Width           =   5130
         End
         Begin VB.TextBox Text2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   480
            TabIndex        =   29
            Top             =   240
            Width           =   3700
         End
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   28
            Top             =   240
            Width           =   495
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Instagram/Email"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   2
            Left            =   4180
            TabIndex        =   27
            Top             =   0
            Width           =   5100
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Nama"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   490
            TabIndex        =   26
            Top             =   0
            Width           =   3690
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   25
            Top             =   0
            Width           =   500
         End
      End
      Begin MSComctlLib.ListView ListView2 
         Height          =   2325
         Left            =   45
         TabIndex        =   23
         Top             =   1005
         Width           =   6015
         _ExtentX        =   10610
         _ExtentY        =   4101
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         HotTracking     =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   6527
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Alamat"
            Object.Width           =   8996
         EndProperty
      End
      Begin MySIS.Button cmd_customer 
         Height          =   285
         Left            =   5100
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   0
         Width           =   1020
         _extentx        =   1799
         _extenty        =   503
         btype           =   3
         tx              =   "Tambah"
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":0927
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   14737632
         bcolo           =   14737632
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":0953
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   50
      ScaleHeight     =   1665
      ScaleWidth      =   9105
      TabIndex        =   14
      Top             =   50
      Width           =   9135
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   4560
         TabIndex        =   94
         Text            =   "Text1"
         Top             =   1200
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.TextBox TextTipeFee 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5330
         TabIndex        =   88
         Text            =   "0.00"
         Top             =   480
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.TextBox TextNfee 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5580
         TabIndex        =   86
         Text            =   "0.00"
         Top             =   480
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.TextBox TextFee 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5830
         TabIndex        =   85
         Text            =   "0.00"
         Top             =   480
         Visible         =   0   'False
         Width           =   240
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd-MM-yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   69
         Top             =   840
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   84148227
         CurrentDate     =   43234
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   6120
         Locked          =   -1  'True
         TabIndex        =   38
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   1200
         Width           =   2775
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   6120
         TabIndex        =   3
         Text            =   "Text1"
         Top             =   840
         Width           =   2415
      End
      Begin VB.ComboBox cmb_cabang 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   480
         Width           =   2775
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   6120
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   480
         Width           =   2775
      End
      Begin MySIS.Button cmd_sales 
         Height          =   360
         Left            =   8520
         TabIndex        =   18
         Top             =   120
         Width           =   360
         _extentx        =   635
         _extenty        =   635
         btype           =   7
         tx              =   ""
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":0971
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   15790320
         bcolo           =   15790320
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":099D
         picn            =   "penjualan_karcis.frx":09BB
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   6120
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   120
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   1680
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   1200
         Width           =   2775
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   1680
         Locked          =   -1  'True
         TabIndex        =   0
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   120
         Width           =   2775
      End
      Begin MySIS.Button cmd_agen 
         Height          =   360
         Left            =   8520
         TabIndex        =   37
         Top             =   840
         Width           =   360
         _extentx        =   635
         _extenty        =   635
         btype           =   7
         tx              =   ""
         enab            =   -1  'True
         font            =   "penjualan_karcis.frx":0AD7
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   16777215
         bcolo           =   16777215
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":0B03
         picn            =   "penjualan_karcis.frx":0B21
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No. Kunjungan : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   5
         Left            =   120
         TabIndex        =   87
         Top             =   1200
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Departemen : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   4
         Left            =   120
         TabIndex        =   68
         Top             =   480
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Agen : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   3
         Left            =   4560
         TabIndex        =   36
         Top             =   840
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Guide / Sales : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   2
         Left            =   4560
         TabIndex        =   17
         Top             =   120
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tanggal : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   840
         Width           =   1515
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No. Transaksi : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   1515
      End
   End
   Begin MySIS.Button cmd_cari 
      Height          =   405
      Left            =   2760
      TabIndex        =   13
      Top             =   1800
      Width           =   375
      _extentx        =   661
      _extenty        =   714
      btype           =   2
      tx              =   ""
      enab            =   -1  'True
      font            =   "penjualan_karcis.frx":0C3D
      coltype         =   1
      focusr          =   -1  'True
      bcol            =   15790320
      bcolo           =   15790320
      fcol            =   0
      fcolo           =   0
      mcol            =   12632256
      mptr            =   1
      micon           =   "penjualan_karcis.frx":0C69
      picn            =   "penjualan_karcis.frx":0C87
      umcol           =   0   'False
      soft            =   0   'False
      picpos          =   0
      ngrey           =   0   'False
      fx              =   0
      hand            =   0   'False
      check           =   0   'False
      value           =   0   'False
   End
   Begin VB.PictureBox PictureNav 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   400
      Left            =   7250
      ScaleHeight     =   375
      ScaleWidth      =   1905
      TabIndex        =   10
      Top             =   1800
      Width           =   1935
      Begin MySIS.Button cmd_nav 
         Height          =   375
         Index           =   0
         Left            =   960
         TabIndex        =   11
         Top             =   0
         Width           =   975
         _extentx        =   1720
         _extenty        =   661
         btype           =   2
         tx              =   "Next"
         enab            =   0   'False
         font            =   "penjualan_karcis.frx":1023
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   15790320
         bcolo           =   15790320
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":104F
         picn            =   "penjualan_karcis.frx":106D
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_nav 
         Height          =   375
         Index           =   1
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Width           =   975
         _extentx        =   1720
         _extenty        =   661
         btype           =   2
         tx              =   "Prev"
         enab            =   0   'False
         font            =   "penjualan_karcis.frx":11A5
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   15790320
         bcolo           =   15790320
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "penjualan_karcis.frx":11D1
         picn            =   "penjualan_karcis.frx":11EF
         umcol           =   0   'False
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
   End
   Begin VB.TextBox text_cari 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   50
      TabIndex        =   9
      Top             =   1800
      Width           =   2655
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3495
      Left            =   9240
      TabIndex        =   8
      Top             =   2280
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   6165
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   10
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "KODE"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "NAMA"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "CUR"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "QTY"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "HARGA"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "TOTAL"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "FEEPERSEN"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "FEENOMINAL"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "HJN"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000010&
      Height          =   7020
      Left            =   50
      ScaleHeight     =   6960
      ScaleWidth      =   9075
      TabIndex        =   1
      Top             =   2280
      Width           =   9135
      Begin VB.VScrollBar VScroll1 
         Height          =   6975
         LargeChange     =   10
         Left            =   8830
         Max             =   0
         MousePointer    =   99  'Custom
         TabIndex        =   5
         Top             =   0
         Width           =   255
      End
      Begin VB.PictureBox picScroll 
         Appearance      =   0  'Flat
         BackColor       =   &H80000010&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   6975
         Left            =   0
         ScaleHeight     =   6975
         ScaleWidth      =   8895
         TabIndex        =   6
         Top             =   0
         Width           =   8895
         Begin MySIS.ItemBox ItemBox1 
            Height          =   3015
            Index           =   0
            Left            =   50
            TabIndex        =   7
            Top             =   50
            Width           =   2505
            _extentx        =   4419
            _extenty        =   5318
            picture         =   "penjualan_karcis.frx":1327
            image           =   "penjualan_karcis.frx":1345
         End
      End
   End
   Begin VB.PictureBox PictureBayar 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H80000008&
      Height          =   5175
      Left            =   14640
      ScaleHeight     =   5145
      ScaleWidth      =   5865
      TabIndex        =   48
      Top             =   2280
      Visible         =   0   'False
      Width           =   5895
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         FillColor       =   &H00FFFF80&
         ForeColor       =   &H80000008&
         Height          =   4695
         Left            =   0
         ScaleHeight     =   4695
         ScaleWidth      =   5895
         TabIndex        =   62
         Top             =   360
         Width           =   5895
         Begin VB.CheckBox checkdeposit 
            BackColor       =   &H00FFFFC0&
            Height          =   255
            Left            =   1440
            TabIndex        =   95
            Top             =   1200
            Width           =   255
         End
         Begin VB.CheckBox CheckBank 
            BackColor       =   &H00FFFFC0&
            Height          =   195
            Left            =   1830
            TabIndex        =   84
            Top             =   2550
            Value           =   1  'Checked
            Width           =   240
         End
         Begin VB.TextBox TextTotalIDR 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5560
            TabIndex        =   83
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1620
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.TextBox TextCash 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            TabIndex        =   56
            Text            =   "0.00"
            Top             =   2040
            Width           =   2700
         End
         Begin VB.TextBox TextCharge1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            TabIndex        =   58
            Text            =   "0.00"
            Top             =   2895
            Width           =   800
         End
         Begin VB.TextBox TextCharge2 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3720
            Locked          =   -1  'True
            TabIndex        =   59
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   2895
            Width           =   1860
         End
         Begin VB.ComboBox cmb_bank 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "penjualan_karcis.frx":1363
            Left            =   2880
            List            =   "penjualan_karcis.frx":1365
            Style           =   2  'Dropdown List
            TabIndex        =   57
            Top             =   2475
            Width           =   2700
         End
         Begin VB.TextBox TextDp 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            Locked          =   -1  'True
            TabIndex        =   53
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1200
            Width           =   2700
         End
         Begin VB.TextBox TextDisc2 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3720
            TabIndex        =   51
            Text            =   "0.00"
            Top             =   120
            Width           =   1860
         End
         Begin VB.TextBox TextNetto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            Locked          =   -1  'True
            TabIndex        =   52
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   540
            Width           =   2700
         End
         Begin VB.TextBox TextBalance 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            Locked          =   -1  'True
            TabIndex        =   60
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   3585
            Width           =   2700
         End
         Begin VB.ComboBox cmb_matauang 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   2880
            Style           =   2  'Dropdown List
            TabIndex        =   54
            Top             =   1650
            Width           =   810
         End
         Begin VB.TextBox TextCurrencyConvert 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3720
            Locked          =   -1  'True
            TabIndex        =   55
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1620
            Width           =   1860
         End
         Begin VB.TextBox TextDisc 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2880
            TabIndex        =   50
            Text            =   "0.00"
            Top             =   120
            Width           =   800
         End
         Begin VB.CheckBox Check_pajak 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Ctrl + P"
            Height          =   195
            Left            =   960
            TabIndex        =   63
            Top             =   4320
            Value           =   1  'Checked
            Visible         =   0   'False
            Width           =   855
         End
         Begin MySIS.Button cmd_tutupbayar 
            Height          =   435
            Left            =   2880
            TabIndex        =   64
            Top             =   4200
            Width           =   1215
            _extentx        =   2143
            _extenty        =   767
            btype           =   3
            tx              =   "&Tutup"
            enab            =   -1  'True
            font            =   "penjualan_karcis.frx":1367
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   15790320
            bcolo           =   15790320
            fcol            =   0
            fcolo           =   0
            mcol            =   12632256
            mptr            =   1
            micon           =   "penjualan_karcis.frx":1393
            umcol           =   0   'False
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin MySIS.Button cmd_simpanbayar 
            Height          =   435
            Left            =   4245
            TabIndex        =   61
            Top             =   4200
            Width           =   1335
            _extentx        =   2355
            _extenty        =   767
            btype           =   3
            tx              =   "&Simpan"
            enab            =   -1  'True
            font            =   "penjualan_karcis.frx":13B1
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   14737632
            bcolo           =   14737632
            fcol            =   0
            fcolo           =   0
            mcol            =   12632256
            mptr            =   1
            micon           =   "penjualan_karcis.frx":13DD
            umcol           =   0   'False
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CASH"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   4
            Left            =   105
            TabIndex        =   74
            Top             =   2070
            Width           =   2685
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CHARGE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   6
            Left            =   105
            TabIndex        =   73
            Top             =   2910
            Width           =   2685
         End
         Begin VB.Line Line3 
            BorderColor     =   &H80000010&
            X1              =   5640
            X2              =   360
            Y1              =   3420
            Y2              =   3420
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000010&
            X1              =   5640
            X2              =   360
            Y1              =   1005
            Y2              =   1005
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "BANK"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   5
            Left            =   105
            TabIndex        =   72
            Top             =   2520
            Width           =   2685
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "NETTO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   1
            Left            =   105
            TabIndex        =   71
            Top             =   600
            Width           =   2685
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "BALANCE DUE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   7
            Left            =   105
            TabIndex        =   70
            Top             =   3600
            Width           =   2685
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "DEPOSIT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   2
            Left            =   1680
            TabIndex        =   67
            Top             =   1200
            Width           =   1110
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CURRENCY"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   3
            Left            =   105
            TabIndex        =   66
            Top             =   1665
            Width           =   2685
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "DISCOUNT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   105
            TabIndex        =   65
            Top             =   180
            Width           =   2685
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000010&
            X1              =   5640
            X2              =   360
            Y1              =   4080
            Y2              =   4080
         End
      End
      Begin VB.Image Image2 
         Height          =   300
         Left            =   5190
         Picture         =   "penjualan_karcis.frx":13FB
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         Caption         =   "Payment"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   49
         Top             =   0
         Width           =   9880
      End
   End
End
Attribute VB_Name = "penjualan_karcis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim atas As Double
Dim kiri As Double
Dim margin As Double

Dim pageSize As Integer
Dim pageCount As Integer
Dim pageIndex As Integer
Dim recordCount As Integer

Dim matauang As New Collection
Dim bank As New Collection
Dim edit As Boolean

Function get_nilaitukar(curr As String) As Double
    Dim kurs As PropertyField
    For Each kurs In matauang
        If UCase(kurs.Key) = UCase(curr) Then
            get_nilaitukar = Val(kurs.Value)
            Exit For
        End If
    Next kurs
End Function

Private Sub hitung_total()
    Dim usd, idr, kurs_usd As Double
    
    Dim item As ListItem
    For Each item In ListView1.ListItems
        If UCase(item.SubItems(3)) = "USD" Then
            usd = usd + Val(Format(item.SubItems(6), "##0.00"))
        Else
            usd = usd + Val(Format(item.SubItems(6), "##0.00") / get_nilaitukar("USD"))
        End If
    Next item
    
    usd = usd * 1
    idr = Val(usd) * get_nilaitukar("USD")
    total_idr.Caption = Format(idr, "#,#0.00")
    total_usd.Caption = Format(usd, "#,#0.00")
End Sub

Sub hitung_bayar()
    Dim kurs As PropertyField
    Set kurs = matauang(cmb_matauang.ListIndex + 1)
    Dim bruto, netto, disc, dp, cash, convert, balance, kembalian As Double
    
    bruto = Val(Format(total_idr.Caption, "##0.00"))
    disc = Val(Format(TextDisc2.Text, "##0.00"))
    netto = bruto - disc
    TextNetto.Text = Format(netto, "#,##0.00")
    
    If checkdeposit.Value = 1 Then
        dp = Val(Format(TextDp.Text, "##0.00"))
        If (netto - dp) > 0 Then
            convert = (netto - dp) / Val(kurs.Value)
            TextCurrencyConvert.Text = Format(convert, "#,##0.00")
            CheckBank.Enabled = True
            cash = Val(Format(TextCash.Text, "##0.00"))
            kembalian = cash - convert
            TextBalance.Text = Format(kembalian, "#,##0.00")
        Else
            TextCurrencyConvert.Text = "0.00"
            CheckBank.Value = 0
            convert = 0
            CheckBank.Enabled = False
            TextCash.Text = "0"
            cash = Val(Format(TextCash.Text, "##0.00"))
            TextBalance.Text = "0.00"
        End If
    Else
        dp = 0
        convert = (netto) / Val(kurs.Value)
        TextCurrencyConvert.Text = Format(convert, "#,##0.00")
        cash = Val(Format(TextCash.Text, "##0.00"))
        kembalian = cash - convert
        CheckBank.Enabled = True
        TextBalance.Text = Format(kembalian, "#,##0.00")
    End If
    
    If CheckBank.Value = 1 Then
        balance = (convert - cash) * Val(kurs.Value)
        balance = IIf(balance > 0, balance, 0)
        TextCharge2.Text = Format((Val(Format(TextCharge1.Text, "##0.00")) / 100 + 1) * balance, "#,##0.00")
        TextBalance.Text = "0.00"
    End If
    TextTotalIDR.Text = Format(totalIDR, "#,##0.00")
    
End Sub

Sub show_bayar()

    Picture1.Enabled = False
    Picture2.Enabled = False
    Picture4.Enabled = False
    Picture5.Enabled = False
    Picture6.Enabled = False
    TextCash.Text = "0"
    CheckBank.Value = 0
    checkdeposit.Value = 0
    cmb_matauang.ListIndex = 0
    cmb_bank.ListIndex = 0
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from magen where kdag='" & Text1(4).Text & "' limit 1", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        TextDp.Text = "" & Format(rs1.Fields!masuk - rs1.Fields!keluar, "#,##0.00")
    Else
        TextDp.Text = "0"
    End If
    
    hitung_bayar
        
    PictureBayar.Top = Picture5.Top + Picture5.Height + 50
    PictureBayar.Visible = True
    PictureBayar.ZOrder 0
    TextDisc.SetFocus
End Sub

Sub tutup_bayar()
    Picture1.Enabled = True
    Picture2.Enabled = True
    Picture4.Enabled = True
    Picture5.Enabled = True
    Picture6.Enabled = True
    
    PictureBayar.Visible = False
End Sub

Public Sub tambah_item(ByVal item As ItemBox)
    
    j = ListView1.ListItems.Count + 1
    Set List = ListView1.ListItems.Add(, , j)
    List.Text = j
    List.SubItems(1) = "" & item.kode
    List.SubItems(2) = "" & item.nama
    List.SubItems(3) = "" & item.matauang
    List.SubItems(4) = "" & item.Jumlah
    List.SubItems(9) = "" & Format(item.Harga, "###0.00")
    List.SubItems(5) = "" & Format(item.Harga, "#,##0.00")
    List.SubItems(6) = "" & Format(item.Harga * item.Jumlah, "#,##0.00")
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from paket_agen where kdag='" & Text1(4).Text & "' and bara='" & item.kode & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        List.SubItems(5) = "" & Format(rs1.Fields!Harga, "#,##0.00")
        List.SubItems(6) = "" & Format(rs1.Fields!Harga * item.Jumlah, "#,##0.00")
        'item.Harga = rs1.Fields!Harga
    End If
    List.SubItems(7) = "" & item.FeeNominal
    List.SubItems(8) = "" & item.FeePersen
    
    hitung_total
End Sub

Private Sub tampil_item()
    Dim i As Integer
    Dim row As Integer
    Dim tipe As String
            
    cmd_nav(0).Enabled = False
    cmd_nav(1).Enabled = False
    cmd_cari.Enabled = False
    Picture6.Enabled = False
    
    If OptionTipe(1).Value = True Then
        tipe = "(inv='Barang' or inv='Rakitan')"
    ElseIf OptionTipe(2).Value = True Then
        tipe = "(inv='Konsinyasi')"
    Else
        tipe = "(inv='Jasa')"
    End If
    
    If rs1.State <> 0 Then rs1.Close
    csql = "select count(m.bara) from mstock m join mstock_bo s on m.bara=s.bara where " & tipe & " and s.bo='" & cmb_cabang.Text & "' and (m.bara like '%" & text_cari.Text & "%' or nama like '%" & text_cari.Text & "%')"
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        recordCount = Val(rs1.Fields(0))
        If recordCount > 0 Then
            On Error Resume Next
            pageCount = Int(recordCount / pageSize) + 1
            pageCount = IIf(pageCount = 0, 1, pageCount)
            pageIndex = IIf(pageIndex > pageCount, 1, pageIndex)
        End If
    End If
    
    csql = "select m.*,hjual,currency,feepersen,feenominal,picture from mstock m "
    csql = csql & "join mstock_bo s on m.bara=s.bara "
    csql = csql & "left join zfoto_barang f on m.bara=f.bara where s.bo='" & cmb_cabang.Text & "' and " & tipe
    csql = csql & "and (m.bara like '%" & text_cari.Text & "%' or m.nama like '%" & text_cari.Text & "%') "
    csql = csql & "order by nama asc limit " & Val(pageSize * (pageIndex - 1)) & "," & pageSize
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    UnloadItem i
    ItemBox1(0).Visible = False
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            If i > 0 Then
                If Not HasControl(ItemBox1, i) Then Load ItemBox1(i)
                
                atas = ItemBox1(i - 1).Top + ItemBox1(0).Height + 100
                kiri = ItemBox1(i - 1).Left + ItemBox1(0).Width + 100
                margin = picScroll.Width - ItemBox1(0).Width
                row = row + IIf(kiri >= margin, 1, 0)
    
                ItemBox1(i).Top = IIf(kiri >= margin, atas, ItemBox1(i - 1).Top)
                ItemBox1(i).Left = IIf(kiri >= margin, ItemBox1(0).Left, kiri)
            End If
            
            ItemBox1(i).Visible = True
            ItemBox1(i).kode = "" & rs1.Fields!bara
            ItemBox1(i).nama = "" & rs1.Fields!nama
            ItemBox1(i).keterangan = "" & rs1.Fields!keterangan
            ItemBox1(i).matauang = "" & UCase(rs1.Fields!Currency)
            ItemBox1(i).Harga = "" & Format(rs1.Fields!hjual, "#,##0.00")
            ItemBox1(i).FeeNominal = "" & rs1.Fields!FeeNominal
            ItemBox1(i).FeePersen = "" & rs1.Fields!FeePersen
            ItemBox1(i).Jumlah = 1
            
            Call FillPhoto(rs1, "picture", ItemBox1(i).Image)
            
            i = i + 1
            rs1.MoveNext
        Loop
    End If
            
    cmd_nav(0).Enabled = pageIndex < pageCount
    cmd_nav(1).Enabled = pageIndex > 1
    cmd_cari.Enabled = True
    Picture6.Enabled = True
    
    'adjust the height of the scroll picture box
    picScroll.Height = (ItemBox1.UBound * ItemBox1(0).Height) + (ItemBox1.UBound * 100)
    picScroll.Height = IIf(picScroll.Height < Picture1.Height, Picture1.Height, picScroll.Height)
    
    'maximum scroll value
    VScroll1.Max = row
End Sub

Function get_agen(kode As String) As Boolean
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from magen where kdag='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(4).Text = "" & rs1!kdag
        Text1(5).Text = "" & rs1!namaag
        TextDp.Text = Format(rs1.Fields!masuk - rs1.Fields!keluar, "#,##0.00")
        get_agen = True
    Else
        get_agen = False
    End If
End Function

Sub default_agen()
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msetting where kd='AGU' limit 1", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Text1(6).Text = "" & rs1.Fields!nilai
        get_agen "" & rs1.Fields!nilai
    End If
End Sub

Function get_sales(kode As String) As Boolean
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msales where kdsl='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(2).Text = "" & rs1!kdsl
        Text1(3).Text = "" & rs1!namasl
        TextFee.Text = "" & rs1!persenfee
        TextNfee.Text = "" & rs1!nominalfee
        TextTipeFee.Text = "" & rs1!jenisfee
        get_sales = True
    Else
        get_sales = False
    End If
End Function

Sub default_sales()
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msetting where kd='SLU' limit 1", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        get_sales "" & rs1.Fields!nilai
    End If
End Sub

Sub get_pendingan(nota As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from pending_head where no_pd='" & nota & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        DTPicker1.Value = rs1!tgl
        Text1(1).Text = "" & rs1!no_kunj
        Text1(2).Text = "" & rs1!kdsl
        Text1(3).Text = "" & rs1!namasl
        Text1(4).Text = "" & rs1!kdag
        Text1(5).Text = "" & rs1!namaag
        
        ListView1.ListItems.Clear
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from pending_det where no_pd='" & nota & "'", con, adOpenKeyset, adLockOptimistic
        
        j = 0
        If Not rs1.EOF Then
            Do While Not rs1.EOF
                j = ListView1.ListItems.Count + 1
                Set item = ListView1.ListItems.Add(, , j)
                
                item.Text = j
                item.SubItems(1) = "" & rs1!bara
                item.SubItems(2) = "" & rs1!nama
                item.SubItems(3) = "" & rs1!Currency
                item.SubItems(4) = rs1!qty
                item.SubItems(5) = Format(rs1!Harga, "#,##0.00")
                item.SubItems(6) = Format(rs1!netto, "#,##0.00")
                item.SubItems(7) = Format(rs1!fee, "#,##0.00")
                item.SubItems(8) = Format(rs1!FEEN, "#,##0.00")
                item.SubItems(9) = Format(rs1!HJN, "#,##0.00")
                rs1.MoveNext
            Loop
        End If
        
        ListView2.ListItems.Clear
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from pending_cust where no_pd='" & nota & "'", con, adOpenKeyset, adLockOptimistic
        
        j = 0
        If Not rs1.EOF Then
            Do While Not rs1.EOF
                j = ListView2.ListItems.Count + 1
                Set item = ListView2.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs1!nama
                item.SubItems(3) = "" & rs1!sosmed
                rs1.MoveNext
            Loop
        End If
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "update pending_head set status=1 where no_pd='" & nota & "'", con, adOpenKeyset, adLockOptimistic
        
        get_sales Text1(2).Text
        get_agen Text1(4).Text
        
        hitung_total
        otomatis
    Else
        MsgBox ("Nota Pendingan tidak ada")
    End If
End Sub

Private Sub kosong()
    For z = 0 To Text1.Count - 1
        Text1(z).Text = ""
    Next z
    cmd_batal.Caption = "Ulang"
    ListView1.ListItems.Clear
    ListView2.ListItems.Clear
    
    TextDisc.Text = "0.00"
    TextDp.Text = "0.00"
    TextCash.Text = "0.00"
    
    hitung_total
    edit = False
End Sub

Sub get_matauang()
    On Error GoTo exc
    Set matauang = New Collection
    
    cmb_matauang.Clear
    cmb_matauang.AddItem UCase("IDR")
    
    Dim kurs As New PropertyField
    kurs.Key = "IDR"
    kurs.Value = 1
    matauang.Add kurs
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from kurs", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            cmb_matauang.AddItem UCase(rs1.Fields!Currency)
            
            Set kurs = New PropertyField
            kurs.Key = rs1.Fields!Currency
            kurs.Value = rs1.Fields!nilai
            
            matauang.Add kurs
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_bank()
    Set bank = New Collection
    Dim akun As New PropertyField
    
    cmb_bank.Clear
    cmb_bank.AddItem ""
    bank.Add akun
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where kas=1 order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            cmb_bank.AddItem rs1.Fields!nama
            
            Set akun = New PropertyField
            akun.Key = rs1.Fields!kode
            akun.Value = rs1.Fields!nama
            
            bank.Add akun
            rs1.MoveNext
        Loop
    End If
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Penjualan'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = cmb_cabang.Text
        
        kol3 = rs1.Fields!kol3
        If Check_pajak.Value = 1 Then kol3 = "PJ"
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = cmb_cabang.Text
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = cmb_cabang.Text
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = cmb_cabang.Text
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(NO_EJ) as nota from jual_head where length(NO_EJ)=" & j + z & " and right(NO_EJ," & z & ")='" & nomor_nota & "' and left(NO_EJ," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(NO_EJ) as nota from jual_head where length(NO_EJ)=" & j + z & " and left(NO_EJ," & z & ")='" & nomor_nota & "' and right(NO_EJ," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
        If rs1.State <> 0 Then rs1.Close
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub ButtonCloseStruk_Click()
    PictureStruk.Visible = False
End Sub

Private Sub Check_pajak_Click()
    otomatis
End Sub

Private Sub CheckBank_Click()
    If CheckBank.Value = 0 Then
        TextCharge1.Text = "0.00"
        TextCharge2.Text = "0.00"
        TextCharge1.Enabled = False
        cmb_bank.Enabled = False
        cmb_bank.ListIndex = 0
    Else
        TextCharge1.Enabled = True
        cmb_bank.Enabled = True
        On Error Resume Next
        cmb_bank.ListIndex = 1
    End If
    hitung_bayar
End Sub

Private Sub checkdeposit_Click()
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from magen where kdag='" & Text1(4).Text & "' limit 1", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        TextDp.Text = "" & Format(rs1.Fields!masuk - rs1.Fields!keluar, "#,##0.00")
    Else
        TextDp.Text = "0"
    End If
    hitung_bayar
End Sub

Private Sub cmb_cabang_Change()
    otomatis
    tampil_item
    get_harga_agen
End Sub

Private Sub cmb_cabang_Click()
    otomatis
    tampil_item
    get_harga_agen
End Sub

Private Sub cmb_matauang_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    On Error Resume Next
    TextCash.SetFocus
End If
End Sub

Private Sub cmd_agen_Click()
    cari_agen.Left = Text1(4).Left + 100
    cari_agen.Top = Text1(4).Top + Text1(4).Height + 150
    Set cari_agen.FormPemanggil = Me
    cari_agen.Cari = ""
    cari_agen.Show 1
End Sub

Private Sub cmd_batal_Click()
If cmd_batal.Caption = "Batal" Then
    pesan = MsgBox("Batalkan dan hapus transaksi " & Text1(0).Text & "?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "DELETE FROM jual_head WHERE NO_EJ='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        kosong
        daftar_pjlkasir.tampil_pjl
        Unload Me
    End If
Else
    pesan = MsgBox("Transaksi akan diulang?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        kosong
        default_sales
        default_agen
        otomatis
    End If
End If
End Sub

Private Sub cmd_cari_Click()
    pageIndex = 1
    tampil_item
End Sub

Private Sub cmd_cetak_Click()
    cetak_usb
End Sub

Private Sub cmd_customer_Click()
    If Text2(2).Text <> "" Then
        j = ListView2.ListItems.Count + 1
        Set item = ListView2.ListItems.Add(, , j)
        item.Text = j
        item.SubItems(1) = j
        item.SubItems(2) = Text2(1).Text
        item.SubItems(3) = Text2(2).Text
            
        Text2(1).Text = ""
        Text2(2).Text = ""
    End If
    Text2(1).SetFocus
End Sub

Private Sub cmd_dftpending_Click()
    Set daftar_pending.FormPemanggil = Me
    daftar_pending.Show 1
End Sub

Private Sub cmd_nav_Click(Index As Integer)
    If Index = 0 Then
        If pageIndex < pageCount Then
            pageIndex = pageIndex + 1
            tampil_item
        End If
    ElseIf Index = 1 Then
        If pageIndex > 1 Then
            pageIndex = pageIndex - 1
            tampil_item
        End If
    End If
End Sub

Private Sub cmd_sales_Click()
    cari_sales.Left = Text1(2).Left + 100
    cari_sales.Top = Text1(2).Top + Text1(2).Height + 150
    Set cari_sales.FormPemanggil = Me
    cari_sales.Cari = ""
    cari_sales.Show 1
End Sub

Private Sub cmd_showbayar_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "Detail item masih kosong"
        Exit Sub
    End If
    
    If Text1(1).Text <> "" Then
        show_bayar
    Else
        MsgBox "No Kunjungan belum di isi"
    End If
End Sub

Private Sub cmd_pending_Click()
    On Error GoTo err
    Dim hari, NO, nopend As String
    
    If ListView1.ListItems.Count = 0 Then
        MsgBox "Detail item masih kosong"
        Exit Sub
    End If
    
    If Text1(1).Text <> "" Then
        hari = "PD-" & Format(Date, "yymmdd")
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "SELECT MAX(NO_PD) FROM pending_head WHERE LEFT(NO_PD, 9) = '" & hari & "'", con, adOpenKeyset, adLockOptimistic
        
        If rs1.EOF Then
            nopend = hari + "001"
        Else
            rs1.MoveLast
            If Mid(Trim(rs1.Fields(0)), 10, 3) Then
                 NO = Right(Trim(rs1.Fields(0)), 3)
                 NO = Val(NO) + 1
                 NO = Trim(Str(NO))
                 NO = Left("000", 3 - Len(NO)) + NO
                 nopend = hari + NO
            Else
                 nopend = hari + "001"
            End If
        End If
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "DELETE FROM pending_head WHERE NO_PD='" & nopend & "'", con, adOpenKeyset, adLockOptimistic
        
        csql = "INSERT INTO pending_head (NO_PD,TGL,JAM,NO_KUNJ,KDSL,NAMASL,KDAG,NAMAAG,NETTO,BO,ID) VALUES ('" & _
                nopend & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "',DATE_FORMAT(now(),'%H%i%s'),'" & _
                Text1(1).Text & "','" & Text1(2).Text & "','" & Text1(3).Text & "','" & Text1(4).Text & "','" & _
                Text1(5).Text & "'," & Val(Format(total_idr.Caption, "##0.00")) & ",'" & cmb_cabang.Text & "','" & xy & "')"
        
'        Debug.Print csql
        If rs1.State <> 0 Then rs1.Close
        rs1.Open csql, con, adOpenKeyset, adLockOptimistic
            
        Dim item As ListItem
        For Each item In ListView1.ListItems
            csql = "INSERT INTO pending_det(NO_PD,TGL,JAM,BARA,NAMA,CURRENCY,QTY,HARGA,NETTO,FEE,FEEN,HJN) " & _
                "VALUES('" & nopend & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "',DATE_FORMAT(now(),'%H%i%s'),'" & _
                item.SubItems(1) & "','" & item.SubItems(2) & "','" & item.SubItems(3) & "'," & Format(item.SubItems(4), "##0.00") & "," & _
                Format(item.SubItems(5), "##0.00") & "," & Format(item.SubItems(6), "##0.00") & "," & Format(item.SubItems(7), "##0.00") & "," & Format(item.SubItems(8), "##0.00") & "," & Format(item.SubItems(9), "##0.00") & ")"
'            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next item
        
        For Each item In ListView2.ListItems
            csql = "INSERT INTO pending_cust(NO_PD,NAMA,SOSMED) " & _
                "VALUES('" & nopend & "','" & item.SubItems(2) & "','" & item.SubItems(3) & "')"
                
'            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next item
        
        MsgBox "Pending penjualan sudah disimpan"
        kosong
        default_sales
        default_agen
        otomatis
    Else
        MsgBox "Lengkapi data terlebih dulu"
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub cmd_simpanbayar_Click()
On Error GoTo err
If Val(Format(TextBalance.Text, "##0.00")) < 0 Then
    MsgBox "Isi Cash/Jumlah Pembayaran"
    Exit Sub
End If
If ListView1.ListItems.Count = 0 Then
    MsgBox "Detail item masih kosong"
    Exit Sub
End If
If Text1(1).Text <> "" Then
pesan = MsgBox("Data akan disimpan?", vbQuestion + vbYesNo, "Konfirmasi")
If pesan = vbYes Then
        Dim akun As PropertyField
        Set akun = bank(cmb_bank.ListIndex + 1)
        
        If cmd_batal.Caption = "Batal" Then
            If rs2.State <> 0 Then rs2.Close
            rs2.Open "DELETE FROM jual_head WHERE NO_EJ='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        Else
            otomatis
        End If
        
        Dim fee, qty, total, dpt As Double
        fee = 0
        i = ListView2.ListItems.Count
        If i = 0 Then i = 1
        If TextTipeFee.Text = "2" And UCase(Text1(4).Text) <> UCase(Text1(6).Text) Then
            total = Val(Format(total_idr.Caption, "##0.00"))
            fee = ((Val(TextFee.Text) / 100) * total) + Val(TextNfee.Text)
        End If
        dpt = 0
        If checkdeposit.Value = 1 Then
            If Val(Format(TextNetto.Text, "##0.00")) < Val(Format(TextDp.Text, "##0.00")) Then
                dpt = Val(Format(TextNetto.Text, "##0.00"))
            Else
                dpt = Val(Format(TextDp.Text, "##0.00"))
            End If
        End If
        csql = "INSERT INTO jual_head (NO_EJ,TGL,JAM,NO_KUNJ,KDSL,NAMASL,KDAG,NAMAAG,BRUTO,DISC,NETTO,DP,CASH,BANK,CARD,NCARD,CURRENCY,NCURRENCY,NFEE,BO,ID,TXT,CUST) VALUES ('" & _
                Text1(0).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "',DATE_FORMAT(now(),'%H%i%s'),'" & Text1(1).Text & "','" & Text1(2).Text & "','" & _
                Text1(3).Text & "','" & Text1(4).Text & "','" & Text1(5).Text & "'," & Val(Format(total_idr.Caption, "##0.00")) & "," & _
                Val(Format(TextDisc2.Text, "##0.00")) & "," & Val(Format(TextNetto.Text, "##0.00")) & "," & dpt & _
                "," & Val(Format(TextCash.Text, "##0.00")) & ",'" & akun.Key & "'," & Val(Format(TextCharge1.Text, "##0.00")) & "," & Val(Format(TextCharge2.Text, "##0.00")) & _
                ",'" & cmb_matauang.Text & "'," & Val(Format(TextCurrencyConvert.Text, "##0.00")) & "," & Val(fee) & ",'" & cmb_cabang.Text & "','" & xy & "','" & Check_pajak.Value & "'," & i & ")"
        
'        Debug.Print csql
        If rs1.State <> 0 Then rs1.Close
        rs1.Open csql, con, adOpenKeyset, adLockOptimistic
            
        Dim item As ListItem
        Dim kurs As PropertyField
        Set kurs = matauang(cmb_matauang.ListIndex + 1)
        
        For Each item In ListView1.ListItems
            qty = Val(Format(item.SubItems(4), "##0.00"))
            total = Val(Format(item.SubItems(6), "##0.00"))
            If item.SubItems(3) <> "IDR" And Not IsEmpty(item.SubItems(3)) Then
                total = Val(Format(item.SubItems(6), "##0.00")) * get_nilaitukar(item.SubItems(3))
            End If
            fee = 0
            If TextTipeFee.Text = "1" And UCase(Text1(4).Text) <> UCase(Text1(6).Text) Then
                If item.SubItems(3) <> "IDR" Then
                    fee = (qty * get_nilaitukar(Val(Format(item.SubItems(7), "##0.00")))) + ((Val(Format(item.SubItems(8), "##0.00")) / 100) * total)
                Else
                    fee = (qty * Val(Format(item.SubItems(7), "##0.00"))) + ((Val(Format(item.SubItems(8), "##0.00")) / 100) * total)
                End If
            ElseIf TextTipeFee.Text = "2" And UCase(Text1(4).Text) <> UCase(Text1(6).Text) Then
                'fee = ((Val(TextFee.Text) / 100) * total) + Val(TextNfee.Text)
                fee = ((Val(TextFee.Text) / 100) * total)
            End If
            
            csql = "INSERT INTO jual_det(NO_EJ,TGL,JAM,BARA,NAMA,CURRENCY,QTY,HARGA,BRUTO,NETTO,FEE,BO) " & _
                "VALUES('" & Text1(0).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "',DATE_FORMAT(now(),'%H%i%s'),'" & _
                item.SubItems(1) & "','" & item.SubItems(2) & "','" & item.SubItems(3) & "'," & qty & "," & _
                Format(item.SubItems(5), "##0.00") & "," & total & "," & total & "," & fee & ",'" & cmb_cabang.Text & "')"
                
'            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next item
        
        If TextTipeFee.Text = "1" Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "UPDATE jual_head set nfee=(select SUM(fee)fee from jual_det WHERE no_ej='" & Text1(0).Text & "') WHERE no_ej='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        End If
        
        For Each item In ListView2.ListItems
            csql = "INSERT INTO jual_cust(NO_EJ,NAMA,SOSMED) " & _
                "VALUES('" & Text1(0).Text & "','" & item.SubItems(2) & "','" & item.SubItems(3) & "')"
                
'            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next item
        
        MsgBox "Penjualan " & Text1(0).Text & " sudah disimpan"
        
        PictureStruk.Visible = True
        cetak_struk
        
        If cmd_batal.Caption = "Batal" Then
            daftar_pjlkasir.tampil_pjl
            pesan = MsgBox("Cetak Struk?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                cetak_usb
            End If
            Unload Me
        Else
            otomatis
        End If
        kosong
        default_sales
        default_agen
        hitung_bayar
        tutup_bayar
End If
Else
    MsgBox "No Kunjungan belum di isi"
End If
Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Sub edit_penjualan(noej As String)
    On Error GoTo err
        
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from jual_cust where no_ej='" & noej & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        j = 0
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView2.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = "" & rs1.Fields!nama
            item.SubItems(3) = "" & rs1.Fields!sosmed
            rs1.MoveNext
        Loop
    End If
    
    If rs1.State <> 0 Then rs1.Close
    csql = "select d.*,feenominal,feepersen from jual_det d join mstock_bo m on d.bara=m.bara and d.bo=m.bo "
    csql = csql & "where no_ej='" & noej & "'"
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        j = 0
        Do While Not rs1.EOF
            j = j + 1
            Set List = ListView1.ListItems.Add(, , j)
            List.Text = j
            List.SubItems(1) = "" & rs1.Fields!bara
            List.SubItems(2) = "" & rs1.Fields!nama
            List.SubItems(3) = "" & rs1.Fields!Currency
            List.SubItems(4) = "" & rs1.Fields!qty
            List.SubItems(5) = "" & Format(rs1.Fields!Harga, "#,##0.00")
            If rs1.Fields!Currency = "IDR" Then
                List.SubItems(6) = "" & Format(rs1.Fields!netto, "#,##0.00")
            Else
                List.SubItems(6) = "" & Format(rs1.Fields!Harga * rs1.Fields!qty, "#,##0.00")
            End If
            List.SubItems(7) = "" & rs1.Fields!FeeNominal
            List.SubItems(8) = "" & rs1.Fields!FeePersen
            List.SubItems(9) = "" & Format(rs1.Fields!Harga, "#,##0.00")
            rs1.MoveNext
        Loop
    End If
    If rs2.State <> 0 Then rs1.Close
    rs2.Open "select * from jual_head where no_ej='" & noej & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs2.EOF Then
        Text1(1).Text = "" & rs2.Fields!no_kunj
        Text1(2).Text = "" & rs2.Fields!kdsl
        Text1(3).Text = "" & rs2.Fields!namasl
        Text1(4).Text = "" & rs2.Fields!kdag
        Text1(5).Text = "" & rs2.Fields!namaag

        cmb_cabang.Text = "" & rs2.Fields!BO
        DTPicker1.Value = "" & rs2.Fields!tgl
        TextCash.Text = Format(rs2.Fields!cash, "#,##0.00")
        Text1(0).Text = "" & rs2.Fields!no_ej
        cmd_batal.Caption = "Batal"
    End If
    
    hitung_total
    edit = True
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Sub cetak_struk()
    Dim tgh As String
    Dim baris, qty As Integer
    Dim isi As String
    
    'On Error Resume Next
    tgh = Format(Date, "ddmmyy")
    
    ListViewStruk.ListItems.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo where bo='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
    
    ListViewStruk.ListItems.Add(1, , "").SubItems(1) = "" & rs1.Fields!cabang
    If Len(Trim(rs1.Fields!alamat)) > 1 Then
        ListViewStruk.ListItems.Add(2, , "").SubItems(1) = "" & rs1.Fields!alamat
    End If
    ListViewStruk.ListItems.Add(3, , "").SubItems(1) = Now() & Space(1) & xy
    ListViewStruk.ListItems.Add(4, , "").SubItems(1) = "----------------------------------------"
    ListViewStruk.ListItems.Add(5, , "").SubItems(1) = ""
    qty = 0
    baris = ListViewStruk.ListItems.Count
    
    For z = 1 To ListView1.ListItems.Count
        ListViewStruk.ListItems.Add(baris + z, , "").SubItems(1) = UCase(ListView1.ListItems(z).SubItems(2))
        isi = Space(2) & ListView1.ListItems(z).SubItems(4) & " x " & ListView1.ListItems(z).SubItems(5)
        isi = isi & Space(15) & "=" & Space(13 - Len(ListView1.ListItems(z).SubItems(6))) & ListView1.ListItems(z).SubItems(6)
        ListViewStruk.ListItems.Add(baris + z + 1, , "").SubItems(1) = isi
        qty = qty + Format(ListView1.ListItems(z).SubItems(4), "########0")
        baris = baris + 1
    Next z
            
    baris = ListViewStruk.ListItems.Count
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(29) & "-----------"
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(18) & " Total" & Space(2) & "=" & Space(13 - Len(Trim(TextNetto.Text))) & "" & Trim(TextNetto.Text)
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(18) & " Qty" & Space(4) & "=" & Space(13 - Len(Format(qty, "###,##0"))) & "" & Format(qty, "###,##0")
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(29) & "-----------"
    If checkdeposit.Value = 1 Then
        baris = baris + 1
        ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(18) & "Deposit " & "=" & Space(13 - Len(Trim(TextDp.Text))) & "" & Trim(TextDp.Text)
    End If

    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(18) & " Cash" & Space(3) & "=" & Space(13 - Len(Trim(TextCash.Text))) & "" & Trim(TextCash.Text)
    If CheckBank.Value = 1 Then
        baris = baris + 1
        ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = "TIPE   : " & cmb_bank.Text
        baris = baris + 1
        ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = "CHARGE : " & TextCharge1.Text & "%" & Space(12) & "=" & Space(13 - Len(Trim(TextCharge2.Text))) & "" & Trim(TextCharge2.Text)
    End If
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(29) & "-----------"
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = Space(15) & " Kembali" & Space(3) & "=" & Space(13 - Len(Trim(TextBalance.Text))) & "" & Trim(TextBalance.Text)
    If Len(Trim(Text1(0).Text)) > 1 Then
        baris = baris + 1
        ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = "Nota  : " & Text1(0).Text
    End If
    
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = "----------------------------------------"
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = ""
    baris = baris + 1
    ListViewStruk.ListItems.Add(baris, , "").SubItems(1) = "        ***** THANK YOU *****        "
    'FileCopy App.Path & "\cetak.dat", App.Path & "\NOTAEJ\" & tgh & "\" & xy & "\" & Text3(0).Text & ".dat"
End Sub

Sub cetak_usb()
    On Error Resume Next
    Dim X As Printer
    Dim PrinterName As String
    
    Printer.FontName = "Courier New"
    For i = 1 To ListViewStruk.ListItems.Count
        If i = 1 Then
            Printer.FontName = "Arial"
            Printer.FontSize = 10
            Printer.FontBold = True
            Printer.Print ListViewStruk.ListItems(i).SubItems(1)
            Printer.FontBold = False
        Else
            Printer.FontSize = 8
            Printer.FontBold = False
            Printer.FontName = "Courier New"
            Printer.Print ListViewStruk.ListItems(i).SubItems(1)
        End If
    Next i
    Printer.EndDoc
End Sub

Private Sub cmd_tutupbayar_Click()
    tutup_bayar
End Sub

Private Sub cmb_matauang_Change()
    hitung_bayar
End Sub

Private Sub cmb_matauang_Click()
    hitung_bayar
End Sub
Sub get_harga_agen()
For z = 1 To ListView1.ListItems.Count
    ListView1.ListItems(z).SubItems(5) = "" & Format(ListView1.ListItems(z).SubItems(9), "#,##0.00")
        
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select harga from paket_agen where tgl1<='" & Format(DTPicker1.Value, "yyyy-MM-dd") & "' and tgl2>='" & Format(DTPicker1.Value, "yyyy-MM-dd") & "' and kdag='" & Text1(4).Text & "' and bara='" & ListView1.ListItems(z).SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        ListView1.ListItems(z).SubItems(5) = "" & Format(rs1.Fields!Harga, "#,##0.00")
    End If
    ListView1.ListItems(z).SubItems(6) = "" & Format(Val(Format(ListView1.ListItems(z).SubItems(5), "#,##0.00")) * Val(Format(ListView1.ListItems(z).SubItems(4), "#,##0.00")), "#,##0.00")
    
    hitung_total
Next z
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbCtrlMask And KeyCode = vbKeyT Then
        Check_pajak.Value = IIf(Check_pajak.Value = 1, 0, 1)
    ElseIf Shift = vbCtrlMask And KeyCode = vbKeyP Then
        PictureStruk.Visible = True
    ElseIf KeyCode = vbKeyEnd Then
        If Text1(1).Text <> "" Then show_bayar
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        If PictureStruk.Visible = True Then
            PictureStruk.Visible = False
        End If
        If PictureBayar.Visible = True Then
            tutup_bayar
        ElseIf Picture7.Visible = True Then
            Picture7.Visible = False
        Else
            pesan = MsgBox("Anda ingin keluar dari aplikasi?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                Unload Me
                If Not MDIForm1.Visible Then
                    login_option.Show 1
                End If
            End If
        End If
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    koneksi
    
    kosong
    default_sales
    default_agen
    
    get_cabang
    get_matauang
    get_bank
    otomatis
    
    pageIndex = 1
    pageSize = 50
    tampil_item
    
    DTPicker1.Value = Date
    cmb_matauang.ListIndex = 0
    
    Set cmd_nav(0).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
    Set cmd_nav(1).MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
    Set VScroll1.MouseIcon = LoadPicture(App.Path & "\Icon\hand.cur")
End Sub

Private Sub Form_Resize()
    Picture2.Width = Me.Width / 1.9
    Picture1.Width = Picture2.Width
    Picture1.Height = Me.Height - Picture1.Top - 100
    
    Picture5.Left = Picture2.Width + 100
    Picture5.Width = Me.Width - Picture5.Left - 100
    cmd_showbayar.Left = Picture5.Width - cmd_showbayar.Width
    cmd_batal.Left = cmd_showbayar.Left - cmd_batal.Width - 100
    
    PictureNav.Left = Picture1.Width - PictureNav.Width + 50
    VScroll1.Left = Picture1.Width - VScroll1.Width - 50
    VScroll1.Height = Picture1.Height - 50
    picScroll.Width = VScroll1.Left
    
    PictureStruk.Left = picScroll.Width - PictureStruk.Width + 50
    
    PictureTotal1.Left = Picture2.Width + 100
    PictureTotal1.Width = Me.Width - PictureTotal1.Left - 100
    total_usd.Left = PictureTotal1.Width - total_usd.Width - 100
    
    PictureTotal2.Left = Picture2.Width + 100
    PictureTotal2.Width = Me.Width - PictureTotal2.Left - 100
    total_idr.Left = PictureTotal2.Width - total_idr.Width - 100
    
    ListView1.Left = Picture5.Left
    ListView1.Width = Picture5.Width
    ListView1.Height = Me.Height - (ListView1.Top + SSTab1.Height + 200)
    Picture7.Left = ListView1.Left
    
    PictureBayar.Left = ListView1.Left
    PictureBayar.Width = ListView1.Width
    Picture3.Left = PictureBayar.Width - Picture3.Width
    Image2.Left = PictureBayar.Width - Image2.Width - 10
    Label7.Width = PictureBayar.Width
    
    SSTab1.Top = ListView1.Top + ListView1.Height + 100
    SSTab1.Width = ListView1.Width
    SSTab1.Left = ListView1.Left
    
    Picture4.Width = SSTab1.Width - 150
    ListView2.Width = Picture4.Width
    ListView2.Height = SSTab1.Height - Picture4.Height - 400
    cmd_customer.Left = SSTab1.Width - cmd_customer.Width - 5
    
    Dim i As Integer
    Dim row As Integer
    For i = 1 To ItemBox1.UBound
        atas = ItemBox1(i - 1).Top + ItemBox1(0).Height + 100
        kiri = ItemBox1(i - 1).Left + ItemBox1(0).Width + 100
        margin = picScroll.Width - ItemBox1(0).Width
        row = row + IIf(kiri >= margin, 1, 0)
        
        ItemBox1(i).Top = IIf(kiri >= margin, atas, ItemBox1(i - 1).Top)
        ItemBox1(i).Left = IIf(kiri >= margin, ItemBox1(0).Left, kiri)
        ItemBox1(i).Visible = True
    Next
    VScroll1.Max = row
End Sub

Private Sub Image1_Click()
    PictureStruk.Visible = False
End Sub

Private Sub Image2_Click()
    Picture1.Enabled = True
    Picture2.Enabled = True
    Picture4.Enabled = True
    Picture5.Enabled = True
    Picture6.Enabled = True
    
    PictureBayar.Visible = False
End Sub

Private Sub ItemBox1_DetailClick(Index As Integer)
    detail_paket.ItemBox = ItemBox1(Index)
    detail_paket.Show 1
End Sub

Private Sub ItemBox1_TambahClick(Index As Integer)
    tambah_item ItemBox1(Index)
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        Picture7.Top = ListView1.Top + ListView1.SelectedItem.Top
        Picture7.Left = ListView1.Left + ListView1.SelectedItem.Left - 20
        Picture7.Width = ListView1.Width
        Picture7.Visible = True
        
        Text3(0).Text = ListView1.SelectedItem.SubItems(1)
        Text3(1).Text = ListView1.SelectedItem.SubItems(2)
        Text3(2).Text = ListView1.SelectedItem.SubItems(3)
        Text3(3).Text = ListView1.SelectedItem.SubItems(4)
        Text3(4).Text = ListView1.SelectedItem.SubItems(5)
        Text3(5).Text = ListView1.SelectedItem.SubItems(6)
        With Text3(3)
            .SelStart = 0
            .SelLength = Len(.Text)
            .SetFocus
        End With
    End If
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    If ListView1.ListItems.Count > 0 Then
        If KeyCode = vbKeyDelete Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                hitung_total
            End If
        ElseIf KeyCode = vbKeyReturn Then
            Picture7.Top = ListView1.Top + ListView1.SelectedItem.Top
            Picture7.Left = ListView1.Left + ListView1.SelectedItem.Left - 20
            Picture7.Visible = True
            
            Text3(0).Text = ListView1.SelectedItem.SubItems(1)
            Text3(1).Text = ListView1.SelectedItem.SubItems(2)
            Text3(2).Text = ListView1.SelectedItem.SubItems(3)
            Text3(3).Text = ListView1.SelectedItem.SubItems(4)
            Text3(4).Text = ListView1.SelectedItem.SubItems(5)
            Text3(5).Text = ListView1.SelectedItem.SubItems(6)
            With Text3(3)
                .SelStart = 0
                .SelLength = Len(.Text)
                .SetFocus
            End With
        End If
    End If
End Sub

Private Sub ListView2_DblClick()
    If ListView2.ListItems.Count > 0 Then
        Text2(1).Text = ListView2.SelectedItem.SubItems(2)
        Text2(2).Text = ListView2.SelectedItem.SubItems(3)
    End If
End Sub

Private Sub ListView2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And ListView2.ListItems.Count > 0 Then
        pesan = MsgBox("Hapus item " & ListView2.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            ListView2.ListItems.Remove ListView2.SelectedItem.Index
        End If
    End If
End Sub

Private Sub OptionTipe_Click(Index As Integer)
    tampil_item
End Sub


Private Sub text_cari_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        pageIndex = 1
        tampil_item
    End If
End Sub

Private Sub Text1_Change(Index As Integer)
    If Index = 2 Then Text1(3).Text = ""
    If Index = 4 Then Text1(5).Text = ""
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        If Index = 2 Then
            cari_sales.Left = Text1(2).Left + 100
            cari_sales.Top = Text1(2).Top + Text1(2).Height + 150
            Set cari_sales.FormPemanggil = Me
            cari_sales.Cari = Text1(2).Text
            cari_sales.Show 1
        ElseIf Index = 4 Then
            cari_agen.Left = Text1(4).Left + 100
            cari_agen.Top = Text1(4).Top + Text1(4).Height + 150
            Set cari_agen.FormPemanggil = Me
            cari_agen.Cari = Text1(4).Text
            cari_agen.Show 1
        End If
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        If Index = 2 And Text1(2).Text <> "" Then
            If get_sales(Text1(2).Text) = False Then
                MsgBox ("Kode sales tidak ada")
                cari_sales.Left = Text1(2).Left + 100
                cari_sales.Top = Text1(2).Top + Text1(2).Height + 150
                Set cari_sales.FormPemanggil = Me
                cari_sales.Cari = Text1(2).Text
                cari_sales.Show 1
            End If
        ElseIf Index = 4 And Text1(4).Text <> "" Then
           If get_agen(Text1(4).Text) = False Then
                MsgBox ("Kode agen tidak ada")
                cari_agen.Left = Text1(4).Left + 100
                cari_agen.Top = Text1(4).Top + Text1(4).Height + 150
                Set cari_agen.FormPemanggil = Me
                cari_agen.Cari = Text1(4).Text
                cari_agen.Show 1
            End If
        End If
        SendKeys "{tab}"
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub Text2_GotFocus(Index As Integer)
    Select Case Index
        Case 1 To 2
            With Text2(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text2_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 1 Then
            KeyAscii = 0
            If Text2(1).Text = "" Then Exit Sub
            SendKeys "{tab}"
        ElseIf Index = 2 Then
            KeyAscii = 0
            If Text2(2).Text = "" Then Exit Sub
            
            j = ListView2.ListItems.Count + 1
            Set item = ListView2.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = Text2(1).Text
            item.SubItems(3) = Text2(2).Text
            
            Text2(1).Text = ""
            Text2(2).Text = ""
            Text2(1).SetFocus
        End If
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub Text3_Change(Index As Integer)
    If Index = 3 Then
        Text3(5).Text = Format(Val(Format(Text3(3).Text, "##0.00")) * Val(Format(Text3(4).Text, "##0.00")), "#,##0.00")
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        For i = 0 To Text3.Count - 1
            Text3(i).Text = ""
        Next
    ElseIf KeyAscii = vbKeyReturn Then
        Dim item As ListItem
        Set item = ListView1.SelectedItem
        item.SubItems(4) = "" & Text3(3).Text
        item.SubItems(5) = "" & Text3(4).Text
        item.SubItems(6) = "" & Text3(5).Text
        
        For i = 0 To Text3.Count - 1
            Text3(i).Text = ""
        Next
        Picture7.Visible = False
        hitung_total
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub TextCash_Change()
    hitung_bayar
End Sub

Private Sub TextCash_GotFocus()
    With TextCash
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub TextCash_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub TextCash_LostFocus()
    TextCash.Text = Format(IIf(TextCash.Text = "", 0, TextCash.Text), "#,##0.00")
End Sub

Private Sub TextCharge1_Change()
    TextCharge1.Text = Format(IIf(TextCharge1.Text = "", 0, TextCharge1.Text), "#,##0.00")
    TextCharge2.Text = Format(Val(Format(TextCharge1.Text, "##0.00") / 100) * Val(Format(TextBalance.Text, "##0.00")), "#,##0.00")
    hitung_bayar
End Sub

Private Sub TextCharge1_GotFocus()
    With TextCharge1
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub TextCharge1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub TextCharge1_LostFocus()
    TextCharge1.Text = Format(IIf(TextCharge1.Text = "", 0, TextCharge1.Text), "#,##0.00")
End Sub


Private Sub TextDisc_Change()
    On Error Resume Next
    TextDisc2.Text = Format(Val(Format(TextDisc.Text, "##0.00") / 100) * Val(Format(total_idr.Caption, "##0.00")), "#,##0.00")
    hitung_bayar
End Sub

Private Sub TextDisc_GotFocus()
    With TextDisc
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub TextDisc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub TextDisc2_GotFocus()
    With TextDisc2
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub TextDisc2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub TextDisc2_LostFocus()
    TextDisc2.Text = Format(IIf(TextDisc2.Text = "", 0, TextDisc2.Text), "#,##0.00")
    If Val(Format(total_idr.Caption, "##0.00")) > 0 Then
        TextDisc.Text = Format((Val(Format(TextDisc2.Text, "##0.00")) / Val(Format(total_idr.Caption, "##0.00")) * 100), "#,##0.00")
    End If
    hitung_bayar
End Sub

Private Sub VScroll1_Change()
    picScroll.Top = -(ItemBox1(0).Height * VScroll1.Value)
End Sub

Private Sub VScroll1_Scroll()
    Call VScroll1_Change
End Sub

Sub UnloadItem(ByVal startIndex As Integer)
    For Each ctrl In ItemBox1
      If ctrl.Index > startIndex Then
         Unload ctrl
      End If
   Next ctrl
End Sub

Function HasControl(ctrlArray As Object, ByVal idx As Integer) As Boolean
   Dim result As Boolean, ctrl As Control
   For Each ctrl In ctrlArray
      If ctrl.Index = idx Then
         result = True
         Exit For
      End If
   Next ctrl
   HasControl = result
End Function
