VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form input_fee 
   Caption         =   "Penarikan Fee"
   ClientHeight    =   6495
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11670
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6495
   ScaleWidth      =   11670
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   660
      Left            =   50
      TabIndex        =   12
      Top             =   1320
      Width           =   11500
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   8600
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   330
         Width           =   2925
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   3300
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   330
         Width           =   2500
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   550
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   13
         Top             =   330
         Width           =   2750
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   5800
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   330
         Width           =   2800
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fee Guide"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   8595
         TabIndex        =   31
         Top             =   45
         Width           =   2900
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No. Transaksi"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   555
         TabIndex        =   20
         Top             =   45
         Width           =   2750
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   5805
         TabIndex        =   19
         Top             =   45
         Width           =   2790
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No. Kunjungan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   3300
         TabIndex        =   18
         Top             =   45
         Width           =   2500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   550
         Index           =   0
         Left            =   50
         TabIndex        =   17
         Top             =   45
         Width           =   500
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3165
      Left            =   60
      TabIndex        =   21
      Top             =   1920
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   5583
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   1
         Text            =   "No"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "No. Transaksi"
         Object.Width           =   4851
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Sales / Guide"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "Total"
         Object.Width           =   4939
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Fee Guide"
         Object.Width           =   5027
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   5175
      Left            =   0
      ScaleHeight     =   5145
      ScaleWidth      =   11565
      TabIndex        =   22
      Top             =   0
      Width           =   11595
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   3
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   37
         Top             =   840
         Width           =   1935
      End
      Begin VB.ComboBox combo_akun 
         Height          =   315
         Left            =   4800
         Style           =   2  'Dropdown List
         TabIndex        =   34
         Top             =   480
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   8280
         MaxLength       =   15
         TabIndex        =   6
         Top             =   120
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   2
         Left            =   10200
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         Left            =   4800
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   120
         Width           =   1935
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   1440
         TabIndex        =   24
         Top             =   480
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   100532227
         CurrentDate     =   43159
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Index           =   1
         Left            =   13245
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_fee.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Kunjungan :"
         Height          =   255
         Index           =   5
         Left            =   0
         TabIndex        =   38
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Sales/Guide :"
         Height          =   255
         Index           =   3
         Left            =   6840
         TabIndex        =   32
         Top             =   135
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   135
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal :"
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   28
         Top             =   495
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept / Gudang :"
         Height          =   255
         Index           =   2
         Left            =   3360
         TabIndex        =   27
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Akun :"
         Height          =   255
         Index           =   4
         Left            =   3360
         TabIndex        =   26
         Top             =   525
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1305
         Left            =   0
         TabIndex        =   30
         Top             =   0
         Width           =   11550
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1305
      ScaleWidth      =   11565
      TabIndex        =   3
      Top             =   5160
      Width           =   11595
      Begin VB.Frame Frame2 
         Height          =   1215
         Left            =   8200
         TabIndex        =   5
         Top             =   0
         Width           =   3270
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   1155
            Locked          =   -1  'True
            TabIndex        =   35
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   1155
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   225
            Width           =   2000
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Total Fee :"
            Height          =   195
            Index           =   0
            Left            =   315
            TabIndex        =   33
            Top             =   720
            Width           =   765
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Total PJL :"
            Height          =   195
            Index           =   1
            Left            =   330
            TabIndex        =   7
            Top             =   285
            Width           =   765
         End
      End
      Begin VB.TextBox Text5 
         Height          =   800
         Left            =   5040
         MultiLine       =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   405
         Width           =   3050
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_fee.frx":001C
         PICN            =   "input_fee.frx":0038
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_fee.frx":0192
         PICN            =   "input_fee.frx":01AE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button3 
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   120
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Hapus Item"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_fee.frx":05E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Batalkan"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_fee.frx":0600
         PICN            =   "input_fee.frx":061C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label5 
         Caption         =   "Keterangan :"
         Height          =   255
         Left            =   5040
         TabIndex        =   11
         Top             =   135
         Width           =   1215
      End
   End
End
Attribute VB_Name = "input_fee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim akun As New Collection
Dim edit As Boolean

Private Sub Button5_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub Combo1_Click()
    Frame3.Visible = Combo1.Text = "CEK/BG"
    Text1(3).Text = ""
    Text1(4).Text = ""
    Text1(5).Text = ""
End Sub

Private Sub cmd_batal_Click()
    On Error GoTo err
    pesan = MsgBox("Hapus penarikan fee " & Text1(0).Text & "?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from fee_head where kdtrk='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
                
        kosong
        otomatis
        
        If daftar_fee.Visible Then
            daftar_fee.tampil_fee
        End If
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo err
    If KeyCode = vbKeyDelete Then
        KeyAscii = 0
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "delete from fee_det where kdtrk='" & Text1(0).Text & "' and no_ej='" & ListView1.SelectedItem.SubItems(2) & "'", con, adOpenKeyset, adLockOptimistic
                
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
        End If
        Text3(0).SetFocus
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub Image2_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub Text1_Change(Index As Integer)
    Select Case Index
        Case 1
            Text1(2).Text = ""
        Case 3
            Text4(0).Text = "0.00"
            Text4(1).Text = "0.00"
            Text5.Text = ""
            
            ListView1.ListItems.Clear
            hitung_total
    End Select
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        cari_sales.Left = 1550
        cari_sales.Top = 3800
        Set cari_sales.FormPemanggil = Me
        cari_sales.Show 1
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        If Index = 1 Then
            get_sales Text1(1).Text
            Text3(0).SetFocus
        ElseIf Index = 3 Then
            get_feepjl Text1(3).Text
        End If
    End If
End Sub

Private Sub Text3_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 3
                Text3(z).Text = ""
                Text3(z).Locked = True
            Next z
    End Select
End Sub

Private Sub Text3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
'        cari_penjualan.Left = 700
'        cari_penjualan.Top = 4600
'        cari_penjualan.Text1.Text = Text1(1).Text
'        Set cari_penjualan.FormPemanggil = Me
'        cari_penjualan.Show 1
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text3(0).Text <> "" Then
                    get_penjualan Text3(0).Text
                End If
            End If
        Case 3
            If KeyAscii = 13 Then
                If Text3(0).Text = "" And Val(Format(Text3(3).Text, "##0.00")) = 0 Then
                    MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
                    Exit Sub
                End If
                
                tambah_item
                hitung_total
            End If
        Case Else
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
            Else
                If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
                KeyAscii = 0
                End If
            End If
    End Select
End Sub

Private Sub Text3_GotFocus(Index As Integer)
    Select Case Index
        Case 1 To 3
            With Text3(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Button1_Click(Index As Integer)
    Select Case Index
        Case 1
            cari_sales.Left = 1550
            cari_sales.Top = 3800
            Set cari_sales.FormPemanggil = Me
            cari_sales.Show 1
    End Select
End Sub

Private Sub Button3_Click(Index As Integer)
    On Error GoTo err
    If Index = 0 Then
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "delete from fee_det where kdtrk='" & Text1(0).Text & "' and no_ej='" & ListView1.SelectedItem.SubItems(2) & "'", con, adOpenKeyset, adLockOptimistic
                
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
            Text3(0).SetFocus
        End If
    End If
    Exit Sub
err:
    MsgBox "Error : " & err.Description
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "Detail penjualan masih kosong", vbOKOnly
        Text3(0).SetFocus
        Exit Sub
    End If
    
    If edit Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from fee_head where kdtrk='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    Else
        otomatis
    End If
    
    On Error GoTo exc
    csql = "INSERT INTO fee_head (kdtrk,no_kunj,namasl,tgl,netto,tarik,ket,bo,usr) VALUES ('" & _
            Text1(0).Text & "','" & Text1(3).Text & "','" & Text1(2).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & _
            Val(Format(Text4(0).Text, "##0.00")) & "," & Val(Format(Text4(1).Text, "##0.00")) & ",'" & _
            Text5.Text & "','" & cmb_cabang.Text & "','" & xy & "')"
    
'    Debug.Print csql
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    If ListView1.ListItems.Count > 0 Then
        Dim no_ej, tgl, no_kunj As String
        Dim netto, fee, tarik As Double
        
        For z = 1 To ListView1.ListItems.Count
            no_ej = ListView1.ListItems(z).ListSubItems.item(2)
            no_kunj = ListView1.ListItems(z).ListSubItems.item(3)
            netto = Val(Format(ListView1.ListItems(z).ListSubItems.item(4), "##0.00"))
            fee = Val(Format(ListView1.ListItems(z).ListSubItems.item(5), "##0.00"))
                
            csql = "INSERT INTO fee_det(kdtrk,no_ej,tgl,namasl,netto,tarik) VALUES('" & _
                    Text1(0).Text & "','" & no_ej & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & _
                    "','" & no_kunj & "'," & netto & "," & fee & ")"
            
            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next
    End If
        
    MsgBox ("Input penarikan fee sudah disimpan")
    
    kosong
    otomatis
    
    If daftar_fee.Visible Then
        daftar_fee.tampil_fee
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    koneksi
    get_cabang
    kosong
'    otomatis
    get_akun
End Sub

Private Sub Form_Activate()
    Text1(3).SetFocus
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture2.Top = Me.Height - 2000
'    Picture2.Width = Me.Width - 255
    Picture1.Height = Me.Height - 2000
'    Picture1.Width = Me.Width - 255
    ListView1.Height = Picture1.Height - 2350
'    ListView1.Width = Me.Width - 495
'    Label1.Width = Me.Width - 50
End Sub

Private Sub tambah_item()
    Dim item As ListItem
    For Each item In ListView1.ListItems
        If item.SubItems(2) = Text3(0).Text Then
            Text3(0).Text = ""
            Text3(0).SetFocus
            Exit Sub
        End If
    Next item
    
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(2) = Text3(0).Text
    item.SubItems(3) = Text3(1).Text
    item.SubItems(4) = Text3(2).Text
    item.SubItems(5) = Text3(3).Text
    Text3(0).Text = ""
    Text3(0).SetFocus
End Sub

Private Sub hitung_total()
    Dim netto, fee As Double
    If ListView1.ListItems.Count > 0 Then
        For z = 1 To ListView1.ListItems.Count
            netto = netto + Val(Format(ListView1.ListItems(z).SubItems(4), "##0.00"))
            fee = fee + Val(Format(ListView1.ListItems(z).SubItems(5), "##0.00"))
        Next
    End If
    
    Text4(0).Text = Format(Val(netto), "#,##0.00")
    Text4(1).Text = Format(Val(fee), "#,##0.00")
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Sub get_sales(kode As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msales where kdsl='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(1).Text = "" & rs1!kdsl
        Text1(2).Text = "" & rs1!namasl
                
        Text3(0).Locked = False
'        Text3(0).SetFocus
        
        get_feesales rs1!kdsl
    Else
        MsgBox ("Kode sales tidak ada")
        cari_sales.Left = 1550
        cari_sales.Top = 3800
        Set cari_sales.FormPemanggil = Me
        cari_sales.Show 1
    End If
End Sub

Sub get_feesales(kode As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    csql = "select * from jual_head where kdsl='" & kode & "' and tgl='" & Format(DTPicker1.Value, "yyyy-MM-dd") & "' "
    csql = csql & "and tarik=0 and bo='" & cmb_cabang.Text & "' order by tgl,no_kunj"
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    j = 0
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = "" & rs1.Fields!no_ej
            item.SubItems(3) = "" & rs1.Fields!no_kunj
            item.SubItems(4) = "" & Format(rs1.Fields!netto, "#,##0")
            item.SubItems(5) = "" & Format(rs1.Fields!nfee, "#,##0")
            rs1.MoveNext
        Loop
    End If
    hitung_total
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_feepjl(nokunj As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    csql = "select * from jual_head where tgl='" & Format(DTPicker1.Value, "yyyy-MM-dd") & "' "
    csql = csql & "and tarik=0 and no_kunj='" & nokunj & "' and bo='" & cmb_cabang.Text & "' order by tgl,no_kunj"
    Debug.Print csql
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    j = 0
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = "" & rs1.Fields!no_ej
            item.SubItems(3) = "" & rs1.Fields!namasl
            item.SubItems(4) = "" & Format(rs1.Fields!netto, "#,##0")
            item.SubItems(5) = "" & Format(rs1.Fields!nfee, "#,##0")
            rs1.MoveNext
        Loop
    End If
    hitung_total
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub tampil_fee(nota As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
        
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from fee_head where kdtrk='" & nota & "'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Text1(0).Text = "" & rs1.Fields!kdtrk
        Text1(1).Text = "" & rs1.Fields!kdsl
        Text1(2).Text = "" & rs1.Fields!namasl
        DTPicker1.Value = "" & rs1.Fields!tgl
        cmb_cabang.Text = "" & rs1.Fields!BO
        
        Text4(0).Text = Format(rs1.Fields!netto, "#,##0")
        Text4(1).Text = Format(rs1.Fields!tarik, "#,##0")
        Text5.Text = "" & rs1.Fields!ket
        
        If rs2.State <> 0 Then rs1.Close
        rs2.Open "select * from fee_det where kdtrk='" & nota & "' ", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            j = ListView1.ListItems.Count + 1
            Do While Not rs2.EOF
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!no_ej
                item.SubItems(3) = "" & rs2.Fields!no_kunj
                item.SubItems(4) = "" & Format(rs2.Fields!netto, "#,##0")
                item.SubItems(5) = "" & Format(rs2.Fields!tarik, "#,##0")
                rs2.MoveNext
            Loop
        End If
        edit = True
        cmd_batal.Enabled = True
    End If
    
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_penjualan(kode As String)
    csql = "select * from jual_head where no_ej='" & kode & "' and kdsl='" & Text1(1).Text & "' and tgl='" & Format(DTPicker1.Value, "yyyy-MM-dd") & "' "
    csql = csql & "and tarik=0 and bo='" & cmb_cabang.Text & "' limit 1"
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text3(0).Text = "" & rs1.Fields!no_ej
        Text3(1).Text = "" & rs1.Fields!namasl
        Text3(2).Text = "" & Format(rs1.Fields!netto, "#,##0")
        Text3(3).Text = "" & Format(rs1.Fields!nfee, "#,##0")
        SendKeys "{tab}"
'        Text3(4).SetFocus
    Else
        MsgBox ("Penjualan tidak ada atau sudah dilakukan penarikan")
    End If
End Sub

Sub get_akun()
    Set akun = New Collection
    combo_akun.Clear
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis=1 order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim coa As PropertyField
        Do While Not rs1.EOF
            combo_akun.AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            
            Set coa = New PropertyField
            coa.Key = rs1.Fields!kode
            coa.Value = rs1.Fields!nama
            
            akun.Add coa
            rs1.MoveNext
        Loop
    End If
End Sub

Sub kosong()
    For z = 0 To 3
        Text1(z).Text = ""
    Next z
    
    For z = 0 To 3
        Text3(z).Text = ""
    Next z
    
    Text4(0).Text = "0.00"
    Text4(1).Text = "0.00"
    Text5.Text = ""
    
    DTPicker1.Value = Date
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    cmd_batal.Enabled = False
End Sub

Sub otomatis()
    On Error GoTo err
    Dim hari, NO, notarik As String
    
    hari = "TF-" & Format(Date, "yymmdd")
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "SELECT MAX(KDTRK) FROM fee_head WHERE LEFT(KDTRK, 9) = '" & hari & "'", con, adOpenKeyset, adLockOptimistic
        
    If rs1.EOF Then
        notarik = hari + "001"
    Else
        rs1.MoveLast
        If Mid(Trim(rs1.Fields(0)), 10, 3) Then
            NO = Right(Trim(rs1.Fields(0)), 3)
            NO = Val(NO) + 1
            NO = Trim(Str(NO))
            NO = Left("000", 3 - Len(NO)) + NO
            notarik = hari + NO
        Else
            notarik = hari + "001"
        End If
    End If
    Text1(0).Text = notarik
    Exit Sub
err:
    MsgBox ("error : " & err.Description)
End Sub
