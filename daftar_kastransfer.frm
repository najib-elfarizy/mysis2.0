VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form daftar_kastransfer 
   Caption         =   "Daftar Kas Transfer"
   ClientHeight    =   6390
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9855
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6390
   ScaleWidth      =   9855
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   5655
      Left            =   0
      ScaleHeight     =   5625
      ScaleWidth      =   9825
      TabIndex        =   4
      Top             =   0
      Width           =   9855
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Index           =   0
         Left            =   1680
         TabIndex        =   16
         Top             =   480
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   582
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   104923139
         CurrentDate     =   43218
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Desc"
         Height          =   195
         Left            =   4560
         TabIndex        =   8
         Top             =   895
         Width           =   855
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Asc"
         Height          =   195
         Left            =   3840
         TabIndex        =   7
         Top             =   895
         Value           =   -1  'True
         Width           =   615
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "daftar_kastransfer.frx":0000
         Left            =   1680
         List            =   "daftar_kastransfer.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   840
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1680
         TabIndex        =   5
         Text            =   "Text1"
         Top             =   160
         Width           =   4575
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   4215
         Left            =   120
         TabIndex        =   9
         Top             =   1320
         Width           =   9615
         _ExtentX        =   16960
         _ExtentY        =   7435
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "No. Transaksi"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tanggal"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Akun1"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Dari Akun"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Akun2"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Ke Akun"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Keterangan"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Text            =   "Jumlah"
            Object.Width           =   4057
         EndProperty
      End
      Begin MySIS.Button cmd_cari 
         Height          =   375
         Left            =   6360
         TabIndex        =   10
         Top             =   120
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tampil"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kastransfer.frx":0026
         PICN            =   "daftar_kastransfer.frx":0042
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Index           =   1
         Left            =   4200
         TabIndex        =   17
         Top             =   480
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   582
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   104923139
         CurrentDate     =   43218
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "s/d"
         Height          =   255
         Index           =   3
         Left            =   3800
         TabIndex        =   18
         Top             =   525
         Width           =   255
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kata Kunci : "
         Height          =   255
         Index           =   0
         Left            =   170
         TabIndex        =   14
         Top             =   180
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Urut Berdasar : "
         Height          =   255
         Index           =   2
         Left            =   290
         TabIndex        =   13
         Top             =   855
         Width           =   1335
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   5280
         Width           =   6135
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tampilkan Data : "
         Height          =   195
         Index           =   1
         Left            =   360
         TabIndex        =   11
         Top             =   500
         Width           =   1260
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1245
         Index           =   0
         Left            =   -240
         TabIndex        =   15
         Top             =   0
         Width           =   10095
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   9825
      TabIndex        =   0
      Top             =   5640
      Width           =   9855
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   1
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kastransfer.frx":03DC
         PICN            =   "daftar_kastransfer.frx":03F8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   1440
         TabIndex        =   2
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kastransfer.frx":082E
         PICN            =   "daftar_kastransfer.frx":084A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kastransfer.frx":09A4
         PICN            =   "daftar_kastransfer.frx":09C0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "daftar_kastransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_edit_Click()
    If ListView1.ListItems.Count > 0 Then
        input_kastransfer.Label1(0).Caption = "Edit Kas Transfer"
        input_kastransfer.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        input_kastransfer.DTPicker1.Value = ListView1.SelectedItem.SubItems(2)
        input_kastransfer.Text1(1).Text = ListView1.SelectedItem.SubItems(8)
        input_kastransfer.Text1(2).Text = ListView1.SelectedItem.SubItems(7)
        input_kastransfer.AkunDari = ListView1.SelectedItem.SubItems(3)
        input_kastransfer.AkunTujuan = ListView1.SelectedItem.SubItems(5)
        input_kastransfer.edit = True
        input_kastransfer.Show 1
    End If
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_tambah_Click()
    input_kastransfer.otomatis
    input_kastransfer.Show 1
End Sub

Private Sub cmd_cari_Click()
    tampil_kas
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        input_kastransfer.Label1(0).Caption = "Edit Kas Transfer"
        input_kastransfer.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        input_kastransfer.DTPicker1.Value = ListView1.SelectedItem.SubItems(2)
        input_kastransfer.Text1(1).Text = ListView1.SelectedItem.SubItems(8)
        input_kastransfer.Text1(2).Text = ListView1.SelectedItem.SubItems(7)
        input_kastransfer.AkunDari = ListView1.SelectedItem.SubItems(3)
        input_kastransfer.AkunTujuan = ListView1.SelectedItem.SubItems(5)
        input_kastransfer.edit = True
        input_kastransfer.Show 1
    End If
End Sub

Sub tampil_kas()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    csql = "select h.*, GROUP_CONCAT(case when d.kredit>0 and urut=1 then d.kode else '' end SEPARATOR '') akun1,"
    csql = csql & "GROUP_CONCAT(case when d.kredit>0 and urut=1 then d.nama else '' end SEPARATOR '') kas1,"
    csql = csql & "GROUP_CONCAT(case when d.debet>0 and urut=2 then d.kode else '' end SEPARATOR '') akun2,"
    csql = csql & "GROUP_CONCAT(case when d.debet>0 and urut=2 then d.nama else '' end SEPARATOR '') kas2 "
    csql = csql & "from jour_head h join jour_det d on h.vc=d.vc  where jenis='KAS TRANSFER' "
    
    If Text1.Text <> "" Then
        csql = csql & "and (h.vc like '%" & Text1.Text & "%' or h.ket like '%" & Text1.Text & "%') "
    End If
    
    csql = csql & "and h.tgl between '" & Format(DTPicker1(0).Value, "yyyy-MM-dd") & "' and '" & Format(DTPicker1(1).Value, "yyyy-MM-dd") & "'"
    
    If Combo1.ListIndex = 1 Then
        csql = csql & " order by h.tgl "
    Else
        csql = csql & " order by h.vc "
    End If
        
    If Option1.Value = True Then
        csql = csql & "asc"
    Else
        csql = csql & "desc"
    End If
    
'    Debug.Print csql
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    j = 0
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!vc
            item.SubItems(2) = "" & Format(rs1.Fields!tgl, "dd-mm-yyyy")
            item.SubItems(3) = "" & rs1.Fields!Akun1
            item.SubItems(4) = "" & rs1.Fields!Akun1 & " - " & rs1.Fields!kas1
            item.SubItems(5) = "" & rs1.Fields!Akun2
            item.SubItems(6) = "" & rs1.Fields!Akun2 & " - " & rs1.Fields!kas2
            item.SubItems(7) = "" & rs1.Fields!ket
            item.SubItems(8) = "" & Format(rs1.Fields!nilai, "#,##0")
        rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    Text1.Text = ""
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    Combo1.Text = Combo1.List(0)
    DTPicker1(0).Value = DateSerial(Year(Date), Month(Date), 1)
    DTPicker1(1).Value = Date
    
    koneksi
    tampil_kas
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1290
    ListView1.Height = Picture2.Height - 1450
    Picture2.Width = Me.Width - 255
    Picture5.Width = Me.Width - 255
    ListView1.Width = Me.Width - 495
    Label1(0).Width = Me.Width - 50
End Sub


