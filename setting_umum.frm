VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form setting_umum 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setting Umum"
   ClientHeight    =   6465
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9825
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   9825
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   11456
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "setting_umum.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Picture5"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Picture1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   5175
         Left            =   120
         ScaleHeight     =   5145
         ScaleWidth      =   9585
         TabIndex        =   4
         Top             =   480
         Width           =   9615
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   1
            Left            =   1320
            TabIndex        =   8
            Text            =   "Text1"
            Top             =   615
            Width           =   2415
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   0
            Left            =   1320
            TabIndex        =   5
            Text            =   "Text1"
            Top             =   240
            Width           =   2415
         End
         Begin MySIS.Button cmd_agen 
            Height          =   315
            Index           =   0
            Left            =   3720
            TabIndex        =   6
            Top             =   220
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   556
            BTYPE           =   7
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   15790320
            BCOLO           =   15790320
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "setting_umum.frx":001C
            PICN            =   "setting_umum.frx":0038
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button cmd_agen 
            Height          =   315
            Index           =   1
            Left            =   3720
            TabIndex        =   9
            Top             =   600
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   556
            BTYPE           =   7
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   15790320
            BCOLO           =   15790320
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "setting_umum.frx":04D2
            PICN            =   "setting_umum.frx":04EE
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Caption         =   "Sales Umum : "
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   10
            Top             =   615
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Caption         =   "Agen Umum : "
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   7
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   120
         ScaleHeight     =   705
         ScaleWidth      =   9585
         TabIndex        =   1
         Top             =   5640
         Width           =   9615
         Begin MySIS.Button cmd_simpan 
            Height          =   495
            Left            =   6960
            TabIndex        =   2
            Top             =   120
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   "Simpan"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "setting_umum.frx":0988
            PICN            =   "setting_umum.frx":09A4
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button cmd_keluar 
            Height          =   495
            Left            =   8280
            TabIndex        =   3
            Top             =   120
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   "Keluar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "setting_umum.frx":0AFE
            PICN            =   "setting_umum.frx":0B1A
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
      End
   End
End
Attribute VB_Name = "setting_umum"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_agen_Click(Index As Integer)
    If Index = 0 Then
        cari_agen.Left = (Screen.Width - cari_agen.Width) / 2
        cari_agen.Top = (Screen.Height - cari_agen.Height) / 2
        Set cari_agen.FormPemanggil = Me
        cari_agen.Cari = Text1(0).Text
        cari_agen.Show 1
    ElseIf Index = 1 Then
        cari_sales.Left = (Screen.Width - cari_sales.Width) / 2
        cari_sales.Top = (Screen.Height - cari_sales.Height) / 2
        Set cari_sales.FormPemanggil = Me
        cari_sales.Cari = Text1(1).Text
        cari_sales.Show 1
    End If
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
End Sub

Private Sub cmd_simpan_Click()
    On Error GoTo exc
    
    If rs1.State <> 0 Then rs1.Close
    csql = "replace into msetting values('AGU','" & Text1(0).Text & "')"
    csql = csql & ",('SLU','" & Text1(1).Text & "')"
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    MsgBox "Pengaturan disimpan"
    Exit Sub
exc:
    MsgBox "Error : " & err.Description
End Sub

Private Sub Form_Load()
    koneksi
    
    For z = 0 To Text1.Count - 1
        Text1(z).Text = ""
    Next z
    
    get_settings
End Sub

Sub get_settings()
    On Error GoTo exc
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msetting", con, adOpenKeyset, adLockOptimistic
    
    Do While Not rs1.EOF
        On Error Resume Next
        
        If rs1.Fields!kd = "AGU" Then Text1(0).Text = rs1.Fields!nilai
        If rs1.Fields!kd = "SLU" Then Text1(1).Text = rs1.Fields!nilai
        
        rs1.MoveNext
    Loop
    Exit Sub
exc:

End Sub
