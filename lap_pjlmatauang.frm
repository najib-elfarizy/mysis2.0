VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "crviewer.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form lap_pjlmatauang 
   Caption         =   "Lap. Nilai Penjualan"
   ClientHeight    =   6855
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11040
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11040
   Begin VB.Frame Frame1 
      Height          =   1050
      Left            =   5
      TabIndex        =   1
      Top             =   -50
      Width           =   11055
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   345
         Index           =   0
         Left            =   1200
         TabIndex        =   2
         Top             =   240
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   105250819
         CurrentDate     =   43218
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   345
         Index           =   1
         Left            =   1200
         TabIndex        =   3
         Top             =   600
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   105250819
         CurrentDate     =   43218
      End
      Begin MySIS.Button cmd_filter 
         Height          =   375
         Left            =   5520
         TabIndex        =   6
         Top             =   240
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tampil"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "lap_pjlmatauang.frx":0000
         PICN            =   "lap_pjlmatauang.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Mata Uang : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   7
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tgl Awal : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tgl Akhir :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   4
         Top             =   600
         Width           =   885
      End
   End
   Begin CRVIEWERLibCtl.CRViewer CRViewer1 
      Height          =   5895
      Left            =   0
      TabIndex        =   0
      Top             =   960
      Width           =   11055
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   0   'False
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "lap_pjlmatauang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim temp As String

Public Sub ShowReport()
    On Error Resume Next
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
    temp = App.Path & "\Report\lap_pjlmatauang.rpt"
    
    If fso.FileExists(temp) Then
        Set cryApp = New CRAXDRT.Application
        Set rptApp = cryApp.OpenReport(temp)
        rptApp.EnableParameterPrompting = False
    
        If rs2.State <> 0 Then rs2.Close
        csql = "SELECT TGL,CURRENCY,NETTO FROM jual_head WHERE (tgl between '" & Format(DTPicker1(0).Value, "yyyy-MM-dd") & "' and '" & Format(DTPicker1(1).Value, "yyyy-MM-dd") & "')"
        
        If Combo1.ListIndex > 0 Then
            csql = csql & "AND currency='" & Combo1.Text & "'"
        End If
        
        rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        rptApp.Database.SetDataSource rs2
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mperusahaan limit 1", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            On Error Resume Next
            rptApp.ParameterFields.GetItemByName("NAMA_PERUSAHAAN").AddCurrentValue "" & rs1.Fields!NAMA_PERUSAHAAN
            rptApp.ParameterFields.GetItemByName("ALAMAT1").AddCurrentValue "" & rs1.Fields!alamat1
            rptApp.ParameterFields.GetItemByName("ALAMAT2").AddCurrentValue "" & rs1.Fields!alamat2
            rptApp.ParameterFields.GetItemByName("TELP").AddCurrentValue "" & rs1.Fields!telp
            rptApp.ParameterFields.GetItemByName("FAX").AddCurrentValue "" & rs1.Fields!fax
            rptApp.ParameterFields.GetItemByName("NPWP").AddCurrentValue "" & rs1.Fields!npwp
        End If
        
        rptApp.ParameterFields.GetItemByName("TGL1").AddCurrentValue DTPicker1(0).Value
        rptApp.ParameterFields.GetItemByName("TGL2").AddCurrentValue DTPicker1(1).Value
        rptApp.ParameterFields.GetItemByName("USER").AddCurrentValue "" & xy
        
        CRViewer1.ReportSource = rptApp
        CRViewer1.ViewReport
        
        Do While CRViewer1.IsBusy
            DoEvents
        Loop
          
        CRViewer1.Zoom 1
    Else
        MsgBox "Report File Doesn't Exist!"
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_matauang()
    On Error GoTo exc
    Combo1.AddItem "Mata Uang"
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from kurs", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            Combo1.AddItem UCase(rs1.Fields!Currency)
            rs1.MoveNext
        Loop
    End If
    Combo1.ListIndex = 0
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_filter_Click()
    ShowReport
End Sub

Private Sub Form_Load()
    DTPicker1(0).Value = Date
    DTPicker1(1).Value = Date
    
    get_matauang
    ShowReport
End Sub

Private Sub Form_Resize()
    Frame1.Width = Me.Width - 230
    CRViewer1.Top = Frame1.Top + Frame1.Height + 20
    CRViewer1.Width = Me.Width - 250
    CRViewer1.Height = Me.Height - CRViewer1.Top - 550
End Sub
