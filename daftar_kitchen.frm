VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form daftar_kitchen 
   Caption         =   "Daftar Kitchen"
   ClientHeight    =   5655
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7815
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5655
   ScaleWidth      =   7815
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   7785
      TabIndex        =   4
      Top             =   4920
      Width           =   7815
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   5400
         TabIndex        =   5
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kitchen.frx":0000
         PICN            =   "daftar_kitchen.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   1440
         TabIndex        =   6
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kitchen.frx":0452
         PICN            =   "daftar_kitchen.frx":046E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_hapus 
         Height          =   495
         Left            =   4080
         TabIndex        =   7
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Hapus"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kitchen.frx":05C8
         PICN            =   "daftar_kitchen.frx":05E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kitchen.frx":073E
         PICN            =   "daftar_kitchen.frx":075A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_item 
         Height          =   495
         Left            =   2760
         TabIndex        =   9
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Item"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_kitchen.frx":0AF4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   0
      ScaleHeight     =   4905
      ScaleWidth      =   7785
      TabIndex        =   0
      Top             =   0
      Width           =   7815
      Begin MSComctlLib.ListView ListView1 
         Height          =   4335
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   7646
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "KITCHEN"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "KETERANGAN"
            Object.Width           =   8819
         EndProperty
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   5280
         Width           =   6135
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   405
         Index           =   0
         Left            =   -240
         TabIndex        =   2
         Top             =   0
         Width           =   8055
      End
   End
End
Attribute VB_Name = "daftar_kitchen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_item_Click()
    If ListView1.ListItems.Count > 0 Then
        item_kitchen.kitchen = ListView1.SelectedItem.SubItems(1)
        item_kitchen.Label1(0).Caption = "Input Item " & ListView1.SelectedItem.SubItems(1)
        item_kitchen.Show 1
    End If
End Sub

Private Sub cmd_tambah_Click()
'If ListView1.ListItems.Count > 0 Then
    master_kitchen.Text1(0).Locked = False
    master_kitchen.Text1(0).Text = ""
    master_kitchen.Label1(0).Caption = "Tambah Data Kitchen"
    master_kitchen.Show 1
'End If
End Sub

Private Sub cmd_edit_Click()
    If ListView1.ListItems.Count > 0 Then
        master_kitchen.Text1(0).Locked = True
        master_kitchen.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        master_kitchen.Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        master_kitchen.Label1(0).Caption = "Edit Data Kitchen"
        master_kitchen.Show 1
    End If
End Sub

Private Sub cmd_hapus_Click()
    On Error GoTo exc
    pesan = MsgBox("Hapus Data Kitchen " & ListView1.SelectedItem.SubItems(1) & "?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from mkitchen where kitchen='" & ListView1.SelectedItem.SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from kitchen_item where kitchen='" & ListView1.SelectedItem.SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
        
        tampil_kitchen
    End If
    Exit Sub
exc:
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        master_kitchen.Text1(0).Locked = True
        master_kitchen.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        master_kitchen.Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        master_kitchen.Label1(0).Caption = "Edit Data Kitchen"
        master_kitchen.Show 1
    End If
End Sub

Sub tampil_kitchen()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mkitchen order by kitchen", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!kitchen
            item.SubItems(2) = "" & rs1.Fields!ket_kitchen
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub Form_Load()
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    koneksi
    tampil_kitchen
End Sub

Private Sub Form_Resize()
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1290
    ListView1.Height = Me.Height - 1890
End Sub
