VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form daftar_pending 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Daftar Pending Penjualan"
   ClientHeight    =   6075
   ClientLeft      =   2760
   ClientTop       =   3690
   ClientWidth     =   11145
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6075
   ScaleWidth      =   11145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   7900
      ScaleHeight     =   375
      ScaleWidth      =   3255
      TabIndex        =   2
      Top             =   0
      Width           =   3255
      Begin VB.CommandButton cmd_nav 
         Caption         =   ">>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2880
         TabIndex        =   6
         Top             =   0
         Width           =   350
      End
      Begin VB.CommandButton cmd_nav 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   2640
         TabIndex        =   5
         Top             =   0
         Width           =   255
      End
      Begin VB.CommandButton cmd_nav 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1800
         TabIndex        =   4
         Top             =   0
         Width           =   255
      End
      Begin VB.CommandButton cmd_nav 
         Caption         =   "<<"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1485
         TabIndex        =   3
         Top             =   0
         Width           =   350
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         Height          =   210
         Left            =   0
         TabIndex        =   8
         Top             =   75
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         TabIndex        =   7
         Top             =   15
         Width           =   615
      End
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4575
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5655
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   11145
      _ExtentX        =   19659
      _ExtentY        =   9975
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "No Kunjungan"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "Tanggal"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Guide"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Agency"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "Keterangan"
         Object.Width           =   4410
      EndProperty
   End
End
Attribute VB_Name = "daftar_pending"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim pageIndex As Integer
Dim pageSize As Integer
Dim pageCount As Integer
Dim recordCount As Integer
Public FormPemanggil As Form
Public Cari As String

Private Sub cmd_nav_Click(Index As Integer)
    Select Case Index
        Case 0
            If pageIndex > 1 Then
                pageIndex = 1
                tampil_item
            End If
        Case 1
            If pageIndex > 1 Then
                pageIndex = pageIndex - 1
                tampil_item
            End If
        Case 2
            If pageIndex < pageCount Then
                pageIndex = pageIndex + 1
                tampil_item
            End If
        Case 3
            If pageIndex < pageCount Then
                pageIndex = pageCount
                tampil_item
            End If
    End Select
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then Unload Me
End Sub

Private Sub Form_Load()
    pageIndex = 1
    pageSize = 100
End Sub

Private Sub Form_Activate()
    Text1.Text = Cari
    tampil_item
End Sub

Sub tampil_item()
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    j = 0
    If rs1.State <> 0 Then rs1.Close
    
    csql = "select count(kdag) from pending_head where status=0 "
    If Text1.Text <> "" Then
        csql = csql & "and no_kunj = '" & Text1.Text & "' or namaag like '%" & Text1.Text & "%' "
    End If
    
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            recordCount = Val(rs1.Fields(0))
            If recordCount > 0 Then
                pageCount = Int(recordCount / pageSize) + 1
                pageCount = IIf(pageCount = 0, 1, pageCount)
                pageIndex = IIf(pageIndex > pageCount, 1, pageIndex)
            End If
            rs1.MoveNext
        Loop
        Label1.Caption = "" & pageIndex & "/" & pageCount
        Label2.Caption = recordCount & " item"
    End If
    
    If rs1.State <> 0 Then rs1.Close
    
    csql = "select * from pending_head where status=0 "
    If Text1.Text <> "" Then
        csql = csql & "and no_kunj = '" & Text1.Text & "' or namaag like '%" & Text1.Text & "%' "
    End If
    csql = csql & "limit " & Val(pageSize * (pageIndex - 1)) & "," & pageSize
    
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = "" & rs1.Fields!no_pd
            item.SubItems(1) = "" & rs1.Fields!no_kunj
            item.SubItems(2) = "" & rs1.Fields!tgl
            item.SubItems(4) = "" & rs1.Fields!namasl
            item.SubItems(3) = "" & rs1.Fields!namaag
            item.SubItems(5) = "" & rs1.Fields!ket
            rs1.MoveNext
        Loop
    End If
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        If FormPemanggil Is penjualan_karcis Then
            penjualan_karcis.get_pendingan ListView1.SelectedItem.Text
        End If
    End If
    Unload Me
End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If ListView1.ListItems.Count > 0 Then
            If FormPemanggil Is penjualan_karcis Then
                penjualan_karcis.get_pendingan ListView1.SelectedItem.Text
            End If
        End If
        Unload Me
    End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then tampil_item
End Sub

