VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PropertyField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mkey As String
Private mValue As Variant

Public Property Get Key() As String
    Key = mkey
End Property

Public Property Let Key(ByVal Key As String)
    mkey = Key
End Property

Public Property Get Value() As Variant
    Value = mValue
End Property

Public Property Let Value(ByVal Value As Variant)
    mValue = Value
End Property

Public Function Init(Key As String, Value As Variant) As PropertyField
  Dim instance As New PropertyField
  instance.Key = Key
  instance.Value = Value
  Set Init = instance
End Function
