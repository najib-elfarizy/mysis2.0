VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form input_dp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Input Deposito"
   ClientHeight    =   3990
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3990
   ScaleWidth      =   5655
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3255
      Left            =   0
      ScaleHeight     =   3225
      ScaleWidth      =   5625
      TabIndex        =   8
      Top             =   0
      Width           =   5655
      Begin VB.TextBox Text1 
         Height          =   915
         Index           =   2
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   4
         Top             =   2160
         Width           =   3975
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "input_dp.frx":0000
         Left            =   1440
         List            =   "input_dp.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1500
         Width           =   3975
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   1080
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   661
         _Version        =   393216
         Format          =   84082689
         CurrentDate     =   43234
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   0
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   3
         Text            =   "0.00"
         Top             =   1850
         Width           =   3975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Keterangan : "
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   15
         Top             =   2200
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Nominal : "
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   14
         Top             =   1850
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Agen : "
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   13
         Top             =   1500
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   525
         Index           =   0
         Left            =   -240
         TabIndex        =   12
         Top             =   0
         Width           =   5895
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi : "
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   11
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   5280
         Width           =   6135
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal : "
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   1080
         Width           =   1095
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   5625
      TabIndex        =   5
      Top             =   3240
      Width           =   5655
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   1440
         TabIndex        =   6
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_dp.frx":0004
         PICN            =   "input_dp.frx":0020
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4200
         TabIndex        =   7
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_dp.frx":017A
         PICN            =   "input_dp.frx":0196
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "input_dp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public agency As New Collection

Public Property Let Agen(kode As String)
    Dim item As PropertyField
    For Each item In agency
        If kode = item.Key Then
            Combo1.Text = item.Key & Space(3) & item.Value
        End If
    Next item
End Property

Private Sub cmd_keluar_Click()
    Unload Me
End Sub

Private Sub cmd_simpan_Click()
    On Error GoTo exc
    If Text1(0).Text <> "" And Text1(1).Text <> "" And Combo1.Text <> "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from kartu_deposit where nota='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs1.EOF Then
            csql = "update kartu_deposit set kdag='" & agency(Combo1.ListIndex + 1).Key & "',namaag='" & agency(Combo1.ListIndex + 1).Value & _
            "',tgl='" & Format(DTPicker1.Value, "yyyy-mm-dd") & "', masuk=" & Format(Text1(1).Text, "##0.00") & ",ket='" & Text1(2).Text & _
            "',usr='" & xy & "' where nota='" & Text1(0).Text & "'"
        Else
            csql = "insert into kartu_deposit (nota,kdag,namaag,tgl,masuk,ket,usr) values ('" & Text1(0).Text & "','" & agency(Combo1.ListIndex + 1).Key & _
            "','" & agency(Combo1.ListIndex + 1).Value & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & Format(Text1(1).Text, "##0.00") & ",'" & _
            Text1(2).Text & "','" & xy & "')"
        End If
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        
        MsgBox ("Simpan sukses!"), vbInformation, "Info"
        If daftar_deposit.Visible = True Then
            daftar_deposit.tampil_dp
        End If
        Unload Me
    Else
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    Text1(0).Text = ""
    On Error GoTo exc
    Me.KeyPreview = True
    
    DTPicker1.Value = Date
    get_agen
    Exit Sub
exc:
    MsgBox ("error")
    
End Sub

Private Sub Text1_GotFocus(Index As Integer)
    Select Case Index
        Case 0, 1
            With Text1(Index)
            .SelStart = 0
            .SelLength = Len(.Text)
            End With
    End Select
End Sub

Public Sub otomatis()
    Text1(0).Text = Format(Date, "yymmdd") & Format(Time, "HHmmss")
End Sub

Private Sub get_agen()
    On Error GoTo exc
    
    Combo1.Clear
    Set agency = New Collection
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from magen order by kdag asc", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim Agen As PropertyField
        Do While Not rs1.EOF
            Combo1.AddItem rs1.Fields!kdag & Space(3) & rs1.Fields!namaag
            
            Set Agen = New PropertyField
            Agen.Key = rs1.Fields!kdag
            Agen.Value = rs1.Fields!namaag
            
            agency.Add Agen
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Text1_LostFocus(Index As Integer)
    If Index = 1 Then Text1(1).Text = Format(Text1(1).Text, "#,##0.00")
End Sub
