VERSION 5.00
Begin VB.Form detail_paket 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detail Paket"
   ClientHeight    =   7365
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   9885
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7365
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MySIS.Button ButtonTambah 
      Height          =   375
      Left            =   7560
      TabIndex        =   2
      Top             =   6960
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   2
      TX              =   "&Pilih"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   15790320
      BCOLO           =   15790320
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "detail_paket.frx":0000
      PICN            =   "detail_paket.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6855
      Left            =   6240
      ScaleHeight     =   6825
      ScaleWidth      =   3615
      TabIndex        =   1
      Top             =   0
      Width           =   3650
      Begin VB.TextBox TextKeterangan 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   4
         Text            =   "detail_paket.frx":04B6
         Top             =   2280
         Width           =   3375
      End
      Begin VB.Label nama 
         AutoSize        =   -1  'True
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   3255
         WordWrap        =   -1  'True
      End
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "&Tutup"
      Height          =   375
      Left            =   8640
      TabIndex        =   0
      Top             =   6960
      Width           =   1250
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   6855
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   6135
   End
End
Attribute VB_Name = "detail_paket"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim item As ItemBox

Public Property Get ItemBox() As ItemBox
    Set ItemBox = item
End Property

Public Property Let ItemBox(ByVal ItemBox As ItemBox)
    Set item = ItemBox
    Set Image1.picture = item.picture
    nama.Caption = item.nama & vbNewLine & vbNewLine & item.matauang & " " & Format(item.harga, "#,##0.00")
    TextKeterangan.Text = item.keterangan
End Property

Private Sub ButtonTambah_Click()
    penjualan_karcis.tambah_item ItemBox
    Unload Me
End Sub

Private Sub CancelButton_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then Unload Me
End Sub
