VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form daftar_perkiraan 
   Caption         =   "Daftar Perkiraan"
   ClientHeight    =   7725
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   16470
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7725
   ScaleWidth      =   16470
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Tambah / Edit Perkiraan"
      Height          =   4335
      Left            =   9600
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   6735
      Begin VB.CommandButton cmd_close 
         Caption         =   "Tutup"
         Height          =   495
         Left            =   120
         TabIndex        =   24
         Top             =   3720
         Width           =   1215
      End
      Begin VB.CommandButton cmd_simpan 
         Caption         =   "Simpan"
         Height          =   495
         Left            =   5400
         TabIndex        =   23
         Top             =   3720
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         Height          =   3255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   6495
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   1440
            TabIndex        =   15
            Top             =   2280
            Width           =   4815
         End
         Begin VB.CheckBox Check1 
            Height          =   195
            Left            =   1440
            TabIndex        =   14
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   2160
            MaxLength       =   4
            TabIndex        =   13
            Top             =   1920
            Width           =   1335
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            MaxLength       =   1
            TabIndex        =   12
            Top             =   1920
            Width           =   495
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   1560
            Width           =   4815
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   1200
            Width           =   4815
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Detail (Perkiraan dapat diposting)"
            Height          =   255
            Left            =   1440
            TabIndex        =   9
            Top             =   720
            Width           =   3495
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Header (Perkiraan tidak dapat diposting)"
            Height          =   255
            Left            =   1440
            TabIndex        =   8
            Top             =   360
            Value           =   -1  'True
            Width           =   3495
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "-"
            Height          =   255
            Index           =   6
            Left            =   1920
            TabIndex        =   22
            Top             =   1920
            Width           =   255
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Kas/Bank :"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   21
            Top             =   2640
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Nama :"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   20
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Kode perkiraan :"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   19
            Top             =   1920
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Posisi dibawah :"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   18
            Top             =   1560
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Kelompok :"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   17
            Top             =   1200
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Tipe Perkiraan"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   16
            Top             =   360
            Width           =   1335
         End
      End
      Begin VB.Image Image1 
         Height          =   300
         Left            =   6000
         Picture         =   "daftar_perkiraan.frx":0000
         Top             =   0
         Width           =   675
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6975
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   12303
      _Version        =   393216
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Aktiva"
      TabPicture(0)   =   "daftar_perkiraan.frx":040B
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ListView1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Kewajiban"
      TabPicture(1)   =   "daftar_perkiraan.frx":0427
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ListView1(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Modal"
      TabPicture(2)   =   "daftar_perkiraan.frx":0443
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "ListView1(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Pendapatan"
      TabPicture(3)   =   "daftar_perkiraan.frx":045F
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "ListView1(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "HPP"
      TabPicture(4)   =   "daftar_perkiraan.frx":047B
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "ListView1(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Biaya"
      TabPicture(5)   =   "daftar_perkiraan.frx":0497
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "ListView1(5)"
      Tab(5).ControlCount=   1
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   0
         Left            =   120
         TabIndex        =   25
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   1
         Left            =   -74880
         TabIndex        =   26
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   2
         Left            =   -74880
         TabIndex        =   27
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   3
         Left            =   -74880
         TabIndex        =   28
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   4
         Left            =   -74880
         TabIndex        =   29
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6375
         Index           =   5
         Left            =   -74880
         TabIndex        =   30
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   11245
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   4057
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "Tipe"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "Kas"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   9345
      TabIndex        =   0
      Top             =   6960
      Width           =   9375
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4080
         TabIndex        =   1
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_perkiraan.frx":04B3
         PICN            =   "daftar_perkiraan.frx":04CF
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   1440
         TabIndex        =   2
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_perkiraan.frx":0905
         PICN            =   "daftar_perkiraan.frx":0921
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_hapus 
         Height          =   495
         Left            =   2760
         TabIndex        =   3
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Hapus"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_perkiraan.frx":0A7B
         PICN            =   "daftar_perkiraan.frx":0A97
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_perkiraan.frx":0BF1
         PICN            =   "daftar_perkiraan.frx":0C0D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "daftar_perkiraan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim spasi As Integer
Dim parent As String
Dim kdss, kdgr, jnss As String

Private Sub cmd_hapus_Click()
    If ListView1(SSTab1.Tab).SelectedItem Is Nothing Then Exit Sub
    
    kdgr = SSTab1.Tab + 1 & "-0000"
    kdss = Trim(ListView1(SSTab1.Tab).SelectedItem.SubItems(1))
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where kelompok='" & kdss & "'", con, adOpenKeyset, adLockOptimistic
    
    If rs1.EOF Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from jour_det where kode='" & kdss & "' limit 1", con, adOpenKeyset, adLockOptimistic
        If rs1.EOF Then
            pesan = MsgBox("Hapus akun " & ListView1(SSTab1.Tab).SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "delete from makun where kode='" & kdss & "'", con, adOpenKeyset, adLockOptimistic
                
                Text2.Text = ""
                tampil_akun SSTab1.Tab
            End If
        Else
            MsgBox ("Kode akun tidak bisa dihapus karena sudah memiliki transaksi jurnal!"), vbInformation, "Info"
        End If
    Else
        MsgBox ("Tidak bisa dihapus, akun header dan masih isi akun detail dibawahnya!"), vbInformation, "Info"
    End If
End Sub

Private Sub cmd_edit_Click()
    If ListView1(SSTab1.Tab).SelectedItem Is Nothing Then Exit Sub
    If ListView1(SSTab1.Tab).SelectedItem.Index > 1 Then
        Dim i As Integer
        For i = 0 To Combo1.ListCount - 1
            If Left(Combo1.List(i), 6) = ListView1(SSTab1.Tab).SelectedItem.Text Then
                Combo1.ListIndex = i
                Exit For
            End If
        Next i
        For i = 0 To Combo2.ListCount - 1
            If Left(Combo2.List(i), 6) = ListView1(SSTab1.Tab).SelectedItem.Tag Then
                Combo2.ListIndex = i
                Exit For
            End If
        Next i
        
        Text1.Text = Left(Combo1.Text, 1)
        Text2.Text = Right(ListView1(SSTab1.Tab).SelectedItem.SubItems(1), 4)
        Text3.Text = ListView1(SSTab1.Tab).SelectedItem.SubItems(2)
        Option1.Value = ListView1(SSTab1.Tab).SelectedItem.SubItems(3) = "H"
        Option2.Value = ListView1(SSTab1.Tab).SelectedItem.SubItems(3) = "D"
        Check1.Value = IIf(ListView1(SSTab1.Tab).SelectedItem.SubItems(4) = "Kas", 1, 0)
        
        Frame1.Visible = True
        Frame1.Top = SSTab1.Height / 2 - Frame1.Height / 2
        Frame1.Left = SSTab1.Width / 2 - Frame1.Width / 2
        Picture1.Enabled = False
        SSTab1.Enabled = False
    End If
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_tambah_Click()
    Option2.Value = True
    Check1.Enabled = True
    Check1.Value = 0
    On Error Resume Next
    Combo1.ListIndex = 0
    Combo2.ListIndex = -1
    Text2.Text = ""
    Text3.Text = ""
    Frame1.Visible = True
    Frame1.Top = SSTab1.Height / 2 - Frame1.Height / 2
    Frame1.Left = SSTab1.Width / 2 - Frame1.Width / 2
    Picture1.Enabled = False
    SSTab1.Enabled = False
End Sub

Private Sub cmd_simpan_Click()
    jnss = IIf(Option1.Value = True, 0, 1)
    
    If Combo1.Text <> "" And Combo2.Text <> "" And Text1.Text <> "" And Text2.Text <> "" And Text3.Text <> "" Then
        kdss = Text1.Text & "-" & Text2.Text
        kdgr = Text1.Text & "-0000"
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from makun where kode='" & kdss & "'", con, adOpenKeyset, adLockOptimistic
        If rs1.EOF Then
            pesan = MsgBox("Simpan data akun baru?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "insert into makun (kode,nama,jenis,kas,kelompok,grup,bo) values ('" & kdss & "','" & Text3.Text & "','" & jnss & "','" & Check1.Value & "','" & Left(Combo2.Text, 6) & "','" & kdgr & "','" & xx & "')", con, adOpenKeyset, adLockOptimistic
                Text2.Text = ""
                Text3.Text = ""
            End If
        Else
            pesan = MsgBox("Kode akun sudah ada, simpan perubahan?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "update makun set nama='" & Text3.Text & "',jenis='" & jnss & "', grup='" & kdgr & "',kas='" & Check1.Value & "',kelompok='" & Left(Combo2.Text, 6) & "',bo='" & xx & "' WHERE kode='" & kdss & "'", con, adOpenKeyset, adLockOptimistic
                Text2.Text = ""
                Text3.Text = ""
            End If
        End If
        
        tampil_akun SSTab1.Tab
    Else
        MsgBox ("Lengkapi data terlebih dahulu"), vbInformation, "Info"
    End If
End Sub

Private Sub cmd_close_Click()
    Frame1.Visible = False
    Picture1.Enabled = True
    SSTab1.Enabled = True
End Sub

Private Sub Image1_Click()
    Frame1.Visible = False
    Picture1.Enabled = True
    SSTab1.Enabled = True
End Sub

Private Sub Option1_Click()
    If Text1.Text = "1" And Option2.Value = True Then
        Check1.Enabled = True
    Else
        Check1.Enabled = False
        Check1.Value = 0
    End If
End Sub

Private Sub Option2_Click()
    If Text1.Text = "1" And Option2.Value = True Then
        Check1.Enabled = True
    Else
        Check1.Enabled = False
        Check1.Value = 0
    End If
End Sub

Private Sub Combo1_Click()
    Combo2.Clear
    Combo2.AddItem Combo1.Text
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where left(kode,1)='" & Left(Combo1.Text, 1) & "' and jenis=0 order by kode", con, adOpenKeyset, adLockOptimistic
    
    Do While Not rs1.EOF
        Combo2.AddItem (rs1.Fields!kode & "   " & rs1.Fields!nama)
        rs1.MoveNext
    Loop
    Text1.Text = Left(Combo1.Text, 1)
End Sub

Private Sub Text1_Change()
    Text2.Text = ""
    Text3.Text = ""
    Check1.Value = 0
    If Text1.Text = "1" And Option2.Value = True Then
        Check1.Enabled = True
    Else
        Check1.Enabled = False
        Check1.Value = 0
    End If
End Sub

Private Sub Form_Load()
    tampil_akun 0
    tampil_kelompok
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture1.Top = Me.Height - 1305
    Picture1.Width = Me.Width - Me.Width * 0.3
    SSTab1.Height = Me.Height - 1250
    SSTab1.Width = Me.Width - Me.Width * 0.3
    
    For z = 0 To 5
        ListView1(z).Width = SSTab1.Width - 300
        ListView1(z).Height = SSTab1.Height - 600
    Next z
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    tampil_akun SSTab1.Tab
End Sub

Sub tampil_kelompok()
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun_group order by kode", con, adOpenKeyset, adLockOptimistic
    Do While Not rs1.EOF
        Combo1.AddItem (rs1.Fields!kode & "   " & rs1.Fields!nama)
        rs1.MoveNext
    Loop
End Sub

Sub tampil_akun(Index As Integer)
    Dim kelompok As String
    Select Case Index
        Case 0
            kelompok = "1-0000"
        Case 1
            kelompok = "2-0000"
        Case 2
            kelompok = "3-0000"
        Case 3
            kelompok = "4-0000"
        Case 4
            kelompok = "5-0000"
        Case 5
            kelompok = "6-0000"
    End Select
    
    ListView1(Index).ListItems.Clear
    ListView1(Index).View = lvwReport
    
    i = 1
    Set item = ListView1(Index).ListItems.Add(, , i)
    item.Text = i
    item.SubItems(1) = kelompok
    item.SubItems(2) = UCase(SSTab1.Caption)
    item.SubItems(3) = "H"
    item.SubItems(4) = ""
    
    Dim record As New Recordset
    record.Open "select * from makun where kelompok='" & kelompok & "' order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not record.EOF Then
        Do While Not record.EOF
            i = i + 1
            spasi = 4
            Set item = ListView1(Index).ListItems.Add(, , i)
            item.Text = kelompok
            item.Tag = record.Fields!kelompok
            item.SubItems(1) = Space(spasi) & record.Fields!kode
            item.SubItems(2) = "" & record.Fields!nama
            item.SubItems(3) = IIf(record.Fields!jenis = 0, "H", "D")
            item.SubItems(4) = IIf(record.Fields!kas = 1, "Kas", "")
            
            get_akuntree Index, spasi + 4, record.Fields!kode, kelompok
            record.MoveNext
        Loop
    End If
    
    If record.State <> 0 Then record.Close
End Sub

Sub get_akuntree(Index As Integer, spasi As Integer, kode As String, kelompok As String)
    Dim record As New Recordset
    record.Open "select * from makun where kelompok='" & kode & "' order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not record.EOF Then
        Do While Not record.EOF
            i = i + 1
            Set item = ListView1(Index).ListItems.Add(, , i)
            
            item.Text = kelompok
            item.Tag = record.Fields!kelompok
            item.SubItems(1) = Space(spasi) & record.Fields!kode
            item.SubItems(2) = "" & record.Fields!nama
            item.SubItems(3) = IIf(record.Fields!jenis = 0, "H", "D")
            item.SubItems(4) = IIf(record.Fields!kas = 1, "Kas", "")
            
            get_akuntree Index, spasi + 4, record.Fields!kode, kelompok
            record.MoveNext
        Loop
        If record.State <> 0 Then record.Close
    End If
End Sub
