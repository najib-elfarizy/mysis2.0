VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form setting_perkiraan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setting Perkiraan"
   ClientHeight    =   8790
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10950
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8790
   ScaleWidth      =   10950
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      ScaleHeight     =   465
      ScaleWidth      =   10905
      TabIndex        =   4
      Top             =   0
      Width           =   10935
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   60
         Width           =   3615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept/Gudang : "
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   6
         Top             =   100
         Width           =   1095
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7455
      Left            =   0
      TabIndex        =   3
      Top             =   600
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   13150
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Pembelian"
      TabPicture(0)   =   "settiing_perkiraan.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Picture1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Penjualan"
      TabPicture(1)   =   "settiing_perkiraan.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Picture2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Hutang Piutang"
      TabPicture(2)   =   "settiing_perkiraan.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Persediaan"
      TabPicture(3)   =   "settiing_perkiraan.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Lain-lain"
      TabPicture(4)   =   "settiing_perkiraan.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   6855
         Left            =   -74880
         ScaleHeight     =   6825
         ScaleWidth      =   8025
         TabIndex        =   25
         Top             =   480
         Width           =   8055
         Begin VB.ComboBox cmb_pjl_kredit 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   35
            Top             =   2280
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pjk_pjl 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   34
            Top             =   4800
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pjl_tunai 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   33
            Top             =   840
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pjl_kartu 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   32
            Top             =   1200
            Width           =   5415
         End
         Begin VB.ComboBox cmb_dp 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   31
            Top             =   2640
            Width           =   5415
         End
         Begin VB.ComboBox cmb_hpp_putus 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   30
            Top             =   5880
            Width           =   5415
         End
         Begin VB.ComboBox cmb_hpp_kons 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   29
            Top             =   6240
            Width           =   5415
         End
         Begin VB.ComboBox cmb_fee_pjl 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   28
            Top             =   3840
            Width           =   5415
         End
         Begin VB.ComboBox cmb_rtr_pjl 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   27
            Top             =   3000
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pjl 
            Height          =   315
            Left            =   2400
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   480
            Width           =   5415
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Data Akun HPP Penjualan"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   6
            Left            =   0
            TabIndex        =   50
            Top             =   5280
            Width           =   7935
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Data Akun Pajak Penjualan"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   7
            Left            =   0
            TabIndex        =   49
            Top             =   4320
            Width           =   7935
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Penjualan Kredit"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   48
            Top             =   2280
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pajak Penjualan"
            Height          =   255
            Index           =   11
            Left            =   240
            TabIndex        =   47
            Top             =   4800
            Width           =   2055
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Penjualan Kredit"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   8
            Left            =   0
            TabIndex        =   46
            Top             =   1800
            Width           =   7935
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Penjualan Tunai"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   45
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Charge/cas EDC"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   44
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Uang Muka/DP"
            Height          =   255
            Left            =   240
            TabIndex        =   43
            Top             =   2640
            Width           =   2055
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Penjualan Kasir"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   9
            Left            =   0
            TabIndex        =   42
            Top             =   0
            Width           =   7935
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "HPP Putus"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   41
            Top             =   5880
            Width           =   2055
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "HPP Konsinyasi"
            Height          =   255
            Index           =   10
            Left            =   240
            TabIndex        =   40
            Top             =   6240
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Fee Penjualan"
            Height          =   255
            Index           =   10
            Left            =   240
            TabIndex        =   39
            Top             =   3840
            Width           =   2055
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Fee Penjualan"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   12
            Left            =   0
            TabIndex        =   38
            Top             =   3360
            Width           =   7935
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Return Penjualan"
            Height          =   255
            Index           =   15
            Left            =   240
            TabIndex        =   37
            Top             =   3000
            Width           =   2055
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pendapatan Jual"
            Height          =   255
            Index           =   20
            Left            =   240
            TabIndex        =   36
            Top             =   540
            Width           =   2055
         End
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   6855
         Left            =   120
         ScaleHeight     =   6825
         ScaleWidth      =   7785
         TabIndex        =   7
         Top             =   480
         Width           =   7815
         Begin VB.ComboBox cmb_pbl_bonus 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   1680
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pbl_kontan 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   1320
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pbl_krd 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   600
            Width           =   5415
         End
         Begin VB.ComboBox cmb_pajak_masukan 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   3840
            Width           =   5415
         End
         Begin VB.ComboBox cmb_retur 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   2760
            Width           =   5415
         End
         Begin VB.ComboBox cmb_htg_pajak 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   4200
            Width           =   5415
         End
         Begin VB.ComboBox cmbPbltunai 
            Height          =   315
            Left            =   2160
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   960
            Width           =   5415
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Data Akun Pembelian"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   9
            Left            =   0
            TabIndex        =   24
            Top             =   0
            Width           =   7935
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pajak Masukan"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   23
            Top             =   4200
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Bonus Qty Pembelian"
            Height          =   255
            Index           =   3
            Left            =   0
            TabIndex        =   22
            Top             =   1680
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pembelian Kontan/Bonus"
            Height          =   255
            Index           =   4
            Left            =   0
            TabIndex        =   21
            Top             =   1320
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pembelian Kredit"
            Height          =   255
            Index           =   6
            Left            =   0
            TabIndex        =   20
            Top             =   600
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Data Akun Retur Pembelian"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   8
            Left            =   0
            TabIndex        =   19
            Top             =   2280
            Width           =   7935
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pdt. Pajak Masukan"
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   18
            Top             =   3840
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Retur Pembelian"
            Height          =   255
            Index           =   2
            Left            =   0
            TabIndex        =   17
            Top             =   2760
            Width           =   2055
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFC0&
            Caption         =   "Data Akun Pajak Masukan"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   405
            Index           =   7
            Left            =   -120
            TabIndex        =   16
            Top             =   3360
            Width           =   7935
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Pembelian Tunai"
            Height          =   255
            Index           =   5
            Left            =   0
            TabIndex        =   15
            Top             =   960
            Width           =   2055
         End
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   10905
      TabIndex        =   0
      Top             =   8040
      Width           =   10935
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "settiing_perkiraan.frx":008C
         PICN            =   "settiing_perkiraan.frx":00A8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   1440
         TabIndex        =   2
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "settiing_perkiraan.frx":0202
         PICN            =   "settiing_perkiraan.frx":021E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "setting_perkiraan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_keluar_Click()
    Unload Me
End Sub

Private Sub cmd_simpan_Click()
    'simpan
End Sub

Private Sub Form_Load()
    kosong
    get_cabang
    tampil_akun
End Sub

Private Sub kosong()
'    Combo1.Clear
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Private Sub tampil_akun()
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis=1", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            cmb_pbl_krd.AddItem rs1.Fields!kode & Space(5) & rs1.Fields!nama
            rs1.MoveNext
        Loop
        Combo1.ListIndex = 0
    End If
End Sub

