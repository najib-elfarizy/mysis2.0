VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form daftar_matauang 
   Caption         =   "Nilai Tukar"
   ClientHeight    =   5685
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   7875
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   5685
   ScaleWidth      =   7875
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   0
      ScaleHeight     =   4905
      ScaleWidth      =   7785
      TabIndex        =   4
      Top             =   0
      Width           =   7815
      Begin VB.PictureBox Picture4 
         Height          =   585
         Left            =   120
         ScaleHeight     =   525
         ScaleWidth      =   7515
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   480
         Width           =   7575
         Begin VB.TextBox Text1 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            Height          =   315
            Index           =   0
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   0
            TabStop         =   0   'False
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox Text1 
            Alignment       =   2  'Center
            Height          =   315
            Index           =   1
            Left            =   480
            TabIndex        =   1
            Top             =   240
            Width           =   3345
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            Height          =   315
            Index           =   2
            Left            =   3810
            TabIndex        =   2
            Top             =   240
            Width           =   3705
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   3
            Left            =   0
            TabIndex        =   15
            Top             =   0
            Width           =   500
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   " Mata Uang"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   495
            TabIndex        =   14
            Top             =   0
            Width           =   3330
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Nilai Tukar"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   2
            Left            =   3825
            TabIndex        =   13
            Top             =   0
            Width           =   3690
         End
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   3735
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   6588
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "No"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Mata Uang"
            Object.Width           =   5821
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Nilai Tukar"
            Object.Width           =   6526
         EndProperty
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   405
         Left            =   -240
         TabIndex        =   6
         Top             =   0
         Width           =   8055
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   5280
         Width           =   6135
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   7785
      TabIndex        =   3
      Top             =   4920
      Width           =   7815
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4080
         TabIndex        =   8
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_matauang.frx":0000
         PICN            =   "daftar_matauang.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   1440
         TabIndex        =   9
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_matauang.frx":0452
         PICN            =   "daftar_matauang.frx":046E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_hapus 
         Height          =   495
         Left            =   2760
         TabIndex        =   10
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Hapus"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_matauang.frx":05C8
         PICN            =   "daftar_matauang.frx":05E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_matauang.frx":073E
         PICN            =   "daftar_matauang.frx":075A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "daftar_matauang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub jcbutton1_Click()
    Unload Me
End Sub

Private Sub cmd_tambah_Click()
    kosong
End Sub

Private Sub cmd_edit_Click()
    If ListView1.ListItems.Count > 0 Then
        Label1.Caption = "Edit Kurs"
        Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        Text1(2).Text = ListView1.SelectedItem.SubItems(3)
        Text1(1).SetFocus
    End If
End Sub

Private Sub cmd_hapus_Click()
    On Error GoTo exc
    pesan = MsgBox("Hapus Nilai Tukar Mata Uang " & ListView1.SelectedItem.SubItems(2) & "?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from kurs where currency='" & ListView1.SelectedItem.SubItems(2) & "'", con, adOpenKeyset, adLockOptimistic
        tampil_kurs
        kosong
    End If
    Exit Sub
exc:
End Sub

Sub kosong()
    Label1.Caption = "Kurs Baru"
    Text1(1).Text = ""
    Text1(2).Text = ""
    Text1(1).SetFocus
End Sub

Sub tampil_kurs()
    On Error GoTo exc
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from kurs", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = j
            item.SubItems(2) = "" & UCase(rs1.Fields!currency)
            item.SubItems(3) = Format(rs1.Fields!nilai, "#,##0.00")
        rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub Form_Activate()
    kosong
End Sub

Private Sub Form_Load()
    'cmd_tambah.PictureNormal (MDIForm1.ImageList1.ListImages(2).Picture)
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    koneksi
    tampil_kurs
End Sub

Private Sub Form_Resize()
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1290
    ListView1.Height = Me.Height - 1890
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        Label1.Caption = "Edit Kurs"
        Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        Text1(2).Text = ListView1.SelectedItem.SubItems(3)
        Text1(1).SetFocus
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If Text1(Index).Text = "" Then Exit Sub
    If KeyAscii = 13 Then
        If Index = 1 Then
            KeyAscii = 0
            SendKeys "{tab}"
        ElseIf Index = 2 Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select * from kurs where currency='" & Text1(1).Text & "'", con, adOpenKeyset, adLockOptimistic
            
            If Not rs1.EOF Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "update kurs set nilai=" & Format(Text1(2).Text, "##0.00") & ",usr='" & xy & "' where currency='" & Text1(1).Text & "'", con, adOpenKeyset, adLockOptimistic
            Else
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "insert into kurs (currency,nilai,usr) values ('" & Text1(1).Text & "'," & Format(Text1(2).Text, "##0.00") & ",'" & xy & "')", con, adOpenKeyset, adLockOptimistic
            End If
            
            kosong
            tampil_kurs
            MsgBox ("Simpan sukses!"), vbInformation, "Info"
        End If
    End If
End Sub
