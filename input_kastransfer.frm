VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form input_kastransfer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Input Kas Transfer"
   ClientHeight    =   4335
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   5655
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3615
      Left            =   0
      ScaleHeight     =   3585
      ScaleWidth      =   5625
      TabIndex        =   9
      Top             =   0
      Width           =   5655
      Begin VB.ComboBox Combo2 
         Height          =   315
         ItemData        =   "input_kastransfer.frx":0000
         Left            =   1440
         List            =   "input_kastransfer.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1850
         Width           =   3975
      End
      Begin VB.TextBox Text1 
         Height          =   915
         Index           =   2
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   5
         Top             =   2520
         Width           =   3975
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "input_kastransfer.frx":0004
         Left            =   1440
         List            =   "input_kastransfer.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1500
         Width           =   3975
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   1080
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   104923139
         CurrentDate     =   43234
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   0
         TabStop         =   0   'False
         Text            =   "Text1"
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   4
         Text            =   "0.00"
         Top             =   2200
         Width           =   3975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Transfer Ke : "
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   17
         Top             =   1850
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Keterangan : "
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   16
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah : "
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   15
         Top             =   2200
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dari Akun : "
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   14
         Top             =   1500
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   525
         Index           =   0
         Left            =   0
         TabIndex        =   13
         Top             =   0
         Width           =   5655
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi : "
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   5280
         Width           =   6135
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal : "
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   1095
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   5625
      TabIndex        =   8
      Top             =   3600
      Width           =   5655
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   1440
         TabIndex        =   6
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_kastransfer.frx":0008
         PICN            =   "input_kastransfer.frx":0024
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4200
         TabIndex        =   7
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "input_kastransfer.frx":017E
         PICN            =   "input_kastransfer.frx":019A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "input_kastransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Akun1 As New Collection
Dim Akun2 As New Collection
Dim kas1, kas2 As PropertyField
Public edit As Boolean

Public Property Let AkunDari(kode As String)
    Dim item As PropertyField
    For Each item In Akun1
        If kode = item.Key Then
            Combo1.Text = item.Key & Space(3) & item.Value
            Exit For
        End If
    Next item
End Property

Public Property Let AkunTujuan(kode As String)
    Dim item As PropertyField
    For Each item In Akun2
        If kode = item.Key Then
            Combo2.Text = item.Key & Space(3) & item.Value
            Exit For
        End If
    Next item
End Property

Private Sub cmd_keluar_Click()
    Unload Me
End Sub

Private Sub cmd_simpan_Click()
    On Error GoTo exc
    If Text1(1).Text = "" Or Text1(2).Text = "" Or Combo1.Text = "" Or Combo2.Text = "" And Combo1.Text <> Combo2.Text Then
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
        Exit Sub
    End If
    
    If Val(Format(Text1(1).Text, "#,##0.00")) = 0 Then
        MsgBox ("Isi nilai transfer!"), vbInformation, "Peringatan"
    Else
        pesan = MsgBox("Simpan jurnal kas transfer?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If edit = True Then
                If rs1.State <> 0 Then rs1.Close
                rs1.Open "delete from jour_head where vc='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            Else
                otomatis
            End If
            
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "insert into jour_head (TGL,VC,JENIS,KET,NILAI,BO,USR) values ('" & Format(DTPicker1.Value, "yyyy-MM-dd") & "','" & Text1(0).Text & "','KAS TRANSFER','" & Text1(2).Text & "','" & Format(Text1(1).Text, "##0.00") & "','" & xx & "','" & xy & "')", con, adOpenKeyset, adLockOptimistic
            
            Set kas1 = Akun1(Combo1.ListIndex + 1)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "insert into jour_det (TGL,VC,KODE,NAMA,KET,KREDIT,BO,USR,URUT) values ('" & Format(DTPicker1.Value, "yyyy-MM-dd") & "','" & Text1(0).Text & "','" & kas1.Key & "','" & kas1.Value & "','" & Text1(2).Text & "','" & Format(Text1(1).Text, "##0.00") & "','" & xx & "','" & xy & "','1')", con, adOpenKeyset, adLockOptimistic
            
            Set kas2 = Akun2(Combo2.ListIndex + 1)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "insert into jour_det (TGL,VC,KODE,NAMA,KET,DEBET,BO,USR,URUT) values ('" & Format(DTPicker1.Value, "yyyy-MM-dd") & "','" & Text1(0).Text & "','" & kas2.Key & "','" & kas2.Value & "','" & Text1(2).Text & "','" & Format(Text1(1).Text, "##0.00") & "','" & xx & "','" & xy & "','2')", con, adOpenKeyset, adLockOptimistic
            
            MsgBox ("Data telah disimpan"), vbInformation, "Info"
            
            On Error Resume Next
            otomatis
            edit = False
            Text1(1).Text = "0"
            Text1(2).Text = ""
            daftar_kastransfer.tampil_kas
        End If
    End If
    Exit Sub
exc:
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "delete from jour_head where vc='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    MsgBox ("Data gagal disimpan"), vbInformation, "Info"
End Sub

Private Sub Form_Load()
    Text1(0).Text = ""
    On Error GoTo exc
    Me.KeyPreview = True
    
    DTPicker1.Value = Date
    edit = False
    get_akun
    Exit Sub
exc:
    MsgBox ("error")
    
End Sub

Private Sub Text1_GotFocus(Index As Integer)
    Select Case Index
        Case 0, 1
            With Text1(Index)
            .SelStart = 0
            .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text1_LostFocus(Index As Integer)
    If Index = 1 Then Text1(1).Text = Format(Text1(1).Text, "#,##0")
End Sub

Private Sub get_akun()
    On Error GoTo exc
    
    Combo1.Clear
    Combo2.Clear
    Set Akun1 = New Collection
    Set Akun2 = New Collection
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where kas=1 order by kode asc", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim akun As PropertyField
        Do While Not rs1.EOF
            Combo1.AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            Combo2.AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            
            Set akun = New PropertyField
            akun.Key = rs1.Fields!kode
            akun.Value = rs1.Fields!nama
            
            Akun1.Add akun
            Akun2.Add akun
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Public Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol1, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Transfer Kas'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = xx
        
        kol3 = rs1.Fields!kol3
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = xx
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = xx
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = xx
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(vc) as nota from jour_head where length(vc)=" & j + z & " and right(vc," & z & ")='" & nomor_nota & "' and left(vc," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(vc) as nota from po_head where length(vc)=" & j + z & " and left(vc," & z & ")='" & nomor_nota & "' and right(vc," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
    End If
    Exit Sub
exc:
    MsgBox "Error : " & err.Description
End Sub

