VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "crviewer.dll"
Begin VB.Form lap_perkiraan 
   Caption         =   "Daftar Akun"
   ClientHeight    =   6855
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11040
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11040
   Begin CRVIEWERLibCtl.CRViewer CRViewer1 
      Height          =   6855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11055
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   0   'False
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "lap_perkiraan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim akun As ADODB.Recordset

Public Sub ShowReport()
'    On Error GoTo exc
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
        
    If fso.FileExists(App.Path & "\Report\lap_perkiraan.rpt") Then
        Set cryApp = New CRAXDRT.Application
        Set rptApp = cryApp.OpenReport(App.Path & "\Report\lap_perkiraan.rpt")
        rptApp.EnableParameterPrompting = False
        
        create_akun "1-0000"
        create_akun "2-0000"
        create_akun "3-0000"
        create_akun "4-0000"
        create_akun "5-0000"
        create_akun "6-0000"
        rptApp.Database.SetDataSource akun
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mperusahaan limit 1", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            On Error Resume Next
            rptApp.ParameterFields.GetItemByName("NAMA_PERUSAHAAN").AddCurrentValue "" & rs1.Fields!NAMA_PERUSAHAAN
            rptApp.ParameterFields.GetItemByName("ALAMAT1").AddCurrentValue "" & rs1.Fields!alamat1
            rptApp.ParameterFields.GetItemByName("ALAMAT2").AddCurrentValue "" & rs1.Fields!alamat2
            rptApp.ParameterFields.GetItemByName("TELP").AddCurrentValue "" & rs1.Fields!telp
            rptApp.ParameterFields.GetItemByName("FAX").AddCurrentValue "" & rs1.Fields!fax
            rptApp.ParameterFields.GetItemByName("NPWP").AddCurrentValue "" & rs1.Fields!npwp
        End If
        
        rptApp.ParameterFields.GetItemByName("USER").AddCurrentValue "" & xy
        
        CRViewer1.ReportSource = rptApp
        CRViewer1.ViewReport
        
        Do While CRViewer1.IsBusy
            DoEvents
        Loop
          
        CRViewer1.Zoom 1
    Else
        MsgBox "Report File Doesn't Exist!"
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub create_akun(kelompok As String)
    Select Case kelompok
        Case "1-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "AKTIVA", "", "")
        Case "2-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "KEWAJIBAN", "", "")
        Case "3-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "MODAL", "", "")
        Case "4-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "PENDAPATAN", "", "")
        Case "5-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "HPP", "", "")
        Case "6-0000"
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(kelompok, "BIAYA", "", "")
    End Select
    
    Dim record As New Recordset
    record.Open "select * from makun where kelompok='" & kelompok & "' order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not record.EOF Then
        Do While Not record.EOF
            i = i + 1
            spasi = 4
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(Space(spasi) & record.Fields!kode, "" & record.Fields!nama, IIf(record.Fields!jenis = 0, "H", "D"), IIf(record.Fields!kas = 1, "Kas", ""))
            
            get_akuntree spasi + 4, record.Fields!kode, kelompok
            record.MoveNext
        Loop
    End If
    
    akun.Update
    If record.State <> 0 Then record.Close
End Sub

Sub get_akuntree(spasi As Integer, kode As String, kelompok As String)
    Dim record As New Recordset
    record.Open "select * from makun where kelompok='" & kode & "' order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not record.EOF Then
        Do While Not record.EOF
            i = i + 1
            akun.AddNew Array("kode", "nama", "jenis", "kas"), Array(Space(spasi) & record.Fields!kode, "" & record.Fields!nama, IIf(record.Fields!jenis = 0, "H", "D"), IIf(record.Fields!kas = 1, "Kas", ""))
            
            get_akuntree spasi + 4, record.Fields!kode, kelompok
            record.MoveNext
        Loop
        If record.State <> 0 Then record.Close
    End If
End Sub

Private Sub CRViewer1_RefreshButtonClicked(UseDefault As Boolean)
    ShowReport
End Sub

Private Sub Form_Load()
    Set akun = New ADODB.Recordset
    akun.Fields.Append "kode", adVariant
    akun.Fields.Append "nama", adVariant
    akun.Fields.Append "jenis", adVariant
    akun.Fields.Append "kas", adVariant
    akun.Open
    
    ShowReport
End Sub

Private Sub Form_Resize()
    CRViewer1.Width = Me.Width - 250
    CRViewer1.Height = Me.Height - CRViewer1.Top
End Sub
