/*
 Navicat Premium Data Transfer

 Source Server         : LOCAL
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost:3306
 Source Schema         : mysis

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : 65001

 Date: 14/05/2018 17:49:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for beli_det
-- ----------------------------
DROP TABLE IF EXISTS `beli_det`;
CREATE TABLE `beli_det`  (
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `QTY` double NULL DEFAULT 0,
  `HSATUAN` double NULL DEFAULT 0,
  `SATUAN_DSR` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SATUAN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISI` double NULL DEFAULT NULL,
  `DISC1` double NULL DEFAULT 0,
  `DISC2` double NULL DEFAULT 0,
  `DISCRP` double NULL DEFAULT 0,
  `BONUSQTY` double NULL DEFAULT 0,
  `TOTAL` double NULL DEFAULT 0,
  `PPN` double NULL DEFAULT 0,
  `HBELI` double NULL DEFAULT 0,
  `HJUAL` double NULL DEFAULT NULL,
  `GOL` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGL` datetime(0) NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KODE` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `URUT` int(11) NULL DEFAULT NULL,
  `TIPE` tinyint(4) NULL DEFAULT 0,
  `WAKTU` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `KDGOL` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDTOKO` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  INDEX `NOTA`(`NOTA`) USING BTREE,
  INDEX `URUT`(`URUT`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE,
  INDEX `KODE`(`KODE`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for beli_head
-- ----------------------------
DROP TABLE IF EXISTS `beli_head`;
CREATE TABLE `beli_head`  (
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NOPO` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDSP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMASP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` datetime(0) NULL DEFAULT NULL,
  `NILAI` double NULL DEFAULT 0,
  `BONUS` double NULL DEFAULT 0,
  `DISCFAKTUR` double NULL DEFAULT 0,
  `BRUTO` double NULL DEFAULT 0,
  `DISC` double NULL DEFAULT 0,
  `DISCTUNAI` double NULL DEFAULT 0,
  `DISCREG1` double NULL DEFAULT 0,
  `DISCREG2` double NULL DEFAULT 0,
  `DPP` double NULL DEFAULT 0,
  `PPN_INCLUDE` tinyint(4) NULL DEFAULT 1,
  `PPN` double NULL DEFAULT 0,
  `BIAYA_LAIN` double NULL DEFAULT 0,
  `GOL` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KET` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `REMARK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `JATUH` datetime(0) NULL DEFAULT NULL,
  `BAYAR` double NULL DEFAULT 0,
  `KREDIT` double NULL DEFAULT 0,
  `LUNAS` tinyint(1) NULL DEFAULT 0,
  `TT` tinyint(1) NULL DEFAULT 0,
  `TGLUNAS` datetime(0) NULL DEFAULT NULL,
  `NETTO` double NULL DEFAULT 0,
  `KIRIM` tinyint(4) NULL DEFAULT 0,
  `NOSJ` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLSJ` datetime(0) NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `CETAK` smallint(6) NULL DEFAULT 0,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `JENIS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KDM` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `JNS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Kredit',
  PRIMARY KEY (`NOTA`) USING BTREE,
  INDEX `NOTA`(`NOTA`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE,
  INDEX `KODE`(`KDSP`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bo
-- ----------------------------
DROP TABLE IF EXISTS `bo`;
CREATE TABLE `bo`  (
  `NO` int(11) NULL DEFAULT 0,
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `CABANG` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ALAMAT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KOTA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TLP` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FAX` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODE` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KON` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  `BAYAR` double NULL DEFAULT 0,
  `AWAL` double NULL DEFAULT 0,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT_NPWP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGL_NPWP` date NULL DEFAULT NULL,
  `SERI_FP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `NAMA_NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_agen
-- ----------------------------
DROP TABLE IF EXISTS `deposit_agen`;
CREATE TABLE `deposit_agen`  (
  `KDAG` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAAG` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MASUK` double NULL DEFAULT NULL,
  `KELUAR` double NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jour_det
-- ----------------------------
DROP TABLE IF EXISTS `jour_det`;
CREATE TABLE `jour_det`  (
  `TGL` date NULL DEFAULT NULL,
  `VC` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `KODE` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `NAMA` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `DEBET` double NULL DEFAULT 0,
  `KREDIT` double NULL DEFAULT 0,
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `WKT` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `TAHUNAN` smallint(1) NULL DEFAULT 0,
  `KDPT` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `URUT` int(11) NULL DEFAULT 0,
  INDEX `TGL`(`TGL`) USING BTREE,
  INDEX `VC`(`VC`) USING BTREE,
  INDEX `KODE`(`KODE`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jour_head
-- ----------------------------
DROP TABLE IF EXISTS `jour_head`;
CREATE TABLE `jour_head`  (
  `TGL` date NULL DEFAULT NULL,
  `VC` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `JENIS` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `NILAI` double NULL DEFAULT 0,
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `WKT` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `TAHUNAN` smallint(1) NULL DEFAULT 0,
  `KDPT` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  INDEX `TGL`(`TGL`) USING BTREE,
  INDEX `VC`(`VC`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jual_cust
-- ----------------------------
DROP TABLE IF EXISTS `jual_cust`;
CREATE TABLE `jual_cust`  (
  `NO_EJ` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDPL` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOSMED` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jual_det
-- ----------------------------
DROP TABLE IF EXISTS `jual_det`;
CREATE TABLE `jual_det`  (
  `TGL` date NULL DEFAULT NULL,
  `JAM` time(0) NULL DEFAULT NULL,
  `NO_EJ` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `HARGA` double NULL DEFAULT 0,
  `QTY` double NULL DEFAULT 0,
  `DISC1` double NULL DEFAULT 0,
  `DISC2` double NULL DEFAULT 0,
  `DISCRP` double NULL DEFAULT 0,
  `AVER` double NULL DEFAULT 0,
  `AVER_KONSI` double NULL DEFAULT 0,
  `BRUTO` double NULL DEFAULT 0,
  `NETTO` double NULL DEFAULT 0,
  `BKP` tinyint(1) NULL DEFAULT 1,
  `ID` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KDGOL` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLC` datetime(0) NULL DEFAULT NULL,
  `BEST` tinyint(1) NULL DEFAULT 0,
  `FEE` double NULL DEFAULT 0,
  `kdtoko` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `CASHBACK` double NULL DEFAULT 0,
  `STN` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PCS',
  `STNSTD` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PCS',
  `ISI` double NULL DEFAULT 1,
  INDEX `NO_EJ`(`NO_EJ`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE,
  INDEX `ID`(`ID`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jual_head
-- ----------------------------
DROP TABLE IF EXISTS `jual_head`;
CREATE TABLE `jual_head`  (
  `TGL` date NULL DEFAULT NULL,
  `JAM` time(0) NULL DEFAULT NULL,
  `NO_EJ` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  `KDAG` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDSL` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `QTY` double NULL DEFAULT 0,
  `AVER` double NULL DEFAULT 0,
  `AVER_KONSI` double NULL DEFAULT 0,
  `BRUTO` double NULL DEFAULT 0,
  `DISC` double NULL DEFAULT 0,
  `NETTO` double NULL DEFAULT 0,
  `DP` double NULL DEFAULT 0,
  `CASH` double NULL DEFAULT 0,
  `BANK` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `CARD` double NULL DEFAULT 0,
  `NCARD` double NULL DEFAULT 0,
  `CURRENCY` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NCURRENCY` double NULL DEFAULT 0,
  `VOUCHER` double NULL DEFAULT 0,
  `KODE` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLC` datetime(0) NULL DEFAULT NULL,
  `NPOINT` double NULL DEFAULT 0,
  `NFEE` double NULL DEFAULT 0,
  `IP_KOMPUTER` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  INDEX `no_ej`(`NO_EJ`) USING BTREE,
  INDEX `tgl`(`TGL`) USING BTREE,
  INDEX `jam`(`JAM`) USING BTREE,
  INDEX `ID`(`ID`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kartu_deposit
-- ----------------------------
DROP TABLE IF EXISTS `kartu_deposit`;
CREATE TABLE `kartu_deposit`  (
  `NOTA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `KDAG` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAAG` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` date NULL DEFAULT NULL,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  `KET` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`NOTA`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kitchen_item
-- ----------------------------
DROP TABLE IF EXISTS `kitchen_item`;
CREATE TABLE `kitchen_item`  (
  `KITCHEN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  `NAMA` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`KITCHEN`, `BARA`) USING BTREE,
  INDEX `KITCHEN`(`KITCHEN`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kstock
-- ----------------------------
DROP TABLE IF EXISTS `kstock`;
CREATE TABLE `kstock`  (
  `TGL` datetime(0) NULL DEFAULT '0000-00-00 00:00:00',
  `NOTA` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TIPE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  `HPP` double NULL DEFAULT 0,
  `HARGA` double NULL DEFAULT 0,
  `KODE` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  INDEX `TGL`(`TGL`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE,
  INDEX `KODE`(`KODE`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ksupp
-- ----------------------------
DROP TABLE IF EXISTS `ksupp`;
CREATE TABLE `ksupp`  (
  `KDSP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` date NULL DEFAULT NULL,
  `NETTO` double NULL DEFAULT NULL,
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TIPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BO` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  INDEX `kode`(`KDSP`) USING BTREE,
  INDEX `tgl`(`TGL`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kurs
-- ----------------------------
DROP TABLE IF EXISTS `kurs`;
CREATE TABLE `kurs`  (
  `CURRENCY` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NILAI` double NULL DEFAULT 0,
  `TGLU` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for magen
-- ----------------------------
DROP TABLE IF EXISTS `magen`;
CREATE TABLE `magen`  (
  `KDAG` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMAAG` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KOTA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROPINSI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEGARA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODEPOS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMAIL` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KONTAK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLREG` date NULL DEFAULT NULL,
  `TGLB` date NULL DEFAULT NULL,
  `TGLD` date NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BUFF` int(11) NULL DEFAULT 0,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT_NPWP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLNPWP` date NULL DEFAULT NULL,
  `REK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAREK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BANK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  PRIMARY KEY (`KDAG`) USING BTREE,
  INDEX `kdsl`(`KDAG`) USING BTREE,
  INDEX `nama`(`NAMAAG`) USING BTREE,
  INDEX `alamat`(`ALAMAT`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for makun
-- ----------------------------
DROP TABLE IF EXISTS `makun`;
CREATE TABLE `makun`  (
  `KODE` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMA` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KELOMPOK` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GRUP` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JENIS` tinyint(4) NULL DEFAULT 0,
  `KAS` tinyint(4) NULL DEFAULT 0,
  `XAWAL` double NULL DEFAULT 0,
  `AWAL` double NULL DEFAULT 0,
  `DEBET` double NULL DEFAULT 0,
  `KREDIT` double NULL DEFAULT 0,
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  PRIMARY KEY (`KODE`, `BO`) USING BTREE,
  INDEX `KODE`(`KODE`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for makun_group
-- ----------------------------
DROP TABLE IF EXISTS `makun_group`;
CREATE TABLE `makun_group`  (
  `KODE` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMA` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`KODE`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for makun_set
-- ----------------------------
DROP TABLE IF EXISTS `makun_set`;
CREATE TABLE `makun_set`  (
  `KTG` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JENIS` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODE` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mbrand
-- ----------------------------
DROP TABLE IF EXISTS `mbrand`;
CREATE TABLE `mbrand`  (
  `MERK` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_MERK` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`MERK`) USING BTREE,
  INDEX `MERK`(`MERK`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mcabang
-- ----------------------------
DROP TABLE IF EXISTS `mcabang`;
CREATE TABLE `mcabang`  (
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KETERANGAN` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ALAMAT1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ALAMAT2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FUNGSI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`BO`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mcustomer
-- ----------------------------
DROP TABLE IF EXISTS `mcustomer`;
CREATE TABLE `mcustomer`  (
  `KDPL` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMAPL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KOTA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROPINSI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEGARA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODEPOS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMAIL` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KONTAK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLREG` date NULL DEFAULT NULL,
  `TOPY` tinyint(4) NULL DEFAULT 0,
  `JT` tinyint(4) NULL DEFAULT 0,
  `PPN` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLB` date NULL DEFAULT NULL,
  `TGLD` date NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BUFF` int(11) NULL DEFAULT 0,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT_NPWP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLNPWP` date NULL DEFAULT NULL,
  `AWAL` double NULL DEFAULT 0,
  `XAWAL` double NULL DEFAULT 0,
  `PLAFON` double NULL DEFAULT 0,
  `PENJUALAN` double NULL DEFAULT 0,
  `PIUTANG` double NULL DEFAULT 0,
  `BAYAR` double NULL DEFAULT 0,
  `POINT` int(11) NULL DEFAULT 0,
  `REK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAREK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BANK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GRUP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TIPE_POTONGAN` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`KDPL`) USING BTREE,
  INDEX `kdpl`(`KDPL`) USING BTREE,
  INDEX `nama`(`NAMAPL`) USING BTREE,
  INDEX `alamat`(`ALAMAT`) USING BTREE,
  INDEX `grup`(`GRUP`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mgrup_customer
-- ----------------------------
DROP TABLE IF EXISTS `mgrup_customer`;
CREATE TABLE `mgrup_customer`  (
  `KODE` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `GRUP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `POTONGAN` double NULL DEFAULT 0,
  `LEVEL` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`KODE`) USING BTREE,
  INDEX `grup`(`GRUP`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mkitchen
-- ----------------------------
DROP TABLE IF EXISTS `mkitchen`;
CREATE TABLE `mkitchen`  (
  `KITCHEN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_KITCHEN` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`KITCHEN`) USING BTREE,
  INDEX `KITCHEN`(`KITCHEN`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mktg
-- ----------------------------
DROP TABLE IF EXISTS `mktg`;
CREATE TABLE `mktg`  (
  `KTG` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_KTG` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`KTG`) USING BTREE,
  INDEX `KTG`(`KTG`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mktgsub
-- ----------------------------
DROP TABLE IF EXISTS `mktgsub`;
CREATE TABLE `mktgsub`  (
  `SUBKTG` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_SUBKTG` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KTG` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SUBKTG`) USING BTREE,
  INDEX `SUBKTG`(`SUBKTG`) USING BTREE,
  INDEX `KTG`(`KTG`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mlokasi
-- ----------------------------
DROP TABLE IF EXISTS `mlokasi`;
CREATE TABLE `mlokasi`  (
  `LOKASI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_LOKASI` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`LOKASI`) USING BTREE,
  INDEX `MERK`(`LOKASI`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mnomor
-- ----------------------------
DROP TABLE IF EXISTS `mnomor`;
CREATE TABLE `mnomor`  (
  `JENIS` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL1` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL2` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL3` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL4` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL5` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL6` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KOL7` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DGT` int(11) NULL DEFAULT 4
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mperusahaan
-- ----------------------------
DROP TABLE IF EXISTS `mperusahaan`;
CREATE TABLE `mperusahaan`  (
  `NAMA_PERUSAHAAN` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ALAMAT1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ALAMAT2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL_PKP` date NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for msales
-- ----------------------------
DROP TABLE IF EXISTS `msales`;
CREATE TABLE `msales`  (
  `KDSL` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMASL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KOTA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROPINSI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEGARA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODEPOS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMAIL` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KONTAK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLREG` date NULL DEFAULT NULL,
  `TOD` tinyint(4) NULL DEFAULT 0,
  `JT` tinyint(4) NULL DEFAULT 0,
  `TGLB` date NULL DEFAULT NULL,
  `TGLD` date NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BUFF` int(11) NULL DEFAULT 0,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT_NPWP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLNPWP` date NULL DEFAULT NULL,
  `AWAL` double NULL DEFAULT 0,
  `XAWAL` double NULL DEFAULT 0,
  `PENJUALAN` double NULL DEFAULT 0,
  `FEE` double NULL DEFAULT 0,
  `TARIK` double NULL DEFAULT 0,
  `REK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAREK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BANK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JENISFEE` int(11) NULL DEFAULT 0,
  `PERSENFEE` double NULL DEFAULT 0,
  `NOMINALFEE` double NULL DEFAULT 0,
  PRIMARY KEY (`KDSL`) USING BTREE,
  INDEX `kdsl`(`KDSL`) USING BTREE,
  INDEX `nama`(`NAMASL`) USING BTREE,
  INDEX `alamat`(`ALAMAT`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for msatuan
-- ----------------------------
DROP TABLE IF EXISTS `msatuan`;
CREATE TABLE `msatuan`  (
  `SATUAN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_SATUAN` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`SATUAN`) USING BTREE,
  INDEX `SATUAN`(`SATUAN`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mstock
-- ----------------------------
DROP TABLE IF EXISTS `mstock`;
CREATE TABLE `mstock`  (
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `BARA2` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `INV` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Barang',
  `NAMA` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KETERANGAN` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `MERK` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBKTG` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SATUAN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PCS',
  `plu` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `MINORDER` double NULL DEFAULT 1,
  `TGL_REG` datetime(0) NULL DEFAULT NULL,
  `LAST_UPDATE` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`BARA`) USING BTREE,
  INDEX `bara`(`BARA`) USING BTREE,
  INDEX `SUBKTG`(`SUBKTG`) USING BTREE,
  INDEX `MERK`(`MERK`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mstock_bo
-- ----------------------------
DROP TABLE IF EXISTS `mstock_bo`;
CREATE TABLE `mstock_bo`  (
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  `AVER` double(11, 3) NULL DEFAULT 0.000,
  `QTY` double NULL DEFAULT 0,
  `QTY2` double NULL DEFAULT 1,
  `QTY3` double NULL DEFAULT 1,
  `QTY4` double NULL DEFAULT 1,
  `HJUAL` double NULL DEFAULT 0,
  `HJUAL2` double NULL DEFAULT 0,
  `HJUAL3` double NULL DEFAULT 0,
  `HJUAL4` double NULL DEFAULT 0,
  `CURRENCY` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `XAWAL` double NULL DEFAULT 0,
  `AWAL` double NULL DEFAULT 0,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  `BO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `MIN` double NULL DEFAULT 0,
  `MAX` double NULL DEFAULT 0,
  `RAK` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HBELI` double NULL DEFAULT 0,
  `AKTIF` tinyint(4) NULL DEFAULT 1,
  `BKP` tinyint(4) NULL DEFAULT 1,
  `MSTN` tinyint(4) NULL DEFAULT 0,
  `SERIAL` tinyint(4) NULL DEFAULT 0,
  `DISC` tinyint(4) NULL DEFAULT 0,
  `TETAP` tinyint(4) NULL DEFAULT 1,
  `FEEPERSEN` double NULL DEFAULT 0,
  `FEENOMINAL` double NULL DEFAULT 0,
  `FEE` tinyint(4) NULL DEFAULT 0,
  `POIN` tinyint(4) NULL DEFAULT 1,
  `DATE_REG` datetime(0) NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_UPDATE` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `MUP` double NULL DEFAULT 0,
  `ACC_INV` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ACC_PJL` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ACC_HPP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ACC_PJK` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`BARA`, `BO`) USING BTREE,
  INDEX `sku`(`BARA`) USING BTREE,
  INDEX `bo`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mstock_satuan
-- ----------------------------
DROP TABLE IF EXISTS `mstock_satuan`;
CREATE TABLE `mstock_satuan`  (
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `SATUAN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ISI` double NULL DEFAULT 1,
  `HARGA` double NULL DEFAULT 0,
  `BARA2` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`BARA`, `SATUAN`, `BO`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE,
  INDEX `BO`(`BO`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mstock_supp
-- ----------------------------
DROP TABLE IF EXISTS `mstock_supp`;
CREATE TABLE `mstock_supp`  (
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-',
  `HARGA` double(11, 3) NULL DEFAULT 0.000,
  `PPN` double NULL DEFAULT 0,
  `HBELI` double NULL DEFAULT 0,
  `TGL` datetime(0) NULL DEFAULT NULL,
  `TGLU` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `MASTER` tinyint(4) NULL DEFAULT 0,
  `KDSP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`BARA`, `KDSP`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE,
  INDEX `KDSP`(`KDSP`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for msupp
-- ----------------------------
DROP TABLE IF EXISTS `msupp`;
CREATE TABLE `msupp`  (
  `KDSP` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `NAMASP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KOTA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROPINSI` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEGARA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KODEPOS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TELP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FAX` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EMAIL` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KONTAK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLREG` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `TOD` tinyint(4) NULL DEFAULT 0,
  `HARI` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JT` tinyint(4) NULL DEFAULT 0,
  `PPN` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLB` date NULL DEFAULT NULL,
  `TGLD` date NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `BUFF` int(11) NULL DEFAULT 0,
  `NPWP` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ALAMAT_NPWP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLNPWP` date NULL DEFAULT NULL,
  `AWAL` double NULL DEFAULT 0,
  `XAWAL` double NULL DEFAULT 0,
  `MASUK` double NULL DEFAULT 0,
  `KELUAR` double NULL DEFAULT 0,
  `RETUR` double NULL DEFAULT 0,
  `REK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMAREK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BANK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`KDSP`) USING BTREE,
  INDEX `kdsp`(`KDSP`) USING BTREE,
  INDEX `nama`(`NAMASP`) USING BTREE,
  INDEX `alamat`(`ALAMAT`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mtable
-- ----------------------------
DROP TABLE IF EXISTS `mtable`;
CREATE TABLE `mtable`  (
  `MEJA` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KET_MEJA` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  PRIMARY KEY (`MEJA`) USING BTREE,
  INDEX `MEJA`(`MEJA`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for paket_agen
-- ----------------------------
DROP TABLE IF EXISTS `paket_agen`;
CREATE TABLE `paket_agen`  (
  `KDPKT` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDAG` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL1` date NULL DEFAULT NULL,
  `TGL2` date NULL DEFAULT NULL,
  `HARGA` double NULL DEFAULT 0,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLC` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`KDPKT`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for po_det
-- ----------------------------
DROP TABLE IF EXISTS `po_det`;
CREATE TABLE `po_det`  (
  `NOPO` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GOL` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `HBELI` double NULL DEFAULT 0,
  `MAX` double NULL DEFAULT 0,
  `SALDO` int(11) NULL DEFAULT 0,
  `PESAN` int(11) NULL DEFAULT 0,
  `KIRIM` int(11) NULL DEFAULT 0,
  `TOTAL` double NULL DEFAULT 0,
  `SATUAN_DSR` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `SATUAN` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PCS',
  `ISI` double NULL DEFAULT 0,
  `URUT` int(11) NULL DEFAULT 0,
  `KDGOL` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bara2` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `qtykonv` int(11) NULL DEFAULT 1,
  INDEX `NOPO`(`NOPO`) USING BTREE,
  INDEX `bara`(`BARA`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for po_head
-- ----------------------------
DROP TABLE IF EXISTS `po_head`;
CREATE TABLE `po_head`  (
  `NOPO` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` datetime(0) NULL DEFAULT NULL,
  `KDSP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMASP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DISC` double NULL DEFAULT 0,
  `DISCTUNAI` double NULL DEFAULT 0,
  `PPN` double NULL DEFAULT 0,
  `SUBTOTAL` double NULL DEFAULT 0,
  `TOTAL` double NULL DEFAULT 0,
  `DP` double NULL DEFAULT 0,
  `NODU` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `DU` smallint(6) NULL DEFAULT 0,
  `STS` tinyint(4) NULL DEFAULT 1,
  `KET` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `TGEXP` datetime(0) NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `PLPT` tinyint(4) NULL DEFAULT 1,
  `EDP` tinyint(1) NULL DEFAULT 0,
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGLD` date NULL DEFAULT NULL,
  INDEX `NOPO`(`NOPO`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for rbeli_det
-- ----------------------------
DROP TABLE IF EXISTS `rbeli_det`;
CREATE TABLE `rbeli_det`  (
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BARA` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMA` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `QTY` double NULL DEFAULT 0,
  `HSATUAN` double NULL DEFAULT 0,
  `SATUAN_DSR` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SATUAN` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISI` double NULL DEFAULT NULL,
  `DISC1` double NULL DEFAULT 0,
  `DISC2` double NULL DEFAULT 0,
  `DISCRP` double NULL DEFAULT 0,
  `BONUSQTY` double NULL DEFAULT 0,
  `TOTAL` double NULL DEFAULT 0,
  `PPN` double NULL DEFAULT 0,
  `HBELI` double NULL DEFAULT 0,
  `HJUAL` double NULL DEFAULT NULL,
  `GOL` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `TGL` datetime(0) NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KODE` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `URUT` int(11) NULL DEFAULT NULL,
  `TIPE` tinyint(4) NULL DEFAULT 0,
  `WAKTU` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `KDGOL` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KDTOKO` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  INDEX `NOTA`(`NOTA`) USING BTREE,
  INDEX `URUT`(`URUT`) USING BTREE,
  INDEX `BARA`(`BARA`) USING BTREE,
  INDEX `KODE`(`KODE`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for rbeli_head
-- ----------------------------
DROP TABLE IF EXISTS `rbeli_head`;
CREATE TABLE `rbeli_head`  (
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `KDSP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMASP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` datetime(0) NULL DEFAULT NULL,
  `NILAI` double NULL DEFAULT NULL,
  `GOL` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KET` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REMARK` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `JATUH` datetime(0) NULL DEFAULT NULL,
  `BAYAR` double NULL DEFAULT 0,
  `DISC` double NULL DEFAULT 0,
  `PPN` double NULL DEFAULT 0,
  `LUNAS` tinyint(1) NULL DEFAULT 0,
  `TT` tinyint(1) NULL DEFAULT 0,
  `KIRIM` tinyint(4) NULL DEFAULT 0,
  `TGLUNAS` datetime(0) NULL DEFAULT NULL,
  `NETTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `NOSJ` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGLSJ` datetime(0) NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `CETAK` smallint(6) NULL DEFAULT 0,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `NOTA_P` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `KDGOL` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`NOTA`) USING BTREE,
  INDEX `NOTA`(`NOTA`) USING BTREE,
  INDEX `TGL`(`TGL`) USING BTREE,
  INDEX `KODE`(`KDSP`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu`  (
  `ID_GRUP1` int(11) NOT NULL DEFAULT 1,
  `KET_GRUP1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ID_GRUP2` int(11) NOT NULL DEFAULT 1,
  `KET_GRUP2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `ID_ITEM` int(11) NOT NULL DEFAULT 1,
  `KET_ITEM` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `FORM_MENU` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_GRUP1`, `ID_GRUP2`, `ID_ITEM`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbluser
-- ----------------------------
DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE `tbluser`  (
  `id_user` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nama` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bo` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `minta` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login` tinyint(1) NULL DEFAULT 0,
  `nota` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `komp` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-',
  `tgl` date NULL DEFAULT NULL,
  `kode_finger` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tgl_reg` datetime(0) NULL DEFAULT NULL,
  `tgl_clerk` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tt_det
-- ----------------------------
DROP TABLE IF EXISTS `tt_det`;
CREATE TABLE `tt_det`  (
  `NOTT` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NOTA` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL_NOTA` datetime(0) NULL DEFAULT NULL,
  `TGL_JT` datetime(0) NULL DEFAULT NULL,
  `NETTO` double NULL DEFAULT 0,
  `DISC` double NULL DEFAULT 0,
  `BAYAR` double NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tt_head
-- ----------------------------
DROP TABLE IF EXISTS `tt_head`;
CREATE TABLE `tt_head`  (
  `NOTT` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `KDSP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAMASP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL` datetime(0) NULL DEFAULT NULL,
  `TOTAL` double NULL DEFAULT 0,
  `JENIS_BAYAR` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'TUNAI',
  `NOBG` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TGL_BG` date NULL DEFAULT NULL,
  `BANK_BG` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AN_BG` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AKUN` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KET` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BO` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USR` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`NOTT`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for zfoto_barang
-- ----------------------------
DROP TABLE IF EXISTS `zfoto_barang`;
CREATE TABLE `zfoto_barang`  (
  `bara` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Picture` mediumblob NOT NULL,
  PRIMARY KEY (`bara`) USING BTREE,
  INDEX `bara`(`bara`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for zfoto_perusahaan
-- ----------------------------
DROP TABLE IF EXISTS `zfoto_perusahaan`;
CREATE TABLE `zfoto_perusahaan`  (
  `gambar` mediumblob NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Triggers structure for table beli_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_beli_head`;
delimiter ;;
CREATE TRIGGER `trig_beli_head` AFTER INSERT ON `beli_head` FOR EACH ROW BEGIN
IF new.jns ="Kredit" then
    UPDATE msupp SET masuk=masuk+new.netto where kdsp=new.kdsp ;
     INSERT INTO KSUPP (KDSP, TGL, NETTO, NOTA, TIPE, BO, USR) VALUES (new.kdsp, new.tgl, new.netto, new.nota, 'B', new.bo, new.usr);
else
     INSERT INTO KSUPP (KDSP, TGL, NETTO, NOTA, TIPE, BO, USR) VALUES (new.kdsp, new.tgl, new.netto, new.nota, 'T', new.bo, new.usr);
end if;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table beli_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_beli_head_del`;
delimiter ;;
CREATE TRIGGER `trig_beli_head_del` AFTER DELETE ON `beli_head` FOR EACH ROW BEGIN
IF old.jns ="Kredit" then
     UPDATE msupp SET masuk=masuk-old.netto where kdsp=old.kdsp ;
     DELETE FROM KSUPP WHERE kdsp=old.kdsp AND nota=old.nota AND tipe='B' ;
else
     DELETE FROM KSUPP WHERE kdsp=old.kdsp AND nota=old.nota AND tipe='T' ;
end if;
DELETE FROM beli_det WHERE nota=old.nota  ;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table jour_det
-- ----------------------------
DROP TRIGGER IF EXISTS `jour_insert`;
delimiter ;;
CREATE TRIGGER `jour_insert` AFTER INSERT ON `jour_det` FOR EACH ROW BEGIN
update makun set debet=debet+new.debet,kredit=kredit+new.kredit where kode=new.kode;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table jour_det
-- ----------------------------
DROP TRIGGER IF EXISTS `jour_del`;
delimiter ;;
CREATE TRIGGER `jour_del` AFTER DELETE ON `jour_det` FOR EACH ROW BEGIN
update makun set debet=debet-old.debet,kredit=kredit-old.kredit where kode=old.kode;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table jour_head
-- ----------------------------
DROP TRIGGER IF EXISTS `jour_head_del`;
delimiter ;;
CREATE TRIGGER `jour_head_del` AFTER DELETE ON `jour_head` FOR EACH ROW BEGIN
delete from jour_det where vc=old.vc;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table jual_det
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_jual_det_insert`;
delimiter ;;
CREATE TRIGGER `trig_jual_det_insert` AFTER INSERT ON `jual_det` FOR EACH ROW BEGIN 
IF (SELECT COUNT(bara) FROM tjual WHERE bara=new.bara AND id=new.id AND tgl=new.tgl) > 0 THEN 
    UPDATE tjual SET qty=qty+new.qty*new.isi, bruto=bruto+new.bruto, netto=netto+new.netto, aver_konsi=aver_konsi+new.aver_konsi, aver=aver+new.aver,fee=fee+new.fee+new.cashback where bara=new.bara AND id=new.id AND tgl=new.tgl; 
ELSE 
    INSERT INTO tjual (bara, tgl, qty, bruto, netto, aver, aver_konsi, id, kdgol, bo,fee,kdtoko) VALUES (new.bara, new.tgl, new.qty*new.isi, new.bruto, new.netto, new.aver, new.aver_konsi, new.id, new.kdgol, new.bo,new.fee+new.cashback,new.kdtoko); 
end IF; 
IF (SELECT COUNT(bara) FROM kbara WHERE bara=new.bara AND tgl=new.tgl AND TIPE='J') > 0 THEN 
    UPDATE kbara SET qty=qty+new.qty*new.isi, netto=((qty*netto)+(new.netto))/(qty+new.qty*new.isi) where bara=new.bara AND tgl=new.tgl AND TIPE='J'; 
ELSE 
    INSERT INTO kbara (bara, tgl, qty, netto, tipe, bo) VALUES (new.bara, new.tgl, new.qty*new.isi, (new.netto/(new.qty*new.isi)), 'J', new.bo); 
end IF; 
IF (SELECT COUNT(bara) FROM mstock WHERE bara=new.bara and tipebest=1 AND best1>=new.tgl AND best2<=new.tgl AND dbest>0) > 0 THEN 
    UPDATE mstock SET KELUAR=KELUAR+new.qty*new.isi, dbest=dbest-new.qty*new.isi WHERE BARA=new.bara; 
ELSE 
    UPDATE mstock SET KELUAR=KELUAR+new.qty*new.isi WHERE BARA=new.bara; 
end IF; 
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table kartu_deposit
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_dp`;
delimiter ;;
CREATE TRIGGER `insert_dp` AFTER INSERT ON `kartu_deposit` FOR EACH ROW BEGIN
	UPDATE magen m JOIN (SELECT sum(masuk)masuk, sum(keluar)keluar from kartu_deposit WHERE kdag=new.kdag) k
	SET m.masuk=k.masuk, m.keluar=k.keluar WHERE m.kdag=new.kdag;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table kartu_deposit
-- ----------------------------
DROP TRIGGER IF EXISTS `update_dp`;
delimiter ;;
CREATE TRIGGER `update_dp` AFTER UPDATE ON `kartu_deposit` FOR EACH ROW BEGIN
	UPDATE magen m JOIN (SELECT sum(masuk)masuk, sum(keluar)keluar from kartu_deposit WHERE kdag=new.kdag) k
	SET m.masuk=k.masuk, m.keluar=k.keluar WHERE m.kdag=new.kdag;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table po_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_del`;
delimiter ;;
CREATE TRIGGER `trig_del` AFTER DELETE ON `po_head` FOR EACH ROW BEGIN
DELETE FROM po_det WHERE nopo=old.nopo ;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table rbeli_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_rbeli_head`;
delimiter ;;
CREATE TRIGGER `trig_rbeli_head` AFTER INSERT ON `rbeli_head` FOR EACH ROW BEGIN
UPDATE msupp SET retur=retur+new.netto where kode=new.kode ;
INSERT INTO KSUPP (KODE, TGL, NETTO, NOTA, TIPE, BO, USR) VALUES (new.kode, new.tgl, new.netto, new.nota, 'R', new.bo, new.usr);
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table rbeli_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_rbeli_head_del`;
delimiter ;;
CREATE TRIGGER `trig_rbeli_head_del` AFTER DELETE ON `rbeli_head` FOR EACH ROW BEGIN
UPDATE msupp SET retur=retur-old.netto where kode=old.kode ;
DELETE FROM rbeli_det WHERE nota=old.nota ;
DELETE FROM KSUPP WHERE kode=old.kode AND nota=old.nota AND tipe='R' ;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table tt_head
-- ----------------------------
DROP TRIGGER IF EXISTS `trig_tt_head`;
delimiter ;;
CREATE TRIGGER `trig_tt_head` AFTER DELETE ON `tt_head` FOR EACH ROW BEGIN
DELETE FROM tt_det WHERE nott=old.nott;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
