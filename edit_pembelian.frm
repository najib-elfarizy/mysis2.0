VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form edit_pembelian 
   Caption         =   "Edit Pembelian"
   ClientHeight    =   8145
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   20250
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8145
   ScaleWidth      =   20250
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   0
      ScaleHeight     =   2265
      ScaleWidth      =   15315
      TabIndex        =   6
      Top             =   4920
      Width           =   15345
      Begin VB.Frame frame_tempo 
         Caption         =   "Jatuh Tempo"
         Height          =   855
         Left            =   4920
         TabIndex        =   131
         Top             =   0
         Visible         =   0   'False
         Width           =   3050
         Begin VB.TextBox Text6 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   132
            Text            =   "10"
            Top             =   375
            Width           =   495
         End
         Begin MSComCtl2.DTPicker DTPicker2 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "M/d/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   3
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   133
            Top             =   360
            Width           =   1450
            _ExtentX        =   2566
            _ExtentY        =   661
            _Version        =   393216
            CustomFormat    =   "dd/MM/yyyy"
            Format          =   200081411
            CurrentDate     =   43166
         End
         Begin MSComCtl2.UpDown UpDown1 
            Height          =   360
            Left            =   2160
            TabIndex        =   134
            Top             =   360
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   635
            _Version        =   393216
            Value           =   10
            OrigLeft        =   4425
            OrigTop         =   360
            OrigRight       =   4680
            OrigBottom      =   720
            Max             =   100
            Min             =   1
            Enabled         =   -1  'True
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "/ "
            Height          =   240
            Index           =   0
            Left            =   1600
            TabIndex        =   136
            Top             =   420
            Width           =   105
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Hari"
            Height          =   240
            Index           =   1
            Left            =   2520
            TabIndex        =   135
            Top             =   405
            Width           =   375
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   11610
         TabIndex        =   126
         Top             =   0
         Width           =   3630
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Index           =   9
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1600
            Width           =   2400
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   360
            Index           =   6
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   41
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   255
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   7
            Left            =   1150
            TabIndex        =   43
            Text            =   "0.00"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   360
            Index           =   8
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   45
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   960
            Width           =   2000
         End
         Begin MySIS.Button Button2 
            Height          =   345
            Index           =   3
            Left            =   3195
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   240
            Width           =   345
            _extentx        =   609
            _extenty        =   609
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":0000
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":0024
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin MySIS.Button Button2 
            Height          =   345
            Index           =   4
            Left            =   3195
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
            _extentx        =   609
            _extenty        =   609
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":0042
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":0066
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin MySIS.Button Button2 
            Height          =   345
            Index           =   5
            Left            =   3195
            TabIndex        =   46
            TabStop         =   0   'False
            Top             =   960
            Width           =   345
            _extentx        =   609
            _extenty        =   609
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":0084
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":00A8
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Grand Total :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   165
            TabIndex        =   130
            Top             =   1700
            Width           =   930
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "DP PO :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   510
            TabIndex        =   129
            Top             =   300
            Width           =   585
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tunai / DP :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   210
            TabIndex        =   128
            Top             =   675
            Width           =   885
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Kredit :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   600
            TabIndex        =   127
            Top             =   1035
            Width           =   495
         End
         Begin VB.Line Line3 
            BorderColor     =   &H8000000A&
            X1              =   195
            X2              =   3550
            Y1              =   1440
            Y2              =   1440
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2150
         Left            =   8040
         TabIndex        =   120
         Top             =   10
         Width           =   3495
         Begin MySIS.Button Button2 
            Height          =   320
            Index           =   0
            Left            =   3100
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   200
            Width           =   315
            _extentx        =   556
            _extenty        =   529
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":00C6
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":00EA
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   320
            Index           =   0
            Left            =   1080
            MaxLength       =   5
            TabIndex        =   30
            Text            =   "0.00"
            Top             =   200
            Width           =   585
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   320
            Index           =   1
            Left            =   1680
            TabIndex        =   31
            Text            =   "0.00"
            Top             =   200
            Width           =   1400
         End
         Begin VB.Frame Frame5 
            Height          =   450
            Left            =   1080
            TabIndex        =   121
            Top             =   420
            Width           =   2350
            Begin VB.OptionButton option_pajak 
               Caption         =   "Exclude"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   1200
               TabIndex        =   34
               TabStop         =   0   'False
               Top             =   180
               Width           =   1095
            End
            Begin VB.OptionButton option_pajak 
               Caption         =   "Include"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   120
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   160
               Value           =   -1  'True
               Width           =   1095
            End
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   320
            Index           =   4
            Left            =   1680
            TabIndex        =   38
            Text            =   "0.00"
            Top             =   1250
            Width           =   1400
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   320
            Index           =   3
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   36
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   900
            Width           =   1400
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   320
            Index           =   2
            Left            =   1080
            MaxLength       =   5
            TabIndex        =   35
            Text            =   "0.00"
            Top             =   900
            Width           =   585
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   5
            Left            =   1065
            Locked          =   -1  'True
            TabIndex        =   40
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1680
            Width           =   2355
         End
         Begin MySIS.Button Button2 
            Height          =   315
            Index           =   2
            Left            =   3105
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   1250
            Width           =   315
            _extentx        =   556
            _extenty        =   529
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":0108
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":012C
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin MySIS.Button Button2 
            Height          =   315
            Index           =   1
            Left            =   3105
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   900
            Width           =   315
            _extentx        =   556
            _extenty        =   529
            btype           =   2
            tx              =   "+"
            enab            =   -1  'True
            font            =   "edit_pembelian.frx":014A
            coltype         =   1
            focusr          =   -1  'True
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_pembelian.frx":016E
            umcol           =   -1  'True
            soft            =   0   'False
            picpos          =   0
            ngrey           =   0   'False
            fx              =   0
            hand            =   0   'False
            check           =   0   'False
            value           =   0   'False
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Pajak :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   480
            TabIndex        =   125
            Top             =   600
            Width           =   495
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Potongan :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   195
            TabIndex        =   124
            Top             =   200
            Width           =   780
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Biaya Lain :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   150
            TabIndex        =   123
            Top             =   1320
            Width           =   825
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Sub Total :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   200
            TabIndex        =   122
            Top             =   1750
            Width           =   780
         End
         Begin VB.Line Line1 
            BorderColor     =   &H8000000A&
            X1              =   150
            X2              =   3400
            Y1              =   1600
            Y2              =   1600
         End
      End
      Begin VB.TextBox Text5 
         Height          =   950
         Left            =   4920
         MultiLine       =   -1  'True
         TabIndex        =   119
         Top             =   1200
         Width           =   3050
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   1650
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Simpan"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":018C
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":01B8
         picn            =   "edit_pembelian.frx":01D6
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   1650
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Keluar"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":0332
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":035E
         picn            =   "edit_pembelian.frx":037C
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   1650
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Batalkan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":07B4
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":07E0
         picn            =   "edit_pembelian.frx":07FE
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button3 
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   120
         Width           =   1155
         _extentx        =   2037
         _extenty        =   741
         btype           =   3
         tx              =   "Hapus Item"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":095A
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":0986
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button cmd_cetak 
         Height          =   375
         Left            =   2760
         TabIndex        =   118
         Top             =   120
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Cetak"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":09A4
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":09D0
         picn            =   "edit_pembelian.frx":09EE
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Label Label5 
         Caption         =   "Keterangan :"
         Height          =   255
         Left            =   4920
         TabIndex        =   137
         Top             =   960
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   660
      Left            =   50
      TabIndex        =   25
      Top             =   1300
      Width           =   15270
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   8250
         TabIndex        =   67
         Text            =   "0"
         Top             =   600
         Visible         =   0   'False
         Width           =   1485
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   13650
         Locked          =   -1  'True
         TabIndex        =   19
         Text            =   "0"
         Top             =   330
         Width           =   1550
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   12550
         Locked          =   -1  'True
         TabIndex        =   17
         Text            =   "0"
         Top             =   330
         Width           =   1100
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   11350
         Locked          =   -1  'True
         TabIndex        =   16
         Text            =   "0"
         Top             =   330
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   10560
         Locked          =   -1  'True
         TabIndex        =   15
         Text            =   "0"
         Top             =   330
         Width           =   800
      End
      Begin VB.ComboBox cmb_satuan 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6250
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   320
         Width           =   2000
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   8250
         Locked          =   -1  'True
         TabIndex        =   13
         Text            =   "0"
         Top             =   330
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   5050
         Locked          =   -1  'True
         TabIndex        =   11
         Text            =   "0"
         Top             =   330
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   550
         MaxLength       =   15
         TabIndex        =   9
         Top             =   330
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   2050
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   330
         Width           =   3000
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   9760
         Locked          =   -1  'True
         TabIndex        =   14
         Text            =   "0"
         Top             =   330
         Width           =   800
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   6250
         TabIndex        =   51
         Text            =   "0"
         Top             =   620
         Visible         =   0   'False
         Width           =   1965
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   13650
         TabIndex        =   57
         Top             =   45
         Width           =   1545
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Bonus Qty"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   12550
         TabIndex        =   55
         Top             =   45
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Disc (Rp)"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   11350
         TabIndex        =   53
         Top             =   45
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Disc 2"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   10560
         TabIndex        =   52
         Top             =   45
         Width           =   795
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Disc 1"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   9760
         TabIndex        =   50
         Top             =   45
         Width           =   800
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Harga Satuan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   8250
         TabIndex        =   49
         Top             =   45
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   550
         Index           =   0
         Left            =   50
         TabIndex        =   48
         Top             =   45
         Width           =   500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2050
         TabIndex        =   29
         Top             =   45
         Width           =   3000
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Qty Terima"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   5050
         TabIndex        =   28
         Top             =   45
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Satuan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   6250
         TabIndex        =   27
         Top             =   45
         Width           =   1980
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Barcode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   550
         TabIndex        =   26
         Top             =   45
         Width           =   1500
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2925
      Left            =   70
      TabIndex        =   5
      Top             =   1920
      Width           =   15180
      _ExtentX        =   26776
      _ExtentY        =   5159
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   18
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "No."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Kode Barang"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Nama Barang"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Qty Terima"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Satuan Dasar"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Satuan"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   7
         Text            =   "Satuan"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Isi Satuan"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Harga Beli"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Text            =   "Harga Satuan"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   11
         Text            =   "Disc 1"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Disc 1 (Rp)"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   13
         Text            =   "Disc 2"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Disc 2 (Rp)"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   15
         Text            =   "Disc Rp"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   16
         Text            =   "Bonus Qty"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   17
         Text            =   "Total"
         Object.Width           =   2734
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   0
      ScaleHeight     =   4905
      ScaleWidth      =   15315
      TabIndex        =   7
      Top             =   0
      Width           =   15350
      Begin VB.Frame Frame4 
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   1215
         Left            =   7200
         TabIndex        =   60
         Top             =   0
         Visible         =   0   'False
         Width           =   5295
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   1080
            TabIndex        =   66
            Top             =   850
            Width           =   75
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   1080
            TabIndex        =   65
            Top             =   500
            Width           =   75
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1080
            TabIndex        =   64
            Top             =   150
            Width           =   75
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Telp :"
            Height          =   240
            Index           =   7
            Left            =   240
            TabIndex        =   63
            Top             =   840
            Width           =   510
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Kota :"
            Height          =   240
            Index           =   6
            Left            =   240
            TabIndex        =   62
            Top             =   480
            Width           =   495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Alamat :"
            Height          =   240
            Index           =   5
            Left            =   240
            TabIndex        =   61
            Top             =   120
            Width           =   720
         End
      End
      Begin VB.ComboBox cmb_cabang 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4920
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   480
         Width           =   2175
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   4920
         MaxLength       =   15
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   3360
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   840
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   8
         Top             =   840
         Width           =   1935
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   1440
         TabIndex        =   0
         Top             =   480
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   101515267
         CurrentDate     =   43159
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1440
         MaxLength       =   25
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Width           =   1935
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Index           =   0
         Left            =   6750
         TabIndex        =   68
         TabStop         =   0   'False
         Top             =   120
         Width           =   315
         _extentx        =   556
         _extenty        =   556
         btype           =   3
         tx              =   "+"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":0D8A
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":0DAE
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Index           =   1
         Left            =   6750
         TabIndex        =   69
         TabStop         =   0   'False
         Top             =   840
         Width           =   315
         _extentx        =   556
         _extenty        =   556
         btype           =   3
         tx              =   "+"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":0DCC
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":0DF0
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept/Gudang : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   3720
         TabIndex        =   24
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Pemesanan :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3480
         TabIndex        =   23
         Top             =   135
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   22
         Top             =   855
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         TabIndex        =   21
         Top             =   495
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   135
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1300
         Left            =   0
         TabIndex        =   18
         Top             =   0
         Width           =   15255
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   5
      Left            =   5040
      TabIndex        =   70
      Top             =   7200
      Visible         =   0   'False
      Width           =   4900
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   5
         Left            =   960
         TabIndex        =   72
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   5
         Left            =   960
         TabIndex        =   71
         Top             =   840
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   5
         Left            =   960
         TabIndex        =   73
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":0E0E
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":0E32
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   5
         Left            =   2400
         TabIndex        =   74
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":0E50
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":0E74
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   5
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   5
         Left            =   4250
         Picture         =   "edit_pembelian.frx":0E92
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   76
         Top             =   570
         Width           =   645
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   75
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   11
         Left            =   0
         TabIndex        =   77
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   4
      Left            =   0
      TabIndex        =   78
      Top             =   7200
      Visible         =   0   'False
      Width           =   4900
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   4
         Left            =   960
         TabIndex        =   80
         Top             =   840
         Width           =   3735
      End
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   4
         Left            =   960
         TabIndex        =   79
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   4
         Left            =   960
         TabIndex        =   81
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":129D
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":12C1
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   4
         Left            =   2400
         TabIndex        =   82
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":12DF
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":1303
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   84
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   83
         Top             =   570
         Width           =   645
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   4
         Left            =   4250
         Picture         =   "edit_pembelian.frx":1321
         Top             =   0
         Width           =   675
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   4
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   9
         Left            =   0
         TabIndex        =   85
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   0
      Left            =   14760
      TabIndex        =   86
      Top             =   0
      Visible         =   0   'False
      Width           =   4900
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   0
         Left            =   960
         TabIndex        =   88
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   0
         Left            =   960
         TabIndex        =   87
         Top             =   840
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   0
         Left            =   960
         TabIndex        =   89
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":172C
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":1750
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   0
         Left            =   2400
         TabIndex        =   90
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":176E
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":1792
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   0
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   0
         Left            =   4250
         Picture         =   "edit_pembelian.frx":17B0
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   92
         Top             =   570
         Width           =   645
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   91
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   0
         TabIndex        =   93
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   1
      Left            =   14760
      TabIndex        =   94
      Top             =   1920
      Visible         =   0   'False
      Width           =   4900
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   1
         Left            =   960
         TabIndex        =   96
         Top             =   840
         Width           =   3735
      End
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   1
         Left            =   960
         TabIndex        =   95
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   1
         Left            =   960
         TabIndex        =   97
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":1BBB
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":1BDF
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   1
         Left            =   2400
         TabIndex        =   98
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":1BFD
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":1C21
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   100
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   99
         Top             =   570
         Width           =   645
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   1
         Left            =   4250
         Picture         =   "edit_pembelian.frx":1C3F
         Top             =   0
         Width           =   675
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   1
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   0
         TabIndex        =   101
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   2
      Left            =   14760
      TabIndex        =   102
      Top             =   3840
      Visible         =   0   'False
      Width           =   4900
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   2
         Left            =   960
         TabIndex        =   104
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   2
         Left            =   960
         TabIndex        =   103
         Top             =   840
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   2
         Left            =   960
         TabIndex        =   105
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":204A
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":206E
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   2
         Left            =   2400
         TabIndex        =   106
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":208C
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":20B0
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   2
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   2
         Left            =   4250
         Picture         =   "edit_pembelian.frx":20CE
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   108
         Top             =   570
         Width           =   645
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   107
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   0
         TabIndex        =   109
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Index           =   3
      Left            =   14760
      TabIndex        =   110
      Top             =   5760
      Visible         =   0   'False
      Width           =   4900
      Begin VB.ComboBox combo_akun 
         Height          =   360
         Index           =   3
         Left            =   960
         TabIndex        =   112
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Index           =   3
         Left            =   960
         TabIndex        =   111
         Top             =   840
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Index           =   3
         Left            =   960
         TabIndex        =   113
         Top             =   1440
         Width           =   1395
         _extentx        =   2461
         _extenty        =   556
         btype           =   2
         tx              =   "Simpan"
         enab            =   0   'False
         font            =   "edit_pembelian.frx":24D9
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":24FD
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Index           =   3
         Left            =   2400
         TabIndex        =   114
         Top             =   1440
         Width           =   1455
         _extentx        =   2566
         _extenty        =   556
         btype           =   2
         tx              =   "Batal"
         enab            =   -1  'True
         font            =   "edit_pembelian.frx":251B
         coltype         =   1
         focusr          =   -1  'True
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_pembelian.frx":253F
         umcol           =   -1  'True
         soft            =   0   'False
         picpos          =   0
         ngrey           =   0   'False
         fx              =   0
         hand            =   0   'False
         check           =   0   'False
         value           =   0   'False
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   3
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Image Image2 
         Height          =   300
         Index           =   3
         Left            =   4250
         Picture         =   "edit_pembelian.frx":255D
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   116
         Top             =   570
         Width           =   645
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   115
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   0
         TabIndex        =   117
         Top             =   0
         Width           =   4845
      End
   End
End
Attribute VB_Name = "edit_pembelian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim akun As New Collection

Private Sub Button1_Click(Index As Integer)
    Select Case Index
        Case 0
            Set cari_po.FormPemanggil = Me
            cari_po.Show 1
        Case 1
            cari_supplier.Left = 1550
            cari_supplier.Top = 3800
            Set cari_supplier.FormPemanggil = Me
            cari_supplier.Show 1
    End Select
End Sub

Private Sub Button2_Click(Index As Integer)
    frame_akun(Index).Visible = True
    frame_akun(Index).Left = 5500
    frame_akun(Index).Top = 2400
    frame_akun(Index).ZOrder vbBringToFront
    ListView1.Enabled = False
    Picture1.Enabled = False
    Picture2.Enabled = False
    Frame1.Enabled = False
End Sub

Private Sub Button3_Click(Index As Integer)
    If Index = 0 Then
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
            Text3(0).SetFocus
        End If
    End If
End Sub

Private Sub Button5_Click(Index As Integer)
    frame_akun(Index).Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub cmd_cetak_Click()
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
        
    If fso.FileExists(App.Path & "\Report\pembelian.rpt") Then
        Set cryApp = New CRAXDRT.Application
        Set rptApp = cryApp.OpenReport(App.Path & "\Report\pembelian.rpt")
        rptApp.EnableParameterPrompting = False
    
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mperusahaan limit 1", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            On Error Resume Next
            rptApp.ParameterFields.GetItemByName("NAMA_PERUSAHAAN").AddCurrentValue "" & rs1.Fields!NAMA_PERUSAHAAN
            rptApp.ParameterFields.GetItemByName("ALAMAT1").AddCurrentValue "" & rs1.Fields!alamat1
            rptApp.ParameterFields.GetItemByName("ALAMAT2").AddCurrentValue "" & rs1.Fields!alamat2
            rptApp.ParameterFields.GetItemByName("TELP").AddCurrentValue "" & rs1.Fields!telp
            rptApp.ParameterFields.GetItemByName("FAX").AddCurrentValue "" & rs1.Fields!fax
            rptApp.ParameterFields.GetItemByName("NPWP").AddCurrentValue "" & rs1.Fields!npwp
        End If
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "SELECT * FROM beli_head b JOIN beli_det d ON b.nota=d.nota WHERE b.nota='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        rptApp.Database.SetDataSource rs1
            
        form_cetak.Visible = True
        form_cetak.ShowReport rptApp
    Else
        MsgBox "Report File Doesn't Exist!"
    End If
End Sub

Private Sub combo_akun_Click(Index As Integer)
    text_akun(Index).Text = akun(combo_akun(Index).ListIndex + 1).Value
    combo_akun(Index).Text = akun(combo_akun(Index).ListIndex + 1).Key
End Sub

Private Sub Image2_Click(Index As Integer)
    frame_akun(Index).Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub cmd_batal_Click()
    On Error GoTo exc
    pesan = MsgBox("Batalkan transaksi pembelian dg nota " & Text1(0).Text & " ?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from beli_head where nota='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
     
        MsgBox ("Pembelian nota " & Text1(0).Text & " telah dibatalkan")
        
        If daftar_pembelian.Visible Then
            daftar_pembelian.tampil_pbl
        End If
        
        kosong
        otomatis
        Text1(1).SetFocus
    End If
    
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    If Text1(1).Text = "" Then
        MsgBox "Supplier masih kosong", vbOKOnly
        Text1(1).SetFocus
        Exit Sub
    End If
    
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "DELETE FROM beli_head WHERE NOTA='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
     
    csql = "INSERT INTO beli_head (NOTA,KDSP,NAMASP,NOPO,TGL,DISC,DISCTUNAI,PPN_INCLUDE,PPN,BIAYA_LAIN,BRUTO,BAYAR,KREDIT,NETTO,JATUH,KET,BO,USR) VALUES " & _
            "('" & Text1(0).Text & "','" & Text1(1).Text & "','" & Text1(2).Text & "','" & Text1(3).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & _
            Val(Format(Text4(0).Text, "##0.00")) & "," & Val(Format(Text4(1).Text, "##0.00")) & "," & option_pajak(0).Value & "," & _
            Val(Format(Text4(3).Text, "##0.00")) & "," & Val(Format(Text4(4).Text, "##0.00")) & "," & _
            Val(Format(Text4(5).Text, "##0.00")) & "," & Val(Format(Text4(7).Text, "##0.00")) & "," & _
            Val(Format(Text4(8).Text, "##0.00")) & "," & Val(Format(Text4(9).Text, "##0.00")) & ",'" & _
            Format(DTPicker2.Value, "yyyy-mm-dd") & "','" & Text5.Text & "','" & cmb_cabang.Text & "','" & xy & "')"
        
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        
    If rs2.State <> 0 Then rs2.Close
    rs2.Open "DELETE FROM beli_det WHERE NOTA='" & Text1(0).Text & "' AND BO='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
        
    If ListView1.ListItems.Count > 0 Then
        Dim bara, nama, satuan_dasar, satuan As String
        Dim isi, qty, Harga, disc1, disc2, discRp, bonus, total As Double
        For z = 1 To ListView1.ListItems.Count
            bara = ListView1.ListItems(z).ListSubItems.item(2)
            nama = ListView1.ListItems(z).ListSubItems.item(3)
            qty = Val(Format(ListView1.ListItems(z).ListSubItems.item(4), "##0.00"))
            satuan_dasar = ListView1.ListItems(z).ListSubItems.item(5)
            satuan = ListView1.ListItems(z).ListSubItems.item(6)
            isi = Val(ListView1.ListItems(z).ListSubItems.item(8))
            Harga = Val(Format(ListView1.ListItems(z).ListSubItems.item(10), "##0.00"))
            disc1 = Val(ListView1.ListItems(z).ListSubItems.item(12))
            disc2 = Val(ListView1.ListItems(z).ListSubItems.item(14))
            discRp = Val(Format(ListView1.ListItems(z).ListSubItems.item(15), "##0.00"))
            bonus = Val(Format(ListView1.ListItems(z).ListSubItems.item(16), "##0.00"))
            total = Val(Format(ListView1.ListItems(z).ListSubItems.item(17), "##0.00"))
                
            csql = "INSERT INTO beli_det(NOTA,BARA,NAMA,QTY,HSATUAN,SATUAN_DSR,SATUAN,ISI,DISC1,DISC2,DISCRP,BONUSQTY,TOTAL,TGL,BO) " & _
                    "VALUES('" & Text1(0).Text & "','" & bara & "','" & nama & "'," & qty & "," & Harga & ",'" & satuan_dasar & "','" & _
                    satuan & "'," & isi & "," & disc1 & "," & disc2 & "," & discRp & "," & bonus & "," & total & ",'" & Format(DTPicker1.Value, "yyyy-mm-dd") & "','" & cmb_cabang.Text & "')"
                
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next
    End If
        
    MsgBox ("Input pembelian sudah disimpan")
    
    get_pembelian Text1(0).Text
    If daftar_pembelian.Visible Then
        daftar_pembelian.tampil_pbl
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        KeyAscii = 0
        close_popup
    End If
End Sub

Public Sub close_popup()
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
    If cari_supplier.Visible = True Then
        cari_supplier.Visible = False
        Text1(1).SetFocus
    End If
    If cari_stok.Visible = True Then
        cari_stok.Visible = False
        Text3(0).SetFocus
    End If
End Sub

Private Sub Form_Load()
    koneksi
    get_cabang
    kosong
'    otomatis
End Sub

Private Sub Form_Activate()
    Text1(1).SetFocus
    get_akun
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture2.Top = Me.Height - 2800
'    Picture2.Width = Me.Width - 255
    Picture1.Height = Me.Height - 2800
'    Picture1.Width = Me.Width - 255
    ListView1.Height = Picture1.Height - 2000
'    ListView1.Width = Me.Width - 495
'    Label1.Width = Me.Width - 50
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        KeyAscii = 0
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
        End If
        Text3(0).SetFocus
    End If
End Sub

Private Sub option_pajak_Click(Index As Integer)
    option_pajak(Index).Value = option_pajak(Index).Value
    hitung_total
End Sub

Private Sub Text1_Change(Index As Integer)
    Select Case Index
        Case 0
'            kosong
        Case 1
            Text1(2).Text = ""
            Frame4.Visible = False
    End Select
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        If Index = 1 Then
            cari_supplier.Left = 1550
            cari_supplier.Top = 3800
            Set cari_supplier.FormPemanggil = Me
            cari_supplier.Show 1
        ElseIf Index = 3 Then
            Set cari_po.FormPemanggil = Me
            cari_po.Show 1
        End If
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
        kosong
    End If
    
    If KeyAscii = 13 Then
        If Index = 0 Then
            get_pembelian Text1(0).Text
        ElseIf Index = 1 Then
            get_supplier Text1(1).Text
        ElseIf Index = 3 Then
            get_po Text1(3).Text
        End If
    End If
End Sub

Private Sub cmb_satuan_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{tab}"
End Sub

Private Sub cmb_satuan_Click()
    Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
    
    isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
    hsatuan = Val(Text3(10).Text) * isi
    
    disc1 = (Val(Format(Text3(5).Text, "##0.00")) / 100) * hsatuan
    disc2 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * (hsatuan - disc1)
    discRp = Val(Format(Text3(7).Text, "##0.00"))
    qty = Val(Format(Text3(2).Text, "##0.00"))
    
    total = (hsatuan - disc1 - disc2 - discRp) * qty
    
    Text3(4).Text = Format(hsatuan, "#,##0.00")
    Text3(9).Text = Format(total, "#,##0.00")
End Sub

Private Sub Text3_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 10
                Text3(z).Text = IIf(z > 1, "0.00", "")
                Text3(z).Locked = True
            Next z
            Text3(4).Text = "0.00"
            cmb_satuan.Clear
            cmb_satuan.Locked = True
        Case 2 To 8
            On Error Resume Next
            Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
    
            isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
            hsatuan = Val(Text3(10).Text) * isi
            
            disc1 = (Val(Format(Text3(5).Text, "##0.00")) / 100) * hsatuan
            disc2 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * (hsatuan - disc1)
            discRp = Val(Format(Text3(7).Text, "##0.00"))
            qty = Val(Format(Text3(2).Text, "##0.00"))
            
            total = (hsatuan - disc1 - disc2 - discRp) * qty
            
            Text3(4).Text = Format(hsatuan, "#,##0.00")
            Text3(9).Text = Format(total, "#,##0.00")
    End Select
End Sub

Private Sub Text3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
        cari_stok.Left = 700
        cari_stok.Top = 4600
        cari_stok.bara = Text3(0).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text3(0).Text <> "" Then
                    cmb_satuan.Clear
                    get_item Text3(0).Text
                End If
            End If
        Case 9
            If KeyAscii = 13 Then
                If Text3(0).Text = "" And Val(Format(Text3(9).Text, "##0.00")) = 0 Then
                    MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
                    Exit Sub
                End If
                tambah_item
                hitung_total
            End If
        Case Else
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
            Else
                If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
                KeyAscii = 0
                End If
            End If
    End Select
End Sub

Private Sub Text3_GotFocus(Index As Integer)
    Select Case Index
        Case 2 To 9
            With Text3(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text3_LostFocus(Index As Integer)
    Select Case Index
        Case 2 To 8
            On Error Resume Next
            Text3(Index).Text = IIf(Text3(Index).Text = "", "0.00", Text3(Index).Text)
            If Index = 5 Or Index = 6 Or Index = 8 Then
                Text3(Index).Text = Format(Text3(Index).Text, "#0.00")
            Else
                Text3(Index).Text = Format(Text3(Index).Text, "#,##0.00")
            End If
            
            Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
            isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
            hsatuan = Val(Text3(10).Text) * isi
            
            disc1 = (Val(Format(Text3(5).Text, "##0.00")) / 100) * hsatuan
            disc2 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * (hsatuan - disc1)
            discRp = Val(Format(Text3(7).Text, "##0.00"))
            qty = Val(Format(Text3(2).Text, "##0.00"))
            
            total = (hsatuan - disc1 - disc2 - discRp) * qty
            
            Text3(4).Text = Format(hsatuan, "#,##0.00")
            Text3(9).Text = Format(total, "#,##0.00")
    End Select
End Sub

Private Sub Text4_Change(Index As Integer)
    Dim Value, bruto As Double
    bruto = Val(Format(Text4(5).Text, "##0.00"))
    Select Case Index
        Case 0
'            Value = Format(Text4(0).Text, "##0.00")
'            Text4(1).Text = Format(Value / 100 * bruto, "#,##0.00")
        Case 1
'            value = Val(Format(Text4(1).Text, "##0.00"))
'            If value > 0 Then
'                Text4(0).Text = Format(value / bruto * 100, "##0.00")
'            End If
        Case 2
'            Value = Format(Text4(2).Text, "##0.00")
'            Text4(3).Text = Format(Value / 100 * bruto, "#,##0.00")
    End Select
'    hitung_total
End Sub

Private Sub Text4_GotFocus(Index As Integer)
    Select Case Index
        Case 0 To 9
            With Text4(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text4_LostFocus(Index As Integer)
    Text4(Index).Text = IIf(Text4(Index).Text = "", "0.00", Text4(Index).Text)
    If Index = 0 Or Index = 2 Then
        Text4(Index).Text = Format(Text4(Index).Text, "#0.00")
    Else
        Text4(Index).Text = Format(Text4(Index).Text, "#,##0.00")
    End If
            
    Dim potongan, bruto As Double
    bruto = Val(Format(Text4(5).Text, "##0.00"))
    
    If Index = 0 Then
        potongan = Val(Format(Text4(0).Text, "##0.00"))
        Text4(1).Text = Format(potongan / 100 * bruto, "#,##0.00")
    End If
    
    If Index = 1 Then
        potongan = Val(Format(Text4(1).Text, "##0.00"))
        If bruto > 0 Then
            Text4(0).Text = Format(potongan / bruto * 100, "##0.00")
        End If
    End If
    
    hitung_total
End Sub

Private Sub Text4_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    Else
        If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") Or KeyAscii = vbKeyBack _
            Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub DTPicker2_Change()
    DTPicker2.Value = IIf(DTPicker2.Value <= DTPicker1.Value, DateAdd("d", 1, DTPicker1.Value), DTPicker2.Value)
    UpDown1.Value = DateDiff("d", DTPicker1.Value, DTPicker2.Value)
End Sub

Private Sub UpDown1_Change()
    Text6.Text = UpDown1.Value
    DTPicker2.Value = DateAdd("d", UpDown1.Value, DTPicker1.Value)
End Sub

Private Sub tambah_item()
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(2) = Text3(0).Text
    item.SubItems(3) = Text3(1).Text
    item.SubItems(4) = Text3(2).Text
    item.SubItems(5) = Text3(3).Text 'satuan dasar
    item.SubItems(6) = cmb_satuan.Text 'satuan
    Dim isi As Double
    isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
    item.SubItems(7) = cmb_satuan.Text & IIf(Val(isi) > 1, " (" & isi & ")", "")
    item.SubItems(8) = isi
    
    Dim disc1, discc2, Harga As Double
    Harga = Val(Format(Text3(4).Text, "##0.00"))
    disc1 = Val(Format(Text3(5).Text, "##0.00"))
    disc2 = Val(Format(Text3(6).Text, "##0.00"))
    
    item.SubItems(9) = Text3(10).Text 'hbeli
    item.SubItems(10) = Text3(4).Text
    item.SubItems(11) = Text3(5).Text
    item.SubItems(12) = disc1 / 100 * Harga
    item.SubItems(13) = Text3(6).Text
    item.SubItems(14) = disc2 / 100 * (Harga - disc1)
    item.SubItems(15) = Text3(7).Text
    item.SubItems(16) = Text3(8).Text
    item.SubItems(17) = Text3(9).Text
    
    Text3(0).Text = ""
    Text3(0).SetFocus
End Sub

Private Sub hitung_total()
    Dim potongan, ppn, lain, dp, bayar, bruto, total, kredit As Double
    If ListView1.ListItems.Count > 0 Then
        For z = 1 To ListView1.ListItems.Count
            bruto = bruto + Val(Format(ListView1.ListItems(z).ListSubItems.item(17), "##0.00"))
        Next
    End If
        
    dp = Val(Format(Text4(6), "##0.00"))
    bayar = Val(Format(Text4(7), "##0.00"))
    potongan = Val(Format(Text4(1), "##0.00"))
        
    ppn = Val(Format(Text4(2).Text, "##0.00"))
    ppn = IIf(ppn > 0, (ppn / 100) * (bruto - potongan), 0)
    Text4(3).Text = Format(ppn, "#,##0.00")
    
    ppn = IIf(option_pajak(0).Value = True, ppn, 0)
    total = (bruto - potongan) + ppn
        
    kredit = total - dp - bayar
    kredit = IIf(kredit < 0, 0, kredit)
        
    frame_tempo.Visible = kredit > 0
    Text4(5).Text = Format(bruto, "#,##0.00")
    Text4(8).Text = Format(kredit, "#,##0.00")
    Text4(9).Text = Format(total, "#,##0.00")
End Sub

Sub kosong()
    Text1(1).Text = ""
    
    For z = 0 To 9
        Text3(z).Text = IIf(z > 1, "0.00", "")
        Text3(z).Locked = True
    Next z
    cmb_satuan.Clear
    cmb_satuan.Locked = True
    
    For z = 0 To 9
        Text4(z).Text = "0.00"
    Next z
    Text4(2).Text = "10.00" ' ppn
    
    UpDown1.Value = 10 'jatuh tempo
    DTPicker2.Value = DateAdd("d", 10, Date) 'jatuh tempo
    DTPicker1.Value = Date
    
    cmd_simpan.Enabled = False
    cmd_batal.Enabled = False
    cmd_cetak.Enabled = False
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
End Sub

Sub tampil_pembelian(nota As String)
    If Text1(0).Text <> "" Then
        pesan = IIf(nota = Text1(0).Text, "dilanjutkan", "mengganti dengan nota " & nota)
        pesan = MsgBox("Form edit pembelian " & Text1(0).Text & " sedang terbuka, apakah ingin " & pesan & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbNo Then
            Exit Sub
        Else
            get_pembelian (nota)
        End If
    Else
        get_pembelian (nota)
    End If
End Sub

Sub get_pembelian(nota As String)
    On Error GoTo exc
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from beli_head where nota='" & nota & "'", con, adOpenKeyset, adLockOptimistic
        
    If Not rs1.EOF Then
        Caption = "Edit Pembelian : " & rs1.Fields!nota
        cmd_simpan.Enabled = IIf(rs1.Fields!Lunas = 0, True, False)
        cmd_batal.Enabled = IIf(rs1.Fields!Lunas = 0, True, False)
        cmd_cetak.Enabled = True
            
        Text1(0).Text = rs1.Fields!nota
        DTPicker1.Value = rs1.Fields!tgl
        Text1(1).Text = "" & rs1.Fields!kdsp
        Text1(2).Text = "" & rs1.Fields!namasp
        Text1(3).Text = "" & rs1.Fields!nopo
        cmb_cabang.Text = "" & rs1.Fields!bo
    
        Text4(0).Text = Format(rs1.Fields!disc, "##0.00")
        Text4(1).Text = Format(rs1.Fields!disctunai, "#,#0.00")
        
        Text4(3).Text = Format(rs1.Fields!ppn, "#,#0.00")
        If rs1.Fields!ppn > 0 And rs1.Fields!bruto > 0 Then
            Text4(2).Text = Format((rs1.Fields!ppn / (rs1.Fields!bruto - rs1.Fields!disctunai)) * 100, "##0.00")
        End If
        
        option_pajak(0).Value = IIf(rs1.Fields!ppn_include = 1, 1, 0)
        option_pajak(1).Value = IIf(rs1.Fields!ppn_include = 0, 1, 0)
        
        Text4(4).Text = Format(rs1.Fields!biaya_lain, "#,#0.00")
                    
        Text4(5).Text = Format(rs1.Fields!bruto, "#,#0.00")
        Text4(7).Text = Format(rs1.Fields!bayar, "#,#0.00")
        Text4(8).Text = Format(rs1.Fields!kredit, "#,#0.00")
        Text4(9).Text = Format(rs1.Fields!netto, "#,#0.00")
        
        If Not IsNull(rs1.Fields!jatuh) Or Not IsEmpty(rs1.Fields!jatuh) Then
            DTPicker2.Value = rs1.Fields!jatuh
            UpDown1.Value = DateDiff("d", rs1.Fields!tgl, rs1.Fields!jatuh)
        End If
            
        frame_tempo.Visible = rs1.Fields!kredit > 0 And Not IsEmpty(rs1.Fields!jatuh)
        Text5.Text = "" & rs1.Fields!ket
            
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from beli_det where nota='" & nota & "'", con, adOpenKeyset, adLockOptimistic
            
        If Not rs2.EOF Then
            ListView1.ListItems.Clear
            Do While Not rs2.EOF
                j = ListView1.ListItems.Count + 1
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!bara
                item.SubItems(3) = "" & rs2.Fields!nama
                item.SubItems(4) = Format(rs2.Fields!qty, "##0.00")
                item.SubItems(5) = "" & rs2.Fields!satuan_dsr
                item.SubItems(6) = "" & rs2.Fields!satuan
                item.SubItems(7) = rs2.Fields!satuan & IIf(rs2.Fields!isi > 1, " (" & rs2.Fields!isi & ")", "")
                item.SubItems(8) = rs2.Fields!isi
                
                Dim disc1, disc2, Harga As Double
                Harga = Val(rs2.Fields!hsatuan)
                disc1 = Val(rs2.Fields!disc1)
                disc2 = Val(rs2.Fields!disc2)
                
                item.SubItems(9) = Format(rs2.Fields!hbeli, "#,#0.00")
                item.SubItems(10) = Format(rs2.Fields!hsatuan, "#,#0.00")
                item.SubItems(11) = Format(disc1 / Harga * 100, "##0.00")
                item.SubItems(12) = disc1
                item.SubItems(13) = Format(disc2 / Harga * 100, "##0.00")
                item.SubItems(14) = disc2
                item.SubItems(15) = Format(rs2.Fields!discRp, "#,#0.00")
                item.SubItems(16) = Format(rs2.Fields!bonusqty, "##0.00")
                item.SubItems(17) = Format(rs2.Fields!total, "#,#0.00")
                rs2.MoveNext
            Loop
        End If
            
        If Text1(1).Text = "" Then Exit Sub
            
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from msupp where kdsp='" & Text1(1).Text & "' limit 1", con, adOpenKeyset, adLockOptimistic
                    
        If Not rs1.EOF Then
            Text1(2).Text = "" & rs1!namasp
            Label2(8).Caption = "" & rs1!alamat
            Label2(9).Caption = "" & rs1!kota
            Label2(10).Caption = "" & rs1!telp
                        
            Frame4.Visible = True
            For z = 0 To 9
                Text3(z).Text = IIf(z < 2, "", "0.00")
                Text3(z).Locked = False
            Next z
            Text3(0).SetFocus
        End If
    Else
        cmd_cetak.Enabled = False
        cmd_batal.Enabled = False
        Caption = "Edit Pembelian : "
        MDIForm1.tabref
        
        MsgBox ("Data Pembelian tidak ditemukan!")
        Text1(0).SetFocus
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_item(bara As String)
    If rs1.State <> 0 Then rs1.Close
    csql = "select m.bara, m.nama, m.satuan, coalesce(s.hbeli, mb.aver)hbeli from mstock m join mstock_bo mb on mb.bara=m.bara " & _
            "left join mstock_supp s on m.bara=s.bara and kdsp='" & Text1(1).Text & "' where m.bara='" & bara & "' and mb.bo='" & cmb_cabang.Text & "' limit 1"
        
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Text3(0).Text = "" & rs1!bara
        Text3(1).Text = "" & rs1!nama
        Text3(3).Text = "" & rs1!satuan
        Text3(10).Text = rs1!hbeli
        With cmb_satuan
            .AddItem (rs1!satuan)
            .ItemData(.NewIndex) = 1 'isi
        End With
        Text3(4).Text = Format(rs1!hbeli, "#,##0.00")
                        
        'cmb_satuan.Clear
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from mstock_satuan where bara='" & Text3(0).Text & "' and bo='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            Do While Not rs2.EOF
                With cmb_satuan
                    .AddItem rs2!satuan
                    .ItemData(.NewIndex) = IIf(Val(rs2!isi) < 1, 1, Val(rs2!isi)) 'isi
                End With
                rs2.MoveNext
            Loop
        End If
                        
        Text3(4).Text = Format(Val(Text3(10).Text) * Val(cmb_satuan.ItemData(0)), "#,##0.00")
        cmb_satuan.ListIndex = 0
        cmb_satuan.Locked = False
            
        For z = 1 To 9
            Text3(z).Locked = False
        Next z
    Else
        MsgBox ("Kode barang tidak ada")
        cari_stok.Left = 700
        cari_stok.Top = 4600
        cari_stok.bara = Text3(0).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
    SendKeys "{tab}"
End Sub

Sub get_supplier(kode As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msupp where kdsp='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(1).Text = "" & rs1!kdsp
        Text1(2).Text = "" & rs1!namasp
        Label2(8).Caption = "" & rs1!alamat
        Label2(9).Caption = "" & rs1!kota
        Label2(10).Caption = "" & rs1!telp
                
        Frame4.Visible = True
        For z = 0 To 9
            Text3(z).Text = IIf(z < 2, "", "0.00")
            Text3(z).Locked = False
        Next z
'        Text3(0).SetFocus
        SendKeys "{tab}"
    Else
        MsgBox ("Kode supplier tidak ada")
        Frame4.Visible = False
        cari_supplier.Left = 1550
        cari_supplier.Top = 3800
        Set cari_supplier.FormPemanggil = Me
        cari_supplier.Show 1
    End If
End Sub

Sub get_po(kode As String)
    kosong
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select p.*, alamat, kota, telp from po_head p left join msupp m on p.kdsp=m.kdsp where nopo='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(1).Text = "" & rs1!kdsp
        Text1(2).Text = "" & rs1!namasp
        Label2(8).Caption = "" & rs1!alamat
        Label2(9).Caption = "" & rs1!kota
        Label2(10).Caption = "" & rs1!telp
        
        Text1(3).Text = rs1.Fields!nopo
        Text4(6).Text = Format(rs1.Fields!dp, "#,#0.00")
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from po_det where nopo='" & Text1(3).Text & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            ListView1.ListItems.Clear
            Do While Not rs2.EOF
                j = ListView1.ListItems.Count + 1
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!bara
                item.SubItems(3) = "" & rs2.Fields!nama
                item.SubItems(4) = Format(rs2.Fields!pesan, "#,#0.00")
                item.SubItems(5) = "" & rs2.Fields!satuan_dsr
                item.SubItems(6) = "" & rs2.Fields!satuan
                item.SubItems(7) = rs2.Fields!satuan & IIf(rs2.Fields!isi > 1, " (" & rs2.Fields!isi & ")", "")
                item.SubItems(8) = rs2.Fields!isi
                    
'                Dim disc1, disc2, Harga As Double
'                Harga = Val(rs2.Fields!hsatuan)
'                disc1 = Val(rs2.Fields!disc1)
'                disc2 = Val(rs2.Fields!disc2)
                    
                item.SubItems(9) = Format(rs2.Fields!hbeli, "#,#0.00")
                item.SubItems(10) = Format(rs2.Fields!hbeli, "#,#0.00")
                item.SubItems(11) = "0.00" 'Format(disc1 / Harga * 100, "##0.00")
                item.SubItems(12) = "0.00" 'disc1
                item.SubItems(13) = "0.00" 'Format(disc2 / Harga * 100, "##0.00")
                item.SubItems(14) = "0.00" 'disc2
                item.SubItems(15) = "0.00" 'Format(rs2.Fields!discRp, "#,#0.00")
                item.SubItems(16) = "0.00" 'Format(rs2.Fields!bonusqty, "##0.00")
                item.SubItems(17) = Format(rs2.Fields!total, "#,#0.00")
                rs2.MoveNext
            Loop
            hitung_total
        End If
    Else
        MsgBox ("Nota pemesanan tidak ada")
    End If
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!bo)
            cmb_cabang.AddItem (rs1.Fields!bo)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Sub get_akun()
    Set akun = New Collection
    For z = 0 To combo_akun.Count - 1
        combo_akun(z).Clear
    Next z
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis=1 order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim coa As PropertyField
        Do While Not rs1.EOF
            For z = 0 To combo_akun.Count - 1
                combo_akun(z).AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            Next z
            
            Set coa = New PropertyField
            coa.Key = rs1.Fields!kode
            coa.Value = rs1.Fields!nama
            
            akun.Add coa
            rs1.MoveNext
        Loop
    End If
End Sub

Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Pembelian'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = cmb_cabang.Text
        
        kol3 = rs1.Fields!kol3
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = cmb_cabang.Text
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = cmb_cabang.Text
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = cmb_cabang.Text
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nota) as nota from beli_head where length(nota)=" & j + z & " and right(nota," & z & ")='" & nomor_nota & "' and left(nota," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nota) as nota from beli_head where length(nota)=" & j + z & " and left(nota," & z & ")='" & nomor_nota & "' and right(nota," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub
