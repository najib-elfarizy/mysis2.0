VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form master_stock 
   Caption         =   "Data Item"
   ClientHeight    =   8370
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   19545
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8370
   ScaleWidth      =   19545
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame10 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1215
      Left            =   0
      TabIndex        =   152
      Top             =   7680
      Visible         =   0   'False
      Width           =   10575
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   280
         Left            =   6430
         TabIndex        =   156
         Top             =   360
         Width           =   1750
         _ExtentX        =   3096
         _ExtentY        =   503
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   83755011
         CurrentDate     =   43158
      End
      Begin VB.CheckBox Check9 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Supplier Utama"
         Height          =   255
         Left            =   8200
         TabIndex        =   157
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox Text7 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   5040
         TabIndex        =   155
         Text            =   "0"
         Top             =   360
         Width           =   1380
      End
      Begin VB.TextBox Text7 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   75
         TabIndex        =   154
         Top             =   360
         Width           =   1845
      End
      Begin VB.TextBox Text7 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   1920
         TabIndex        =   153
         TabStop         =   0   'False
         Top             =   360
         Width           =   3135
      End
      Begin MySIS.Button Button9 
         Height          =   315
         Index           =   0
         Left            =   5040
         TabIndex        =   165
         Top             =   840
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         BTYPE           =   2
         TX              =   "Simpan"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button9 
         Height          =   315
         Index           =   1
         Left            =   6480
         TabIndex        =   166
         Top             =   840
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         BTYPE           =   2
         TX              =   "Batal"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Master"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   8160
         TabIndex        =   161
         Top             =   45
         Width           =   1620
      End
      Begin VB.Image Image1 
         Height          =   300
         Left            =   9840
         Picture         =   "master_stock.frx":0038
         Top             =   0
         Width           =   675
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Supplier"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   75
         TabIndex        =   160
         Top             =   45
         Width           =   4965
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Harga Beli"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   5040
         TabIndex        =   159
         Top             =   45
         Width           =   1380
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tanggal Kirim"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   6420
         TabIndex        =   158
         Top             =   45
         Width           =   1740
      End
   End
   Begin VB.Frame Frame8 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   735
      Left            =   12600
      TabIndex        =   136
      Top             =   5880
      Visible         =   0   'False
      Width           =   10335
      Begin VB.TextBox Text6 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   5
         Left            =   7920
         TabIndex        =   148
         Text            =   "0"
         Top             =   360
         Width           =   1620
      End
      Begin VB.TextBox Text6 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   1
         Left            =   2280
         TabIndex        =   144
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox Text6 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   0
         Left            =   75
         TabIndex        =   143
         Top             =   360
         Width           =   2205
      End
      Begin VB.TextBox Text6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   2
         Left            =   4320
         TabIndex        =   145
         Text            =   "0"
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Text6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   4
         Left            =   6900
         TabIndex        =   147
         Text            =   "0"
         Top             =   360
         Width           =   1020
      End
      Begin VB.TextBox Text6 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Index           =   3
         Left            =   5520
         Locked          =   -1  'True
         TabIndex        =   146
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   360
         Width           =   1380
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Barcode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2280
         TabIndex        =   142
         Top             =   45
         Width           =   2055
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "MUP (%)"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   6900
         TabIndex        =   141
         Top             =   45
         Width           =   1020
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Harga Pokok"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   5520
         TabIndex        =   140
         Top             =   45
         Width           =   1380
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Isi"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   4320
         TabIndex        =   139
         Top             =   45
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Satuan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   75
         TabIndex        =   138
         Top             =   45
         Width           =   2205
      End
      Begin VB.Image Image2 
         Height          =   300
         Left            =   9600
         Picture         =   "master_stock.frx":0443
         Top             =   30
         Width           =   675
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFC0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Harga Jual"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   7920
         TabIndex        =   137
         Top             =   45
         Width           =   1620
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Daftar Akun Perkiraan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Left            =   12600
      TabIndex        =   162
      Top             =   4320
      Visible         =   0   'False
      Width           =   6735
      Begin VB.TextBox Text8 
         Height          =   285
         Left            =   120
         TabIndex        =   163
         Top             =   240
         Width           =   6495
      End
      Begin MSComctlLib.ListView ListView11 
         Height          =   3375
         Left            =   120
         TabIndex        =   164
         Top             =   600
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   5953
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame9 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Daftar Lokasi/Rak"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   12600
      TabIndex        =   149
      Top             =   3360
      Visible         =   0   'False
      Width           =   6135
      Begin VB.TextBox Text5 
         Height          =   285
         Left            =   120
         TabIndex        =   150
         Top             =   240
         Width           =   5895
      End
      Begin MSComctlLib.ListView ListView10 
         Height          =   3135
         Left            =   120
         TabIndex        =   151
         Top             =   600
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Lokasi"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Keterangan"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   12465
      TabIndex        =   28
      Top             =   6840
      Width           =   12495
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   31
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":084E
         PICN            =   "master_stock.frx":086A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   1440
         TabIndex        =   32
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":09C4
         PICN            =   "master_stock.frx":09E0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame Frame7 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Daftar Satuan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   12600
      TabIndex        =   133
      Top             =   2160
      Visible         =   0   'False
      Width           =   6135
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   120
         TabIndex        =   134
         Top             =   240
         Width           =   5895
      End
      Begin MSComctlLib.ListView ListView9 
         Height          =   3135
         Left            =   120
         TabIndex        =   135
         Top             =   600
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Merk"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Keterangan"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Daftar Merk"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   12600
      TabIndex        =   130
      Top             =   1080
      Visible         =   0   'False
      Width           =   6135
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   120
         TabIndex        =   131
         Top             =   240
         Width           =   5895
      End
      Begin MSComctlLib.ListView ListView8 
         Height          =   3135
         Left            =   120
         TabIndex        =   132
         Top             =   600
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Merk"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Keterangan"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Daftar Kategori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   12600
      TabIndex        =   124
      Top             =   0
      Visible         =   0   'False
      Width           =   6855
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   120
         TabIndex        =   126
         Text            =   "Text2"
         Top             =   240
         Width           =   6615
      End
      Begin MSComctlLib.ListView ListView7 
         Height          =   3135
         Left            =   120
         TabIndex        =   125
         Top             =   600
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode KTG"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Kategori"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Kode Sub"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Sub Kategori"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Object.Width           =   0
         EndProperty
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   6855
      Left            =   0
      ScaleHeight     =   6825
      ScaleWidth      =   12465
      TabIndex        =   26
      Top             =   0
      Width           =   12495
      Begin VB.TextBox Text9 
         Height          =   1635
         Left            =   7080
         MultiLine       =   -1  'True
         TabIndex        =   167
         Text            =   "master_stock.frx":0E16
         Top             =   720
         Width           =   3975
      End
      Begin MSComDlg.CommonDialog codi 
         Left            =   9480
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   ".jpg"
         DialogTitle     =   "Pilih Foto"
         Filter          =   "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF"
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Left            =   3135
         TabIndex        =   48
         Top             =   1680
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":0E1C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   2
         Left            =   5880
         MaxLength       =   15
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   960
         Width           =   1095
      End
      Begin TabDlg.SSTab SSTab1 
         Height          =   4335
         Left            =   120
         TabIndex        =   44
         Top             =   2445
         Width           =   12255
         _ExtentX        =   21616
         _ExtentY        =   7646
         _Version        =   393216
         Tabs            =   8
         TabsPerRow      =   8
         TabHeight       =   520
         TabCaption(0)   =   "General"
         TabPicture(0)   =   "master_stock.frx":0E38
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Picture3"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Multi Unit"
         TabPicture(1)   =   "master_stock.frx":0E54
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Picture4"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Assembly"
         TabPicture(2)   =   "master_stock.frx":0E70
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "ListView1"
         Tab(2).Control(1)=   "Button5"
         Tab(2).Control(2)=   "Button18"
         Tab(2).Control(3)=   "Label2(8)"
         Tab(2).ControlCount=   4
         TabCaption(3)   =   "Gudang"
         TabPicture(3)   =   "master_stock.frx":0E8C
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Picture6"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Lot Number"
         TabPicture(4)   =   "master_stock.frx":0EA8
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "ListView2"
         Tab(4).Control(1)=   "ListView3"
         Tab(4).Control(2)=   "Button14"
         Tab(4).Control(3)=   "Button15"
         Tab(4).Control(4)=   "Label2(11)"
         Tab(4).Control(5)=   "Label2(10)"
         Tab(4).ControlCount=   6
         TabCaption(5)   =   "Accounting"
         TabPicture(5)   =   "master_stock.frx":0EC4
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "Picture7"
         Tab(5).ControlCount=   1
         TabCaption(6)   =   "Supplier"
         TabPicture(6)   =   "master_stock.frx":0EE0
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "Label2(35)"
         Tab(6).Control(1)=   "Button17"
         Tab(6).Control(2)=   "Button12"
         Tab(6).Control(3)=   "ListView6"
         Tab(6).ControlCount=   4
         TabCaption(7)   =   "Lain-lain"
         TabPicture(7)   =   "master_stock.frx":0EFC
         Tab(7).ControlEnabled=   0   'False
         Tab(7).Control(0)=   "Picture8"
         Tab(7).ControlCount=   1
         Begin VB.PictureBox Picture8 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3375
            Left            =   -74880
            ScaleHeight     =   3345
            ScaleWidth      =   11265
            TabIndex        =   115
            Top             =   480
            Width           =   11295
         End
         Begin VB.PictureBox Picture7 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3255
            Left            =   -74880
            ScaleHeight     =   3225
            ScaleWidth      =   10545
            TabIndex        =   60
            Top             =   480
            Width           =   10575
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   27
               Left            =   1320
               MaxLength       =   15
               TabIndex        =   107
               Text            =   "PJK-PJL"
               Top             =   2280
               Width           =   1215
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   26
               Left            =   2520
               TabIndex        =   106
               Text            =   "PJK-PJL"
               Top             =   2280
               Width           =   3975
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   25
               Left            =   1320
               MaxLength       =   15
               TabIndex        =   103
               Text            =   "PJL"
               Top             =   1680
               Width           =   1215
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   24
               Left            =   2520
               TabIndex        =   102
               Text            =   "PJL"
               Top             =   1680
               Width           =   3975
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   23
               Left            =   1320
               MaxLength       =   15
               TabIndex        =   99
               Text            =   "PJL"
               Top             =   1080
               Width           =   1215
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   22
               Left            =   2520
               TabIndex        =   98
               Text            =   "Text1"
               Top             =   1080
               Width           =   3975
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   21
               Left            =   1320
               MaxLength       =   15
               TabIndex        =   95
               Text            =   "INV"
               Top             =   480
               Width           =   1215
            End
            Begin VB.TextBox Text1 
               Height          =   315
               Index           =   20
               Left            =   2520
               TabIndex        =   94
               Text            =   "INV"
               Top             =   480
               Width           =   3975
            End
            Begin MySIS.Button Button8 
               Height          =   315
               Index           =   0
               Left            =   960
               TabIndex        =   96
               Top             =   480
               Width           =   315
               _ExtentX        =   556
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "+"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0F18
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin MySIS.Button Button8 
               Height          =   315
               Index           =   1
               Left            =   960
               TabIndex        =   100
               Top             =   1080
               Width           =   315
               _ExtentX        =   556
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "+"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0F34
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin MySIS.Button Button8 
               Height          =   315
               Index           =   2
               Left            =   960
               TabIndex        =   104
               Top             =   1680
               Width           =   315
               _ExtentX        =   556
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "+"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0F50
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin MySIS.Button Button8 
               Height          =   315
               Index           =   3
               Left            =   960
               TabIndex        =   108
               Top             =   2280
               Width           =   315
               _ExtentX        =   556
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "+"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0F6C
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Kode Perkiraan untuk Pajak Penjualan : "
               Height          =   255
               Index           =   32
               Left            =   960
               TabIndex        =   105
               Top             =   2040
               Width           =   3135
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Kode Perkiraan untuk H.P.P : "
               Height          =   255
               Index           =   31
               Left            =   960
               TabIndex        =   101
               Top             =   1440
               Width           =   2655
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Kode Perkiraan untuk Penjualan : "
               Height          =   255
               Index           =   30
               Left            =   960
               TabIndex        =   97
               Top             =   840
               Width           =   2655
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Kode Perkiraan untuk Inventory : "
               Height          =   255
               Index           =   29
               Left            =   960
               TabIndex        =   93
               Top             =   240
               Width           =   2655
            End
         End
         Begin VB.PictureBox Picture6 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3375
            Left            =   -74880
            ScaleHeight     =   3345
            ScaleWidth      =   10545
            TabIndex        =   59
            Top             =   480
            Width           =   10575
            Begin MSComctlLib.ListView ListView5 
               Height          =   2655
               Left            =   120
               TabIndex        =   92
               Top             =   600
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   4683
               LabelWrap       =   -1  'True
               HideSelection   =   -1  'True
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   13
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "NO"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Gudang"
                  Object.Width           =   3528
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   2
                  Text            =   "Quantity"
                  Object.Width           =   2117
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Text            =   "Unit"
                  Object.Width           =   2646
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   4
                  Text            =   "Harga Pokok"
                  Object.Width           =   2646
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   5
                  Text            =   "Mup (%)"
                  Object.Width           =   2117
               EndProperty
               BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   6
                  Text            =   "Harga Jual"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   7
                  Text            =   "Qty Step1"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   8
                  Text            =   "Price Step1"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   9
                  Text            =   "Qty Step2"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   10
                  Text            =   "Price Step2"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   11
                  Text            =   "Qty Step3"
                  Object.Width           =   2469
               EndProperty
               BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   12
                  Text            =   "Price Step3"
                  Object.Width           =   2469
               EndProperty
            End
            Begin MySIS.Button Button16 
               Height          =   315
               Left            =   120
               TabIndex        =   127
               Top             =   120
               Width           =   2355
               _ExtentX        =   4154
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "Load Stock Gudang/Cabang"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0F88
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
         End
         Begin VB.PictureBox Picture4 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3375
            Left            =   -74880
            ScaleHeight     =   3345
            ScaleWidth      =   10545
            TabIndex        =   58
            Top             =   480
            Width           =   10575
            Begin MSComctlLib.ListView ListView4 
               Height          =   2775
               Left            =   120
               TabIndex        =   89
               Top             =   480
               Width           =   10335
               _ExtentX        =   18230
               _ExtentY        =   4895
               LabelWrap       =   -1  'True
               HideSelection   =   -1  'True
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   -2147483643
               BorderStyle     =   1
               Appearance      =   1
               NumItems        =   7
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "NO"
                  Object.Width           =   0
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Satuan"
                  Object.Width           =   3528
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Barcode"
                  Object.Width           =   3528
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   3
                  Text            =   "Isi"
                  Object.Width           =   2117
               EndProperty
               BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   4
                  Text            =   "Harga Pokok"
                  Object.Width           =   2822
               EndProperty
               BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   5
                  Text            =   "Mup (%)"
                  Object.Width           =   2293
               EndProperty
               BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Alignment       =   1
                  SubItemIndex    =   6
                  Text            =   "Harga Jual"
                  Object.Width           =   2822
               EndProperty
            End
            Begin MySIS.Button Button7 
               Height          =   315
               Left            =   120
               TabIndex        =   90
               Top             =   120
               Width           =   315
               _ExtentX        =   556
               _ExtentY        =   556
               BTYPE           =   3
               TX              =   "+"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   49152
               FCOLO           =   49152
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "master_stock.frx":0FA4
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Tambah Unit / Satuan"
               Height          =   255
               Index           =   28
               Left            =   600
               TabIndex        =   91
               Top             =   165
               Width           =   3735
            End
         End
         Begin VB.PictureBox Picture3 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   3735
            Left            =   120
            ScaleHeight     =   3705
            ScaleWidth      =   11985
            TabIndex        =   57
            Top             =   480
            Width           =   12015
            Begin VB.Frame Frame4 
               BackColor       =   &H80000005&
               Height          =   3495
               Left            =   8640
               TabIndex        =   78
               Top             =   120
               Width           =   3255
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   31
                  Left            =   2400
                  MaxLength       =   15
                  TabIndex        =   25
                  Text            =   "Text1"
                  Top             =   1320
                  Width           =   735
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   30
                  Left            =   2400
                  MaxLength       =   15
                  TabIndex        =   24
                  Text            =   "Text1"
                  Top             =   960
                  Width           =   735
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   435
                  Index           =   19
                  Left            =   120
                  Locked          =   -1  'True
                  MaxLength       =   15
                  TabIndex        =   87
                  Text            =   "Text1"
                  Top             =   480
                  Width           =   1575
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   18
                  Left            =   2400
                  MaxLength       =   15
                  TabIndex        =   22
                  Text            =   "Text1"
                  Top             =   240
                  Width           =   735
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   17
                  Left            =   2400
                  MaxLength       =   15
                  TabIndex        =   23
                  Text            =   "Text1"
                  Top             =   600
                  Width           =   735
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Standar MarkUP (%) : "
                  Height          =   255
                  Index           =   37
                  Left            =   120
                  TabIndex        =   120
                  Top             =   1320
                  Width           =   2175
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Minimum Order Supplier : "
                  Height          =   255
                  Index           =   36
                  Left            =   120
                  TabIndex        =   119
                  Top             =   960
                  Width           =   2175
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Saldo : "
                  Height          =   255
                  Index           =   27
                  Left            =   240
                  TabIndex        =   88
                  Top             =   240
                  Width           =   1455
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Min : "
                  Height          =   255
                  Index           =   26
                  Left            =   1800
                  TabIndex        =   86
                  Top             =   240
                  Width           =   495
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Max : "
                  Height          =   255
                  Index           =   25
                  Left            =   1800
                  TabIndex        =   85
                  Top             =   600
                  Width           =   495
               End
            End
            Begin VB.Frame Frame2 
               BackColor       =   &H80000005&
               Caption         =   "Pilihan"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000080FF&
               Height          =   1095
               Left            =   120
               TabIndex        =   71
               Top             =   2520
               Width           =   8415
               Begin VB.CheckBox Check8 
                  BackColor       =   &H80000005&
                  Caption         =   "Fee"
                  Height          =   195
                  Left            =   4800
                  TabIndex        =   114
                  Top             =   480
                  Width           =   855
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   29
                  Left            =   6720
                  MaxLength       =   15
                  TabIndex        =   111
                  Text            =   "Text1"
                  Top             =   720
                  Width           =   1575
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   28
                  Left            =   6720
                  MaxLength       =   15
                  TabIndex        =   110
                  Text            =   "Text1"
                  Top             =   360
                  Width           =   1575
               End
               Begin VB.CheckBox Check7 
                  BackColor       =   &H80000005&
                  Caption         =   "Point "
                  Height          =   195
                  Left            =   4800
                  TabIndex        =   109
                  Top             =   240
                  Width           =   855
               End
               Begin VB.CheckBox Check1 
                  BackColor       =   &H80000005&
                  Caption         =   "Item Masih Aktif"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   77
                  Top             =   240
                  Width           =   1575
               End
               Begin VB.CheckBox Check2 
                  BackColor       =   &H80000005&
                  Caption         =   "Item Kena Pajak"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   76
                  Top             =   480
                  Width           =   1575
               End
               Begin VB.CheckBox Check3 
                  BackColor       =   &H80000005&
                  Caption         =   "Allow change Discount"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   75
                  Top             =   720
                  Width           =   2055
               End
               Begin VB.CheckBox Check4 
                  BackColor       =   &H80000005&
                  Caption         =   "Allow change Price"
                  Height          =   195
                  Left            =   2520
                  TabIndex        =   74
                  Top             =   720
                  Width           =   2055
               End
               Begin VB.CheckBox Check5 
                  BackColor       =   &H80000005&
                  Caption         =   "Multi unit/ satuan/ harga"
                  Height          =   195
                  Left            =   2520
                  TabIndex        =   73
                  Top             =   240
                  Width           =   2055
               End
               Begin VB.CheckBox Check6 
                  BackColor       =   &H80000005&
                  Caption         =   "Serial/Lot/Expire/IMEI"
                  Height          =   195
                  Left            =   2520
                  TabIndex        =   72
                  Top             =   480
                  Width           =   2055
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Fee (Nominal)"
                  Height          =   255
                  Index           =   34
                  Left            =   5400
                  TabIndex        =   113
                  Top             =   720
                  Width           =   1215
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Fee (%)  "
                  Height          =   255
                  Index           =   33
                  Left            =   5760
                  TabIndex        =   112
                  Top             =   360
                  Width           =   975
               End
            End
            Begin VB.Frame Frame3 
               BackColor       =   &H80000005&
               Caption         =   "Qty Step for Sales Pricing"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000080FF&
               Height          =   2415
               Left            =   3720
               TabIndex        =   70
               Top             =   120
               Width           =   4815
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   16
                  Left            =   3480
                  MaxLength       =   15
                  TabIndex        =   21
                  Text            =   "Text1"
                  Top             =   1440
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   15
                  Left            =   2160
                  MaxLength       =   15
                  TabIndex        =   20
                  Text            =   "Text1"
                  Top             =   1440
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   14
                  Left            =   840
                  MaxLength       =   15
                  TabIndex        =   19
                  Text            =   "Text1"
                  Top             =   1440
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   13
                  Left            =   3480
                  MaxLength       =   15
                  TabIndex        =   18
                  Text            =   "Text1"
                  Top             =   1080
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   12
                  Left            =   2160
                  MaxLength       =   15
                  TabIndex        =   17
                  Text            =   "Text1"
                  Top             =   1080
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   11
                  Left            =   840
                  MaxLength       =   15
                  TabIndex        =   16
                  Text            =   "Text1"
                  Top             =   1080
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   10
                  Left            =   3480
                  MaxLength       =   15
                  TabIndex        =   15
                  Text            =   "Text1"
                  Top             =   720
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   9
                  Left            =   2160
                  MaxLength       =   15
                  TabIndex        =   14
                  Text            =   "Text1"
                  Top             =   720
                  Width           =   1215
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   8
                  Left            =   840
                  MaxLength       =   15
                  TabIndex        =   13
                  Text            =   "Text1"
                  Top             =   720
                  Width           =   1215
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Step 3"
                  Height          =   255
                  Index           =   24
                  Left            =   3840
                  TabIndex        =   84
                  Top             =   480
                  Width           =   855
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Step 2"
                  Height          =   255
                  Index           =   23
                  Left            =   2520
                  TabIndex        =   83
                  Top             =   480
                  Width           =   855
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Step 1"
                  Height          =   255
                  Index           =   22
                  Left            =   1200
                  TabIndex        =   82
                  Top             =   480
                  Width           =   855
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Price"
                  Height          =   255
                  Index           =   21
                  Left            =   120
                  TabIndex        =   81
                  Top             =   1440
                  Width           =   615
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Mup (%)"
                  Height          =   255
                  Index           =   20
                  Left            =   120
                  TabIndex        =   80
                  Top             =   1080
                  Width           =   615
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Qty"
                  Height          =   255
                  Index           =   19
                  Left            =   120
                  TabIndex        =   79
                  Top             =   720
                  Width           =   615
               End
            End
            Begin VB.Frame Frame1 
               BackColor       =   &H80000005&
               Height          =   2415
               Left            =   120
               TabIndex        =   61
               Top             =   120
               Width           =   3495
               Begin VB.OptionButton OptionCurrency 
                  BackColor       =   &H00FFFFFF&
                  Caption         =   "USD"
                  Height          =   255
                  Index           =   1
                  Left            =   1920
                  TabIndex        =   172
                  Top             =   2040
                  Width           =   735
               End
               Begin VB.OptionButton OptionCurrency 
                  BackColor       =   &H00FFFFFF&
                  Caption         =   "IDR"
                  Height          =   255
                  Index           =   0
                  Left            =   1080
                  TabIndex        =   171
                  Top             =   2040
                  Value           =   -1  'True
                  Width           =   735
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   7
                  Left            =   1080
                  MaxLength       =   15
                  TabIndex        =   12
                  Text            =   "Text1"
                  Top             =   1680
                  Width           =   1935
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   6
                  Left            =   2280
                  MaxLength       =   15
                  TabIndex        =   11
                  Text            =   "Text1"
                  Top             =   1320
                  Width           =   735
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   3
                  Left            =   1080
                  MaxLength       =   15
                  TabIndex        =   8
                  Text            =   "Text1"
                  Top             =   600
                  Width           =   1935
               End
               Begin VB.ComboBox Combo6 
                  Height          =   315
                  Left            =   1080
                  Locked          =   -1  'True
                  TabIndex        =   7
                  Text            =   "Combo6"
                  Top             =   240
                  Width           =   1935
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   4
                  Left            =   1080
                  MaxLength       =   15
                  TabIndex        =   9
                  Text            =   "Text1"
                  Top             =   960
                  Width           =   1935
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Index           =   5
                  Left            =   1080
                  MaxLength       =   15
                  TabIndex        =   10
                  Text            =   "Text1"
                  Top             =   1320
                  Width           =   735
               End
               Begin MySIS.Button Button6 
                  Height          =   315
                  Left            =   3015
                  TabIndex        =   62
                  Top             =   240
                  Width           =   315
                  _ExtentX        =   556
                  _ExtentY        =   556
                  BTYPE           =   3
                  TX              =   "+"
                  ENAB            =   -1  'True
                  BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  COLTYPE         =   1
                  FOCUSR          =   -1  'True
                  BCOL            =   13160660
                  BCOLO           =   13160660
                  FCOL            =   49152
                  FCOLO           =   49152
                  MCOL            =   12632256
                  MPTR            =   1
                  MICON           =   "master_stock.frx":0FC0
                  UMCOL           =   -1  'True
                  SOFT            =   0   'False
                  PICPOS          =   0
                  NGREY           =   0   'False
                  FX              =   0
                  HAND            =   0   'False
                  CHECK           =   0   'False
                  VALUE           =   0   'False
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Mata Uang : "
                  Height          =   255
                  Index           =   39
                  Left            =   0
                  TabIndex        =   170
                  Top             =   2040
                  Width           =   1095
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Harga Jual : "
                  Height          =   255
                  Index           =   18
                  Left            =   0
                  TabIndex        =   69
                  Top             =   1680
                  Width           =   1095
               End
               Begin VB.Label Label2 
                  BackStyle       =   0  'Transparent
                  Caption         =   " (%)"
                  Height          =   255
                  Index           =   17
                  Left            =   3000
                  TabIndex        =   68
                  Top             =   1320
                  Width           =   375
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Mrg : "
                  Height          =   255
                  Index           =   16
                  Left            =   1800
                  TabIndex        =   67
                  Top             =   1320
                  Width           =   495
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Harga Pokok: "
                  Height          =   255
                  Index           =   12
                  Left            =   0
                  TabIndex        =   66
                  Top             =   600
                  Width           =   1095
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Unit Satuan : "
                  Height          =   255
                  Index           =   13
                  Left            =   0
                  TabIndex        =   65
                  Top             =   240
                  Width           =   1095
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "Harga Beli : "
                  Height          =   255
                  Index           =   14
                  Left            =   0
                  TabIndex        =   64
                  Top             =   960
                  Width           =   1095
               End
               Begin VB.Label Label2 
                  Alignment       =   1  'Right Justify
                  BackStyle       =   0  'Transparent
                  Caption         =   "MUP(%) : "
                  Height          =   255
                  Index           =   15
                  Left            =   0
                  TabIndex        =   63
                  Top             =   1320
                  Width           =   1095
               End
            End
         End
         Begin MSComctlLib.ListView ListView1 
            Height          =   3135
            Left            =   -74880
            TabIndex        =   45
            Top             =   720
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   5530
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "NO"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Item"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Nama Item"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Text            =   "Qty"
               Object.Width           =   2646
            EndProperty
         End
         Begin MySIS.Button Button5 
            Height          =   315
            Left            =   -74880
            TabIndex        =   52
            Top             =   360
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "+"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":0FDC
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MSComctlLib.ListView ListView2 
            Height          =   2535
            Left            =   -74880
            TabIndex        =   53
            Top             =   720
            Width           =   6495
            _ExtentX        =   11456
            _ExtentY        =   4471
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "NO"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Lot Number"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Text            =   "Qty"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Text            =   "Sisa"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Ket"
               Object.Width           =   2117
            EndProperty
         End
         Begin MSComctlLib.ListView ListView3 
            Height          =   2535
            Left            =   -68400
            TabIndex        =   54
            Top             =   720
            Width           =   5055
            _ExtentX        =   8916
            _ExtentY        =   4471
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "NO"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   2
               SubItemIndex    =   1
               Text            =   "Tanggal"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "No Bukti"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Text            =   "masuk"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   4
               Text            =   "keluar"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Text            =   "Ket"
               Object.Width           =   2117
            EndProperty
         End
         Begin MSComctlLib.ListView ListView6 
            Height          =   3015
            Left            =   -74880
            TabIndex        =   116
            Top             =   840
            Width           =   11295
            _ExtentX        =   19923
            _ExtentY        =   5318
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   6
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "NO"
               Object.Width           =   0
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Kode Supp"
               Object.Width           =   3528
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Nama Supp"
               Object.Width           =   5292
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   3
               Text            =   "Harga Beli"
               Object.Width           =   2646
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   2
               SubItemIndex    =   4
               Text            =   "Tgl Kirim"
               Object.Width           =   2822
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Text            =   "Master"
               Object.Width           =   2646
            EndProperty
         End
         Begin MySIS.Button Button12 
            Height          =   315
            Left            =   -74880
            TabIndex        =   117
            Top             =   480
            Width           =   315
            _ExtentX        =   556
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "+"
            ENAB            =   0   'False
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":0FF8
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button Button14 
            Height          =   315
            Left            =   -74880
            TabIndex        =   122
            Top             =   360
            Width           =   915
            _ExtentX        =   1614
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "Load Sisa"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":1014
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button Button15 
            Height          =   315
            Left            =   -73920
            TabIndex        =   123
            Top             =   360
            Width           =   915
            _ExtentX        =   1614
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "Load All"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":1030
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button Button17 
            Height          =   315
            Left            =   -74520
            TabIndex        =   128
            Top             =   480
            Width           =   2355
            _ExtentX        =   4154
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "Load Daftar Supplier"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":104C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MySIS.Button Button18 
            Height          =   315
            Left            =   -74520
            TabIndex        =   129
            Top             =   360
            Width           =   2355
            _ExtentX        =   4154
            _ExtentY        =   556
            BTYPE           =   3
            TX              =   "Load Item Component"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   49152
            FCOLO           =   49152
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "master_stock.frx":1068
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Daftar Supplier yang membawa barang ini"
            Height          =   255
            Index           =   35
            Left            =   -72000
            TabIndex        =   118
            Top             =   525
            Width           =   3735
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Detail transaksi lot number"
            Height          =   255
            Index           =   11
            Left            =   -68400
            TabIndex        =   56
            Top             =   480
            Width           =   4455
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Silahkan Klik 2X untuk lihat detail transaksi per-lot Number"
            Height          =   255
            Index           =   10
            Left            =   -72840
            TabIndex        =   55
            Top             =   480
            Width           =   4455
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Daftar Item yang masukdalam Assembly/rakitan"
            Height          =   255
            Index           =   8
            Left            =   -72000
            TabIndex        =   46
            Top             =   405
            Width           =   3735
         End
      End
      Begin VB.ComboBox Combo5 
         Height          =   315
         Left            =   4800
         Locked          =   -1  'True
         TabIndex        =   6
         Text            =   "Combo5"
         Top             =   2040
         Width           =   1935
      End
      Begin VB.ComboBox Combo4 
         Height          =   315
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   5
         Text            =   "Combo4"
         Top             =   2040
         Width           =   1935
      End
      Begin VB.ComboBox Combo3 
         Height          =   315
         Left            =   4800
         Locked          =   -1  'True
         TabIndex        =   4
         Text            =   "Combo3"
         Top             =   1680
         Width           =   1935
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "Combo2"
         Top             =   1680
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   0
         Left            =   1200
         MaxLength       =   15
         TabIndex        =   0
         Text            =   "Text1"
         Top             =   960
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   1200
         MaxLength       =   50
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   1320
         Width           =   5775
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   1200
         ScaleHeight     =   345
         ScaleWidth      =   5745
         TabIndex        =   34
         Top             =   480
         Width           =   5775
         Begin VB.OptionButton Option3 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Barang"
            Height          =   195
            Left            =   120
            TabIndex        =   38
            Top             =   80
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton Option4 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Rakitan"
            Height          =   195
            Left            =   1200
            TabIndex        =   37
            Top             =   80
            Width           =   855
         End
         Begin VB.OptionButton Option5 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Konsinyasi"
            Height          =   195
            Left            =   2280
            TabIndex        =   36
            Top             =   80
            Width           =   1095
         End
         Begin VB.OptionButton Option6 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Jasa"
            Height          =   195
            Left            =   3600
            TabIndex        =   35
            Top             =   80
            Width           =   855
         End
      End
      Begin VB.ComboBox cmb_cabang 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   120
         Width           =   1215
      End
      Begin MySIS.Button Button2 
         Height          =   315
         Left            =   3135
         TabIndex        =   49
         Top             =   2040
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":1084
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button3 
         Height          =   315
         Left            =   6735
         TabIndex        =   50
         Top             =   1680
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":10A0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Left            =   6735
         TabIndex        =   51
         Top             =   2040
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":10BC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button13 
         Height          =   315
         Left            =   3140
         TabIndex        =   121
         Top             =   960
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "Kode Otomatis"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "master_stock.frx":10D8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Keterangan : "
         Height          =   255
         Index           =   38
         Left            =   7080
         TabIndex        =   169
         Top             =   480
         Width           =   1095
      End
      Begin VB.Image Image3 
         BorderStyle     =   1  'Fixed Single
         Height          =   1900
         Left            =   11160
         Picture         =   "master_stock.frx":10F4
         Stretch         =   -1  'True
         Top             =   480
         Width           =   1920
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "PLU Timbangan :"
         Height          =   255
         Index           =   9
         Left            =   4440
         TabIndex        =   47
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Lokasi/Rak : "
         Height          =   255
         Index           =   7
         Left            =   3720
         TabIndex        =   43
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Merk : "
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   42
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "SubKategori : "
         Height          =   255
         Index           =   5
         Left            =   3720
         TabIndex        =   41
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kategori : "
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   40
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tipe Item : "
         Height          =   255
         Index           =   1
         Left            =   -120
         TabIndex        =   39
         Top             =   510
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Item : "
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   30
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Item : "
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   29
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept/Gudang : "
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   27
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   405
         Index           =   0
         Left            =   0
         TabIndex        =   168
         Top             =   0
         Visible         =   0   'False
         Width           =   12495
      End
   End
End
Attribute VB_Name = "master_stock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim gambar As ADODB.Stream
Dim pathpic As String

Private Sub Button1_Click()
    Frame5.Left = 1200
    Frame5.Top = 1680
    Frame5.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text2.Text = ""
    tampilktg
    Text2.SetFocus
End Sub

Private Sub Button2_Click()
    Frame6.Left = 1200
    Frame6.Top = 1800
    Frame6.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text3.Text = ""
    tampilmerk
    Text3.SetFocus
End Sub

Private Sub Button3_Click()
    Frame5.Left = 1200
    Frame5.Top = 1680
    Frame5.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text2.Text = ""
    tampilktg
    Text2.SetFocus
End Sub

Private Sub Button4_Click()
    Frame9.Left = 1200
    Frame9.Top = 1800
    Frame9.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text5.Text = ""
    tampil_lokasi
    Text5.SetFocus
End Sub

Private Sub Button6_Click()
    Frame7.Left = 1500
    Frame7.Top = 3000
    Frame7.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text4.Text = ""
    tampil_satuan
    Text4.SetFocus
End Sub

Private Sub Button7_Click()
    Frame8.Left = 400
    Frame8.Top = 3500
    Frame8.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text6(0).Text = ""
    Text6(1).Text = "-"
    Text6(2).Text = "1"
    Text6(3).Text = Text1(3).Text
    Text6(4).Text = "0"
    Text6(5).Text = "0"
    Text6(0).SetFocus
End Sub

Private Sub Button16_Click()
    tampil_gudang
End Sub

Private Sub Button12_Click()
    Frame10.Left = 300
    Frame10.Top = 3300
    Frame10.Visible = True
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text7(0).Text = ""
    Text7(1).Text = "-"
    Text7(2).Text = "0"
    Text7(0).SetFocus
End Sub

Private Sub Button17_Click()
    tampil_supplier
End Sub

Private Sub Button8_Click(Index As Integer)
    Frame11.Left = 1500
    Frame11.Top = 3000
    Frame11.Visible = True
    Frame11.Tag = Index
    Picture2.Enabled = False
    Picture5.Enabled = False
    Text8.Text = ""
    tampil_akun
    Text8.SetFocus
End Sub

Private Sub Button9_Click(Index As Integer)
    If Index = 1 Then
        Frame10.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    Else
        If Text7(0).Text = "" And Val(Text7(2).Text) = 0 Then
            MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
            Exit Sub
        End If
        simpan_supplier
    End If
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    On Error GoTo exc
    Dim nharik As String
    If Text1(0).Locked = False Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mstock where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            MsgBox ("kode Barang sudah ada!, silahkan pakai kode lain atau edit"), vbInformation, "Info"
            Exit Sub
        End If
    End If
    
    If Text1(0).Text <> "" And Text1(1).Text <> "" Then
        Dim tipe As String
        If Option4.Value = True Then
            tipe = "Rakitan"
        ElseIf Option5.Value = True Then
            tipe = "Konsinyasi"
        ElseIf Option6.Value = True Then
            tipe = "Jasa"
        Else
            tipe = "Barang"
        End If
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mstock where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs1.EOF Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "update mstock set inv='" & tipe & "', nama='" & Text1(1).Text & "', merk='" & Combo4.Text & "'," & _
                    "subktg='" & Combo3.Text & "', satuan='" & Combo6.Text & "', plu='" & Text1(2).Text & "', minorder=" & Val(Text1(30).Text) & _
                    ",keterangan='" & Text9.Text & "' where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        Else
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "insert into mstock (bara,inv,nama,keterangan,merk,subktg,satuan,plu,minorder) values ('" & Text1(0).Text & "','" & tipe & "','" & Text1(1).Text & "','" & _
                    Text9.Text & "','" & Combo4.Text & "','" & Combo3.Text & "','" & Combo6.Text & "','" & Text1(2).Text & "'," & Val(Text1(30).Text) & ")", con, adOpenKeyset, adLockOptimistic
        End If
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from mstock_bo where bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
        Dim matauang As String
        matauang = IIf(OptionCurrency(0).Value = True, "IDR", "USD")
        
        If Not rs2.EOF Then
            If rs2.State <> 0 Then rs2.Close
            csql = "update mstock_bo set aver=" & Format(Trim(Text1(3).Text), "##0.00") & ", hbeli=" & Format(Trim(Text1(4).Text), "##0.00") & ", hjual=" & Format(Trim(Text1(7).Text), "##0.00") & _
                    ",rak='" & Combo5.Text & "', qty2=" & Format(Text1(8).Text, "##0") & ", qty3=" & Format(Text1(9).Text, "##0") & ", qty4=" & Format(Text1(10).Text, "##0") & _
                    ",hjual2=" & Format(Trim(Text1(14).Text), "##0.00") & ", hjual3=" & Format(Trim(Text1(15).Text), "##0.00") & ", hjual4=" & Format(Trim(Text1(16).Text), "##0.00") & ", min=" & Format(Text1(18).Text, "##0") & _
                    ",max=" & Format(Text1(17).Text, "##0") & ",mup=" & Format(Trim(Text1(31).Text), "##0.00") & ",feepersen=" & Format(Trim(Text1(28).Text), "##0.00") & ",feenominal=" & Format(Trim(Text1(29).Text), "##0.00") & _
                    ",aktif=" & Check1.Value & ",bkp=" & Check2.Value & ",disc=" & Check3.Value & ",tetap=" & Check4.Value & ",mstn=" & Check5.Value & ",serial=" & Check6.Value & _
                    ",poin=" & Check7.Value & ",fee=" & Check8.Value & ",acc_inv='" & Text1(21).Text & "',acc_pjl='" & Text1(23).Text & "',acc_hpp='" & Text1(25).Text & "'" & _
                    ",acc_pjk='" & Text1(27).Text & "',currency='" & matauang & "' where bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'"
            
'            Debug.Print (csql)
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Else
            If rs2.State <> 0 Then rs2.Close
            csql = "insert into mstock_bo(bara,bo,aver,hbeli,hjual,currency,rak,qty2,qty3,qty4,hjual2,hjual3,hjual4,min,max,mup,feepersen,feenominal,aktif,bkp,disc,tetap,mstn,serial,poin,fee,acc_inv,acc_pjl,acc_hpp,acc_pjk) " & _
                    "values('" & Text1(0).Text & "','" & cmb_cabang.Text & "'," & Format(Text1(3).Text, "##0.00") & ", " & Format(Text1(4).Text, "##0.00") & ", " & Format(Text1(7).Text, "##0.00") & ",'" & matauang & "'" & _
                    ",'" & Combo5.Text & "'," & Format(Text1(8).Text, "##0") & "," & Format(Text1(9).Text, "##0") & "," & Format(Text1(10).Text, "##0") & "," & Format(Text1(14).Text, "##0.00") & _
                    "," & Format(Text1(15).Text, "##0.00") & "," & Format(Text1(16).Text, "##0.00") & "," & Format(Text1(18).Text, "##0") & "," & Format(Text1(17).Text, "##0") & _
                    "," & Format(Text1(31).Text, "##0.00") & "," & Format(Text1(28).Text, "##0.00") & "," & Format(Text1(29).Text, "##0.00") & _
                    "," & Check1.Value & "," & Check2.Value & "," & Check3.Value & "," & Check4.Value & "," & Check5.Value & "," & Check6.Value & _
                    "," & Check7.Value & "," & Check8.Value & ",'" & Text1(21).Text & "','" & Text1(23).Text & "','" & Text1(25).Text & "','" & Text1(27).Text & "')"
            
'            Debug.Print (csql)
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        End If
        
        simpan_gambar
        MsgBox ("simpan sukses!"), vbInformation, "Info"
        
        If daftar_stock.Visible = True Then
            daftar_stock.tampilstock
        End If
    Else
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub simpan_gambar()
    pathpic = codi.filename
    If pathpic <> "" Then
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "delete from zfoto_barang where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
        Dim bytData() As Byte
        Dim strDescription As String
        
        On Error GoTo err
        Open pathpic For Binary As #1
        ReDim bytData(FileLen(pathpic))
        Get #1, , bytData
        Close #1
        
        Set rs3 = New ADODB.Recordset
        rs3.CursorLocation = adUseClient
        rs3.Open "zfoto_barang", con, adOpenKeyset, adLockPessimistic, adCmdTable
        With rs3
             .AddNew
             .Fields("bara").Value = Text1(0).Text
             .Fields("picture").AppendChunk bytData 'adding the picture to the db
             .Update
        End With
    
    End If
    Exit Sub
err:
    If err.Number = 32755 Then     'simple error check.
    Else
        MsgBox err.Description
        err.Clear
    End If
End Sub

Private Sub Form_DblClick()
    Me.WindowState = 2
    MDIForm1.tabref
End Sub

Sub kosong()
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    ListView2.ListItems.Clear
    ListView2.View = lvwReport
    ListView3.ListItems.Clear
    ListView3.View = lvwReport
    ListView4.ListItems.Clear
    ListView4.View = lvwReport
    ListView5.ListItems.Clear
    ListView5.View = lvwReport
    ListView6.ListItems.Clear
    ListView6.View = lvwReport
    For z = 1 To 31
        Text1(z).Text = "0"
    Next z
    Text1(1).Text = ""
    Text1(2).Text = ""
    For z = 20 To 27
        Text1(z).Text = ""
    Next z
    Text9.Text = ""
    
    Image1.picture = Nothing
    
    Check1.Value = 0
    Check2.Value = 0
    Check3.Value = 0
    Check4.Value = 0
    Check5.Value = 0
    Check6.Value = 0
    Check7.Value = 0
    Check8.Value = 0
    Button5.Enabled = False
    Button7.Enabled = False
    Button12.Enabled = False
    ListView1.Enabled = False
    ListView4.Enabled = False
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        KeyAscii = 0
        If Frame5.Visible = True Then
            Frame5.Visible = False
'            Combo2.SetFocus
        End If
        If Frame6.Visible = True Then
            Frame6.Visible = False
'            Combo4.SetFocus
        End If
        If Frame7.Visible = True Then
            Frame7.Visible = False
'            Combo6.SetFocus
        End If
        If Frame8.Visible = True Then
            Frame8.Visible = False
        End If
        If Frame9.Visible = True Then
            Frame9.Visible = False
'            Combo5.SetFocus
        End If
        If Frame10.Visible = True Then
            Frame10.Visible = False
        End If
        If Frame11.Visible = True Then
            Frame11.Visible = False
'            Text1(21).SetFocus
        End If
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub Form_Load()
    koneksi
    kosong
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
        rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo6.Clear
    
    Combo2.AddItem ("")
    Combo3.AddItem ("")
    Combo4.AddItem ("")
    Combo5.AddItem ("")
    Combo6.AddItem ("")
    Me.KeyPreview = True
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1300
    Picture2.Width = Me.Width - 255
    Picture5.Width = Me.Width - 255
    Label1(0).Width = Me.Width - 50
    SSTab1.Width = Me.Width - 495
    SSTab1.Height = Picture2.Height - 2900
    
    ListView1.Width = Me.Width - 735
    ListView1.Height = SSTab1.Height - 850
    
    ListView6.Width = Me.Width - 735
    ListView6.Height = SSTab1.Height - 1000
    
    ListView2.Height = SSTab1.Height - 850
    ListView2.Width = (Me.Width / 2) - 400
    
    ListView3.Height = SSTab1.Height - 850
    ListView3.Width = (Me.Width / 2) - 400
    ListView3.Left = ListView2.Left + ListView3.Width + 40
    
    Label2(11).Left = ListView3.Left
    ListView4.Height = SSTab1.Height - 1200
    
    Picture3.Height = SSTab1.Height - 600
    Picture3.Width = Me.Width - 735
    Picture4.Height = SSTab1.Height - 600
    Picture4.Width = Me.Width - 735
    Picture6.Height = SSTab1.Height - 600
    Picture6.Width = Me.Width - 735
    Picture7.Height = SSTab1.Height - 600
    Picture7.Width = Me.Width - 735
    Picture8.Height = SSTab1.Height - 600
    Picture8.Width = Me.Width - 735
    
    ListView5.Height = Picture6.Height - 750
    ListView5.Width = Me.Width - 1000
    
End Sub

Private Sub Image1_Click()
    Frame10.Visible = False
    Picture2.Enabled = True
    Picture5.Enabled = True
End Sub

Private Sub Image12_Click()
    Frame8.Visible = False
    Picture2.Enabled = True
    Picture5.Enabled = True
End Sub

Private Sub Image3_Click()
    codi.ShowOpen
    If codi.filename <> "" Then
        Image3.picture = LoadPicture(codi.filename)
    End If
End Sub

Private Sub ListView4_DblClick()
    If ListView4.ListItems.Count > 0 Then
        Text6(0).Text = ListView4.SelectedItem.SubItems(1)
        Text6(1).Text = ListView4.SelectedItem.SubItems(2)
        Text6(2).Text = ListView4.SelectedItem.SubItems(3)
        Text6(3).Text = ListView4.SelectedItem.SubItems(4)
        Text6(4).Text = ListView4.SelectedItem.SubItems(5)
        Text6(5).Text = ListView4.SelectedItem.SubItems(6)
        Frame8.Left = 400
        Frame8.Top = 3500
        Frame8.Visible = True
        Picture2.Enabled = False
        Picture5.Enabled = False
    End If
End Sub

Private Sub ListView4_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo exc
    If KeyCode = vbKeyDelete Then
        pesan = MsgBox("Hapus Unit " & ListView4.SelectedItem.SubItems(1) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "delete from mstock_satuan where satuan='" & ListView4.SelectedItem.SubItems(1) & "' and bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            
            MsgBox ("hapus unit sukses!"), vbInformation, "Info"
            Frame8.Visible = False
            Picture2.Enabled = True
            Picture5.Enabled = True
            
            tampil_multiunit
        End If
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub ListView6_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo exc
    If KeyCode = vbKeyDelete Then
        pesan = MsgBox("Hapus Supplier " & ListView6.SelectedItem.SubItems(1) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "delete from mstock_supp where kdsp='" & ListView6.SelectedItem.SubItems(1) & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            
            MsgBox ("hapus supplier sukses!"), vbInformation, "Info"
            
            tampil_supplier
        End If
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub ListView7_DblClick()
    If ListView7.ListItems.Count > 0 Then
        Combo2.Text = ListView7.SelectedItem.SubItems(6)
        Combo3.Text = ListView7.SelectedItem.SubItems(3)
        Frame5.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub ListView8_DblClick()
    If ListView8.ListItems.Count > 0 Then
        Combo4.Text = ListView8.SelectedItem.SubItems(1)
        Frame6.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub ListView9_DblClick()
    If ListView9.ListItems.Count > 0 Then
        Combo6.Text = ListView9.SelectedItem.SubItems(1)
        Frame7.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub ListView10_DblClick()
    If ListView10.ListItems.Count > 0 Then
        Combo5.Text = ListView10.SelectedItem.SubItems(1)
        Frame9.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub ListView11_DblClick()
    If ListView11.ListItems.Count > 0 Then
        Select Case Frame11.Tag
            Case 0
                Text1(21).Text = ListView11.SelectedItem.SubItems(1)
                Text1(20).Text = ListView11.SelectedItem.SubItems(2)
            Case 1
                Text1(23).Text = ListView11.SelectedItem.SubItems(1)
                Text1(22).Text = ListView11.SelectedItem.SubItems(2)
            Case 2
                Text1(25).Text = ListView11.SelectedItem.SubItems(1)
                Text1(24).Text = ListView11.SelectedItem.SubItems(2)
            Case 3
                Text1(27).Text = ListView11.SelectedItem.SubItems(1)
                Text1(26).Text = ListView11.SelectedItem.SubItems(2)
        End Select
        Frame11.Visible = False
        Picture2.Enabled = True
        Picture5.Enabled = True
    End If
End Sub

Private Sub Option3_Click()
If Option3.Value = True Then
    Button5.Enabled = False
    ListView1.Enabled = False
End If
End Sub

Private Sub Option4_Click()
If Option4.Value = True Then
    Button5.Enabled = True
    ListView1.Enabled = True
End If
End Sub

Private Sub Option5_Click()
If Option5.Value = True Then
    Button5.Enabled = False
    ListView1.Enabled = False
End If
End Sub

Private Sub Option6_Click()
If Option6.Value = True Then
    Button5.Enabled = False
    ListView1.Enabled = False
End If
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    Select Case SSTab1.Tab
        Case 1
            tampil_multiunit
        Case 3
            tampil_gudang
        Case 5
            If Text1(0).Text <> "" Then
                get_akun Text1(21).Text, 21
                get_akun Text1(23).Text, 23
                get_akun Text1(25).Text, 25
                get_akun Text1(27).Text, 27
            End If
        Case 6
            tampil_supplier
    End Select
End Sub

Private Sub Text1_Change(Index As Integer)
Select Case Index
    Case 0
        kosong
    Case 5, 6, 11, 12, 13, 31
        ABC = Split(Text1(Index).Text, ".")
        If UBound(ABC) > 1 Then
            sss = ABC(0) & "." & ABC(1)
            Text1(Index).Text = sss
        End If
        Text1(Index).SelStart = Len(Text1(Index).Text)
    
    Case 7
        On Error Resume Next
        Text1(5).Text = Format((Val(Format(Text1(7).Text, "##0.00")) - Val(Format(Text1(3).Text, "##0.00"))) / Val(Format(Text1(3).Text, "##0.00")) * 100, "##0.00")
        Text1(6).Text = Format((Val(Format(Text1(7).Text, "##0.00")) - Val(Format(Text1(3).Text, "##0.00"))) / Val(Format(Text1(7).Text, "##0.00")) * 100, "##0.00")
    
    Case 14
        On Error Resume Next
        Text1(11).Text = Format((Val(Format(Text1(14).Text, "##0.00")) - Val(Format(Text1(3).Text, "##0.00"))) / Val(Format(Text1(3).Text, "##0.00")) * 100, "##0.00")
    
    Case 15
        On Error Resume Next
        Text1(12).Text = Format((Val(Format(Text1(15).Text, "##0.00")) - Val(Format(Text1(3).Text, "##0.00"))) / Val(Format(Text1(3).Text, "##0.00")) * 100, "##0.00")
    
    Case 16
        On Error Resume Next
        Text1(13).Text = Format((Val(Format(Text1(16).Text, "##0.00")) - Val(Format(Text1(3).Text, "##0.00"))) / Val(Format(Text1(3).Text, "##0.00")) * 100, "##0.00")

End Select
End Sub

Private Sub Text1_GotFocus(Index As Integer)
Select Case Index
    Case 0 To 50
        With Text1(Index)
        .SelStart = 0
        .SelLength = Len(.Text)
        End With
        
End Select
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case Index
    Case 0
        If KeyAscii = 13 Then
            KeyAscii = 0
            SendKeys "{tab}"
        Else
            If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
        End If
   
    Case 1, 2
        If KeyAscii = 13 Then
            KeyAscii = 0
            SendKeys "{tab}"
            
        Else
            If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
        End If
   
    Case 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 28, 29, 30, 31
        If KeyAscii = 13 Then
            KeyAscii = 0
            SendKeys "{tab}"
                    
        Else
            If Not (KeyAscii >= Asc("0") _
            And KeyAscii <= Asc("9") _
            Or KeyAscii = vbKeyBack _
            Or KeyAscii = vbKeyDelete _
            Or KeyAscii = 45) Then
            KeyAscii = 0
            End If
        End If
    Case 21, 23, 25, 27
        If KeyAscii = 13 Then
            KeyAscii = 0
            get_akun Text1(Index).Text, Index
        End If
End Select
End Sub

Private Sub Text1_LostFocus(Index As Integer)
Select Case Index
    Case 5
        On Error Resume Next
        If Trim(Text1(5).Text) = "" Then Text1(5).Text = "0"
        Text1(6).Text = Format(Val(Text1(5).Text) / (1 + Val(Text1(5).Text) / 100), "##0.00")
        Text1(7).Text = Format(Val(Format(Text1(3).Text, "##0.00")) * (1 + Val(Text1(5).Text) / 100), "#,##0")
    Case 6
        On Error Resume Next
        If Trim(Text1(6).Text) = "" Then Text1(6).Text = "0"
        Text1(5).Text = Format(Val(Text1(6).Text) / (1 - Val(Text1(6).Text) / 100), "##0.00")
        Text1(7).Text = Format(Val(Format(Text1(3).Text, "##0.00")) * (1 + (Val(Text1(5).Text) / 100)), "#,##0")
        
    Case 11
        On Error Resume Next
        If Trim(Text1(11).Text) = "" Then Text1(11).Text = "0"
        Text1(14).Text = Format(Val(Format(Text1(3).Text, "##0.00")) * (1 + (Val(Text1(11).Text) / 100)), "#,##0")
        
    Case 12
        On Error Resume Next
        If Trim(Text1(12).Text) = "" Then Text1(12).Text = "0"
        Text1(15).Text = Format(Val(Format(Text1(3).Text, "##0.00")) * (1 + (Val(Text1(12).Text) / 100)), "#,##0")
        
    Case 13
        On Error Resume Next
        If Trim(Text1(13).Text) = "" Then Text1(13).Text = "0"
        Text1(16).Text = Format(Val(Format(Text1(3).Text, "##0.00")) * (1 + (Val(Text1(13).Text) / 100)), "#,##0")
        
End Select
End Sub

Sub getitem()
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mstock where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        On Error Resume Next
        If rs1.Fields!inv = "Rakitan" Then
            Option4.Value = True
            Button5.Enabled = True
            ListView1.Enabled = True
        ElseIf rs1.Fields!inv = "Konsinyasi" Then
            Option5.Value = True
        ElseIf rs1.Fields!inv = "Jasa" Then
            Option6.Value = True
        Else
            Option3.Value = True
        End If
        Text1(2).Text = "" & rs1.Fields!plu
        Combo3.Text = "" & rs1.Fields!subktg
        Text9.Text = "" & rs1.Fields!keterangan
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select ktg from mktgsub where subktg='" & rs1.Fields!subktg & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            Combo2.Text = "" & rs2.Fields!ktg
        End If
        
        Combo4.Text = "" & rs1.Fields!merk
        Combo6.Text = "" & rs1.Fields!satuan
        Text1(30).Text = "" & Format(rs1.Fields!MINORDER, "#,##0")
        t1 = 0
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from mstock_bo where bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        If Not rs2.EOF Then
            t1 = Val(rs2.Fields!aver)
            Text1(3).Text = "" & Format(rs2.Fields!aver, "#,##0.00")
            Text1(4).Text = "" & Format(rs2.Fields!hbeli, "#,##0.00")
            
            If rs2.Fields!hjual <> 0 And rs2.Fields!aver <> 0 Then
                Text1(5).Text = "" & Format((rs2.Fields!hjual - rs2.Fields!aver) / rs2.Fields!aver * 100, "##0.00")
                Text1(6).Text = "" & Format((rs2.Fields!hjual - rs2.Fields!aver) / rs2.Fields!hjual * 100, "##0.00")
            End If
            
            Text1(7).Text = "" & Format(rs2.Fields!hjual, "#,##0")
            Combo5.Text = "" & rs2.Fields!rak
            Text1(8).Text = "" & Format(rs2.Fields!qty2, "##0")
            Text1(9).Text = "" & Format(rs2.Fields!qty3, "##0")
            Text1(10).Text = "" & Format(rs2.Fields!qty4, "##0")
            Text1(14).Text = "" & Format(rs2.Fields!hjual2, "#,##0")
            Text1(15).Text = "" & Format(rs2.Fields!hjual3, "#,##0")
            Text1(16).Text = "" & Format(rs2.Fields!hjual4, "#,##0")
            
            OptionCurrency(0).Value = rs2.Fields!Currency = "IDR"
            OptionCurrency(1).Value = rs2.Fields!Currency = "USD"
            
            If rs2.Fields!aver <> 0 Then
                Text1(11).Text = "" & Format((rs2.Fields!hjual2 - rs2.Fields!aver) / rs2.Fields!aver * 100, "##0.00")
            End If
            If rs2.Fields!aver <> 0 Then
                Text1(12).Text = "" & Format((rs2.Fields!hjual3 - rs2.Fields!aver) / rs2.Fields!aver * 100, "##0.00")
            End If
            If rs2.Fields!aver <> 0 Then
                Text1(13).Text = "" & Format((rs2.Fields!hjual4 - rs2.Fields!aver) / rs2.Fields!aver * 100, "##0.00")
            End If
            
            Text1(19).Text = "" & Format(rs2.Fields!AWAL + rs2.Fields!masuk - rs2.Fields!keluar, "#,##0.00")
            Text1(18).Text = "" & Format(rs2.Fields!Min, "##0")
            Text1(17).Text = "" & Format(rs2.Fields!Max, "##0")
            Text1(31).Text = "" & Format(rs2.Fields!mup, "##0.00")
            Text1(28).Text = "" & Format(rs2.Fields!FeePersen, "##0.00")
            Text1(29).Text = "" & Format(rs2.Fields!FeeNominal, "#,##0")
            Check1.Value = Val(rs2.Fields!AKTIF)
            Check2.Value = Val(rs2.Fields!BKP)
            Check3.Value = Val(rs2.Fields!disc)
            Check4.Value = Val(rs2.Fields!TETAP)
            Check5.Value = Val(rs2.Fields!MSTN)
            Check6.Value = Val(rs2.Fields!SERIAL)
            Check7.Value = Val(rs2.Fields!POIN)
            Check8.Value = Val(rs2.Fields!fee)
            
            Text1(21).Text = "" & rs2.Fields!ACC_INV
            Text1(23).Text = "" & rs2.Fields!ACC_PJL
            Text1(25).Text = "" & rs2.Fields!ACC_HPP
            Text1(27).Text = "" & rs2.Fields!ACC_PJK
        End If
        
        ListView4.ListItems.Clear
        ListView4.View = lvwReport
        j = 0
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from mstock_satuan where bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        If Not rs2.EOF Then
            Do While Not rs2.EOF
                j = j + 1
                Set item = ListView4.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = "" & rs2.Fields!satuan
                item.SubItems(2) = "" & rs2.Fields!bara2
                item.SubItems(3) = "" & Format(rs2.Fields!isi, "##0.00")
                item.SubItems(4) = "" & Format(rs2.Fields!isi * t1, "#,##0.00")
                If t1 <> 0 And rs2.Fields!isi <> 0 Then
                    item.SubItems(5) = "" & Format((rs2.Fields!Harga - (rs2.Fields!isi * t1)) / (rs2.Fields!isi * t1) * 100, "##0.00")
                Else
                    item.SubItems(5) = "0.00"
                End If
                item.SubItems(6) = "" & Format(rs2.Fields!Harga, "#,##0")
            rs2.MoveNext
            Loop
        End If
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from zfoto_barang where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        If Not rs2.EOF Then
            If IsNull(rs2.Fields!picture) = False Then
                Set Image3.DataSource = rs2 'setting image1�s datasource
                Image3.DataField = "picture" 'set its datafield.
                Set rs2 = Nothing
            Else
                Image3.picture = LoadPicture("")
            End If
        End If
        If rs2.State <> 0 Then rs2.Close
    
        Button7.Enabled = True
        Button12.Enabled = True
        ListView4.Enabled = True
    End If
Exit Sub
exc:
End Sub

Private Sub Text2_Change()
    ListView6.ListItems.Clear
    ListView6.View = lvwReport
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
        tampilktg
    
Else
    If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
End If
End Sub

Sub tampilktg()
    On Error GoTo exc
    ListView7.ListItems.Clear
    ListView7.View = lvwReport
    j = 0
    kktg = ""
    If Trim(Text2.Text) = "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select mktg.ktg,mktg.ket_ktg,mktgsub.subktg,mktgsub.ket_subktg from mktg left join mktgsub on mktg.ktg=mktgsub.ktg order by mktg.ktg asc,mktgsub.subktg asc", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select mktg.ktg,mktg.ket_ktg,mktgsub.subktg,mktgsub.ket_subktg from mktg left join mktgsub on mktg.ktg=mktgsub.ktg where mktg.ktg like '%" & Text2.Text & "%' or mktg.ket_ktg like '%" & Text2.Text & "%' or mktgsub.subktg like '%" & Text2.Text & "%' or mktgsub.ket_subktg like '%" & Text2.Text & "%' order by mktg.ktg asc,mktgsub.subktg asc", con, adOpenKeyset, adLockOptimistic
    End If
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView7.ListItems.Add(, , j)
            item.Text = j
            If UCase(kktg) <> UCase(rs1.Fields!ktg) Then
                item.SubItems(1) = "" & rs1.Fields!ktg
                item.SubItems(2) = "" & rs1.Fields!ket_ktg
                kktg = UCase(rs1.Fields!ktg)
            Else
                item.SubItems(1) = " "
                item.SubItems(2) = " "
            End If
            item.SubItems(3) = "" & rs1.Fields!subktg
            item.SubItems(4) = "" & rs1.Fields!ket_subktg
            z = 21 - Len(rs1.Fields!ktg)
            item.SubItems(5) = "" & rs1.Fields!ktg & Space(z) & rs1.Fields!ket_ktg
            item.SubItems(6) = "" & rs1.Fields!ktg
            item.SubItems(7) = "" & rs1.Fields!ket_ktg
        rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
MsgBox ("error")
End Sub

Private Sub Text3_Change()
    ListView8.ListItems.Clear
    ListView8.View = lvwReport
End Sub

Private Sub Text4_Change()
    ListView9.ListItems.Clear
    ListView9.View = lvwReport
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
            tampilmerk
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub Text4_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
            tampil_satuan
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Private Sub Text6_GotFocus(Index As Integer)
    Select Case Index
        Case 0 To 5
            With Text6(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text6_LostFocus(Index As Integer)
    If Index = 4 Then
        On Error Resume Next
        If Trim(Text6(4).Text) = "" Then Text6(4).Text = "0"
        Text6(5).Text = Format(Val(Format(Text6(3).Text, "##0.00")) * (1 + (Val(Text6(4).Text) / 100)), "#,##0")
    End If
End Sub

Private Sub Text6_Change(Index As Integer)
    Select Case Index
        Case 2
            On Error Resume Next
            Text6(3).Text = Format(Val(Text6(2).Text) * Val(Format(Text1(3).Text, "##0.00")), "#,##0.00")
        Case 4
            ABC = Split(Text6(Index).Text, ".")
            If UBound(ABC) > 1 Then
                sss = ABC(0) & "." & ABC(1)
                Text6(Index).Text = sss
            End If
            Text6(Index).SelStart = Len(Text6(Index).Text)
        Case 5
            On Error Resume Next
            Text6(4).Text = Format((Val(Format(Text6(5).Text, "##0.00")) - Val(Format(Text6(3).Text, "##0.00"))) / Val(Format(Text6(3).Text, "##0.00")) * 100, "#,##0.00")
    End Select
End Sub

Private Sub Text6_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index < 5 Then
            KeyAscii = 0
            SendKeys "{tab}"
        Else
            If Text6(0).Text = "" And Val(Text6(4).Text) = 0 Then
                MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
                Exit Sub
            End If
            simpan_multiunit
        End If
    Else
        If Index = 2 Or Index = 3 Or Index = 4 Or Index = 5 Then
            If Not (KeyAscii >= Asc("0") _
                And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack _
                Or KeyAscii = vbKeyDelete _
                Or KeyAscii = 45) Then
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub Text7_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 0 Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select kdsp, namasp from msupp where kdsp='" & Text7(0).Text & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
            If Not rs1.EOF Then
                Do While Not rs1.EOF
                    Text7(1).Text = rs1!namasp
                    rs1.MoveNext
                Loop
                SendKeys "{tab}"
                Button9(0).Enabled = True
            Else
                MsgBox ("Kode supplier tidak ada")
                Button9(0).Enabled = False
            End If
        Else
            KeyAscii = 0
            SendKeys "{tab}"
        End If
    Else
        If Index = 2 Then
            If Not (KeyAscii >= Asc("0") _
                And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack _
                Or KeyAscii = vbKeyDelete _
                Or KeyAscii = 45) Then
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub Text7_LostFocus(Index As Integer)
    If Index = 2 Then
        On Error Resume Next
        If Trim(Text7(2).Text) = "" Then Text7(2).Text = "0"
        Text7(2).Text = Format(Val(Format(Text7(2).Text, "##0.00")), "#,##0")
    End If
End Sub

Private Sub Text8_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
            tampil_akun
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub

Sub tampilmerk()
    On Error GoTo exc
    ListView8.ListItems.Clear
    ListView8.View = lvwReport
    
    j = 0
    If Trim(Text3.Text) = "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mbrand order by merk asc", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mbrand where merk like '%" & Text3.Text & "%' or ket_merk like '%" & Text3.Text & "%' order by merk asc", con, adOpenKeyset, adLockOptimistic
    End If
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView8.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!merk
            item.SubItems(2) = "" & rs1.Fields!ket_merk
            rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub tampil_lokasi()
    On Error GoTo exc
    ListView10.ListItems.Clear
    ListView10.View = lvwReport
    
    j = 0
    If Trim(Text5.Text) = "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mlokasi order by lokasi asc", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mlokasi where lokasi like '%" & Text5.Text & "%' or ket_lokasi like '%" & Text5.Text & "%' order by lokasi asc", con, adOpenKeyset, adLockOptimistic
    End If
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView10.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!lokasi
            item.SubItems(2) = "" & rs1.Fields!ket_lokasi
            rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub tampil_satuan()
    On Error GoTo exc
    ListView9.ListItems.Clear
    ListView9.View = lvwReport
    
    j = 0
    If Trim(Text4.Text) = "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from msatuan order by satuan asc", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from msatuan where satuan like '%" & Text4.Text & "%' or ket_satuan like '%" & Text4.Text & "%' order by merk asc", con, adOpenKeyset, adLockOptimistic
    End If
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView9.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!satuan
            item.SubItems(2) = "" & rs1.Fields!ket_satuan
            rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub tampil_gudang()
    On Error GoTo exc
    ListView5.ListItems.Clear
    ListView5.View = lvwReport
    
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select bo, awal+masuk-keluar as saldo, '-' satuan, aver, mup, hjual, hjual2, hjual3, hjual4, qty2, qty3, qty4 from mstock_bo where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView5.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!BO
            item.SubItems(2) = "" & Format(rs1.Fields!saldo, "##0.00")
            item.SubItems(3) = "" & Format(rs1.Fields!satuan, "#,##0")
            item.SubItems(4) = "" & Format(rs1.Fields!aver, "#,##0")
            item.SubItems(5) = "" & Format(rs1.Fields!mup, "##0.00")
            item.SubItems(6) = "" & Format(rs1.Fields!hjual, "#,##0")
            item.SubItems(7) = "" & Format(rs1.Fields!qty2, "##0")
            item.SubItems(8) = "" & Format(rs1.Fields!hjual2, "#,##0")
            item.SubItems(9) = "" & Format(rs1.Fields!qty3, "##0")
            item.SubItems(10) = "" & Format(rs1.Fields!hjual3, "#,##0")
            item.SubItems(11) = "" & Format(rs1.Fields!qty4, "##0")
            item.SubItems(12) = "" & Format(rs1.Fields!hjual4, "#,##0")
            rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub tampil_supplier()
    On Error GoTo exc
    ListView6.ListItems.Clear
    ListView6.View = lvwReport
    
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select s.kdsp, namasp, hbeli, tgl, master from mstock_supp m join msupp s on m.kdsp=s.kdsp where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView6.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!kdsp
            item.SubItems(2) = "" & rs1.Fields!namasp
            item.SubItems(3) = "" & Format(rs1.Fields!hbeli, "#,##0")
            item.SubItems(4) = "" & Format(rs1.Fields!tgl, "dd-mm-yyyy")
            item.SubItems(5) = "" & rs1.Fields!master
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub simpan_supplier()
    On Error GoTo exc
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mstock_supp where kdsp='" & Text7(0).Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "update mstock_supp set hbeli=" & Val(Format(Text7(2).Text, "##0.00")) & ", tgl='" & Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & _
            "master=" & Check9.Value & " where bara='" & Text1(0).Text & "' and kdsp='" & Text7(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "insert into mstock_supp (bara,hbeli,tgl,master,kdsp) values ('" & Text1(0).Text & "'," & Val(Format(Text7(2).Text, "##0.00")) & ",'" & _
            Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & Check9.Value & ",'" & Text7(0).Text & "')", con, adOpenKeyset, adLockOptimistic
    End If
    
    If Check9.Value = 1 Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "update mstock_supp set master=0 where bara='" & Text1(0).Text & "' and kdsp!='" & Text7(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    End If
    
    MsgBox ("simpan sukses!"), vbInformation, "Info"
    
    Frame10.Visible = False
    Picture2.Enabled = True
    Picture5.Enabled = True
    tampil_supplier
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub simpan_multiunit()
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mstock_satuan where satuan='" & Text6(0).Text & "' and bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "update mstock_satuan set isi=" & Val(Text6(2).Text) & ", bara2='" & Text6(1).Text & "', harga=" & Val(Format(Text6(5).Text, "##0.00")) & _
            " where satuan='" & Text6(0).Text & "' and bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "insert into mstock_satuan (bara,satuan,isi,harga,bara2,bo) values ('" & Text1(0).Text & "','" & Text6(0).Text & "','" & _
            Val(Text6(2).Text) & "'," & Val(Format(Text6(5).Text, "##0.00")) & ",'" & Text6(1).Text & "','" & cmb_cabang.Text & "')", con, adOpenKeyset, adLockOptimistic
    End If
    
    MsgBox ("simpan sukses!"), vbInformation, "Info"
    Frame8.Visible = False
    Picture2.Enabled = True
    Picture5.Enabled = True
    
    tampil_multiunit
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub tampil_multiunit()
    ListView4.ListItems.Clear
    ListView4.View = lvwReport
        
    j = 0
    If rs2.State <> 0 Then rs2.Close
    rs2.Open "select * from mstock_satuan where bo='" & cmb_cabang.Text & "' and bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    
    t1 = Val(Format(Text1(3).Text, "##0.00"))
    If Not rs2.EOF Then
        Do While Not rs2.EOF
            j = j + 1
            Set item = ListView4.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs2.Fields!satuan
            item.SubItems(2) = "" & rs2.Fields!bara2
            item.SubItems(3) = "" & Format(rs2.Fields!isi, "##0.00")
            item.SubItems(4) = "" & Format(rs2.Fields!isi * t1, "#,##0.00")
            If t1 <> 0 And rs2.Fields!isi <> 0 Then
                item.SubItems(5) = "" & Format((rs2.Fields!Harga - (rs2.Fields!isi * t1)) / (rs2.Fields!isi * t1) * 100, "##0.00")
            Else
                item.SubItems(5) = "0.00"
            End If
            item.SubItems(6) = "" & Format(rs2.Fields!Harga, "#,##0")
        rs2.MoveNext
        Loop
    End If
End Sub

Private Sub tampil_akun()
    ListView11.ListItems.Clear
    ListView11.View = lvwReport
        
    j = 0
    If Trim(Text8.Text) = "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from makun where jenis='1' order by kode asc", con, adOpenKeyset, adLockOptimistic
    Else
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from makun where jenis='1' AND (kode like '%" & Text8.Text & "%' or nama like '%" & Text8.Text & "%') order by kode asc", con, adOpenKeyset, adLockOptimistic
    End If
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView11.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!kode
            item.SubItems(2) = "" & rs1.Fields!nama
            rs1.MoveNext
        Loop
    End If
End Sub

Private Sub get_akun(kode As String, Index As Integer)
    If kode = "" Then Exit Sub
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis='1' and kode='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            Text1(Index - 1).Text = rs1!nama
            rs1.MoveNext
        Loop
    Else
        MsgBox ("Kode akun tidak ada")
        Frame11.Left = 1500
        Frame11.Top = 3000
        Frame11.Visible = True
        Frame11.Tag = IIf(Index = 21, 0, IIf(Index = 23, 1, IIf(Index = 25, 2, IIf(Index = 27, 3, 0))))
        Picture2.Enabled = False
        Picture5.Enabled = False
        Text8.Text = ""
        tampil_akun
        Text8.SetFocus
    End If
End Sub
