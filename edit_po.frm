VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form edit_po 
   Caption         =   "Edit Pesanan"
   ClientHeight    =   9240
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   15360
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9240
   ScaleWidth      =   15360
   Begin VB.Frame frame_akun 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   1845
      Left            =   0
      TabIndex        =   72
      Top             =   7320
      Visible         =   0   'False
      Width           =   4900
      Begin VB.TextBox text_akun 
         BackColor       =   &H00C0FFFF&
         Height          =   360
         Left            =   960
         TabIndex        =   74
         Top             =   840
         Width           =   3735
      End
      Begin VB.ComboBox combo_akun 
         Height          =   315
         Left            =   960
         TabIndex        =   73
         Text            =   "Combo1"
         Top             =   480
         Width           =   3735
      End
      Begin MySIS.Button Button4 
         Height          =   315
         Left            =   960
         TabIndex        =   75
         Top             =   1440
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         BTYPE           =   2
         TX              =   "Simpan"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_po.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button5 
         Height          =   315
         Left            =   2400
         TabIndex        =   76
         Top             =   1440
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         BTYPE           =   2
         TX              =   "Batal"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_po.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   78
         Top             =   885
         Width           =   735
      End
      Begin VB.Label Labe7 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   77
         Top             =   570
         Width           =   645
      End
      Begin VB.Image Image2 
         Height          =   300
         Left            =   4250
         Picture         =   "edit_po.frx":0038
         Top             =   0
         Width           =   675
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000000&
         Index           =   5
         X1              =   240
         X2              =   4680
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Label Labe7 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         Caption         =   "Kode Akun Perkiraan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   11
         Left            =   0
         TabIndex        =   79
         Top             =   0
         Width           =   4845
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   660
      Left            =   50
      TabIndex        =   29
      Top             =   1300
      Width           =   15270
      Begin VB.ComboBox cmb_satuan 
         Height          =   315
         Left            =   7935
         Locked          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   320
         Width           =   2000
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   4
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   70
         Text            =   "0"
         Top             =   360
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   6735
         Locked          =   -1  'True
         TabIndex        =   33
         Text            =   "0"
         Top             =   330
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   6
         Left            =   11445
         Locked          =   -1  'True
         TabIndex        =   37
         Text            =   "0"
         Top             =   330
         Width           =   1000
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   2050
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   330
         Width           =   3480
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   550
         MaxLength       =   15
         TabIndex        =   30
         Top             =   330
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   5535
         Locked          =   -1  'True
         TabIndex        =   32
         Text            =   "0"
         Top             =   330
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   5
         Left            =   9930
         Locked          =   -1  'True
         TabIndex        =   36
         Text            =   "0"
         Top             =   330
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   7
         Left            =   12450
         Locked          =   -1  'True
         TabIndex        =   38
         Text            =   "0"
         Top             =   330
         Width           =   1200
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   8
         Left            =   13650
         Locked          =   -1  'True
         TabIndex        =   39
         Text            =   "0"
         Top             =   330
         Width           =   1550
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Qty Terima"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   6735
         TabIndex        =   65
         Top             =   45
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Barcode"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   550
         TabIndex        =   47
         Top             =   45
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Satuan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   7935
         TabIndex        =   46
         Top             =   45
         Width           =   1980
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Qty Pesan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   5535
         TabIndex        =   45
         Top             =   45
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2055
         TabIndex        =   44
         Top             =   45
         Width           =   3480
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   550
         Index           =   0
         Left            =   50
         TabIndex        =   43
         Top             =   45
         Width           =   500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Harga Beli"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   9930
         TabIndex        =   42
         Top             =   45
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Disc (%)"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   11445
         TabIndex        =   41
         Top             =   45
         Width           =   1005
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Disc (Rp)"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   12450
         TabIndex        =   40
         Top             =   45
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   13650
         TabIndex        =   34
         Top             =   45
         Width           =   1545
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3165
      Left            =   60
      TabIndex        =   48
      Top             =   1920
      Width           =   15180
      _ExtentX        =   26776
      _ExtentY        =   5583
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "No."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Kode Barang"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Nama Barang"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Qty Pesan"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "Qty Terima"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Satuan Dasar"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Satuan"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   8
         Text            =   "Satuan"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Isi"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   10
         Text            =   "Harga Beli"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   11
         Text            =   "Disc (%)"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Disc 1 (Rp)"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   13
         Text            =   "Disc (Rp)"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   14
         Text            =   "Total"
         Object.Width           =   2646
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   5175
      Left            =   0
      ScaleHeight     =   5145
      ScaleWidth      =   15315
      TabIndex        =   49
      Top             =   0
      Width           =   15350
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   0
         Left            =   1440
         MaxLength       =   25
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   20
         Top             =   840
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   2
         Left            =   3360
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   840
         Width           =   3375
      End
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   57
         Top             =   480
         Width           =   1935
      End
      Begin VB.Frame Frame4 
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Height          =   1215
         Left            =   7200
         TabIndex        =   50
         Top             =   0
         Visible         =   0   'False
         Width           =   5295
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Alamat :"
            Height          =   240
            Index           =   5
            Left            =   240
            TabIndex        =   56
            Top             =   120
            Width           =   720
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Kota :"
            Height          =   240
            Index           =   6
            Left            =   240
            TabIndex        =   55
            Top             =   480
            Width           =   495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Telp :"
            Height          =   240
            Index           =   7
            Left            =   240
            TabIndex        =   54
            Top             =   840
            Width           =   510
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1080
            TabIndex        =   53
            Top             =   150
            Width           =   75
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   1080
            TabIndex        =   52
            Top             =   500
            Width           =   75
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   1080
            TabIndex        =   51
            Top             =   850
            Width           =   75
         End
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   4920
         TabIndex        =   58
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   111542275
         CurrentDate     =   43159
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Index           =   1
         Left            =   6750
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   840
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_po.frx":0443
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "M/d/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   4920
         TabIndex        =   68
         Top             =   480
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   111542275
         CurrentDate     =   43166
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier :"
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   69
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   63
         Top             =   135
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal :"
         Height          =   255
         Index           =   1
         Left            =   3480
         TabIndex        =   62
         Top             =   135
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept / Gudang :"
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   61
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tgl Kirim :"
         Height          =   255
         Index           =   4
         Left            =   3500
         TabIndex        =   60
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1300
         Left            =   0
         TabIndex        =   64
         Top             =   0
         Width           =   15255
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   0
      ScaleHeight     =   2025
      ScaleWidth      =   15315
      TabIndex        =   3
      Top             =   5160
      Width           =   15345
      Begin VB.Frame Frame2 
         Height          =   1935
         Left            =   11610
         TabIndex        =   17
         Top             =   0
         Width           =   3630
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   9
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   1430
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   360
            Index           =   6
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   255
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   7
            Left            =   1150
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   600
            Width           =   2000
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   8
            Left            =   1150
            TabIndex        =   13
            Text            =   "0.00"
            Top             =   960
            Width           =   2000
         End
         Begin MySIS.Button cmd_akun 
            Height          =   345
            Left            =   3200
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   960
            Width           =   345
            _extentx        =   609
            _extenty        =   609
            btype           =   2
            tx              =   "+"
            enab            =   -1
            font            =   "edit_po.frx":045F
            coltype         =   1
            focusr          =   -1
            bcol            =   13160660
            bcolo           =   13160660
            fcol            =   49152
            fcolo           =   49152
            mcol            =   12632256
            mptr            =   1
            micon           =   "edit_po.frx":0483
            umcol           =   -1
            soft            =   0
            picpos          =   0
            ngrey           =   0
            fx              =   0
            hand            =   0
            check           =   0
            value           =   0
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C0C0C0&
            X1              =   150
            X2              =   3500
            Y1              =   1350
            Y2              =   1350
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Kekurangan :"
            Height          =   195
            Index           =   1
            Left            =   135
            TabIndex        =   23
            Top             =   1485
            Width           =   960
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Sub Total :"
            Height          =   195
            Index           =   2
            Left            =   315
            TabIndex        =   22
            Top             =   300
            Width           =   780
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Grand Total :"
            Height          =   195
            Index           =   3
            Left            =   165
            TabIndex        =   21
            Top             =   675
            Width           =   930
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Titip / DP :"
            Height          =   195
            Index           =   4
            Left            =   315
            TabIndex        =   19
            Top             =   1035
            Width           =   780
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1935
         Left            =   8160
         TabIndex        =   6
         Top             =   0
         Width           =   3375
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   5
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   600
            Width           =   1515
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   4
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   240
            Width           =   1515
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   2
            Left            =   1080
            MaxLength       =   5
            TabIndex        =   9
            Text            =   "0.00"
            Top             =   1440
            Width           =   585
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   1
            Left            =   1680
            TabIndex        =   8
            Text            =   "0.00"
            Top             =   1080
            Width           =   1515
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   0
            Left            =   1080
            MaxLength       =   5
            TabIndex        =   7
            Text            =   "0.00"
            Top             =   1095
            Width           =   585
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            Height          =   320
            Index           =   3
            Left            =   1680
            Locked          =   -1  'True
            TabIndex        =   10
            Text            =   "0.00"
            Top             =   1440
            Width           =   1515
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Total Qty Terima :"
            Height          =   195
            Index           =   7
            Left            =   360
            TabIndex        =   67
            Top             =   645
            Width           =   1260
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Total Qty Pesan :"
            Height          =   195
            Index           =   0
            Left            =   390
            TabIndex        =   66
            Top             =   300
            Width           =   1230
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Pajak :"
            Height          =   195
            Index           =   6
            Left            =   480
            TabIndex        =   16
            Top             =   1485
            Width           =   495
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Potongan :"
            Height          =   195
            Index           =   5
            Left            =   195
            TabIndex        =   15
            Top             =   1140
            Width           =   780
         End
      End
      Begin VB.TextBox Text5 
         Height          =   800
         Left            =   5040
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   1130
         Width           =   3050
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Simpan"
         enab            =   -1
         font            =   "edit_po.frx":04A1
         coltype         =   1
         focusr          =   -1
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_po.frx":04CD
         picn            =   "edit_po.frx":04EB
         umcol           =   -1
         soft            =   0
         picpos          =   0
         ngrey           =   0
         fx              =   0
         hand            =   0
         check           =   0
         value           =   0
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Keluar"
         enab            =   -1
         font            =   "edit_po.frx":0647
         coltype         =   1
         focusr          =   -1
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_po.frx":0673
         picn            =   "edit_po.frx":0691
         umcol           =   -1
         soft            =   0
         picpos          =   0
         ngrey           =   0
         fx              =   0
         hand            =   0
         check           =   0
         value           =   0
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Batalkan"
         enab            =   0
         font            =   "edit_po.frx":0AC9
         coltype         =   1
         focusr          =   -1
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_po.frx":0AF5
         picn            =   "edit_po.frx":0B13
         umcol           =   -1
         soft            =   0
         picpos          =   0
         ngrey           =   0
         fx              =   0
         hand            =   0
         check           =   0
         value           =   0
      End
      Begin MySIS.Button Button3 
         Height          =   345
         Left            =   120
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   120
         Width           =   1155
         _extentx        =   2037
         _extenty        =   741
         btype           =   3
         tx              =   "Hapus Item"
         enab            =   -1
         font            =   "edit_po.frx":0C6F
         coltype         =   1
         focusr          =   -1
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   49152
         fcolo           =   49152
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_po.frx":0C9B
         umcol           =   -1
         soft            =   0
         picpos          =   0
         ngrey           =   0
         fx              =   0
         hand            =   0
         check           =   0
         value           =   0
      End
      Begin MySIS.Button cmd_cetak 
         Height          =   375
         Left            =   2760
         TabIndex        =   71
         Top             =   120
         Width           =   1215
         _extentx        =   2143
         _extenty        =   873
         btype           =   3
         tx              =   "Cetak"
         enab            =   -1
         font            =   "edit_po.frx":0CB9
         coltype         =   1
         focusr          =   -1
         bcol            =   13160660
         bcolo           =   13160660
         fcol            =   0
         fcolo           =   0
         mcol            =   12632256
         mptr            =   1
         micon           =   "edit_po.frx":0CE5
         picn            =   "edit_po.frx":0D03
         umcol           =   -1
         soft            =   0
         picpos          =   0
         ngrey           =   0
         fx              =   0
         hand            =   0
         check           =   0
         value           =   0
      End
      Begin VB.Label Label5 
         Caption         =   "Keterangan :"
         Height          =   255
         Left            =   5040
         TabIndex        =   28
         Top             =   855
         Width           =   1215
      End
   End
End
Attribute VB_Name = "edit_po"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim akun As New Collection

Private Sub Button5_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub cmd_akun_Click()
    frame_akun.Visible = True
    frame_akun.Left = 5500
    frame_akun.Top = 2400
    frame_akun.ZOrder vbBringToFront
    ListView1.Enabled = False
    Picture1.Enabled = False
    Picture2.Enabled = False
    Frame1.Enabled = False
End Sub

Private Sub cmd_cetak_Click()
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
        
    If fso.FileExists(App.Path & "\Report\pemesanan.rpt") Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "SELECT * FROM po_head b JOIN po_det d ON b.nopo=d.nopo WHERE b.nopo='" & ListView1.SelectedItem.SubItems(1) & "'", con, adOpenKeyset, adLockOptimistic
        
        Set cryApp = New CRAXDRT.Application
        Set rptApp = cryApp.OpenReport(App.Path & "\Report\pemesanan.rpt")
        rptApp.Database.SetDataSource rs1
            
        form_cetak.Visible = True
        form_cetak.ShowReport rptApp
    Else
        MsgBox "Report File Doesn't Exist!"
    End If
End Sub

Private Sub combo_akun_Click()
    text_akun.Text = akun(combo_akun.ListIndex + 1).Value
    combo_akun.Text = akun(combo_akun.ListIndex + 1).Key
End Sub

Private Sub Image2_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        KeyAscii = 0
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
        End If
        Text3(0).SetFocus
    End If
End Sub


Private Sub Text1_Change(Index As Integer)
    Select Case Index
        Case 1
            Text1(2).Text = ""
            Frame4.Visible = False
    End Select
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        cari_supplier.Left = 1550
        cari_supplier.Top = 3800
        Set cari_supplier.FormPemanggil = Me
        cari_supplier.Show 1
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
        kosong
    End If
    
    If KeyAscii = 13 Then
        If Index = 0 Then
            get_pesanan Text1(0).Text
        ElseIf Index = 1 Then
            get_supplier Text1(1).Text
            Text3(0).SetFocus
        End If
    End If
End Sub

Private Sub cmb_satuan_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{tab}"
End Sub

Private Sub cmb_satuan_Click()
    Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
    
    isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
    hsatuan = Format(Text3(5).Text, "#,##0.00") * isi
    
    disc1 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * hsatuan
    discRp = Val(Format(Text3(7).Text, "##0.00"))
    qty = Val(Format(Text3(2).Text, "##0.00"))
    
    total = (hsatuan - disc1 - discRp) * qty
    
    Text3(8).Text = Format(total, "#,##0.00")
End Sub

Private Sub Text3_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 7
                Text3(z).Text = IIf(z > 1, "0.00", "")
                Text3(z).Locked = True
            Next z
            Text3(4).Text = ""
            cmb_satuan.Clear
            cmb_satuan.Locked = True
        Case 2 To 7
            On Error Resume Next
            Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
    
            isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
            hsatuan = Format(Text3(5).Text, "#,##0.00") * isi
            
            disc1 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * hsatuan
            discRp = Val(Format(Text3(7).Text, "##0.00"))
            qty = Val(Format(Text3(2).Text, "##0.00"))
            
            total = (hsatuan - disc1 - discRp) * qty
            Text3(8).Text = Format(total, "#,##0.00")
    End Select
End Sub

Private Sub Text3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
        cari_stok.Left = 700
        cari_stok.Top = 4600
        cari_stok.bara = Text3(0).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text3(0).Text <> "" Then
                    cmb_satuan.Clear
                    get_item Text3(0).Text
                End If
            End If
        Case 8
            If KeyAscii = 13 Then
                If Text3(0).Text = "" And Val(Format(Text3(7).Text, "##0.00")) = 0 Then
                    MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
                    Exit Sub
                End If
                tambah_item
                hitung_total
            End If
        Case Else
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
            Else
                If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
                KeyAscii = 0
                End If
            End If
    End Select
End Sub

Private Sub Text3_GotFocus(Index As Integer)
    Select Case Index
        Case 2 To 8
            With Text3(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text3_LostFocus(Index As Integer)
    Select Case Index
        Case 2 To 7
            On Error Resume Next
            Text3(Index).Text = IIf(Text3(Index).Text = "", "0.00", Text3(Index).Text)
            If Index = 6 Then
                Text3(Index).Text = Format(Text3(Index).Text, "#0.00")
            Else
                Text3(Index).Text = Format(Text3(Index).Text, "#,##0.00")
            End If
            
            Dim isi, qty, hsatuan, disc1, disc2, discRp, total As Double
            isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
            hsatuan = Format(Text3(5).Text, "#,##0.00") * isi
            
            disc1 = (Val(Format(Text3(6).Text, "##0.00")) / 100) * hsatuan
            discRp = Val(Format(Text3(7).Text, "##0.00"))
            qty = Val(Format(Text3(2).Text, "##0.00"))
            
            total = (hsatuan - disc1 - discRp) * qty
            Text3(8).Text = Format(total, "#,##0.00")
    End Select
End Sub

Private Sub Text4_LostFocus(Index As Integer)
    Text4(Index).Text = IIf(Text4(Index).Text = "", "0.00", Text4(Index).Text)
    If Index = 0 Or Index = 2 Then
        Text4(Index).Text = Format(Text4(Index).Text, "#0.00")
    Else
        Text4(Index).Text = Format(Text4(Index).Text, "#,##0.00")
    End If
    
    Dim potongan, ppn, dp, sub_total, total As Double
    sub_total = Val(Format(Text4(6).Text, "##0.00"))
    
    If Index = 0 Then
        potongan = Val(Format(Text4(0).Text, "##0.00"))
        Text4(1).Text = Format(potongan / 100 * sub_total, "#,##0.00")
    End If
    
    If Index = 1 Then
        potongan = Val(Format(Text4(1).Text, "##0.00"))
        If sub_total > 0 Then
            Text4(0).Text = Format(potongan / sub_total * 100, "##0.00")
        End If
    End If
    
    hitung_total
End Sub

Private Sub Button1_Click(Index As Integer)
    Select Case Index
        Case 1
            cari_supplier.Left = 1550
            cari_supplier.Top = 3800
            Set cari_supplier.FormPemanggil = Me
            cari_supplier.Show 1
    End Select
End Sub

Private Sub Button3_Click()
    If ListView1.ListItems.Count > 0 Then
        pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            ListView1.ListItems.Remove ListView1.SelectedItem.Index
            For z = 1 To ListView1.ListItems.Count
                ListView1.ListItems(z).ListSubItems.item(1) = z
            Next
            hitung_total
        End If
        Text3(0).SetFocus
    End If
End Sub

Private Sub cmd_batal_Click()
    On Error GoTo exc
    pesan = MsgBox("Batalkan transaksi pesanan dg nota " & Text1(0).Text & " ?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from po_head where nopo='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
     
        MsgBox ("Pesanan pembelian nota " & Text1(0).Text & " telah dibatalkan")
        
        If daftar_po.Visible Then
            daftar_po.tampil_po
        End If
        
        kosong
        otomatis
        Text1(1).SetFocus
    End If
    
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    If Text1(1).Text = "" Then
        MsgBox "Supplier masih kosong", vbOKOnly
        Text1(1).SetFocus
        Exit Sub
    End If
    
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "DELETE FROM po_head WHERE nopo='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
    csql = "INSERT INTO po_head (nopo,KDSP,NAMASP,TGL,TGEXP,DISC,DISCTUNAI,PPN,SUBTOTAL,TOTAL,DP,KET,USR) VALUES ('" & _
            Text1(0).Text & "','" & Text1(1).Text & "','" & Text1(2).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "','" & _
            Format(DTPicker2.Value, "yyyy-mm-dd") & "'," & Val(Format(Text4(0).Text, "##0.00")) & "," & _
            Val(Format(Text4(1).Text, "##0.00")) & "," & Val(Format(Text4(3).Text, "##0.00")) & "," & _
            Val(Format(Text4(6).Text, "##0.00")) & "," & Val(Format(Text4(7).Text, "##0.00")) & "," & _
            Val(Format(Text4(8).Text, "##0.00")) & ",'" & Text5.Text & "','" & xy & "')"
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        
    If rs2.State <> 0 Then rs2.Close
    rs2.Open "DELETE FROM po_det WHERE nopo='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
    If ListView1.ListItems.Count > 0 Then
        Dim bara, nama, satuan_dasar, satuan As String
        Dim isi, pesan, terima, Harga, disc1, discRp, total As Double
        For z = 1 To ListView1.ListItems.Count
            bara = ListView1.ListItems(z).ListSubItems.item(2)
            nama = ListView1.ListItems(z).ListSubItems.item(3)
            pesan = Val(Format(ListView1.ListItems(z).ListSubItems.item(4), "##0.00"))
            terima = Val(Format(ListView1.ListItems(z).ListSubItems.item(5), "##0.00"))
            satuan_dasar = ListView1.ListItems(z).ListSubItems.item(6)
            satuan = ListView1.ListItems(z).ListSubItems.item(7)
            isi = Val(ListView1.ListItems(z).ListSubItems.item(9))
            Harga = Val(Format(ListView1.ListItems(z).ListSubItems.item(10), "##0.00"))
            disc1 = Val(ListView1.ListItems(z).ListSubItems.item(12))
            discRp = Val(Format(ListView1.ListItems(z).ListSubItems.item(13), "##0.00"))
            total = Val(Format(ListView1.ListItems(z).ListSubItems.item(14), "##0.00"))
                
            csql = "INSERT INTO po_det(NOPO,BARA,NAMA,PESAN,KIRIM,SATUAN_DSR,SATUAN,ISI,HBELI,TOTAL) " & _
                    "VALUES('" & Text1(0).Text & "','" & bara & "','" & nama & "'," & pesan & "," & terima & _
                    ",'" & satuan_dasar & "','" & satuan & "'," & isi & "," & Harga & "," & total & ")"
            
            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next
    End If
        
    MsgBox ("Input pesanan pembelian sudah disimpan")
    
    get_pesanan Text1(0).Text
    If daftar_po.Visible Then
        daftar_po.tampil_po
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    koneksi
    get_cabang
    kosong
'    otomatis
    
    get_akun
    cmd_batal.Enabled = False
End Sub

Private Sub Form_Activate()
    Text1(1).SetFocus
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture2.Top = Me.Height - 2650
'    Picture2.Width = Me.Width - 255
    Picture1.Height = Me.Height - 2700
'    Picture1.Width = Me.Width - 255
    ListView1.Height = Picture1.Height - 2100
'    ListView1.Width = Me.Width - 495
'    Label1.Width = Me.Width - 50
End Sub

Sub tampil_pesanan(nota As String)
    If Text1(0).Text <> "" Then
        pesan = IIf(nota = Text1(0).Text, "dilanjutkan", "mengganti dengan nota " & nota)
        pesan = MsgBox("Form edit pesanan " & Text1(0).Text & " sedang terbuka, apakah ingin " & pesan & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbNo Then
            Exit Sub
        Else
            get_pesanan (nota)
        End If
    Else
        get_pesanan (nota)
    End If
End Sub

Sub get_pesanan(nopo As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from po_head where nopo='" & nopo & "'", con, adOpenKeyset, adLockOptimistic
        
    If Not rs1.EOF Then
        Caption = "Edit Pesanan : " & rs1.Fields!nopo
        cmd_batal.Enabled = IIf(rs1.Fields!nota <> "", True, False)
        
        Text1(0).Text = rs1.Fields!nopo
        DTPicker1.Value = rs1.Fields!tgl
        DTPicker2.Value = rs1.Fields!tgexp
        Text1(1).Text = "" & rs1.Fields!kdsp
        Text1(2).Text = "" & rs1.Fields!namasp
        
        Text4(0).Text = Format(rs1.Fields!disc, "##0.00")
        Text4(1).Text = Format(rs1.Fields!disctunai, "#,#0.00")
        
        Text4(3).Text = Format(rs1.Fields!ppn, "#,#0.00")
        If rs1.Fields!ppn > 0 And rs1.Fields!subtotal > 0 Then
            Text4(2).Text = Format((rs1.Fields!ppn / (rs1.Fields!subtotal - rs1.Fields!disctunai)) * 100, "##0.00")
        End If

        Text4(6).Text = Format(rs1.Fields!subtotal, "#,#0.00")
        Text4(6).Tag = rs1.Fields!subtotal
        Text4(7).Text = Format(rs1.Fields!total, "#,#0.00")
        Text4(8).Text = Format(rs1.Fields!dp, "#,#0.00")
        Text4(9).Text = Format(rs1.Fields!total - rs1.Fields!dp, "#,#0.00")
    
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from po_det where nopo='" & nopo & "'", con, adOpenKeyset, adLockOptimistic
        
        Dim qtyPesan, qtyTerima As Double
        
        If Not rs2.EOF Then
            ListView1.ListItems.Clear
            Do While Not rs2.EOF
                j = ListView1.ListItems.Count + 1
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!bara
                item.SubItems(3) = "" & rs2.Fields!nama
                qtyPesan = qtyPesan + rs2.Fields!pesan
                item.SubItems(4) = Format(rs2.Fields!pesan, "#,#0.00")
                qtyTerima = qtyTerima + rs2.Fields!kirim
                item.SubItems(5) = Format(rs2.Fields!kirim, "#,#0.00")
                item.SubItems(6) = "" & rs2.Fields!satuan_dsr
                item.SubItems(7) = "" & rs2.Fields!satuan
                item.SubItems(8) = rs2.Fields!satuan & IIf(rs2.Fields!isi > 1, " (" & rs2.Fields!isi & ")", "")
                item.SubItems(9) = rs2!isi
                item.SubItems(10) = Format(rs2.Fields!hbeli, "#,#0.00")
                item.SubItems(11) = "0.00" 'Format(rs2!disc1 / rs2.Fields!hbeli * 100, "##0.00")
                item.SubItems(12) = "0.00" 'Format(rs2!disc1, "#,#0.00")
                item.SubItems(13) = "0.00" 'Format(rs2!discrp, "#,#0.00")
                item.SubItems(14) = Format(rs2.Fields!total, "#,#0.00")
                rs2.MoveNext
            Loop
        End If
        
        Text4(4).Text = Format(qtyPesan, "#,#0.00")
        Text4(5).Text = Format(qtyTerima, "#,#0.00")
        
        If Text1(1).Text = "" Then Exit Sub
        
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from msupp where kdsp='" & Text1(1).Text & "' limit 1", con, adOpenKeyset, adLockOptimistic
                
        If Not rs1.EOF Then
            Text1(2).Text = "" & rs1!namasp
            Label2(8).Caption = "" & rs1!alamat
            Label2(9).Caption = "" & rs1!kota
            Label2(10).Caption = "" & rs1!telp
                    
            Frame4.Visible = True
            For z = 0 To 7
                Text3(z).Text = IIf(z < 2, "", "0.00")
                Text3(z).Locked = False
            Next z
            Text3(0).SetFocus
        End If
    Else
        cmd_batal.Enabled = False
        
        MsgBox ("Data Pesanan pembelian tidak ditemukan!")
        Text1(0).SetFocus
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Sub get_item(bara As String)
    If rs1.State <> 0 Then rs1.Close
    csql = "select m.bara, m.nama, m.satuan, coalesce(s.hbeli, mb.aver)hbeli from mstock m join mstock_bo mb on mb.bara=m.bara " & _
            "left join mstock_supp s on m.bara=s.bara and kdsp='" & Text1(1).Text & "' where m.bara='" & bara & "' and mb.bo='" & cmb_cabang.Text & "' limit 1"
        
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Text3(0).Text = "" & rs1!bara
        Text3(1).Text = "" & rs1!nama
        Text3(2).Text = "1.00"
        Text3(4).Text = "" & rs1!satuan
        With cmb_satuan
            .AddItem (rs1!satuan)
            .ItemData(.NewIndex) = 1 'isi
        End With
        Text3(5).Text = Format(rs1!hbeli, "#,##0.00")
                        
        'cmb_satuan.Clear
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from mstock_satuan where bara='" & Text3(0).Text & "' and bo='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            Do While Not rs2.EOF
                With cmb_satuan
                    .AddItem rs2!satuan
                    .ItemData(.NewIndex) = IIf(Val(rs2!isi) < 1, 1, Val(rs2!isi)) 'isi
                End With
                rs2.MoveNext
            Loop
        End If
                        
        Text3(8).Text = Format(rs1!hbeli * Val(cmb_satuan.ItemData(0)), "#,##0.00")
        cmb_satuan.ListIndex = 0
        cmb_satuan.Locked = False
            
        For z = 1 To 7
            Text3(z).Locked = False
        Next z
    Else
        MsgBox ("Kode barang tidak ada")
        cari_stok.Left = 700
        cari_stok.Top = 4600
        cari_stok.bara = Text3(0).Text
        Set cari_stok.FormPemanggil = Me
        cari_stok.Show 1
    End If
    SendKeys "{tab}"
End Sub

Private Sub tambah_item()
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(2) = Text3(0).Text
    item.SubItems(3) = Text3(1).Text
    item.SubItems(4) = Text3(2).Text
    item.SubItems(5) = Text3(3).Text
    item.SubItems(6) = Text3(4).Text 'satuan dasar
    item.SubItems(7) = cmb_satuan.Text 'satuan
    Dim isi As Double
    isi = cmb_satuan.ItemData(cmb_satuan.ListIndex)
    item.SubItems(8) = cmb_satuan.Text & IIf(Val(isi) > 1, " (" & isi & ")", "")
    item.SubItems(9) = isi
    
    Dim disc1, Harga As Double
    Harga = Val(Format(Text3(5).Text, "##0.00"))
    disc1 = Val(Format(Text3(6).Text, "##0.00"))
    
    item.SubItems(10) = Text3(5).Text 'hbeli
    item.SubItems(11) = Text3(6).Text
    item.SubItems(12) = disc1 / 100 * Harga
    item.SubItems(13) = Text3(7).Text
    item.SubItems(14) = Text3(8).Text
    
    Text3(0).Text = ""
    Text3(0).SetFocus
End Sub

Private Sub hitung_total()
    Dim pesan, terima, potongan, ppn, dp, subtotal, total, kurang As Double
    If ListView1.ListItems.Count > 0 Then
        For z = 1 To ListView1.ListItems.Count
            pesan = pesan + Val(Format(ListView1.ListItems(z).ListSubItems.item(4), "##0.00"))
            terima = terima + Val(Format(ListView1.ListItems(z).ListSubItems.item(5), "##0.00"))
            subtotal = subtotal + Val(Format(ListView1.ListItems(z).ListSubItems.item(14), "##0.00"))
        Next
    End If
        
    dp = Val(Format(Text4(8), "##0.00"))
    potongan = Val(Format(Text4(1), "##0.00"))
        
    ppn = Val(Format(Text4(2).Text, "##0.00"))
    ppn = IIf(ppn > 0, (ppn / 100) * (subtotal - potongan), 0)
    total = (subtotal - potongan) + ppn
        
    kurang = total - dp
    kurang = IIf(kurang < 0, 0, kurang)
        
    Text4(3).Text = Format(ppn, "#,##0.00")
    Text4(4).Text = Format(pesan, "#,##0.00")
    Text4(5).Text = Format(terima, "#,##0.00")
    Text4(6).Text = Format(subtotal, "#,##0.00")
    Text4(7).Text = Format(total, "#,##0.00")
    Text4(9).Text = Format(kurang, "#,##0.00")
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Sub get_supplier(kode As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msupp where kdsp='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(1).Text = "" & rs1!kdsp
        Text1(2).Text = "" & rs1!namasp
        Label2(8).Caption = "" & rs1!alamat
        Label2(9).Caption = "" & rs1!kota
        Label2(10).Caption = "" & rs1!telp
                
        Frame4.Visible = True
        For z = 0 To 7
            Text3(z).Text = IIf(z < 2, "", "0.00")
            Text3(z).Locked = False
        Next z
'        Text3(0).SetFocus
    Else
        MsgBox ("Kode supplier tidak ada")
        Frame4.Visible = False
        cari_supplier.Left = 1550
        cari_supplier.Top = 3800
        Set cari_supplier.FormPemanggil = Me
        cari_supplier.Show 1
    End If
End Sub

Sub kosong()
    Text1(1).Text = ""
    
    For z = 0 To 8
        Text3(z).Text = IIf(z > 1, "0.00", "")
        Text3(z).Locked = True
    Next z
    
    For z = 0 To 9
        Text4(z).Text = "0.00"
    Next z
    
    cmb_satuan.Clear
    cmb_satuan.Locked = True
    
    DTPicker1.Value = Date
    DTPicker2.Value = DateAdd("d", 7, Date)
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    cmd_batal.Enabled = False
End Sub

Sub get_akun()
    Set akun = New Collection
    combo_akun.Clear
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis=1 order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim coa As PropertyField
        Do While Not rs1.EOF
            combo_akun.AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            
            Set coa = New PropertyField
            coa.Key = rs1.Fields!kode
            coa.Value = rs1.Fields!nama
            
            akun.Add coa
            rs1.MoveNext
        Loop
    End If
End Sub

Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Pesanan Beli'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = cmb_cabang.Text
        
        kol3 = rs1.Fields!kol3
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = cmb_cabang.Text
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = cmb_cabang.Text
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = cmb_cabang.Text
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nopo) as nota from po_head where length(nota)=" & j + z & " and right(nopo," & z & ")='" & nomor_nota & "' and left(nopo," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nopo) as nota from po_head where length(nopo)=" & j + z & " and left(nopo," & z & ")='" & nomor_nota & "' and right(nopo," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub
