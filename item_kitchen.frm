VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form item_kitchen 
   Caption         =   "Item Kitchen"
   ClientHeight    =   7095
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7590
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   7590
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Daftar Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   720
      TabIndex        =   10
      Top             =   1000
      Visible         =   0   'False
      Width           =   6855
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   6615
      End
      Begin MSComctlLib.ListView ListView2 
         Height          =   3855
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   6800
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Barcode"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   8643
         EndProperty
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5205
      Left            =   0
      TabIndex        =   9
      Top             =   1125
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   9181
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Barcode"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Nama Barang"
         Object.Width           =   8819
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   -120
      ScaleHeight     =   1065
      ScaleWidth      =   7665
      TabIndex        =   2
      Top             =   0
      Width           =   7695
      Begin VB.TextBox Text1 
         Height          =   350
         Index           =   0
         Left            =   840
         MaxLength       =   20
         TabIndex        =   4
         Top             =   600
         Width           =   1695
      End
      Begin VB.TextBox Text1 
         Height          =   350
         Index           =   1
         Left            =   2520
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   3
         Top             =   600
         Width           =   3135
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   375
         Left            =   5760
         TabIndex        =   8
         Top             =   550
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tambahkan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "item_kitchen.frx":0000
         PICN            =   "item_kitchen.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   0
         Width           =   7575
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Item : "
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   630
         Width           =   615
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   5280
         Width           =   6135
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   7545
      TabIndex        =   0
      Top             =   6360
      Width           =   7575
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   6240
         TabIndex        =   1
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Tutup"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "item_kitchen.frx":03D3
         PICN            =   "item_kitchen.frx":03EF
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "item_kitchen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public kitchen As String

Sub tampil_kitchen_item()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from kitchen_item where kitchen='" & kitchen & "'", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!bara
            item.SubItems(2) = "" & rs1.Fields!nama
            rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub Text1_Change(Index As Integer)
    If Index = 0 Then Text1(1).Text = ""
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
        Frame1.Visible = True
        Picture1.Enabled = False
        Picture2.Enabled = False
        Text2.Text = Text1(0).Text
        Text2.SetFocus
        cari_item
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
        If KeyAscii = 13 And Text1(0).Text <> "" Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "SELECT bara, nama from mstock where bara='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
            If Not rs1.EOF Then
                Text1(0).Text = "" & rs1!bara
                Text1(1).Text = "" & rs1!nama
            Else
                Frame1.Visible = True
                Picture1.Enabled = False
                Picture2.Enabled = False
                Text2.Text = Text1(0).Text
                Text2.SetFocus
                cari_item
            End If
        End If
    End If
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
End Sub

Private Sub cmd_simpan_Click()
    On Error GoTo exc
    If Text1(0).Text <> "" And Text1(1).Text <> "" Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "insert ignore into kitchen_item (kitchen,bara,nama) values ('" & kitchen & "','" & Text1(0).Text & "','" & Text1(1).Text & "')", con, adOpenKeyset, adLockOptimistic
        
        MsgBox ("Simpan sukses!"), vbInformation, "Info"
        tampil_kitchen_item
        Text1(0).Text = ""
    Else
        MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
    End If
    Text1(0).SetFocus
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub Form_Activate()
    Text1(0).SetFocus
    tampil_kitchen_item
End Sub

Private Sub Form_Load()
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    koneksi
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        Frame1.Visible = True
        Picture1.Enabled = False
        Picture2.Enabled = False
        Text2.Text = Text1(0).Text
        Text2.SetFocus
        cari_item
    ElseIf KeyCode = 27 Then
        Frame1.Visible = False
        Picture1.Enabled = True
        Picture2.Enabled = True
        Text1(0).SetFocus
    End If
End Sub

Sub cari_item()
    ListView2.ListItems.Clear
    ListView2.View = lvwReport
    
    If Text2.Text <> "" Then
        csql = "select bara, nama from mstock where bara = '" & Text2.Text & "' or nama like '%" & Text2.Text & "%' "
        
        j = 0
        If rs1.State <> 0 Then rs1.Close
        rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        
        If Not rs1.EOF Then
            Do While Not rs1.EOF
                j = j + 1
                Set item = ListView2.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = "" & rs1.Fields!bara
                item.SubItems(2) = "" & rs1.Fields!nama
                rs1.MoveNext
            Loop
        End If
    End If
End Sub

Private Sub ListView2_DblClick()
    If ListView2.ListItems.Count > 0 Then
        Text1(0).Text = ListView2.SelectedItem.SubItems(1)
        Text1(1).Text = ListView2.SelectedItem.SubItems(2)
        Frame1.Visible = False
        Picture1.Enabled = True
        Picture2.Enabled = True
        Text1(0).SetFocus
    End If
End Sub

Private Sub ListView2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If ListView2.ListItems.Count > 0 Then
            Text1(0).Text = ListView2.SelectedItem.SubItems(1)
            Text1(1).Text = ListView2.SelectedItem.SubItems(2)
        End If
        Frame1.Visible = False
        Picture1.Enabled = True
        Picture2.Enabled = True
        Text1(0).SetFocus
    End If
End Sub

Private Sub Text2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then
        Frame1.Visible = False
        Picture1.Enabled = True
        Picture2.Enabled = True
    End If
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cari_item
        ListView2.SetFocus
    End If
End Sub
