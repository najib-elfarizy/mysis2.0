VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form edit_pembayaran 
   Caption         =   "Edit Pembayaran"
   ClientHeight    =   6495
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11670
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6495
   ScaleWidth      =   11670
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame5"
      Height          =   660
      Left            =   50
      TabIndex        =   12
      Top             =   1680
      Width           =   11500
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   330
         Width           =   1680
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   5
         Left            =   8340
         Locked          =   -1  'True
         TabIndex        =   18
         Text            =   "0"
         Top             =   330
         Width           =   1485
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   2260
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   330
         Width           =   1470
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   550
         MaxLength       =   15
         TabIndex        =   13
         Top             =   330
         Width           =   1700
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   2
         Left            =   3735
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   330
         Width           =   1440
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   4
         Left            =   6840
         Locked          =   -1  'True
         TabIndex        =   17
         Text            =   "0"
         Top             =   330
         Width           =   1500
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   6
         Left            =   9840
         Locked          =   -1  'True
         TabIndex        =   19
         Text            =   "0"
         Top             =   330
         Width           =   1635
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Sisa"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   5160
         TabIndex        =   37
         Top             =   45
         Width           =   1680
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No. Transaksi"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   555
         TabIndex        =   26
         Top             =   45
         Width           =   1700
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Potongan"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   6840
         TabIndex        =   25
         Top             =   45
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tanggal JT"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3735
         TabIndex        =   24
         Top             =   45
         Width           =   1440
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2260
         TabIndex        =   23
         Top             =   45
         Width           =   1470
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   550
         Index           =   0
         Left            =   50
         TabIndex        =   22
         Top             =   45
         Width           =   500
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   8340
         TabIndex        =   21
         Top             =   45
         Width           =   1485
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Bayar"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   9840
         TabIndex        =   20
         Top             =   45
         Width           =   1635
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2805
      Left            =   60
      TabIndex        =   27
      Top             =   2280
      Width           =   11480
      _ExtentX        =   20241
      _ExtentY        =   4948
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "No."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "No. Transaksi"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Tanggal"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Tanggal JT"
         Object.Width           =   2558
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Sisa"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Potongan"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Text            =   "Total"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Bayar"
         Object.Width           =   2646
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   5175
      Left            =   0
      ScaleHeight     =   5145
      ScaleWidth      =   11565
      TabIndex        =   28
      Top             =   0
      Width           =   11595
      Begin VB.Frame Frame3 
         BackColor       =   &H00FFFFC0&
         Height          =   1570
         Left            =   6960
         TabIndex        =   44
         Top             =   0
         Visible         =   0   'False
         Width           =   4455
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   5
            Left            =   1320
            MaxLength       =   25
            TabIndex        =   52
            TabStop         =   0   'False
            Top             =   1180
            Width           =   3015
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   4
            Left            =   1320
            MaxLength       =   25
            TabIndex        =   51
            TabStop         =   0   'False
            Top             =   840
            Width           =   3015
         End
         Begin VB.TextBox Text1 
            Height          =   315
            Index           =   3
            Left            =   1320
            MaxLength       =   25
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   480
            Width           =   3015
         End
         Begin MSComCtl2.DTPicker DTPicker2 
            Height          =   315
            Left            =   1320
            TabIndex        =   49
            Top             =   150
            Width           =   1815
            _ExtentX        =   3201
            _ExtentY        =   556
            _Version        =   393216
            CustomFormat    =   "dd/MM/yyyy"
            Format          =   110297091
            CurrentDate     =   43159
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Atas Nama :"
            Height          =   195
            Index           =   8
            Left            =   240
            TabIndex        =   48
            Top             =   1200
            Width           =   870
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Tanggal Cair :"
            Height          =   195
            Index           =   11
            Left            =   240
            TabIndex        =   47
            Top             =   195
            Width           =   1095
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nama Bank :"
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   46
            Top             =   525
            Width           =   1170
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nomor BG :"
            Height          =   195
            Index           =   7
            Left            =   240
            TabIndex        =   45
            Top             =   840
            Width           =   1185
         End
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "edit_pembayaran.frx":0000
         Left            =   4920
         List            =   "edit_pembayaran.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   43
         Top             =   480
         Width           =   1815
      End
      Begin VB.ComboBox combo_akun 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   41
         Top             =   1200
         Width           =   5295
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   1440
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   1
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   6
         Top             =   840
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Index           =   2
         Left            =   3360
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   840
         Width           =   3015
      End
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   29
         Top             =   480
         Width           =   1935
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   4920
         TabIndex        =   30
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   110297091
         CurrentDate     =   43159
      End
      Begin MySIS.Button Button1 
         Height          =   315
         Index           =   1
         Left            =   6400
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   840
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "+"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_pembayaran.frx":001D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Car Bayar :"
         Height          =   255
         Index           =   5
         Left            =   3480
         TabIndex        =   42
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier :"
         Height          =   255
         Index           =   3
         Left            =   0
         TabIndex        =   38
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   35
         Top             =   135
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tanggal :"
         Height          =   255
         Index           =   1
         Left            =   3480
         TabIndex        =   34
         Top             =   135
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept / Gudang :"
         Height          =   255
         Index           =   2
         Left            =   0
         TabIndex        =   33
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Akun :"
         Height          =   255
         Index           =   4
         Left            =   0
         TabIndex        =   32
         Top             =   1250
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1665
         Left            =   0
         TabIndex        =   36
         Top             =   0
         Width           =   11550
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   0
      ScaleHeight     =   1305
      ScaleWidth      =   11565
      TabIndex        =   3
      Top             =   5160
      Width           =   11595
      Begin VB.Frame Frame2 
         Height          =   1215
         Left            =   8200
         TabIndex        =   5
         Top             =   0
         Width           =   3270
         Begin VB.CheckBox Check1 
            Height          =   255
            Left            =   1150
            TabIndex        =   39
            Top             =   720
            Width           =   855
         End
         Begin VB.TextBox Text4 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1155
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Text            =   "0.00"
            Top             =   225
            Width           =   2000
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Lunas :"
            Height          =   195
            Index           =   0
            Left            =   555
            TabIndex        =   40
            Top             =   720
            Width           =   525
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Total Bayar :"
            Height          =   195
            Index           =   1
            Left            =   195
            TabIndex        =   7
            Top             =   285
            Width           =   900
         End
      End
      Begin VB.TextBox Text5 
         Height          =   800
         Left            =   5040
         MultiLine       =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   405
         Width           =   3050
      End
      Begin MySIS.Button cmd_simpan 
         Height          =   495
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Simpan"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_pembayaran.frx":0039
         PICN            =   "edit_pembayaran.frx":0055
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   2760
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_pembayaran.frx":01AF
         PICN            =   "edit_pembayaran.frx":01CB
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button Button3 
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   120
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Hapus Item"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_pembayaran.frx":0601
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_batal 
         Height          =   495
         Left            =   1440
         TabIndex        =   53
         TabStop         =   0   'False
         Top             =   720
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Batalkan"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "edit_pembayaran.frx":061D
         PICN            =   "edit_pembayaran.frx":0639
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label5 
         Caption         =   "Keterangan :"
         Height          =   255
         Left            =   5040
         TabIndex        =   11
         Top             =   135
         Width           =   1215
      End
   End
End
Attribute VB_Name = "edit_pembayaran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim akun As New Collection

Private Sub Button5_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub cmd_batal_Click()
    On Error GoTo exc
    pesan = MsgBox("Batalkan Pembayaran Hutang dg nota " & Text1(0).Text & " ?", vbQuestion + vbYesNo, "Konfirmasi")
    If pesan = vbYes Then
        If rs1.State <> 0 Then rs1.Close
        rs1.Open "delete from tt_head where nott='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
     
        MsgBox ("Pembayaran Hutang " & Text1(0).Text & " telah dibatalkan")
        
        If daftar_pembayaran.Visible Then
            daftar_pembayaran.tampil_bayar
        End If
        
        kosong
        otomatis
        Text1(1).SetFocus
    End If
    
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Combo1_Click()
    Frame3.Visible = Combo1.Text = "CEK/BG"
    Text1(3).Text = ""
    Text1(4).Text = ""
    Text1(5).Text = ""
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        KeyAscii = 0
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
        End If
        Text3(0).SetFocus
    End If
End Sub

Private Sub Image2_Click()
    frame_akun.Visible = False
    ListView1.Enabled = True
    Picture1.Enabled = True
    Picture2.Enabled = True
    Frame1.Enabled = True
End Sub

Private Sub Text1_Change(Index As Integer)
    Select Case Index
        Case 1
            Text1(2).Text = ""
    End Select
End Sub

Private Sub Text1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 Then
        cari_supplier.Left = 1550
        cari_supplier.Top = 3800
        Set cari_supplier.FormPemanggil = Me
        cari_supplier.Show 1
    End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 1 Then
            get_supplier Text1(1).Text
            Text3(0).SetFocus
        End If
    End If
End Sub

Private Sub Text3_Change(Index As Integer)
    Select Case Index
        Case 0
            For z = 1 To 6
                Text3(z).Text = ""
                Text3(z).Locked = True
            Next z
        Case 4
            Dim total, sisa As Double
            sisa = Val(Format(Text3(3).Text, "##0.00"))
            potongan = Val(Format(Text3(4).Text, "##0.00"))
            Text3(5).Text = Format(sisa - potongan, "#,##0.00")
    End Select
End Sub

Private Sub Text3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = 116 And Index = 0 Then
        cari_pembelian.Left = 700
        cari_pembelian.Top = 4600
        Set cari_pembelian.FormPemanggil = Me
        cari_pembelian.Cari = Text1(1).Text
        cari_pembelian.Show 1
    End If
End Sub

Private Sub Text3_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 0
            If KeyAscii = 13 Then
                KeyAscii = 0
                If Text3(0).Text <> "" Then
                    get_pembelian Text3(0).Text
                End If
            End If
        Case 6
            If KeyAscii = 13 Then
                If Text3(0).Text = "" And Val(Format(Text3(3).Text, "##0.00")) = 0 Then
                    MsgBox ("Lengkapi data terlebih dahulu!"), vbInformation, "Info"
                    Exit Sub
                End If
                
                Dim total, bayar As Double
                total = Val(Format(Text3(5).Text, "##0.00"))
                bayar = Val(Format(Text3(6).Text, "##0.00"))
                
                If total < bayar Then
                    MsgBox ("Jumlah bayar melebihi total = " & total & vbNewLine & "Jumlah bayar akan disesuaikan dengan jumlah total"), vbInformation, "Info"
                    Text3(6).Text = Format(bayar, "#,##0.00")
                End If
                
                tambah_item
                hitung_total
            End If
        Case Else
            If KeyAscii = 13 Then
                KeyAscii = 0
                SendKeys "{tab}"
            Else
                If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") _
                Or KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete Or KeyAscii = 45) Then
                KeyAscii = 0
                End If
            End If
    End Select
End Sub

Private Sub Text3_GotFocus(Index As Integer)
    Select Case Index
        Case 2 To 6
            With Text3(Index)
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
    End Select
End Sub

Private Sub Text3_LostFocus(Index As Integer)
    If Index = 3 Or Index = 4 Or Index = 5 Then
        On Error Resume Next
        Text3(Index).Text = IIf(Text3(Index).Text = "", "0.00", Text3(Index).Text)
        Text3(Index).Text = Format(Text3(Index).Text, "#,##0.00")
    End If
    If Index = 6 Then
        If Text3(0).Text <> "" Then
            Dim total, bayar As Double
            total = Val(Format(Text3(5).Text, "##0.00"))
            bayar = Val(Format(Text3(6).Text, "##0.00"))
                
            bayar = IIf(total < bayar, total, bayar)
            Text3(6).Text = Format(bayar, "#,##0.00")
        End If
    End If
End Sub

Private Sub Button1_Click(Index As Integer)
    Select Case Index
        Case 1
            cari_supplier.Left = 1550
            cari_supplier.Top = 3800
            Set cari_supplier.FormPemanggil = Me
            cari_supplier.Show 1
    End Select
End Sub

Private Sub Button3_Click(Index As Integer)
    If Index = 0 Then
        If ListView1.ListItems.Count > 0 Then
            pesan = MsgBox("Hapus item " & ListView1.SelectedItem.SubItems(3) & "?", vbQuestion + vbYesNo, "Konfirmasi")
            If pesan = vbYes Then
                ListView1.ListItems.Remove ListView1.SelectedItem.Index
                For z = 1 To ListView1.ListItems.Count
                    ListView1.ListItems(z).ListSubItems.item(1) = z
                Next
                hitung_total
            End If
            Text3(0).SetFocus
        End If
    End If
End Sub

Private Sub cmd_keluar_Click()
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub cmd_simpan_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "Detail pembayaran masih kosong", vbOKOnly
        Text3(0).SetFocus
        Exit Sub
    End If
    
    Dim coa As PropertyField
    If combo_akun.ListIndex > -1 Then
        Set coa = akun(combo_akun.ListIndex + 1)
    Else
        Set coa = New PropertyField
    End If
    
    otomatis
    On Error GoTo exc
    csql = "INSERT INTO tt_head (nott,KDSP,NAMASP,TGL,TOTAL,JENIS_BAYAR,TGL_BG,BANK_BG,NOBG,AN_BG,AKUN,KET,BO,USR) VALUES ('" & _
            Text1(0).Text & "','" & Text1(1).Text & "','" & Text1(2).Text & "','" & Format(DTPicker1.Value, "yyyy-mm-dd") & "'," & _
            Val(Format(Text4.Text, "##0.00")) & ",'" & Combo1.Text & "','" & Format(DTPicker2.Value, "yyyy-mm-dd") & "','" & _
            Text1(3).Text & "','" & Text1(4).Text & "','" & Text1(5).Text & "','" & coa.Key & _
            "','" & Text5.Text & "','" & cmb_cabang.Text & "','" & xy & "')"
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        
    If rs2.State <> 0 Then rs2.Close
    rs2.Open "DELETE FROM tt_det WHERE nott='" & Text1(0).Text & "'", con, adOpenKeyset, adLockOptimistic
        
    If ListView1.ListItems.Count > 0 Then
        Dim nota, tgl, tgljt As String
        Dim netto, disc, bayar As Double
        
        For z = 1 To ListView1.ListItems.Count
            nota = ListView1.ListItems(z).ListSubItems.item(2)
            tgl = ListView1.ListItems(z).ListSubItems.item(3)
            tgljt = ListView1.ListItems(z).ListSubItems.item(4)
            netto = Val(Format(ListView1.ListItems(z).ListSubItems.item(5), "##0.00"))
            disc = Val(Format(ListView1.ListItems(z).ListSubItems.item(6), "##0.00"))
            bayar = Val(Format(ListView1.ListItems(z).ListSubItems.item(7), "##0.00"))
                
            csql = "INSERT INTO tt_det(nott,NOTA,TGL_NOTA,TGL_JT,NETTO,DISC,BAYAR) VALUES('" & _
                    Text1(0).Text & "','" & nota & "','" & tgl & "','" & tgljt & "'," & netto & "," & disc & "," & bayar & ")"
            
            Debug.Print csql
            If rs2.State <> 0 Then rs2.Close
            rs2.Open csql, con, adOpenKeyset, adLockOptimistic
        Next
    End If
        
    MsgBox ("Input pesanan pembelian sudah disimpan")
    
    kosong
    otomatis
    
    If daftar_pembayaran.Visible Then
        daftar_pembayaran.tampil_bayar
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub Form_Load()
    koneksi
    get_cabang
    kosong
'    otomatis
    get_akun
End Sub

Private Sub Form_Activate()
    Text1(1).SetFocus
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture2.Top = Me.Height - 2000
'    Picture2.Width = Me.Width - 255
    Picture1.Height = Me.Height - 2000
'    Picture1.Width = Me.Width - 255
    ListView1.Height = Picture1.Height - 2350
'    ListView1.Width = Me.Width - 495
'    Label1.Width = Me.Width - 50
End Sub

Sub get_pembelian(kode As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from beli_head where nota='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text3(0).Text = rs1.Fields!nota
        Text3(1).Text = Format(rs1.Fields!tgl, "yyyy-mm-dd")
        Text3(2).Text = Format(rs1.Fields!jatuh, "yyyy-mm-dd")
        Text3(3).Text = Format(rs1.Fields!kredit, "#,##0.00")
        Text3(4).Text = Format(0, "#,##0.00")
        Text3(5).Text = Format(rs1.Fields!kredit, "#,##0.00")
        Text3(6).Text = Format(rs1.Fields!kredit, "#,##0.00")
        
        Text3(4).Locked = False
        Text3(6).Locked = False
        SendKeys "{tab}"
'        Text3(4).SetFocus
    Else
        MsgBox ("Nota pembelian tidak ada")
    End If
End Sub

Private Sub tambah_item()
    j = ListView1.ListItems.Count + 1
    Set item = ListView1.ListItems.Add(, , j)
    item.Text = j
    item.SubItems(1) = j
    item.SubItems(2) = Text3(0).Text
    item.SubItems(3) = Text3(1).Text
    item.SubItems(4) = Text3(2).Text
    item.SubItems(5) = Text3(3).Text
    item.SubItems(6) = Text3(4).Text
    item.SubItems(7) = Text3(5).Text
    item.SubItems(8) = Text3(6).Text
    
    Text3(0).Text = ""
    Text3(0).SetFocus
End Sub

Private Sub hitung_total()
    Dim bayar As Double
    If ListView1.ListItems.Count > 0 Then
        For z = 1 To ListView1.ListItems.Count
            bayar = bayar + Val(Format(ListView1.ListItems(z).SubItems(8), "##0.00"))
        Next
    End If
    
    Text4.Text = Format(bayar, "#,##0.00")
End Sub

Sub tampil_pembayaran(nota As String)
    If Text1(0).Text <> "" Then
        pesan = IIf(nota = Text1(0).Text, "dilanjutkan", "mengganti dengan nomor " & nota)
        pesan = MsgBox("Form edit pembayaran " & Text1(0).Text & " sedang terbuka, apakah ingin " & pesan & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbNo Then
            Exit Sub
        Else
            get_pembayaran (nota)
        End If
    Else
        get_pembayaran (nota)
    End If
End Sub

Sub get_pembayaran(nott As String)
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from tt_head where nott='" & nott & "'", con, adOpenKeyset, adLockOptimistic
        
    If Not rs1.EOF Then
        Caption = "Edit Pembayaran : " & rs1.Fields!nott
'        cmd_batal.Enabled = IIf(rs1.Fields!nota <> "", True, False)
        
        Text1(0).Text = rs1.Fields!nott
        Text1(1).Text = "" & rs1.Fields!kdsp
        Text1(2).Text = "" & rs1.Fields!namasp
        Text1(3).Text = "" & rs1.Fields!bank_bg
        Text1(4).Text = "" & rs1.Fields!nobg
        Text1(5).Text = "" & rs1.Fields!an_bg
        
        DTPicker1.Value = rs1.Fields!tgl
        DTPicker2.Value = rs1.Fields!tgl_bg
        
        Combo1.Text = rs1.Fields!jenis_bayar
        Dim coa As PropertyField
        Set coa = akun(rs1.Fields!akun)
        combo_akun.Text = coa.Key & Space(3) & coa.Value
        
        Text4.Text = Format(rs1.Fields!total, "#,#0.00")
        Text5.Text = rs1.Fields!ket
        
        If rs2.State <> 0 Then rs2.Close
        rs2.Open "select * from tt_det where nott='" & nott & "'", con, adOpenKeyset, adLockOptimistic
        
        If Not rs2.EOF Then
            ListView1.ListItems.Clear
            Do While Not rs2.EOF
                j = ListView1.ListItems.Count + 1
                Set item = ListView1.ListItems.Add(, , j)
                item.Text = j
                item.SubItems(1) = j
                item.SubItems(2) = "" & rs2.Fields!nota
                item.SubItems(3) = Format(rs2.Fields!tgl_nota, "yyyy-mm-dd")
                item.SubItems(4) = Format(rs2.Fields!tgl_jt, "yyyy-mm-dd")
                item.SubItems(5) = Format(rs2.Fields!netto - rs2.Fields!bayar, "#,#0.00")
                item.SubItems(6) = Format(rs2.Fields!netto, "#,#0.00")
                item.SubItems(7) = Format(rs2.Fields!disc, "#,#0.00")
                item.SubItems(8) = Format(rs2.Fields!bayar, "#,#0.00")
                rs2.MoveNext
            Loop
        End If
    Else
        cmd_batal.Enabled = False
        
        MsgBox ("Data Pesanan pembelian tidak ditemukan!")
        Text1(0).SetFocus
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub get_cabang()
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
            rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
End Sub

Sub get_supplier(kode As String)
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from msupp where kdsp='" & kode & "' limit 1", con, adOpenKeyset, adLockOptimistic
            
    If Not rs1.EOF Then
        Text1(1).Text = "" & rs1!kdsp
        Text1(2).Text = "" & rs1!namasp
                
        For z = 0 To 6
            Text3(z).Text = IIf(z < 2, "", "0.00")
            Text3(z).Locked = False
        Next z
'        Text3(0).SetFocus
    Else
        MsgBox ("Kode supplier tidak ada")
        cari_supplier.Left = 1550
        cari_supplier.Top = 3800
        Set cari_supplier.FormPemanggil = Me
        cari_supplier.Show 1
    End If
End Sub

Sub get_akun()
    Set akun = New Collection
    combo_akun.Clear
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from makun where jenis=1 order by kode", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        Dim coa As PropertyField
        Do While Not rs1.EOF
            combo_akun.AddItem rs1.Fields!kode & Space(3) & rs1.Fields!nama
            
            Set coa = New PropertyField
            coa.Key = rs1.Fields!kode
            coa.Value = rs1.Fields!nama
            
            akun.Add coa, coa.Key
            rs1.MoveNext
        Loop
    End If
End Sub

Sub kosong()
    For z = 0 To 4
        Text1(z).Text = ""
    Next z
    
    For z = 0 To 6
        Text3(z).Text = IIf(z > 2, "0.00", "")
        Text3(z).Locked = True
    Next z
    
    Text4.Text = "0.00"
    Text5.Text = ""
    
    Combo1.ListIndex = 0
    
    DTPicker1.Value = Date
    DTPicker2.Value = DateAdd("d", 7, Date)
    
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
End Sub

Sub otomatis()
    On Error GoTo exc
    
    Dim nomor_nota As String
    Dim NO, kol3, kol5, kol7
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from mnomor where jenis='Hutang'", con, adOpenKeyset, adLockOptimistic
    
    If Not rs1.EOF Then
        j = Val(rs1.Fields!dgt)
        
        kol1 = rs1.Fields!kol1
        If kol1 = "[BLN]" Then kol1 = Format(Date, "MM")
        If kol1 = "[THN]" Then kol1 = Format(Date, "yy")
        If kol1 = "[THNBLN]" Then kol1 = Format(Date, "yyMM")
        If kol1 = "[DEPT]" Then kol1 = cmb_cabang.Text
        
        kol3 = rs1.Fields!kol3
        If kol3 = "[BLN]" Then kol3 = Format(Date, "MM")
        If kol3 = "[THN]" Then kol3 = Format(Date, "yy")
        If kol3 = "[THNBLN]" Then kol3 = Format(Date, "yyMM")
        If kol3 = "[DEPT]" Then kol3 = cmb_cabang.Text
        
        kol5 = rs1.Fields!kol5
        If kol5 = "[BLN]" Then kol5 = Format(Date, "MM")
        If kol5 = "[THN]" Then kol5 = Format(Date, "yy")
        If kol5 = "[THNBLN]" Then kol5 = Format(Date, "yyMM")
        If kol5 = "[DEPT]" Then kol5 = cmb_cabang.Text
        
        kol7 = rs1.Fields!kol7
        If kol7 = "[BLN]" Then kol7 = Format(Date, "MM")
        If kol7 = "[THN]" Then kol7 = Format(Date, "yy")
        If kol7 = "[THNBLN]" Then kol7 = Format(Date, "yyMM")
        If kol7 = "[DEPT]" Then kol7 = cmb_cabang.Text
        
        If kol1 = "[CNT]" Then
            nomor_nota = "" & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6 & kol7
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nott) as nota from tt_head where length(nott)=" & j + z & " and right(nott," & z & ")='" & nomor_nota & "' and left(nott," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = NO & nomor_nota
                Else
                    nomor_nota = Left("0000000000", j - 1) & "1" & nomor_nota
                End If
            End If
        End If
        
        If kol7 = "[CNT]" Then
            nomor_nota = "" & kol1 & rs1.Fields!kol2 & kol3 & rs1.Fields!kol4 & kol5 & rs1.Fields!kol6
            z = Len(nomor_nota)
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "select max(nott) as nota from tt_head where length(nott)=" & j + z & " and left(nott," & z & ")='" & nomor_nota & "' and right(nott," & j & ") REGEXP '^[0-9]+$';", con, adOpenKeyset, adLockOptimistic
            If rs1.EOF Then
                nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
            Else
                If Left(Trim(rs1.Fields(0)), j) Then
                    NO = Trim(rs1.Fields(0))
                    NO = Left(NO, j)
                    NO = Val(NO) + 1
                    NO = Str(NO)
                    NO = Trim(NO)
                    NO = Left("0000000000", j - Len(NO)) + NO
                    nomor_nota = nomor_nota & NO
                Else
                    nomor_nota = nomor_nota & Left("0000000000", j - 1) & "1"
                End If
            End If
        End If
        
        Text1(0).Text = nomor_nota
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub
