VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form daftar_stock 
   Caption         =   "Daftar Item"
   ClientHeight    =   5685
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   7830
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   5685
   ScaleWidth      =   7830
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   0
      ScaleHeight     =   4905
      ScaleWidth      =   7785
      TabIndex        =   1
      Top             =   0
      Width           =   7815
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   1920
         ScaleHeight     =   345
         ScaleWidth      =   5745
         TabIndex        =   14
         Top             =   570
         Width           =   5775
         Begin VB.OptionButton Option7 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Semua"
            Height          =   195
            Left            =   4560
            TabIndex        =   24
            Top             =   80
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton Option6 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Jasa"
            Height          =   195
            Left            =   3600
            TabIndex        =   18
            Top             =   80
            Width           =   855
         End
         Begin VB.OptionButton Option5 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Konsinyasi"
            Height          =   195
            Left            =   2280
            TabIndex        =   17
            Top             =   80
            Width           =   1095
         End
         Begin VB.OptionButton Option4 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Rakitan"
            Height          =   195
            Left            =   1200
            TabIndex        =   16
            Top             =   80
            Width           =   855
         End
         Begin VB.OptionButton Option3 
            BackColor       =   &H00FFFFC0&
            Caption         =   "Barang"
            Height          =   195
            Left            =   120
            TabIndex        =   15
            Top             =   80
            Width           =   855
         End
      End
      Begin VB.ComboBox cmb_cabang 
         Height          =   315
         ItemData        =   "daftar_stock.frx":0000
         Left            =   1920
         List            =   "daftar_stock.frx":000D
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   960
         Width           =   2055
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Desc"
         Height          =   195
         Left            =   4920
         TabIndex        =   10
         Top             =   1365
         Width           =   855
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Asc"
         Height          =   195
         Left            =   4080
         TabIndex        =   9
         Top             =   1365
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "daftar_stock.frx":0025
         Left            =   1920
         List            =   "daftar_stock.frx":0032
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1320
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1920
         TabIndex        =   7
         Text            =   "Text1"
         Top             =   160
         Width           =   4695
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   3135
         Left            =   120
         TabIndex        =   4
         Top             =   1680
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   5530
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Kode"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nama"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Stock"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Satuan"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Rak"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Jenis"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Merk"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   8
            Text            =   "Harga Pokok"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   9
            Text            =   "Harga Jual"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Tipe"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   11
            Text            =   "Min"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   12
            Text            =   "Max"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Dept."
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Supplier"
            Object.Width           =   2293
         EndProperty
      End
      Begin MySIS.Button cmd_cari 
         Height          =   375
         Left            =   6720
         TabIndex        =   23
         Top             =   120
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tampil"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_stock.frx":004A
         PICN            =   "daftar_stock.frx":0066
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tipe Item : "
         Height          =   255
         Index           =   3
         Left            =   360
         TabIndex        =   13
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Dept. / Gudang : "
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   12
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kata Kunci : "
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   160
         Width           =   1575
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Urut Berdasar : "
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   5
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1725
         Index           =   0
         Left            =   -240
         TabIndex        =   3
         Top             =   0
         Width           =   8055
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   5280
         Width           =   6135
      End
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   705
      ScaleWidth      =   7785
      TabIndex        =   0
      Top             =   4920
      Width           =   7815
      Begin MySIS.Button cmd_keluar 
         Height          =   495
         Left            =   4080
         TabIndex        =   19
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Keluar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_stock.frx":0400
         PICN            =   "daftar_stock.frx":041C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_edit 
         Height          =   495
         Left            =   1440
         TabIndex        =   20
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Edit"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_stock.frx":0852
         PICN            =   "daftar_stock.frx":086E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_hapus 
         Height          =   495
         Left            =   2760
         TabIndex        =   21
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Hapus"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_stock.frx":09C8
         PICN            =   "daftar_stock.frx":09E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MySIS.Button cmd_tambah 
         Height          =   495
         Left            =   120
         TabIndex        =   22
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Baru"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "daftar_stock.frx":0B3E
         PICN            =   "daftar_stock.frx":0B5A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "daftar_stock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd_tambah_Click()
    On Error Resume Next
    master_stock.Text1(0).Locked = False
    master_stock.Text1(0).Text = ""
    master_stock.Label1(0).Caption = "     Tambah Data Item"
    master_stock.Show
    master_stock.Caption = "Data Item Baru"
    master_stock.kosong
    master_stock.WindowState = 2
    master_stock.SetFocus
    MDIForm1.tabref
End Sub

Private Sub cmd_cari_Click()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    If Text1.Text = "" Then
        csql = "SELECT mstock.BARA,mstock.NAMA,mstock_bo.AWAL+mstock_bo.MASUK-mstock_bo.KELUAR AS SALDO,mstock.SATUAN,mstock_bo.RAK,mstock.SUBKTG,mstock.MERK,mstock_bo.AVER,mstock_bo.HJUAL," & _
               "mstock.INV,mstock_bo.MIN,mstock_bo.MAX,mstock_bo.BO,mstock_bo.KODE FROM mstock INNER JOIN mstock_bo ON mstock.BARA = mstock_bo.BARA where mstock_bo.bo='" & cmb_cabang.Text & "'"
        If Option3.Value = True Then
            csql = csql & " and mstock.inv='Barang' "
        ElseIf Option4.Value = True Then
            csql = csql & " and mstock.inv='Rakitan' "
        ElseIf Option5.Value = True Then
            csql = csql & " and mstock.inv='Konsinyasi' "
        ElseIf Option6.Value = True Then
            csql = csql & " and mstock.inv='Jasa' "
        End If
        
        If Combo1.Text = "Kode" Then
            csql = csql & " order by bara "
        ElseIf Combo1.Text = "Nama" Then
            csql = csql & " order by nama "
        Else
            csql = csql & " order by subktg "
        End If
        
        If Option1.Value = True Then
            csql = csql & "asc"
        Else
            csql = csql & "desc"
        End If
    
    Else
        csql = "SELECT mstock.BARA,mstock.NAMA,mstock_bo.AWAL+mstock_bo.MASUK-mstock_bo.KELUAR AS SALDO,mstock.SATUAN,mstock_bo.RAK,mstock.SUBKTG,mstock.MERK,mstock_bo.AVER,mstock_bo.HJUAL," & _
               "mstock.INV,mstock_bo.MIN,mstock_bo.MAX,mstock_bo.BO,mstock_bo.KODE FROM mstock INNER JOIN mstock_bo ON mstock.BARA = mstock_bo.BARA where mstock_bo.bo='" & cmb_cabang.Text & "'"
        
        If Option3.Value = True Then
            csql = csql & " and mstock.inv='Barang' "
        ElseIf Option4.Value = True Then
            csql = csql & " and mstock.inv='Rakitan' "
        ElseIf Option5.Value = True Then
            csql = csql & " and mstock.inv='Konsinyasi' "
        ElseIf Option6.Value = True Then
            csql = csql & " and mstock.inv='Jasa' "
        End If
        
        If Combo1.Text = "Kode" Then
            csql = csql & " and mstock.bara like '%" & Text1.Text & "%' or mstock.nama like '%" & Text1.Text & "%' or mstock.subktg like '%" & Text1.Text & "%' order by bara "
        ElseIf Combo1.Text = "Nama" Then
            csql = csql & " and mstock.bara like '%" & Text1.Text & "%' or mstock.nama like '%" & Text1.Text & "%' or mstock.subktg like '%" & Text1.Text & "%' order by nama "
        Else
            csql = csql & " and mstock.bara like '%" & Text1.Text & "%' or mstock.nama like '%" & Text1.Text & "%' or mstock.subktg like '%" & Text1.Text & "%' order by subktg "
        End If
        If Option1.Value = True Then
            csql = csql & "asc"
        Else
            csql = csql & "desc"
        End If
    End If
    
    If rs1.State <> 0 Then rs1.Close
    rs1.Open csql, con, adOpenKeyset, adLockOptimistic
    
    j = 0
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            On Error Resume Next
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!bara
            item.SubItems(2) = "" & rs1.Fields!nama
            item.SubItems(3) = "" & Format(rs1.Fields!saldo, "##0.00")
            item.SubItems(4) = "" & rs1.Fields!satuan
            item.SubItems(5) = "" & rs1.Fields!rak
            item.SubItems(6) = "" & rs1.Fields!subktg
            item.SubItems(7) = "" & rs1.Fields!merk
            item.SubItems(8) = "" & Format(rs1.Fields!aver, "#,##0")
            item.SubItems(9) = "" & Format(rs1.Fields!hjual, "#,##0")
            item.SubItems(10) = "" & rs1.Fields!inv
            item.SubItems(11) = "" & Format(rs1.Fields!Min, "##0")
            item.SubItems(12) = "" & Format(rs1.Fields!Max, "##0")
            item.SubItems(13) = "" & rs1.Fields!BO
            item.SubItems(14) = "" & rs1.Fields!kode
        rs1.MoveNext
        Loop
    End If
Exit Sub
exc:
MsgBox ("error")

End Sub

Private Sub cmd_edit_Click()
    If ListView1.ListItems.Count > 0 Then
        On Error Resume Next
        master_stock.Text1(0).Locked = True
        master_stock.Picture1.Enabled = False
        master_stock.Button13.Enabled = False
        master_stock.cmb_cabang.Enabled = False
        master_stock.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        master_stock.Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        master_stock.Caption = "Edit Item: " & ListView1.SelectedItem.SubItems(1)
        master_stock.Show
        master_stock.WindowState = 2
        master_stock.SetFocus
        master_stock.getitem
        MDIForm1.tabref
    End If
End Sub

Private Sub cmd_hapus_Click()
    On Error GoTo exc
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select (awal+masuk-keluar) as saldo from mstock_bo where bara='" & ListView1.SelectedItem.SubItems(1) & "' and bo='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
    If rs1.Fields!saldo = 0 Then
        pesan = MsgBox("Hapus Data Barang " & ListView1.SelectedItem.SubItems(1) & "?", vbQuestion + vbYesNo, "Konfirmasi")
        If pesan = vbYes Then
            If rs1.State <> 0 Then rs1.Close
            rs1.Open "delete from mstock_bo where bara='" & ListView1.SelectedItem.SubItems(1) & "' and bo='" & cmb_cabang.Text & "'", con, adOpenKeyset, adLockOptimistic
            tampilstock
        End If
    Else
        MsgBox ("Tidak bisa dihapus, barang sudah memiliki stok!"), vbInformation, "Info"
    End If
    Exit Sub
exc:
End Sub

Sub tampilstock()
    On Error GoTo exc
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    j = 0
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "SELECT mstock.BARA,mstock.NAMA,mstock_bo.AWAL+mstock_bo.MASUK-mstock_bo.KELUAR AS SALDO,mstock.SATUAN,mstock_bo.RAK,mstock.SUBKTG,mstock.MERK,mstock_bo.AVER,mstock_bo.HJUAL," & _
             "mstock.INV,mstock_bo.MIN,mstock_bo.MAX,mstock_bo.BO,mstock_bo.KODE FROM mstock INNER JOIN mstock_bo ON mstock.BARA = mstock_bo.BARA where mstock_bo.bo='" & cmb_cabang.Text & "' ORDER BY mstock.BARA LIMIT 100", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            j = j + 1
            Set item = ListView1.ListItems.Add(, , j)
            item.Text = j
            item.SubItems(1) = "" & rs1.Fields!bara
            item.SubItems(2) = "" & rs1.Fields!nama
            item.SubItems(3) = "" & Format(rs1.Fields!saldo, "##0.00")
            item.SubItems(4) = "" & rs1.Fields!satuan
            item.SubItems(5) = "" & rs1.Fields!rak
            item.SubItems(6) = "" & rs1.Fields!subktg
            item.SubItems(7) = "" & rs1.Fields!merk
            item.SubItems(8) = "" & Format(rs1.Fields!aver, "#,##0")
            item.SubItems(9) = "" & Format(rs1.Fields!hjual, "#,##0")
            item.SubItems(10) = "" & rs1.Fields!inv
            item.SubItems(11) = "" & Format(rs1.Fields!Min, "##0")
            item.SubItems(12) = "" & Format(rs1.Fields!Max, "##0")
            item.SubItems(13) = "" & rs1.Fields!BO
            item.SubItems(14) = "" & rs1.Fields!kode
        rs1.MoveNext
        Loop
    End If
    Exit Sub
exc:
    MsgBox ("error")
End Sub

Private Sub cmd_keluar_Click()
    On Error Resume Next
    Unload Me
    MDIForm1.tabref
End Sub

Private Sub Form_Load()
    On Error Resume Next
    Text1.Text = ""
    ListView1.ListItems.Clear
    ListView1.View = lvwReport
    koneksi
    Combo1.Text = Combo1.List(0)
    On Error Resume Next
    cmb_cabang.Clear
    If rs1.State <> 0 Then rs1.Close
    rs1.Open "select * from bo", con, adOpenKeyset, adLockOptimistic
    If Not rs1.EOF Then
        Do While Not rs1.EOF
            z = 11 - Len(rs1.Fields!BO)
            cmb_cabang.AddItem (rs1.Fields!BO)
        rs1.MoveNext
        Loop
    End If
    cmb_cabang.Text = xx
    tampilstock
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Picture5.Top = Me.Height - 1305
    Picture2.Height = Me.Height - 1290
    ListView1.Height = Me.Height - 3200
    Picture2.Width = Me.Width - 255
    Picture5.Width = Me.Width - 255
    ListView1.Width = Me.Width - 495
    Label1(0).Width = Me.Width - 50
End Sub

Private Sub ListView1_DblClick()
    If ListView1.ListItems.Count > 0 Then
        On Error Resume Next
        master_stock.Text1(0).Locked = True
        master_stock.Picture1.Enabled = False
        master_stock.Button13.Enabled = False
        master_stock.cmb_cabang.Enabled = False
        master_stock.Text1(0).Text = ListView1.SelectedItem.SubItems(1)
        master_stock.Text1(1).Text = ListView1.SelectedItem.SubItems(2)
        master_stock.Caption = "Edit Item: " & ListView1.SelectedItem.SubItems(1)
        master_stock.Show
        master_stock.WindowState = 2
        master_stock.SetFocus
        master_stock.getitem
        MDIForm1.tabref
    End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
                
    Else
        If (KeyAscii = 34 Or KeyAscii = 39 Or KeyAscii = 92) Then KeyAscii = 0
    End If
End Sub
