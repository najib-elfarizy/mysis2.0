VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "crviewer.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form lap_tarikfee 
   Caption         =   "Lap. Penarikan Fee"
   ClientHeight    =   6855
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11040
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11040
   Begin VB.Frame Frame1 
      Height          =   810
      Left            =   5
      TabIndex        =   1
      Top             =   -50
      Width           =   11055
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   345
         Index           =   0
         Left            =   1200
         TabIndex        =   2
         Top             =   240
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   102432769
         CurrentDate     =   43218
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   345
         Index           =   1
         Left            =   3240
         TabIndex        =   3
         Top             =   240
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   102432769
         CurrentDate     =   43218
      End
      Begin MySIS.Button cmd_filter 
         Height          =   375
         Left            =   4920
         TabIndex        =   6
         Top             =   240
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Tampil"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "lap_tarikfee.frx":0000
         PICN            =   "lap_tarikfee.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Periode : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   270
         Width           =   975
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   4
         Top             =   250
         Width           =   375
      End
   End
   Begin CRVIEWERLibCtl.CRViewer CRViewer1 
      Height          =   6135
      Left            =   5
      TabIndex        =   0
      Top             =   720
      Width           =   11055
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   0   'False
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "lap_tarikfee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub ShowReport()
'    On Error GoTo exc
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
        
    If fso.FileExists(App.Path & "\Report\lap_tarikfee.rpt") Then
        Set cryApp = New CRAXDRT.Application
        Set rptApp = cryApp.OpenReport(App.Path & "\Report\lap_tarikfee.rpt")
        rptApp.EnableParameterPrompting = False
    
        If rs1.State <> 0 Then rs1.Close
        csql = "SELECT * FROM fee_head b JOIN fee_det d ON b.kdtrk=d.kdtrk WHERE b.tgl between '" & Format(DTPicker1(0).Value, "yyyy-MM-dd") & "' and '" & Format(DTPicker1(1).Value, "yyyy-MM-dd") & "'"
        rs1.Open csql, con, adOpenKeyset, adLockOptimistic
        rptApp.Database.SetDataSource rs1, 3, 1

        If rs1.State <> 0 Then rs1.Close
        rs1.Open "select * from mperusahaan limit 1", con, adOpenKeyset, adLockOptimistic
        If Not rs1.EOF Then
            On Error Resume Next
            rptApp.ParameterFields.GetItemByName("NAMA_PERUSAHAAN").AddCurrentValue "" & rs1.Fields!NAMA_PERUSAHAAN
            rptApp.ParameterFields.GetItemByName("ALAMAT1").AddCurrentValue "" & rs1.Fields!alamat1
            rptApp.ParameterFields.GetItemByName("ALAMAT2").AddCurrentValue "" & rs1.Fields!alamat2
            rptApp.ParameterFields.GetItemByName("TELP").AddCurrentValue "" & rs1.Fields!telp
            rptApp.ParameterFields.GetItemByName("FAX").AddCurrentValue "" & rs1.Fields!fax
            rptApp.ParameterFields.GetItemByName("NPWP").AddCurrentValue "" & rs1.Fields!npwp
        End If
        
        rptApp.ParameterFields.GetItemByName("TGL1").AddCurrentValue DTPicker1(0).Value
        rptApp.ParameterFields.GetItemByName("TGL2").AddCurrentValue DTPicker1(1).Value
        rptApp.ParameterFields.GetItemByName("USER").AddCurrentValue "" & xy
        
        CRViewer1.ReportSource = rptApp
        CRViewer1.ViewReport
        
        Do While CRViewer1.IsBusy
            DoEvents
        Loop
          
        CRViewer1.Zoom 1
    Else
        MsgBox "Report File Doesn't Exist!"
    End If
    Exit Sub
exc:
    MsgBox ("error : " & err.Description)
End Sub

Private Sub cmd_filter_Click()
    ShowReport
End Sub

Private Sub Form_Load()
    ShowReport
End Sub

Private Sub Form_Resize()
    Frame1.Width = Me.Width - 230
    CRViewer1.Top = Frame1.Top + Frame1.Height + 20
    CRViewer1.Width = Me.Width - 250
    CRViewer1.Height = Me.Height - CRViewer1.Top - 550
End Sub
